import {StyleSheet} from 'react-native';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {
  BLACK,
  GRAY,
  GRAY_BORDER,
  LIGHT_GRAY,
  LIGHT_ORANGE,
  WHITE,
} from '../../styles/colors';

const styles = StyleSheet.create({
  feedImageView: {
    width: '100%',
    height: calcHeight(240),
    marginTop: calcHeight(20),
  },
  feedImage: {
    width: '100%',
    height: calcHeight(240),
    resizeMode: 'stretch',
  },
  hitSlopTen: {
    top: calcHeight(10),
    bottom: calcHeight(10),
    right: calcWidth(10),
    left: calcWidth(10),
  },
  hitSlopTwenty: {
    top: calcHeight(20),
    bottom: calcHeight(20),
    right: calcWidth(20),
    left: calcWidth(20),
  },
  rootView: {
    paddingHorizontal: calcWidth(20),
    width: '100%',
    marginTop: moderateScale(35),
  },
  headerRoot: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  noImageCircle: {
    backgroundColor: '#C4C4C4',
    width: moderateScale(40),
    height: moderateScale(40),
    borderRadius: moderateScale(40) / 2,
    marginRight: calcWidth(13),
  },
  imageCircle: {
    borderRadius: moderateScale(40) / 2,
    marginRight: calcWidth(13),
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  imageAuthor: {
    width: moderateScale(40),
    height: moderateScale(40),
  },
  authorText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    fontWeight: '600',
  },
  dateText: {
    fontSize: calcFontSize(12),
    fontFamily: SUB_TITLE_FONT,
    color: LIGHT_GRAY,
  },
  infoView: {
    justifyContent: 'space-around',
  },
  feedTitleView: {
    marginTop: calcHeight(20),
    marginBottom: calcHeight(10),
  },
  feedTitleText: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(20),
  },
  descriptionFeedView: {
    marginBottom: calcHeight(13),
  },
  descriptionFeedText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
    color: LIGHT_GRAY,
  },
  seeMoreText: {
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
  commentInfoText: {
    color: LIGHT_GRAY,
    fontSize: calcFontSize(12),
    fontFamily: SUB_TITLE_FONT,
  },
  commentLikesInfoView: {
    marginTop: calcHeight(15),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  likePhotoAbsolute: {
    position: 'absolute',
    right: -calcWidth(12),
  },
  likePhoto: {
    width: moderateScale(18),
    height: moderateScale(18),
    borderRadius: moderateScale(18) / 2,
    backgroundColor: '#C4C4C4',
    borderWidth: 1,
    borderColor: WHITE,
  },
  whoLikeText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(12),
    color: LIGHT_GRAY,
    marginLeft: calcWidth(15),
  },
  rowDirection: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rightWidthNull: {
    borderRightWidth: 0,
  },
  leftWidthNull: {
    borderLeftWidth: 0,
  },
  trainingInfoView: {
    marginTop: calcHeight(20),
  },
  infoRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  typeOfTrainingText: {
    fontSize: calcFontSize(20),
    fontFamily: MAIN_TITLE_FONT,
    color: BLACK,
  },
  seeDetailsText: {
    color: LIGHT_ORANGE,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    textDecorationLine: 'underline',
  },
  underProgramSection: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: calcHeight(10),
  },
  underSectionText: {
    color: GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  footerView: {
    width: deviceWidth,
    alignSelf: 'center',
    marginTop: calcHeight(16),
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: GRAY_BORDER,
    //paddingVertical: moderateScale(5),
  },
  footerBox: {
    paddingVertical: moderateScale(8),
  },
  valuesView: {
    marginTop: calcHeight(10),
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  valueText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(18),
    marginRight: calcWidth(20),
  },
  chartView: {
    width: deviceWidth * 0.95,
    alignSelf: 'center',
  },
  verticalLineFooter: {
    height: '100%',
    width: moderateScale(1),
    backgroundColor: GRAY_BORDER,
  },
  box: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  threeDotsHitSlop: {
    top: calcHeight(20),
    bottom: calcHeight(20),
    right: calcWidth(20),
    left: calcWidth(20),
  },
});

export default styles;
