import React, {useCallback, useContext, useState} from 'react';
import {Image, processColor, Text, TouchableOpacity, View} from 'react-native';
import {Path, Svg} from 'react-native-svg';
import PropTypes from 'prop-types';
import {BarChart} from 'react-native-charts-wrapper';
import {calcHeight, calcWidth, moderateScale} from '../../utils/dimensions';
import {
  BLACK,
  GRAY,
  GRAY_BORDER,
  LIGHT_ORANGE,
  ORANGE,
} from '../../styles/colors';
import ShareIcon from '../../assets/icons/shareIcon.svg';
import CommentIcon from '../../assets/icons/commentIcon.svg';
import {LocalizationContext} from '../../localization/translations';
import styles from './feedCardStyles';
import {SUB_TITLE_FONT} from '../../styles/fonts';
import ThreeDotsIcon from '../../assets/icons/threeDots.svg';
import LikeIcon from '../../assets/icons/likeIcon.svg';
import LikeIconLiked from '../../assets/icons/likeIconFeedLiked.svg';

const FeedCard = ({
  item,
  onPressThreeDots,
  isChart = true,
  isThreeDots = false,
  isFriendsSection = false,
  isYourSection = false,
  onPressComment = () => {},
}) => {
  const {
    author,
    date,
    typeOfTraining,
    image,
    feedTitle,
    feedDescription,
    comments,
    authorImage,
    trackingOptions,
    generalTrainingInfo,
    detailTrainingInfo,
  } = item;

  const [isLiked, setIsLiked] = useState(false);
  const {translations} = useContext(LocalizationContext);
  const renderImage = useCallback(
    (source, styles) => <Image source={source} style={styles} />,
    [],
  );

  console.log(222, detailTrainingInfo);

  const renderChart = () => {
    return (
      <View style={styles.chartView}>
        <BarChart
          style={{flex: 1, height: calcHeight(255)}}
          drawBorders={false}
          legend={{
            fontFamily: SUB_TITLE_FONT,
            horizontalAlignment: 'CENTER',
          }}
          chartDescription={{text: ''}}
          xAxis={{
            drawGridLines: false,
            drawAxisLine: false,
            drawLabels: false,
          }}
          yAxis={{
            left: {
              drawAxisLine: false,
            },
            right: {
              drawAxisLine: false,
              drawLabels: false,
            },
          }}
          scaleEnabled={false}
          dragEnabled={false}
          pinchZoom={false}
          doubleTapToZoomEnabled={false}
          noDataText={'Opps... no data available!'}
          data={{
            dataSets: [
              {
                label: 'Series 1',
                values: [
                  {x: 1, y: 2},
                  {x: 5, y: 5},
                  {x: 9, y: 8},
                  {x: 13, y: 5},
                  {x: 17, y: 8},
                  {x: 21, y: 5},
                  {x: 25, y: 8},
                  {x: 29, y: 5},
                ],
                config: {
                  color: processColor(ORANGE),
                  valueTextColor: processColor('transparent'),
                },
              },
            ],
          }}
        />
      </View>
    );
  };

  return (
    <View style={styles.rootView}>
      <View style={styles.headerRoot}>
        <View style={styles.box}>
          {authorImage ? (
            <View style={styles.imageCircle}>
              <Image source={authorImage} style={styles.imageAuthor} />
            </View>
          ) : (
            <View style={styles.noImageCircle} />
          )}
          <View style={styles.infoView}>
            <Text style={styles.authorText}>{author}</Text>
            <Text style={styles.dateText}>{date}</Text>
          </View>
        </View>
        {isThreeDots && (
          <TouchableOpacity
            hitSlop={styles.threeDotsHitSlop}
            onPress={() => onPressThreeDots(trackingOptions)}>
            <ThreeDotsIcon widht={calcWidth(23)} height={calcHeight(19)} />
          </TouchableOpacity>
        )}
      </View>
      {!isChart && (
        <>
          <View style={styles.feedImageView}>
            {renderImage(image, styles.feedImage)}
          </View>
          <View style={styles.feedTitleView}>
            <Text style={styles.feedTitleText}>{feedTitle}</Text>
          </View>
          <View style={styles.descriptionFeedView}>
            <Text style={styles.descriptionFeedText}>{feedDescription}</Text>
          </View>
          <TouchableOpacity hitSlop={styles.hitSlopTen}>
            <Text style={styles.seeMoreText}>
              {translations.feedScreen.readMore}
            </Text>
          </TouchableOpacity>
        </>
      )}
      {generalTrainingInfo && (
        <>
          <View style={styles.trainingInfoView}>
            <View style={styles.infoRow}>
              <Text style={styles.typeOfTrainingText}>{typeOfTraining}</Text>
              <TouchableOpacity hitSlop={styles.hitSlopTen}>
                <Text style={styles.seeDetailsText}>See details</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.underProgramSection}>
              <Text style={styles.underSectionText}>
                Week {generalTrainingInfo.week} |
              </Text>
              <Text style={styles.underSectionText}>
                {generalTrainingInfo.typeOfProgram} |
              </Text>
              <Text style={styles.underSectionText}>
                {' '}
                {generalTrainingInfo.companyName}
              </Text>
            </View>
          </View>
          <View style={styles.valuesView}>
            <Text style={styles.valueText}>
              {detailTrainingInfo.minutesCount} min
            </Text>
            <Text style={styles.valueText}>
              {detailTrainingInfo.repsCount} reps
            </Text>
            <Text style={styles.valueText}>
              {detailTrainingInfo.kcalCount} kcal
            </Text>
          </View>
        </>
      )}
      {isChart && renderChart()}

      {!isYourSection && (
        <View style={styles.commentLikesInfoView}>
          <View style={styles.rowDirection}>
            <View style={styles.rowDirection}>
              <View style={[styles.likePhoto, styles.likePhotoAbsolute]} />
              <View style={styles.likePhoto} />
            </View>
            <Text style={styles.whoLikeText}>
              {isLiked ? 'You and 2 other people like it' : '2 people like it'}
            </Text>
          </View>
          {isFriendsSection && (
            <View>
              <Text style={styles.commentInfoText}>
                {comments.length} comments
              </Text>
            </View>
          )}
        </View>
      )}

      <View style={styles.footerView}>
        <View style={[styles.footerBox]}>
          <TouchableOpacity
            hitSlop={styles.hitSlopTwenty}
            onPress={() => setIsLiked(oldData => !oldData)}>
            {isLiked ? (
              <LikeIconLiked width={calcWidth(22)} height={calcHeight(22)} />
            ) : (
              <LikeIcon width={calcWidth(22)} height={calcHeight(22)} />
            )}
          </TouchableOpacity>
        </View>
        {isFriendsSection && (
          <>
            <View style={styles.verticalLineFooter} />
            <View style={[styles.footerBox]}>
              <TouchableOpacity
                hitSlop={styles.hitSlopTwenty}
                onPress={onPressComment}>
                <CommentIcon width={calcWidth(22)} height={calcHeight(22)} />
              </TouchableOpacity>
            </View>
          </>
        )}
        <View style={styles.verticalLineFooter} />
        <View style={[styles.footerBox]}>
          <TouchableOpacity hitSlop={styles.hitSlopTwenty} onPress={() => {}}>
            <ShareIcon width={calcWidth(22)} height={calcHeight(22)} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

FeedCard.propTypes = {
  item: PropTypes.object,
  onPressThreeDots: PropTypes.func,
  isChart: PropTypes.bool,
  isThreeDots: PropTypes.bool,
  isFriendsSection: PropTypes.bool,
  isYourSection: PropTypes.bool,
  onPressComment: PropTypes.func,
};

export default FeedCard;
