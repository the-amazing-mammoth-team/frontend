import React, {useCallback, useState} from 'react';
import {StyleSheet, Text, View, Switch, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';
import ToggleSwitch from 'toggle-switch-react-native';
import {
  GRAY_BORDER,
  LIGHT_GRAY,
  LIGHT_ORANGE,
  MEDIUM_GRAY,
  WHITE,
} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';

const TrackingOptionsModal = ({isModalVisible, onDismiss, trackingOptions}) => {
  const [toggledOptions, setToggledOptions] = useState(trackingOptions);

  const toggle = useCallback(
    async option => {
      if (await toggledOptions.includes(option)) {
        setToggledOptions(toggledOptions.filter(opt => opt !== option));
      } else {
        setToggledOptions([...toggledOptions, option]);
      }
    },
    [toggledOptions],
  );

  const options = [
    {
      id: 1,
      title: 'Silence',
      description: 'Hide their activities and posts on your feed privately.',
    },
  ];

  return (
    <Modal
      isVisible={isModalVisible}
      onSwipeComplete={onDismiss}
      swipeDirection="down"
      style={styles.modalStyleRoot}
      useNativeDriver={true}
      onBackdropPress={onDismiss}>
      <View style={styles.modalStyle}>
        <Text style={styles.modalTitle}>Tracking options</Text>
        {options.map(option => {
          return (
            <View key={option.title} style={styles.optionContainer}>
              <View style={styles.optionContent}>
                <Text style={styles.settingTitle}>{option.title}</Text>
                <Text style={styles.settingsDescription}>
                  {option.description}
                </Text>
              </View>
              <View>
                <ToggleSwitch
                  isOn={toggledOptions.includes(option.title)}
                  onColor={LIGHT_ORANGE}
                  offColor={MEDIUM_GRAY}
                  onToggle={() => toggle(option.title)}
                  animationSpeed={100}
                  thumbOnStyle={styles.thumbStyle}
                  thumbOffStyle={styles.thumbStyle}
                />
              </View>
            </View>
          );
        })}
        <View style={styles.line} />
        <TouchableOpacity hitSlop={styles.hitSlopTen}>
          <Text style={styles.stopFollowText}>Stop following</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

TrackingOptionsModal.propTypes = {
  isModalVisible: PropTypes.bool,
  onDismiss: PropTypes.func,
  trackingOptions: PropTypes.array,
};

const styles = StyleSheet.create({
  modalStyle: {
    backgroundColor: WHITE,
    width: deviceWidth,
    alignSelf: 'center',
    paddingVertical: moderateScale(20),
    bottom: 0,
    borderTopRightRadius: moderateScale(15),
    borderTopLeftRadius: moderateScale(15),
    paddingHorizontal: moderateScale(20),
  },
  modalTitle: {
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
    textAlign: 'center',
    marginBottom: calcHeight(25),
  },
  optionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  optionContent: {
    flexBasis: '75%',
  },
  settingTitle: {
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: calcHeight(5),
  },
  settingsDescription: {
    color: LIGHT_GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
  hitSlopTen: {
    top: calcHeight(10),
    bottom: calcHeight(10),
    right: calcWidth(10),
    left: calcWidth(10),
  },
  stopFollowText: {
    fontSize: calcFontSize(14),
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    textDecorationLine: 'underline',
  },
  line: {
    backgroundColor: GRAY_BORDER,
    width: '100%',
    height: calcHeight(1),
    alignSelf: 'center',
    marginVertical: calcHeight(20),
  },
  modalStyleRoot: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  thumbStyle: {
    width: moderateScale(19),
    height: moderateScale(19),
  },
});

export default TrackingOptionsModal;
