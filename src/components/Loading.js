import React from 'react';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {ORANGE} from '../styles/colors';
import {deviceHeight, deviceWidth} from '../utils/dimensions';

export const Loading = ({noMargin}) => {
  return (
    <ActivityIndicator
      style={[
        styles.indicatorView,
        {marginTop: noMargin ? 0 : deviceHeight * 0.2},
      ]}
      size={'large'}
      color={ORANGE}
    />
  );
};

export const AbsoluteLoading = () => (
  <View style={styles.absoluteIndicatorView}>
    <Loading noMargin={true} />
  </View>
);

const styles = StyleSheet.create({
  indicatorView: {
    alignSelf: 'center',
  },
  absoluteIndicatorView: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    zIndex: 999,
  },
});
