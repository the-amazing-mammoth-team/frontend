import React, {useContext, useRef, useCallback} from 'react';
import moment from 'moment';
import 'moment/locale/es';
//import Tooltip from 'rne-modal-tooltip';
import LinearGradient from 'react-native-linear-gradient';
import {LocalizationContext} from '../../localization/translations';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Tooltip from 'rne-modal-tooltip';
import {
  BLACK,
  GRAY,
  GRAY_BORDER,
  LIGHT_GRAY,
  LIGHT_ORANGE,
  WHITE,
} from '../../styles/colors';
import {
  calcFontSize,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {SUB_TITLE_FONT} from '../../styles/fonts';
import {ScrollView} from 'react-native-gesture-handler';

function parseIsoDatetime(dtstr) {
  var dt = dtstr.split(/[: T-]/).map(parseFloat);
  return new Date(
    dt[0],
    dt[1] - 1,
    dt[2],
    dt[3] || 0,
    dt[4] || 0,
    dt[5] || 0,
    0,
  );
}

const renderTooltipChild = (data, tooltipRef, goToSummary) => {
  return (
    <LinearGradient
      style={styles.tooltipChildContainder}
      colors={[LIGHT_GRAY, LIGHT_GRAY]}
      start={{x: 0, y: 0}}
      end={{x: 0, y: 1}}>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        horizontal
        contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}>
        {data.map(el => {
          return (
            <View style={styles.tooltipChildItem} key={el.id}>
              <SessionsOfDay
                data={[el]}
                callback={() =>
                  requestAnimationFrame(() =>
                    tooltipRef.current?.toggleTooltip(),
                  )
                }
                goToSummary={goToSummary}
              />
            </View>
          );
        })}
      </ScrollView>
    </LinearGradient>
  );
};

const SessionsOfDay = ({data: arr, callback = () => {}, goToSummary}) => {
  const backgroundColor = (() => {
    if (arr.length === 1) {
      return arr[0].backgroundColor;
    } else if (arr.length > 1) {
      return LIGHT_ORANGE;
    } else {
      WHITE;
    }
  })();
  return (
    <TouchableOpacity
      onPress={() => {
        callback();
        setTimeout(() => goToSummary(arr[0].execution_id), 300);
      }}
      disabled={arr.length === 0 || arr.length > 1}>
      <View
        style={[
          styles.dayCircle,
          !arr.length && styles.dayCircleBorder,
          {backgroundColor},
        ]}>
        <Text style={(styles.circleText, {color: WHITE})}>
          {arr.length === 1 && arr[0].name}
          {arr.length > 1 && arr.length}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default ({goals = [], date, navigation, isHeader}) => {
  const {appLanguage} = useContext(LocalizationContext);
  const tooltipRef = useRef();
  let arr = goals?.filter(el => {
    if (el.createdAt) {
      return moment(parseIsoDatetime(el.createdAt)).isSame(
        new Date(date),
        'day',
      );
    }
  });
  const dayName = moment(date)
    .locale(appLanguage)
    .format('dd');
  const goToSummary = useCallback(
    session_execution_id => {
      navigation.push('Summary', {
        data: {
          difficultyFeedback: 1,
          sessionExecutionId: session_execution_id,
        },
      });
    },
    [navigation],
  );
  return (
    <View style={styles.dayComponentView}>
      {isHeader && (
        <Text style={styles.calendarHeaderText}>
          {dayName.charAt(0).toUpperCase() + dayName.slice(1)}
        </Text>
      )}
      <Text style={styles.dayText}>{date.getDate()}</Text>
      {arr.length > 1 ? (
        <Tooltip
          popover={renderTooltipChild(arr, tooltipRef, goToSummary)}
          skipAndroidStatusBar={true}
          withOverlay={true}
          containerStyle={{
            ...styles.tooltipContainder,
          }}
          pointerColor={LIGHT_GRAY}
          width={
            arr.length < 7
              ? arr.length * moderateScale(50)
              : deviceWidth - calcWidth(20)
          }
          ref={tooltipRef}>
          <SessionsOfDay data={arr} goToSummary={goToSummary} />
        </Tooltip>
      ) : (
        <SessionsOfDay data={arr} goToSummary={goToSummary} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  calendarHeaderText: {
    fontSize: calcFontSize(14),
    color: BLACK,
    fontFamily: SUB_TITLE_FONT,
    marginTop: moderateScale(5),
    marginBottom: moderateScale(15),
  },
  circleText: {
    fontFamily: SUB_TITLE_FONT,
    color: WHITE,
    fontSize: calcFontSize(12),
  },
  dayText: {
    textAlign: 'center',
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
    fontSize: calcFontSize(14),
  },
  dayComponentView: {
    alignItems: 'center',
  },
  dayCircle: {
    width: moderateScale(32),
    height: moderateScale(32),
    borderRadius: moderateScale(32) / 2,
    marginTop: moderateScale(5),
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  dayCircleBorder: {
    borderColor: GRAY,
    borderWidth: 1,
  },
  line: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginBottom: moderateScale(15),
    marginTop: moderateScale(-15),
  },
  tooltipContainder: {
    padding: 0,
    overflow: 'hidden',
    height: null,
    maxWidth: deviceWidth - calcWidth(20),
  },
  tooltipChildContainder: {
    width: '100%',
    flexDirection: 'row',
  },
  tooltipChildItem: {
    paddingHorizontal: moderateScale(7),
    paddingTop: 0,
    paddingBottom: moderateScale(5),
  },
});
