import React, {useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import PropTypes from 'prop-types';
import {BLACK, WHITE} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  moderateScale,
  deviceWidth,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT_REGULAR, SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import Cross from '../../assets/images/cross.svg';

const ResultsCard = ({
  totalSessionsCount = 0,
  timeTotalCount = '0',
  kcalCountSession = 0,
  repsCountSession = 0,
}) => {
  const {translations} = useContext(LocalizationContext);
  return (
    <View style={styles.component}>
      <View style={styles.crossView}>
        <Cross width={deviceWidth * 0.8} height={calcHeight(200)} />
      </View>
      <View style={styles.topLeftBox}>
        <Text style={styles.resultsCardTextTitle}>{totalSessionsCount}</Text>
        <Text style={styles.genericBlackText}>
          {translations.progressScreen.totalSessions}
        </Text>
      </View>
      <View style={styles.topRightBox}>
        <Text style={styles.resultsCardTextTitle}>{timeTotalCount}</Text>
        <Text style={styles.genericBlackText}>
          {translations.progressScreen.totalTime}
        </Text>
      </View>
      <View style={styles.bottomLeftBox}>
        <Text style={styles.resultsCardTextTitle}>{kcalCountSession}</Text>
        <Text style={styles.genericBlackText}>
          Kcal/{translations.progressScreen.session}
        </Text>
      </View>
      <View style={styles.bottomRightBox}>
        <Text style={styles.resultsCardTextTitle}>{repsCountSession}</Text>
        <Text style={styles.genericBlackText}>
          Reps/{translations.progressScreen.session}
        </Text>
      </View>
    </View>
  );
};

ResultsCard.propTypes = {
  totalSessionsCount: PropTypes.number,
  timeTotalCount: PropTypes.string,
  kcalCountSession: PropTypes.number,
  repsCountSession: PropTypes.number,
};

const styles = StyleSheet.create({
  component: {
    borderRadius: 10,
    width: '100%',
    backgroundColor: WHITE,
    elevation: 3,
    shadowColor: BLACK,
    shadowRadius: 5,
    shadowOpacity: 0.5,
    shadowOffset: {width: 0, height: 10},
  },
  resultsCardTextTitle: {
    color: BLACK,
    fontFamily: MAIN_TITLE_FONT_REGULAR,
    fontSize: calcFontSize(30),
  },
  genericBlackText: {
    color: BLACK,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
  topLeftBox: {
    alignItems: 'center',
    position: 'absolute',
    top: moderateScale(30),
    left: moderateScale(30),
  },
  topRightBox: {
    alignItems: 'center',
    position: 'absolute',
    top: moderateScale(30),
    right: moderateScale(30),
  },
  bottomLeftBox: {
    alignItems: 'center',
    position: 'absolute',
    bottom: moderateScale(30),
    left: moderateScale(40),
  },
  bottomRightBox: {
    alignItems: 'center',
    position: 'absolute',
    bottom: moderateScale(30),
    right: moderateScale(40),
  },
  crossView: {
    alignSelf: 'center',
  },
});

export default ResultsCard;
