import React from 'react';
import {processColor, Platform, View, Text} from 'react-native';
import {RadarChart} from 'react-native-charts-wrapper';
import {styles} from '../../scenes/Progress/styles';
import {BLACK, GRAY_BORDER, ORANGE, WHITE} from '../../styles/colors';
import {calcFontSize, deviceWidth, moderateScale} from '../../utils/dimensions';
import {SUB_TITLE_FONT} from '../../styles/fonts';

const radarWidth = '100%';
const radarHeight = deviceWidth * 0.55;

const RadarChartComponent = ({data}) => {
  let dataChart = [];
  const maxValue = data
    ? Math.max.apply(
        Math,
        data.map(el => {
          return el.value;
        }),
      )
    : 1;

  if (data) {
    data
      .sort((a, b) => {
        return a.value - b.value;
      })
      .map(item => {
        const nameCountChart =
          item.name.length + Math.round(item.value).toString().length + 1;
        let lineLenght = 0;
        const name =
          nameCountChart < 15
            ? `${item.name}\n${Math.round(item.value)}%`
            : `${item.name.split(' ').reduce((acc, word) => {
                if (word.length + lineLenght < 10) {
                  lineLenght += word.length;
                  acc += ` ${word}`;
                } else {
                  lineLenght = 0;
                  acc += `\n${word}`;
                }
                return acc;
              }, '')} ${Math.round(item.value)}%`;
        dataChart.push({
          //percent in android is 0-100, in ios is 0-1
          value: Platform.OS === 'android' ? item.value : item.value / 100,
          name,
        });
      });
  }

  console.log(dataChart);

  const renderLabels = () => {
    const radiusLabels = radarHeight / 1.5;
    let angle = 0;
    const step = (2 * Math.PI) / dataChart.length;
    console.log(dataChart);
    return dataChart?.map((el, index) => {
      const x = Math.round(radarHeight / 1.45 + radiusLabels * Math.cos(angle));
      const y = Math.round(
        deviceWidth / 2.2 +
          radiusLabels * Math.sin(angle) -
          moderateScale(el.name.length * 2),
      );
      angle += step;
      return (
        <Text style={[styles.label, {left: y, bottom: x}]} key={index}>
          {el.name}
        </Text>
      );
    });
  };

  return (
    <View>
      <RadarChart
        style={[styles.radarChart, {width: radarWidth, height: radarHeight}]}
        data={{
          dataSets: [
            {
              values: dataChart,
              label: 'Summary', //not be shown
              config: {
                color: processColor(ORANGE),
                drawFilled: true,
                fillColor: processColor(ORANGE),
                fillAlpha: 25,
                drawValues: false,
                lineWidth: moderateScale(1),
              },
            },
          ],
        }}
        xAxis={{
          enabled: false,
        }}
        yAxis={{
          axisMinimum: 0,
          axisMaximum: Platform.OS === 'android' ? maxValue : maxValue / 100,
          enabled: false,
        }}
        chartDescription={{text: ''}}
        rotationEnabled={true}
        touchEnabled={false}
        legend={{
          enabled: false,
        }}
        webLineWidth={moderateScale(1)}
        webLineWidthInner={moderateScale(1)}
        webAlpha={Platform.OS === 'android' ? 255 : 1}
        webColor={processColor(GRAY_BORDER)}
        webColorInner={processColor(GRAY_BORDER)}
        skipWebLineCount={0}
      />
      <View style={styles.containerLabels}>{renderLabels()}</View>
    </View>
  );
};

export default RadarChartComponent;
