import React, {useState} from 'react';
import {processColor, StyleSheet} from 'react-native';
import {BarChart, LineChart} from 'react-native-charts-wrapper';
import {moderateScale} from '../../utils/dimensions';
import {LIGHT_GRAY, ORANGE} from '../../styles/colors';

const LineChartComponent = () => {
  const size = 80;
  const [state, setState] = useState({
    data: {
      dataSets: [
        {
          values: [
            {x: 20, y: 200},
            {x: 40, y: 220},
            {x: 65, y: 230},
            {x: 70, y: 230},
          ],

          label: 'user',
          config: {
            drawValues: false,
            colors: [processColor(ORANGE)],
            mode: 'CUBIC_BEZIER',
            drawCircles: false,
            lineWidth: 1,
            drawAxisLine: false,
            drawGridBackground: false,
          },
        },
        {
          values: [
            {x: 20, y: 190},
            {x: 40, y: 190},
            {x: 65, y: 210},
            {x: 70, y: 220},
            {x: 90, y: 230},
            {x: 100, y: 250},
          ],
          label: 'user 2',
          config: {
            drawValues: false,
            colors: [processColor(LIGHT_GRAY)],
            mode: 'CUBIC_BEZIER',
            drawCircles: false,
            lineWidth: 1,
            drawAxisLine: false,
            drawGridBackground: false,
          },
        },
      ],
    },
    legend: {
      enabled: false,
    },
    selectedEntry: '',
  });

  return (
    <LineChart
      style={styles.chart}
      touchEnabled={false}
      data={state.data}
      drawBorders={false}
      chartDescription={{text: ''}}
      legend={state.legend}
      marker={state.marker}
      drawGridBackground={false}
      scaleEnabled={false}
      scaleXEnabled={false}
      scaleYEnabled={false}
      borderColor={processColor('red')}
      borderWidth={0}
      xAxis={{
        enabled: false,
      }}
      yAxis={{
        enabled: false,
      }}
    />
  );
};

const styles = StyleSheet.create({
  chart: {
    width: '100%',
    height: moderateScale(200),
    alignSelf: 'center',
    marginTop: moderateScale(20),
  },
});

export default LineChartComponent;
