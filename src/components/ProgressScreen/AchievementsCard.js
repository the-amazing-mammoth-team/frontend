import React, {useContext} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {BLACK, GRAY, LIGHT_GRAY, WHITE} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import {calcFontSize, moderateScale} from '../../utils/dimensions';

const AchievementsCard = ({onPress, data}) => {
  const {translations} = useContext(LocalizationContext);

  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={onPress}
      style={styles.resultCard}>
      <Text style={styles.title}>
        {translations.profileScreen.achievements}
      </Text>
      <Text style={styles.description}>
        {translations.profileScreen.takeTrainingLevel}
      </Text>
      <View style={styles.iconsRow}>
        {data?.map(item => (
          <View key={item.id} style={styles.iconDistance}>
            <Image source={{uri: item?.iconUrl}} style={styles.iconStyle} />
          </View>
        ))}
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  resultCard: {
    borderRadius: 10,
    paddingHorizontal: moderateScale(20),
    paddingVertical: moderateScale(15),
    width: '100%',
    backgroundColor: WHITE,
    elevation: 3,
    shadowColor: BLACK,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    marginTop: moderateScale(50),
    marginBottom: moderateScale(40),
  },
  title: {
    color: BLACK,
    fontSize: calcFontSize(20),
    fontFamily: MAIN_TITLE_FONT,
  },
  description: {
    color: GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    marginTop: moderateScale(3),
  },
  iconsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: moderateScale(10),
  },
  iconDistance: {
    marginRight: moderateScale(7),
  },
  iconStyle: {
    width: moderateScale(35),
    height: moderateScale(35),
  },
});

export default AchievementsCard;
