import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import PropTypes from 'prop-types';
import {PieChart} from 'react-native-svg-charts';
import {Circle, G, Line, Svg, Text as SvgText} from 'react-native-svg';
import {calcFontSize, deviceWidth, moderateScale} from '../../utils/dimensions';
import {
  BLACK,
  GRAY,
  WHITE,
  YELLOW,
  BUTTON_TITLE,
  MEDIUM_GRAY,
  ORANGE,
  LIGHT_ORANGE,
} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';

const CardPieChart = ({title, description}) => {
  const pieData = [
    {
      key: 1,
      amount: 38,
      svg: {fill: YELLOW},
      title: 'Flexibility',
    },
    {
      key: 2,
      amount: 25,
      svg: {fill: ORANGE},
      title: 'Strength',
    },
    {
      key: 3,
      amount: 19,
      svg: {fill: LIGHT_ORANGE},
      title: 'Resistance',
    },
    {
      key: 4,
      amount: 10,
      svg: {fill: BUTTON_TITLE},
      title: 'Intensity',
    },
    {
      key: 5,
      amount: 30,
      svg: {fill: MEDIUM_GRAY},
      title: 'Technique',
    },
  ];

  const Labels = ({slices}) => {
    return slices.map((slice, index) => {
      const {labelCentroid, pieCentroid, data} = slice;
      console.log(35345, labelCentroid[0]);
      const xMove = labelCentroid[0] > 0 ? 30 : -65;
      return (
        <G key={index}>
          <SvgText
            x={labelCentroid[0] + moderateScale(xMove)}
            y={labelCentroid[1]}
            fill={BLACK}
            fontFamily={SUB_TITLE_FONT}
            fontSize={calcFontSize(12)}>
            {data.title}
          </SvgText>
          <SvgText
            x={labelCentroid[0] + moderateScale(xMove)}
            y={labelCentroid[1] + moderateScale(15)}
            fill={BLACK}
            fontFamily={SUB_TITLE_FONT}
            fontSize={calcFontSize(12)}>
            {data.amount} %
          </SvgText>
        </G>
      );
    });
  };

  return (
    <View style={styles.component}>
      <View style={styles.headerView}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.description}>{description}</Text>
      </View>
      <PieChart
        style={styles.chartView}
        valueAccessor={({item}) => item.amount}
        data={pieData}
        spacing={0}
        innerRadius={'95%'}
        outerRadius={'80%'}
        padAngle={0}>
        <Labels />
      </PieChart>
    </View>
  );
};

const styles = StyleSheet.create({
  component: {
    borderRadius: 10,
    paddingHorizontal: moderateScale(20),
    paddingVertical: moderateScale(15),
    width: '100%',
    backgroundColor: WHITE,
    elevation: 3,
    shadowColor: BLACK,
    shadowRadius: 5,
    shadowOpacity: 0.5,
    shadowOffset: {width: 0, height: 10},
  },
  title: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
    color: BLACK,
  },
  description: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: GRAY,
    marginTop: moderateScale(6),
    marginBottom: moderateScale(30),
  },
  chartView: {
    height: 200,
    width: deviceWidth,
    alignSelf: 'center',
    marginBottom: moderateScale(30),
  },
});

CardPieChart.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
};

export default CardPieChart;
