import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Icon, Button} from 'react-native-elements';
import {MAIN_TITLE_FONT} from '../styles/fonts';
import {BLACK, WHITE} from '../styles/colors';
import {calcFontSize, moderateScale} from '../utils/dimensions';

function Header({title, onPressBackButton, onPressMoreInfo}) {
  return (
    <View style={styles.header}>
      {onPressBackButton && (
        <View style={styles.arrowBack}>
          <Button
            onPress={onPressBackButton}
            buttonStyle={styles.goBackButton}
            icon={<Icon name="arrow-back" />}
          />
        </View>
      )}
      <View>
        <Text style={styles.headerText}>{title}</Text>
      </View>
      {onPressMoreInfo && (
        <View style={styles.dots}>
          <Button
            onPress={onPressMoreInfo}
            buttonStyle={styles.goBackButton}
            icon={{name: 'more-vert', type: 'material'}}
          />
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomColor: '#E5E5E5',
    marginBottom: 12,
    backgroundColor: WHITE,
    paddingVertical: moderateScale(17),
  },
  arrowBack: {
    alignSelf: 'flex-start',
    width: 50,
    height: 50,
    position: 'absolute',
    left: 0,
    top: 10,
  },
  dots: {
    alignSelf: 'flex-start',
    width: 50,
    height: 50,
    position: 'absolute',
    right: 0,
    top: 10,
  },
  headerText: {
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
    color: BLACK,
  },
  goBackButton: {
    backgroundColor: WHITE,
  },
});

export default Header;
