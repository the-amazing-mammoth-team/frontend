import React, {useContext} from 'react';
import {Text, TouchableOpacity, View, StyleSheet, Modal} from 'react-native';
import PropTypes from 'prop-types';
import {WebView} from 'react-native-webview';
import {ORANGE, WHITE} from '../../styles/colors';
import {LocalizationContext} from '../../localization/translations';
import {calcFontSize, deviceWidth, moderateScale} from '../../utils/dimensions';
import {MAIN_TITLE_FONT} from '../../styles/fonts';
import {Loading} from '../Loading';

const WebViewModal = ({isVisible, onButtonPress, link}) => {
  const {translations} = useContext(LocalizationContext);
  return (
    <Modal visible={isVisible} transparent={true} animationType="fade">
      <View style={styles.overlay}>
        <View style={styles.modalContainer}>
          <WebView style={{flex: 1}} source={{uri: link}} />
          <TouchableOpacity style={styles.modalButton} onPress={onButtonPress}>
            <Text style={styles.buttonText}>
              {translations.signUpWithEmailScreen.return}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

WebViewModal.propTypes = {
  isVisible: PropTypes.bool,
  onButtonPress: PropTypes.func,
};

const styles = StyleSheet.create({
  overlay: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalContainer: {
    backgroundColor: WHITE,
    width: '90%',
    height: '90%',
    borderRadius: moderateScale(10),
    paddingHorizontal: moderateScale(10),
    paddingVertical: moderateScale(10),
  },
  modalButton: {
    position: 'absolute',
    bottom: moderateScale(15),
    backgroundColor: ORANGE,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: moderateScale(25),
    alignSelf: 'center',
  },
  buttonText: {
    color: WHITE,
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
    paddingHorizontal: moderateScale(30),
    paddingVertical: moderateScale(5),
    textAlign: 'center',
  },
});

export default WebViewModal;
