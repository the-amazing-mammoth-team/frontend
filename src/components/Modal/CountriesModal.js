import Modal from 'react-native-modal';
import PropTypes from 'prop-types';
import {StyleSheet, Text, TouchableOpacity, View, FlatList} from 'react-native';
import React, {useCallback} from 'react';
import {BLACK, GRAY_BORDER, WHITE} from '../../styles/colors';
import {
  calcFontSize,
  deviceHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {countriesList} from '../../utils/countries';
import {SUB_TITLE_FONT} from '../../styles/fonts';

const CountriesModal = ({isModalVisible, onDismiss, onSelect}) => {
  const renderItem = useCallback(
    ({item}) => {
      return (
        <TouchableOpacity
          onPress={() => {
            onSelect('country', item.name);
            onDismiss();
          }}
          style={styles.countryRow}>
          <Text style={styles.countryName}>{item.name}</Text>
          <View style={styles.line} />
        </TouchableOpacity>
      );
    },
    [onDismiss, onSelect],
  );

  return (
    <Modal
      isVisible={isModalVisible}
      useNativeDriver={true}
      animationIn={'slideInLeft'}
      animationOut={'slideOutLeft'}
      onBackdropPress={onDismiss}>
      <View style={styles.modalStyle}>
        <View style={{height: deviceHeight * 0.7}}>
          <FlatList
            data={countriesList}
            renderItem={renderItem}
            keyExtractor={item => item.code}
            initialNumToRender={10}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </View>
    </Modal>
  );
};

CountriesModal.propTypes = {
  isModalVisible: PropTypes.bool,
  onDismiss: PropTypes.func,
  onSelect: PropTypes.func,
};

const styles = StyleSheet.create({
  modalStyle: {
    backgroundColor: WHITE,
    width: deviceWidth * 0.9,
    height: deviceHeight * 0.8,
    alignSelf: 'center',
    borderRadius: moderateScale(20),
    paddingHorizontal: moderateScale(20),
    paddingVertical: moderateScale(33),
  },
  countryRow: {
    width: '100%',
    paddingVertical: moderateScale(10),
  },
  countryName: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
    color: BLACK,
  },
  line: {
    height: moderateScale(1),
    backgroundColor: GRAY_BORDER,
    width: '100%',
    marginBottom: moderateScale(5),
    marginTop: moderateScale(5),
  },
});

export default CountriesModal;
