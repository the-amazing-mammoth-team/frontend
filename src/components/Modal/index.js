import React from 'react';
import {Modal, View, Text, StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';
import CautionIcon from '../../assets/icons/Caution.svg';
import {WHITE, LIGHT_GRAY, LIGHT_ORANGE} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {MButton} from '../buttons';
import {LocalizationContext} from '../../localization/translations';

function MModal() {
  const {translations} = React.useContext(LocalizationContext);
  return (
    <Modal transparent={true} animationType="slide" visible={true}>
      <View style={styles.overlay}>
        <View style={styles.modal}>
          <View>
            <CautionIcon height={50} fill="#000000" />
          </View>
          <Text style={styles.modalTitle}>
            {translations.recoverPasswordScreen.sucessChangingPassword}
          </Text>
          <Text style={styles.modalText}>
            {translations.settingsScreen.descriptionModal}
          </Text>
          <View>
            <MButton
              // onPress={}
              main
              title={translations.settingsScreen.continueProgram}
              buttonExtraStyles={styles.buttonExtraStyles}
            />
          </View>
          <View>
            <Button
              buttonStyle={styles.button}
              titleStyle={styles.buttonText}
              // onPress={toggleModal}
              title={translations.settingsScreen.deleteTraining}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE,
  },
  blockTitle: {
    color: LIGHT_GRAY,
    fontFamily: SUB_TITLE_FONT,
    marginBottom: 10,
  },
  allSettings: {
    padding: 20,
    paddingTop: 0,
    marginTop: -30,
  },
  settingTitle: {
    fontSize: 18,
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: 5,
  },
  settingsDescription: {
    color: LIGHT_GRAY,
    fontFamily: SUB_TITLE_FONT,
    marginBottom: 10,
  },
  blockContainer: {
    borderBottomWidth: 0.5,
    borderColor: LIGHT_GRAY,
    paddingBottom: 15,
    paddingTop: 25,
  },
  optionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // borderWidth: 3,
    // borderColor: 'yellow',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  optionContent: {
    flexBasis: '75%',
  },
  deleteWorkout: {
    color: LIGHT_ORANGE,
    textDecorationStyle: 'solid',
    textDecorationLine: 'underline',
    fontFamily: SUB_TITLE_FONT,
  },
  deleteWorkoutContainer: {
    padding: 20,
    paddingBottom: 40,
    fontFamily: SUB_TITLE_FONT,
  },
  modal: {
    margin: 50,
    marginLeft: 15,
    marginRight: 15,
    padding: 35,
    backgroundColor: WHITE,
    borderRadius: 10,
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
  },
  modalTitle: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: 20,
    marginTop: 10,
  },
  modalText: {
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
    lineHeight: 20,
  },
  buttonExtraStyles: {
    marginBottom: 0,
    width: 'auto',
  },
  buttonText: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: 14,
    backgroundColor: WHITE,
    textDecorationLine: 'underline',
  },
  button: {
    backgroundColor: WHITE,
  },
});

export default MModal;
