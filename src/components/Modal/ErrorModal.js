import React from 'react';
import {Text, TouchableOpacity, View, StyleSheet} from 'react-native';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';
import {BLACK, ORANGE, WHITE} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';

const ErrorModal = ({
  isModalVisible,
  onDismiss,
  title,
  description,
  textButton = 'Accept',
  onButtonPress = onDismiss,
}) => {
  return (
    <Modal
      isVisible={isModalVisible}
      useNativeDriver={true}
      onBackdropPress={onDismiss}>
      <View style={styles.modalStyle}>
        <Text style={styles.modalTitle}>{title}</Text>
        <Text style={styles.modalDescription}>{description}</Text>
        <TouchableOpacity style={styles.modalButton} onPress={onButtonPress}>
          <Text style={styles.buttonText}>{textButton}</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

ErrorModal.propTypes = {
  isModalVisible: PropTypes.bool,
  onDismiss: PropTypes.func,
  title: PropTypes.string,
  description: PropTypes.string,
};

const styles = StyleSheet.create({
  modalStyle: {
    backgroundColor: WHITE,
    width: deviceWidth * 0.9,
    alignSelf: 'center',
    borderRadius: moderateScale(10),
    paddingHorizontal: moderateScale(20),
    paddingVertical: moderateScale(33),
  },
  modalTitle: {
    fontSize: calcFontSize(25),
    fontFamily: MAIN_TITLE_FONT,
    textAlign: 'left',
    marginBottom: calcHeight(10),
    color: BLACK,
  },
  modalDescription: {
    color: BLACK,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    marginBottom: moderateScale(30),
  },
  modalButton: {
    width: '95%',
    backgroundColor: ORANGE,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: moderateScale(15),
    borderRadius: moderateScale(25),
    alignSelf: 'center',
  },
  buttonText: {
    color: WHITE,
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
  },
});

export default ErrorModal;
