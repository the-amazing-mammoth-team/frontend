import React, {useContext} from 'react';
import {Text, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import PropTypes from 'prop-types';
import {styles} from '../../scenes/Profile/styles';
import Cross from '../../assets/images/cross.svg';
import {calcHeight, calcWidth} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import {LIGHT_ORANGE, ORANGE, WHITE} from '../../styles/colors';

const GradientCard = ({
  mediumPace,
  totalKcal,
  totalTime,
  totalReps,
  noGradient = false,
}) => {
  const {translations} = useContext(LocalizationContext);

  const gradientColors = [ORANGE, LIGHT_ORANGE];
  const gradientStart = {x: 0, y: 0};
  const gradientEnd = {x: 1, y: 0};

  return (
    <LinearGradient
      start={gradientStart}
      end={gradientEnd}
      colors={noGradient ? [WHITE, WHITE] : gradientColors}
      style={styles.linearGradient}>
      <View style={[styles.resultView, styles.topLeft]}>
        <Text
          style={[
            styles.resultsCardTextTitle,
            noGradient && styles.resultsCardTextTitleBlack,
          ]}>
          {totalReps}
        </Text>
        <Text
          style={[
            styles.resultsCardTextName,
            noGradient && styles.resultsCardTextNameBlack,
          ]}>
          {translations.profileScreen.totalReps}
        </Text>
      </View>
      <View style={[styles.resultView, styles.topRight]}>
        <Text
          style={[
            styles.resultsCardTextTitle,
            noGradient && styles.resultsCardTextTitleBlack,
          ]}>
          {totalTime}
        </Text>
        <Text
          style={[
            styles.resultsCardTextName,
            noGradient && styles.resultsCardTextNameBlack,
          ]}>
          {translations.profileScreen.totalTime}
        </Text>
      </View>
      <View style={[styles.resultView, styles.bottomLeft]}>
        <Text
          style={[
            styles.resultsCardTextTitle,
            noGradient && styles.resultsCardTextTitleBlack,
          ]}>
          {totalKcal}
        </Text>
        <Text
          style={[
            styles.resultsCardTextName,
            noGradient && styles.resultsCardTextNameBlack,
          ]}>
          {translations.profileScreen.totalKcal}
        </Text>
      </View>
      <View style={[styles.resultView, styles.bottomRight]}>
        <Text
          style={[
            styles.resultsCardTextTitle,
            noGradient && styles.resultsCardTextTitleBlack,
          ]}>
          {mediumPace}
        </Text>
        <Text
          style={[
            styles.resultsCardTextName,
            noGradient && styles.resultsCardTextNameBlack,
          ]}>
          {translations.profileScreen.mediumPace}
        </Text>
      </View>
    </LinearGradient>
  );
};

GradientCard.propTypes = {
  mediumPace: PropTypes.number,
  totalKcal: PropTypes.number,
  totalTime: PropTypes.number,
  totalReps: PropTypes.number,
  noGradient: PropTypes.bool,
};

export default GradientCard;
