import React, {useContext} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';
import {LocalizationContext} from '../../localization/translations';
import {styles} from '../../scenes/Profile/styles';
import EditIcon from '../../assets/icons/editIcon.svg';
import {calcHeight, calcWidth} from '../../utils/dimensions';
import {BLACK} from '../../styles/colors';

const ProfileInfoTable = ({
  weight,
  idealWeight,
  BMI,
  idealBMI,
  bodyFat,
  idealBodyFat,
  muscleWeight,
  idealMuscleWeight,
  goToEditUser,
}) => {
  const {translations} = useContext(LocalizationContext);

  return (
    <View style={styles.table}>
      <View style={styles.tableHeaderView}>
        <View style={styles.tableTitleView}>
          <Text style={styles.tableTitleText}>
            {translations.profileScreen.profile}
          </Text>
        </View>
      </View>
      <View style={[styles.tableRow, styles.noBorderBottom]}>
        <View style={styles.tableFirstColumn} />
        <View style={styles.tableSecondColumn}>
          <Text style={[styles.tableText, styles.lightOrangeText]}>
            {translations.diagnosisScreen.current}
          </Text>
        </View>
        <View style={styles.tableSecondColumn}>
          <Text style={[styles.tableText, styles.orangeText]}>
            {translations.diagnosisScreen.ideal}
          </Text>
        </View>
      </View>
      <View style={styles.tableRow}>
        <View style={styles.tableFirstColumn}>
          <Text
            style={[
              styles.tableText,
              styles.darkGrayText,
              styles.marginLeftTen,
            ]}>
            {translations.diagnosisScreen.weight}
          </Text>
        </View>
        <View style={styles.tableSecondColumn}>
          <Text style={styles.tableText}>{weight} Kg</Text>
        </View>
        <View style={styles.tableSecondColumn}>
          <Text style={styles.tableText}>{idealWeight} Kg</Text>
        </View>
      </View>
      <View style={styles.tableRow}>
        <View style={styles.tableFirstColumn}>
          <Text
            style={[
              styles.tableText,
              styles.darkGrayText,
              styles.marginLeftTen,
            ]}>
            {translations.diagnosisScreen.BMI}
          </Text>
        </View>
        <View style={styles.tableSecondColumn}>
          <Text style={styles.tableText}>{BMI}</Text>
        </View>
        <View style={styles.tableSecondColumn}>
          <Text style={styles.tableText}>{idealBMI}</Text>
        </View>
      </View>
      <View style={styles.tableRow}>
        <View style={styles.tableFirstColumn}>
          <Text
            style={[
              styles.tableText,
              styles.darkGrayText,
              styles.marginLeftTen,
            ]}>
            {translations.diagnosisScreen.bodyFat}
          </Text>
        </View>
        <View style={styles.tableSecondColumn}>
          <Text style={styles.tableText}>{bodyFat} Kg</Text>
        </View>
        <View style={styles.tableSecondColumn}>
          <Text style={styles.tableText}>{idealBodyFat} Kg</Text>
        </View>
      </View>
      <View style={[styles.tableRow, styles.noBorderBottom]}>
        <View style={styles.tableFirstColumn}>
          <Text
            style={[
              styles.tableText,
              styles.darkGrayText,
              styles.marginLeftTen,
            ]}>
            {translations.diagnosisScreen.muscleWeight}
          </Text>
        </View>
        <View style={styles.tableSecondColumn}>
          <Text style={styles.tableText}>{muscleWeight} Kg</Text>
        </View>
        <View style={styles.tableSecondColumn}>
          <Text style={styles.tableText}>{idealMuscleWeight} Kg</Text>
        </View>
      </View>
    </View>
  );
};

ProfileInfoTable.propTypes = {
  goToEditUser: PropTypes.func,
  weight: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  idealWeight: PropTypes.string,
  BMI: PropTypes.string,
  idealBMI: PropTypes.string,
  bodyFat: PropTypes.string,
  idealBodyFat: PropTypes.string,
  muscleWeight: PropTypes.string,
  idealMuscleWeight: PropTypes.string,
};

export default ProfileInfoTable;
