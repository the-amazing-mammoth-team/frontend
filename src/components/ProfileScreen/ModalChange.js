import React, {useContext} from 'react';
import {View, Text, StyleSheet, Modal} from 'react-native';
// import Mixpanel from 'react-native-mixpanel';
import {Button} from 'react-native-elements';
import {WHITE, LIGHT_ORANGE} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {MButton} from '../buttons';
import CautionIcon from '../../assets/icons/Caution.svg';
import {LocalizationContext} from '../../localization/translations';
import {deviceHeight, moderateScale} from '../../utils/dimensions';

const ModalChange = ({isVisible, actionOk, actionCancel}) => {
  const {translations} = useContext(LocalizationContext);
  return (
    <Modal transparent={true} animationType="slide" visible={isVisible}>
      <View style={styles.overlay}>
        <View style={styles.modal}>
          <CautionIcon height={50} fill="#000000" />
          <Text style={styles.modalTitle}>
            {translations.profileScreen.confirmSaveChanges}
          </Text>
          <Text style={styles.modalText}>
            {translations.profileScreen.confirmSaveChangesDesc}
          </Text>
          <MButton
            main
            title={translations.profileScreen.save}
            buttonExtraStyles={styles.buttonExtraStyles}
            onPress={actionOk}
          />
          <Button
            buttonStyle={styles.button}
            titleStyle={styles.buttonText}
            onPress={actionCancel}
            title={translations.profileScreen.dontSave}
          />
        </View>
      </View>
    </Modal>
  );
};

export default ModalChange;

const styles = StyleSheet.create({
  button: {
    backgroundColor: WHITE,
  },
  buttonText: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: 14,
    textDecorationLine: 'underline',
    marginTop: moderateScale(5),
  },
  modal: {
    marginTop: deviceHeight * 0.1,
    marginLeft: 15,
    marginRight: 15,
    padding: 35,
    backgroundColor: WHITE,
    borderRadius: 10,
    alignItems: 'stretch',
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
  },
  modalTitle: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: 20,
    marginTop: 10,
  },
  modalText: {
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
    lineHeight: 20,
  },
  buttonExtraStyles: {
    marginBottom: 0,
    width: 'auto',
  },
});
