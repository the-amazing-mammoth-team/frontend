import React, {useCallback, useContext, useState} from 'react';
import {SceneMap, TabBar, TabView} from 'react-native-tab-view';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
} from '../../utils/dimensions';
import {BLACK, LIGHT_GRAY, LIGHT_ORANGE} from '../../styles/colors';
import {SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import ResultCard from './ResultCard';

const ResultsCarousel = () => {
  const {translations} = useContext(LocalizationContext);
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'first', title: translations.profileScreen.days},
    {key: 'second', title: translations.profileScreen.exercises},
  ]);

  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={styles.indicatorStyle}
      style={styles.tabBarStyle}
      renderLabel={({route, focused, color}) => (
        <Text
          style={[
            styles.renderLabelText,
            focused && styles.renderLabelTextFocused,
          ]}>
          {route.title}
        </Text>
      )}
    />
  );

  const mockExercises = [
    {
      id: 1,
      title: 'Bulgarian squat',
    },
    {
      id: 2,
      title: 'Exercise 2',
    },
  ];
  const renderItem = useCallback(({item}) => {
    return <ResultCard item={item} />;
  }, []);

  const FirstRoute = useCallback(
    () => (
      <View>
        <FlatList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          keyExtractor={item => item.id.toString()}
          data={mockExercises}
          contentContainerStyle={styles.flatlistContent}
          renderItem={renderItem}
        />
      </View>
    ),
    [mockExercises, renderItem],
  );
  const SecondRoute = useCallback(
    () => (
      <View>
        <FlatList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          keyExtractor={item => item.id.toString()}
          data={mockExercises}
          contentContainerStyle={styles.flatlistContent}
          renderItem={renderItem}
        />
      </View>
    ),
    [mockExercises, renderItem],
  );

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });

  const initialLayout = deviceWidth;

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
      renderTabBar={renderTabBar}
    />
  );
};

const styles = StyleSheet.create({
  indicatorStyle: {
    backgroundColor: LIGHT_ORANGE,
  },
  tabBarStyle: {
    backgroundColor: 'white',
    elevation: 0,
  },
  renderLabelText: {
    fontFamily: SUB_TITLE_FONT,
    color: LIGHT_GRAY,
    fontSize: calcFontSize(16),
  },
  renderLabelTextFocused: {
    color: BLACK,
  },
  flatlistContent: {
    marginTop: calcHeight(20),
    marginLeft: calcWidth(10),
    paddingVertical: calcHeight(20),
  },
});

export default ResultsCarousel;
