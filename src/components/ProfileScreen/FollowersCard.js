import React, {useContext} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../../utils/dimensions';
import {BLACK, GRAY_BORDER, WHITE} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import UsersPlusIcon from '../../assets/icons/usersPlusIcon.svg';

const FollowersCard = ({
  followersCount,
  youFollowedCount,
  onPressAdd = () => {},
}) => {
  const {translations} = useContext(LocalizationContext);

  return (
    <View style={styles.resultCard}>
      <View>
        <View style={styles.followersFollowedRow}>
          <View style={styles.columnCenter}>
            <Text style={styles.countText}>{followersCount}</Text>
            <Text style={styles.blackText}>
              {translations.profileScreen.followers}
            </Text>
          </View>
          <View style={styles.separateLine} />
          <View style={styles.columnCenter}>
            <Text style={styles.countText}>{youFollowedCount}</Text>
            <Text style={styles.blackText}>
              {translations.profileScreen.followed}
            </Text>
          </View>
        </View>
      </View>
      <TouchableOpacity onPress={onPressAdd} hitSlop={styles.hitSlopFifteen}>
        <UsersPlusIcon width={moderateScale(26)} height={moderateScale(26)} />
      </TouchableOpacity>
    </View>
  );
};

FollowersCard.propTypes = {
  followersCount: PropTypes.number,
  youFollowedCount: PropTypes.number,
  onPressAdd: PropTypes.func,
};

const styles = StyleSheet.create({
  resultCard: {
    borderRadius: 10,
    paddingHorizontal: calcWidth(20),
    paddingVertical: calcHeight(10),
    width: '100%',
    backgroundColor: WHITE,
    elevation: 3,
    shadowColor: BLACK,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    marginTop: calcHeight(35),
    marginBottom: -calcHeight(10),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    color: BLACK,
    fontSize: calcFontSize(20),
    fontFamily: MAIN_TITLE_FONT,
  },
  countText: {
    color: BLACK,
    fontSize: calcFontSize(20),
    fontFamily: SUB_TITLE_FONT,
  },
  blackText: {
    color: BLACK,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  separateLine: {
    width: moderateScale(1),
    height: '100%',
    backgroundColor: GRAY_BORDER,
    marginHorizontal: moderateScale(20),
  },
  hitSlopFifteen: {
    top: moderateScale(15),
    bottom: moderateScale(15),
    right: moderateScale(15),
    left: moderateScale(15),
  },
  followersFollowedRow: {
    flexDirection: 'row',
    marginLeft: moderateScale(15),
  },
  columnCenter: {
    alignItems: 'center',
  },
});

export default FollowersCard;
