import React from 'react';
import {processColor, StyleSheet, Text, View} from 'react-native';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
} from '../../utils/dimensions';
import {LIGHT_GRAY, ORANGE} from '../../styles/colors';
import {SUB_TITLE_BOLD_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {BarChart} from 'react-native-charts-wrapper';

const ResultCard = ({item}) => {
  return (
    <View style={styles.resultCard}>
      <View style={styles.titleView}>
        <Text style={styles.exerciseTitle}>{item.title}</Text>
      </View>
      <BarChart
        style={{flex: 1, height: calcHeight(230)}}
        drawBorders={false}
        legend={{
          fontFamily: SUB_TITLE_FONT,
          horizontalAlignment: 'CENTER',
        }}
        chartDescription={{text: ''}}
        xAxis={{
          drawGridLines: false,
          drawAxisLine: false,
          drawLabels: false,
        }}
        yAxis={{
          left: {
            drawAxisLine: false,
          },
          right: {
            drawAxisLine: false,
            drawLabels: false,
          },
        }}
        scaleEnabled={false}
        dragEnabled={false}
        pinchZoom={false}
        doubleTapToZoomEnabled={false}
        noDataText={'Opps... no data available!'}
        data={{
          dataSets: [
            {
              label: 'Series 1',
              values: [
                {x: 1, y: 15},
                {x: 2, y: 15},
                {x: 3, y: 14},
                {x: 4, y: 15},
                {x: 5, y: 12},
                {x: 6, y: 10},
                {x: 7, y: 10},
                {x: 8, y: 10},
                {x: 9, y: 10},
              ],
              config: {
                color: processColor(ORANGE),
                valueTextColor: processColor('black'),
              },
            },
          ],
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  resultCard: {
    borderRadius: 10,
    paddingHorizontal: calcWidth(10),
    paddingVertical: calcHeight(15),
    width: deviceWidth * 0.8,
    height: calcHeight(220),
    backgroundColor: '#fff',
    marginRight: calcWidth(15),
    elevation: 3,
    shadowColor: '#10aaae',
    shadowRadius: 5,
    shadowOpacity: 0.5,
    shadowOffset: {width: 0, height: 10},
  },
  titleView: {
    alignSelf: 'center',
  },
  exerciseTitle: {
    color: LIGHT_GRAY,
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(16),
  },
});

export default ResultCard;
