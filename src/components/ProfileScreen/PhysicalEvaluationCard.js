import React, {useContext} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {styles} from '../../scenes/Profile/styles';
import {LocalizationContext} from '../../localization/translations';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY_BORDER,
  LIGHT_GRAY,
  LIGHT_ORANGE,
  ORANGE,
} from '../../styles/colors';
import EditIcon from '../../assets/icons/editIcon.svg';
import {SUB_TITLE_FONT} from '../../styles/fonts';

const PhysicalEvaluationCard = () => {
  const {translations} = useContext(LocalizationContext);

  return (
    <View style={localStyles.root}>
      <View style={localStyles.headerRow}>
        <View style={localStyles.titleView}>
          <Text style={styles.objectivesSectionText}>
            {translations.diagnosisScreen.physicalEvaluation}
          </Text>
        </View>
        <TouchableOpacity hitSlop={styles.hitSlopFifteen}>
          <EditIcon
            fill={LIGHT_GRAY}
            width={calcWidth(17)}
            height={calcHeight(17)}
          />
        </TouchableOpacity>
      </View>
      <View style={localStyles.scaleRoot}>
        <Text style={localStyles.scaleText}>
          {translations.profileScreen.loseFat}: -4 Kg
        </Text>
        <View style={localStyles.scaleRow}>
          <View style={localStyles.grayScale}>
            <LinearGradient
              start={{x: 1, y: 0}}
              end={{x: 0, y: 1}}
              colors={[ORANGE, LIGHT_ORANGE]}
              style={[localStyles.linearGradient, {flex: 0.5}]}
            />
          </View>
          <Text style={localStyles.rightScaleText}>- 0,8 / 4 Kg</Text>
        </View>
      </View>
      <View style={localStyles.scaleRoot}>
        <Text style={localStyles.scaleText}>
          {translations.profileScreen.recommendedDailyIntake}: 1569 Kcal
        </Text>
        <View style={localStyles.scaleRow}>
          <View style={localStyles.grayScale}>
            <LinearGradient
              start={{x: 1, y: 0}}
              end={{x: 0, y: 1}}
              colors={[ORANGE, LIGHT_ORANGE]}
              style={[localStyles.linearGradient, {flex: 0.7}]}
            />
          </View>
          <Text style={localStyles.rightScaleText}>1023 Kcal</Text>
        </View>
      </View>
      <View style={[styles.tableRow, styles.noBorderBottom]}>
        <View style={localStyles.tableFirstColumn}>
          <Text
            style={[
              styles.tableText,
              styles.darkGrayText,
              styles.bottomSeparation,
            ]}>
            {translations.diagnosisScreen.achieveGoal}
          </Text>
          <Text style={[styles.trainingDaysLabel, styles.grayText]}>
            {translations.diagnosisScreen.trainingOneTwoTimeWeek}
          </Text>
          <Text style={[styles.trainingDaysLabel, styles.grayText]}>
            {translations.diagnosisScreen.trainingTwoThreeTimeWeek}
          </Text>
          <Text style={[styles.trainingDaysLabel, styles.grayText]}>
            {translations.diagnosisScreen.trainingFiveSixTimeWeek}
          </Text>
        </View>
        <View style={localStyles.tableThirdColumn}>
          <Text style={[styles.tableText, styles.bottomSeparation]}>
            71 {translations.diagnosisScreen.weeks}
          </Text>
          <Text style={[styles.grayText]}>
            65 {translations.diagnosisScreen.weeks}
          </Text>
          <Text style={styles.grayText}>
            61 {translations.diagnosisScreen.weeks}
          </Text>
          <Text style={styles.grayText}>
            59 {translations.diagnosisScreen.weeks}
          </Text>
        </View>
      </View>
    </View>
  );
};

const localStyles = StyleSheet.create({
  root: {
    borderRadius: 10,
    paddingHorizontal: calcWidth(10),
    paddingVertical: calcHeight(5),
    width: '100%',
    backgroundColor: '#fff',
    elevation: 5,
    shadowColor: BLACK,
    shadowRadius: 5,
    shadowOpacity: 0.5,
    shadowOffset: {width: 0, height: 10},
    marginTop: calcHeight(50),
    marginBottom: calcHeight(50),
    flex: 1,
  },
  headerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleView: {
    marginTop: calcHeight(10),
  },
  tableFirstColumn: {
    flexBasis: '80%',
  },
  tableSecondColumn: {
    flexBasis: '20%',
  },
  tableThirdColumn: {
    flexBasis: '30%',
  },
  scaleText: {
    color: BUTTON_TITLE,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    marginBottom: moderateScale(7),
  },
  rightScaleText: {
    color: LIGHT_GRAY,
    fontSize: calcFontSize(12),
    fontFamily: SUB_TITLE_FONT,
  },
  scaleRow: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  grayScale: {
    maxWidth: deviceWidth * 0.6,
    borderRadius: moderateScale(5),
    height: moderateScale(11),
    backgroundColor: GRAY_BORDER,
    flex: 1,
    flexDirection: 'row',
  },
  scaleRoot: {
    marginTop: calcHeight(10),
  },
  linearGradient: {
    maxWidth: deviceWidth * 0.6,
    borderBottomLeftRadius: moderateScale(5),
    borderTopLeftRadius: moderateScale(5),
    height: moderateScale(11),
  },
});

export default PhysicalEvaluationCard;
