import React from 'react';
import {Modal, View, Text, TouchableWithoutFeedback} from 'react-native';
import {Button} from 'react-native-elements';
import {MButton} from '../../components/buttons';
import {BLACK, LIGHT_ORANGE, WHITE} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {moderateScale} from '../../utils/dimensions';
import CautionIcon from '../../assets/icons/Caution.svg';

export default ({
  goNext,
  goBack,
  visible,
  icon,
  title,
  subTitle,
  goNextText,
  goBackText,
  closeModal = () => {},
}) => {
  return (
    <Modal transparent={true} animationType="slide" visible={visible}>
      <TouchableWithoutFeedback onPress={closeModal}>
        <View style={styles.overlay}>
          <TouchableWithoutFeedback onPress={null}>
            <View style={styles.modal}>
              {icon && (
                <View>
                  <CautionIcon height={50} fill="#000000" />
                </View>
              )}
              <Text style={styles.modalTitle}>{title}</Text>
              <Text style={styles.modalText}>{subTitle}</Text>
              <View>
                <MButton
                  onPress={goBack}
                  main
                  title={goBackText}
                  buttonExtraStyles={[
                    styles.buttonExtraStyles,
                    !icon && {marginTop: moderateScale(100)},
                  ]}
                />
              </View>
              <View>
                <Button
                  buttonStyle={styles.button}
                  titleStyle={styles.buttonText}
                  onPress={goNext}
                  title={goNextText}
                />
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};

const styles = {
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
  },
  modal: {
    marginTop: moderateScale(80),
    marginHorizontal: moderateScale(10),
    paddingHorizontal: moderateScale(25),
    paddingTop: moderateScale(30),
    paddingBottom: moderateScale(15),
    backgroundColor: WHITE,
    borderRadius: 10,
  },
  modalTitle: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: 20,
    marginTop: 10,
    color: BLACK,
  },
  modalText: {
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
    lineHeight: 20,
    color: BLACK,
  },
  buttonExtraStyles: {
    width: 'auto',
  },
  buttonText: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: 14,
    backgroundColor: WHITE,
    textDecorationLine: 'underline',
  },
  button: {
    backgroundColor: WHITE,
  },
};
