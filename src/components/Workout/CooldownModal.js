import React, {useContext} from 'react';
import {View, Text, StyleSheet, Modal} from 'react-native';
// import Mixpanel from 'react-native-mixpanel';
import {WHITE, LIGHT_ORANGE, BLACK} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {MButton} from '../buttons';
import HandLikeIcon from '../../assets/icons/onBoardingSvgImage.svg';
import {LocalizationContext} from '../../localization/translations';
import {
  calcFontSize,
  deviceHeight,
  moderateScale,
} from '../../utils/dimensions';

const CooldownModal = ({isVisible, callback}) => {
  const {translations} = useContext(LocalizationContext);
  return (
    <Modal transparent={true} animationType="slide" visible={isVisible}>
      <View style={styles.overlay}>
        <View style={styles.modal}>
          <View>
            <View style={{alignItems: 'center'}}>
              <HandLikeIcon height={100} fill="#000000" />
            </View>
          </View>
          <Text style={styles.modalTitle}>
            {translations.workoutListScreen.congratulations}
          </Text>
          <Text style={styles.modalText}>
            {translations.workoutListScreen.timeToCoolDown}
          </Text>
          <MButton
            main
            title={translations.workoutListScreen.ok}
            buttonExtraStyles={styles.buttonExtraStyles}
            onPress={callback}
          />
        </View>
      </View>
    </Modal>
  );
};

export default CooldownModal;

const styles = StyleSheet.create({
  button: {
    backgroundColor: WHITE,
  },
  buttonText: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: 14,
    textDecorationLine: 'underline',
    marginTop: moderateScale(5),
  },
  modal: {
    marginTop: deviceHeight * 0.1,
    marginLeft: 15,
    marginRight: 15,
    padding: 35,
    backgroundColor: WHITE,
    borderRadius: 10,
    alignItems: 'stretch',
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
  },
  modalTitle: {
    textAlign: 'center',
    fontSize: calcFontSize(20),
    fontFamily: MAIN_TITLE_FONT,
    color: BLACK,
    marginBottom: 20,
    marginTop: 10,
  },
  modalText: {
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
    lineHeight: 20,
    color: BLACK,
    fontSize: calcFontSize(14),
    marginBottom: moderateScale(50),
  },
  buttonExtraStyles: {
    marginBottom: 0,
    width: 'auto',
  },
});
