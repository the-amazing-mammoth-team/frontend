import React, {useContext} from 'react';
import {View, Text, StyleSheet, Modal} from 'react-native';
import {Button} from 'react-native-elements';
// import Mixpanel from 'react-native-mixpanel';
import {WHITE, LIGHT_ORANGE, BLACK} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {MButton} from '../buttons';
import HandLikeIcon from '../../assets/icons/onBoardingSvgImage.svg';
import {LocalizationContext} from '../../localization/translations';
import {
  calcFontSize,
  deviceHeight,
  moderateScale,
} from '../../utils/dimensions';

const CooldownModalCancel = ({isVisible, actionOk, actionCancel}) => {
  const {translations} = useContext(LocalizationContext);
  return (
    <Modal transparent={true} animationType="slide" visible={isVisible}>
      <View style={styles.overlay}>
        <View style={styles.modal}>
          <Text style={styles.modalTitle}>
            {translations.profileScreen.areYouSure}
          </Text>
          <Text style={styles.modalText}>
            {translations.cooldown.cancelModalDesc}
          </Text>
          <MButton
            main
            title={translations.cooldown.chooseCooldown}
            buttonExtraStyles={styles.buttonExtraStyles}
            onPress={actionOk}
          />
          <Button
            buttonStyle={styles.button}
            titleStyle={styles.buttonText}
            onPress={actionCancel}
            title={translations.cooldown.iDontWantToStretch}
          />
        </View>
      </View>
    </Modal>
  );
};

export default CooldownModalCancel;

const styles = StyleSheet.create({
  button: {
    backgroundColor: WHITE,
    marginBottom: moderateScale(10),
  },
  buttonText: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    textDecorationLine: 'underline',
  },
  modal: {
    marginTop: deviceHeight * 0.25,
    marginLeft: 15,
    marginRight: 15,
    padding: moderateScale(20),
    backgroundColor: WHITE,
    borderRadius: 10,
    alignItems: 'stretch',
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
  },
  modalTitle: {
    textAlign: 'center',
    fontSize: calcFontSize(20),
    fontFamily: MAIN_TITLE_FONT,
    color: BLACK,
    marginBottom: 20,
    marginTop: 10,
  },
  modalText: {
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
    lineHeight: 20,
    color: BLACK,
    fontSize: calcFontSize(14),
    marginBottom: moderateScale(25),
  },
  buttonExtraStyles: {
    width: 'auto',
  },
});
