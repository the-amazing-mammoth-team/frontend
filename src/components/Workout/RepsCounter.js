import React, {useContext, useState} from 'react';
import {Modal, View, Text, StyleSheet} from 'react-native';
import {WHITE, GRAY_BORDER} from '../../styles/colors';
import {MAIN_TITLE_FONT} from '../../styles/fonts';
import {Input, Icon, Button} from 'react-native-elements';
import {MButton} from '../buttons';
import {LocalizationContext} from '../../localization/translations';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
    padding: 50,
    paddingLeft: 15,
    paddingRight: 15,
  },
  inner: {
    backgroundColor: WHITE,
    flex: 1,
    borderRadius: 10,
    padding: 25,
  },
  modalTitle: {
    textAlign: 'center',
    fontSize: 18,
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: 20,
  },
  inputStyles: {
    backgroundColor: GRAY_BORDER,
    borderRadius: 25,
    borderWidth: 0,
    borderBottomWidth: 0,
    height: 55,
    fontSize: 25,
  },
  containerStyles: {
    backgroundColor: GRAY_BORDER,
    borderRadius: 25,
    borderBottomWidth: 0,
    borderWidth: 0,
    paddingRight: 10,
  },
  pad: {
    flexDirection: 'row',
    // borderWidth: 1,
    flex: 1,
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  padKey: {
    backgroundColor: WHITE,
    width: '100%',
  },
  padKeyButton: {
    flexBasis: '33.333333333%',
    flexGrow: 0,
    flexShrink: 0,
    height: '25%',
    backgroundColor: WHITE,
    alignItems: 'center',
    justifyContent: 'center',
  },
  padButtonTitle: {
    flex: 1,
    color: '#000000',
    fontSize: 40,
  },
  buttonContainer: {
    alignItems: 'center',
  },
  iconStyle: {
    fontSize: 30,
  },
});

function RepsCounter({closeModal, isVisible}) {
  const [repsCount, setRepsCount] = useState(null);
  const setCount = ({key}) => {
    if (repsCount) {
      setRepsCount(`${repsCount}${key}`);
    } else {
      setRepsCount(key);
    }
  };

  const {translations} = useContext(LocalizationContext);

  const handleCloseModal = () => {
    setRepsCount(null);
    closeModal(repsCount);
  };

  return (
    <Modal transparent visible={isVisible}>
      <View style={styles.overlay}>
        <View style={styles.inner}>
          <Text style={styles.modalTitle}>
            {translations.repsCounterScreen.repetitionsQuestion}
          </Text>
          <Input
            disabled={true}
            value={repsCount}
            inputContainerStyle={styles.containerStyles}
            containerStyle={styles.containerStyles}
            inputStyle={styles.inputStyles}
            placeholder=""
            rightIcon={
              <Icon
                onPress={() => setRepsCount(null)}
                type="font-awesome"
                name="times"
                iconStyle={styles.iconStyle}
              />
            }
          />
          <View style={styles.pad}>
            <Button
              titleStyle={styles.padButtonTitle}
              buttonStyle={styles.padKey}
              containerStyle={styles.padKeyButton}
              title="1"
              onPress={() => setCount({key: '1'})}
            />
            <Button
              titleStyle={styles.padButtonTitle}
              buttonStyle={styles.padKey}
              containerStyle={styles.padKeyButton}
              title="2"
              onPress={() => setCount({key: '2'})}
            />
            <Button
              titleStyle={styles.padButtonTitle}
              buttonStyle={styles.padKey}
              containerStyle={styles.padKeyButton}
              title="3"
              onPress={() => setCount({key: '3'})}
            />
            <Button
              titleStyle={styles.padButtonTitle}
              buttonStyle={styles.padKey}
              containerStyle={styles.padKeyButton}
              title="4"
              onPress={() => setCount({key: '4'})}
            />
            <Button
              titleStyle={styles.padButtonTitle}
              buttonStyle={styles.padKey}
              containerStyle={styles.padKeyButton}
              title="5"
              onPress={() => setCount({key: '5'})}
            />
            <Button
              titleStyle={styles.padButtonTitle}
              buttonStyle={styles.padKey}
              containerStyle={styles.padKeyButton}
              title="6"
              onPress={() => setCount({key: '6'})}
            />
            <Button
              titleStyle={styles.padButtonTitle}
              buttonStyle={styles.padKey}
              containerStyle={styles.padKeyButton}
              title="7"
              onPress={() => setCount({key: '7'})}
            />
            <Button
              titleStyle={styles.padButtonTitle}
              buttonStyle={styles.padKey}
              containerStyle={styles.padKeyButton}
              title="8"
              onPress={() => setCount({key: '8'})}
            />
            <Button
              titleStyle={styles.padButtonTitle}
              buttonStyle={styles.padKey}
              containerStyle={styles.padKeyButton}
              title="9"
              onPress={() => setCount({key: '9'})}
            />
            <Button
              titleStyle={styles.padButtonTitle}
              buttonStyle={styles.padKey}
              containerStyle={styles.padKeyButton}
              title="0"
              onPress={() => setCount({key: '0'})}
            />
          </View>
        </View>
        <View style={styles.buttonContainer}>
          <MButton
            main
            title={translations.repsCounterScreen.send}
            onPress={handleCloseModal}
          />
        </View>
      </View>
    </Modal>
  );
}

export default RepsCounter;
