import React, {useContext} from 'react';
import {StyleSheet, View, Image, Text, TouchableOpacity} from 'react-native';
import {
  GRAY_BORDER,
  LIGHT_GRAY,
  LIGHT_ORANGE,
  BLACK,
  ORANGE,
} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {
  moderateScale,
  calcFontSize,
  calcWidth,
  calcHeight,
} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import CheckedIcon from '../../assets/icons/checkIcon.svg';

const Exercise = ({item, navigation, currentExerciseId, onChangeExercise}) => {
  const {translations} = useContext(LocalizationContext);
  const {name} = item;
  return (
    <>
      <View style={styles.exerciseContainer}>
        <Image source={{uri: item.thumbnail}} style={styles.image} />
        <View style={styles.exerciseContent}>
          <Text style={styles.exerciseName}>{name}</Text>
          <Text
            style={styles.knowMore}
            onPress={() =>
              navigation.navigate('ExerciseDetails', {exerciseId: item.id})
            }>
            {translations.searchExerciseScreen.knowMore}
          </Text>
        </View>
        {currentExerciseId && (
          <TouchableOpacity
            onPress={() => onChangeExercise(item)}
            hitSlop={styles.hitSlopCheckIcon}
            style={[
              styles.circle,
              currentExerciseId === item.id && styles.circleChecked,
            ]}>
            {currentExerciseId && currentExerciseId === item.id && (
              <CheckedIcon width={calcWidth(12)} height={calcHeight(8)} />
            )}
          </TouchableOpacity>
        )}
      </View>
      <View style={styles.exerciseLine} />
    </>
  );
};

export default Exercise;

const styles = StyleSheet.create({
  exerciseLine: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
  },
  exerciseContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: moderateScale(15),
    alignItems: 'center',
  },
  image: {
    width: moderateScale(100),
    height: moderateScale(100),
    backgroundColor: LIGHT_GRAY,
    borderRadius: moderateScale(5),
  },
  exerciseName: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(22),
    color: BLACK,
  },
  knowMore: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
    textDecorationLine: 'underline',
  },
  exerciseContent: {
    flex: 1,
    marginHorizontal: moderateScale(15),
  },
  circle: {
    width: moderateScale(17),
    height: moderateScale(17),
    borderRadius: moderateScale(17) / 2,
    backgroundColor: LIGHT_GRAY,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 0,
  },
  circleChecked: {
    backgroundColor: ORANGE,
  },
  hitSlopCheckIcon: {
    top: calcHeight(20),
    bottom: calcHeight(20),
    right: calcWidth(20),
    left: calcWidth(20),
  },
});
