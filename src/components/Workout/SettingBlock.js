import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
// import Mixpanel from 'react-native-mixpanel';
import ToggleSwitch from 'toggle-switch-react-native';
import {LIGHT_ORANGE, MEDIUM_GRAY, GRAY, BLACK} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {moderateScale, deviceWidth, calcFontSize} from '../../utils/dimensions';

const SettingBlock = ({data, onPress}) => {
  return (
    <View key={data.title} style={styles.optionContainer}>
      <View style={styles.optionContent}>
        <Text style={styles.settingTitle}>{data.title}</Text>
        <Text style={styles.settingsDescription}>{data.description}</Text>
      </View>
      <View>
        <ToggleSwitch
          isOn={data.status}
          onColor={LIGHT_ORANGE}
          offColor={MEDIUM_GRAY}
          onToggle={isOn => onPress(isOn, data)}
          animationSpeed={100}
          thumbOnStyle={styles.thumbStyle}
          thumbOffStyle={styles.thumbStyle}
        />
      </View>
    </View>
  );
};

export default SettingBlock;

const styles = StyleSheet.create({
  settingTitle: {
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: 5,
    color: BLACK,
  },
  settingsDescription: {
    fontSize: calcFontSize(14),
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
  },
  optionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    alignItems: 'center',
    marginVertical: moderateScale(15),
  },
  optionContent: {
    flexBasis: '75%',
  },
  thumbStyle: {
    width: moderateScale(19),
    height: moderateScale(19),
  },
  image: {
    width: deviceWidth * 0.4,
    height: deviceWidth * 0.4,
    marginTop: moderateScale(30),
    alignSelf: 'center',
    borderRadius: deviceWidth * 0.02,
  },
});
