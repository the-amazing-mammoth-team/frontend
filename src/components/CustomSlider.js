import {Text, View, StyleSheet, Platform} from 'react-native';
import React from 'react';
import Slider from 'rn-range-slider';
import LinearGradient from 'react-native-linear-gradient';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../utils/dimensions';
import {SUB_TITLE_FONT} from '../styles/fonts';
import {GRAY, LIGHT_GRAY, LIGHT_ORANGE, ORANGE, WHITE} from '../styles/colors';

const renderThumb = disabled => (
  <View
    style={[
      styles.thumb,
      {backgroundColor: disabled ? 'transparent' : WHITE},
      Platform.OS === 'android' && disabled && {elevation: 0},
    ]}
  />
);
const renderRail = (currentValue, maxValue, disabled) =>
  renderProgressBlocks(currentValue, maxValue, false, disabled);
const renderRailSelected = (currentValue, maxValue, disabled) =>
  renderProgressBlocks(currentValue, maxValue, true, disabled);

const gradientColors = [LIGHT_ORANGE, ORANGE];
const gradientStart = {x: 0, y: 0};
const gradientEnd = {x: 1, y: 0};

const renderProgressBlocks = (currentValue, maxValue, selected, disabled) => {
  const colors = !disabled ? gradientColors : [ORANGE, ORANGE];
  const array = !disabled
    ? [...new Array(selected ? currentValue : maxValue - 1)]
    : [...new Array(selected ? currentValue : maxValue)];
  const value = disabled ? currentValue - 1 : currentValue - 2;
  return (
    <LinearGradient
      style={styles.progressView}
      colors={!selected ? colors : ['transparent', 'transparent']}
      start={gradientStart}
      end={gradientEnd}>
      <View style={[styles.flexBasis, styles.row]}>
        {array?.map((item, index) => {
          return (
            <View
              key={`${selected && 's'}-${index}`}
              style={[
                [
                  styles.progressRectangle,
                  !selected && {
                    marginRight: moderateScale(-0.2),
                    borderLeftWidth: index !== 0 ? 1.5 : 0,
                  },
                  !selected &&
                    Platform.OS === 'android' && {
                      borderTopColor: 'transparent',
                      borderBottomColor: 'transparent',
                      borderRightColor: 'transparent',
                    },
                  !selected && value < index && styles.filledRectangle,
                  !selected &&
                    Platform.OS === 'android' &&
                    index === maxValue - (disabled ? 1 : 2) && {
                      borderTopRightRadius: moderateScale(10),
                      borderBottomRightRadius: moderateScale(10),
                    },
                ],
              ]}
            />
          );
        })}
      </View>
    </LinearGradient>
  );
};

const renderNumbers = maxValue => {
  const array = [...new Array(maxValue - 1)];
  return (
    <View style={styles.row}>
      <View style={styles.progressNumbers}>
        {array?.map((item, index) => {
          return (
            <View
              style={[
                styles.flexBasis,
                styles.row,
                {marginLeft: index !== 0 ? moderateScale(-6) : 0},
              ]}
              key={index}>
              <Text style={[styles.sliderValueText]}>{index + 1}</Text>
              {index === maxValue - 2 && (
                <Text
                  style={[
                    styles.sliderValueText,
                    {textAlign: 'right', marginRight: moderateScale(-2)},
                  ]}>
                  {index + 2}
                </Text>
              )}
            </View>
          );
        })}
      </View>
    </View>
  );
};

const CustomSlider = ({
  maxValue,
  onChange = () => {},
  disabled,
  value,
  minValueText,
  maxValueText,
}) => {
  return (
    <>
      <Slider
        style={styles.flexBasis}
        min={1}
        max={maxValue === 1 ? 2 : maxValue}
        low={value}
        step={1}
        renderThumb={() => renderThumb(disabled)}
        renderRail={() => renderRail(value, maxValue, disabled)}
        renderRailSelected={() => renderRailSelected(value, maxValue, disabled)}
        onValueChanged={onChange}
        disableRange={true}
        styles={{transform: [{translateX: 1}]}}
      />
      {!disabled && renderNumbers(maxValue)}
      {!disabled && (
        <View style={styles.minMaxValueView}>
          <Text style={[styles.sliderValueText, {textAlign: 'left'}]}>
            {minValueText}
          </Text>
          <Text style={[styles.sliderValueText, {textAlign: 'right'}]}>
            {maxValueText}
          </Text>
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  sliderValueText: {
    flex: 1,
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    textAlign: 'left',
  },
  minMaxValueView: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: moderateScale(10),
  },
  progressView: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: moderateScale(10),
  },
  progressNumbers: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: moderateScale(7),
    marginRight: moderateScale(7),
  },
  progressRectangle: {
    flex: 1,
    height: calcHeight(10),
    borderColor: WHITE,
  },
  filledRectangle: {
    backgroundColor: LIGHT_GRAY,
  },
  thumb: {
    width: calcWidth(20),
    height: calcWidth(20),
    borderRadius: moderateScale(20),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    elevation: 4,
  },
  flexBasis: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
  },
});

export default CustomSlider;
