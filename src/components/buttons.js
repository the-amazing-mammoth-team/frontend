import React from 'react';
import {Button, Icon} from 'react-native-elements';
import {StyleSheet, Platform} from 'react-native';
import {MAIN_TITLE_FONT} from '../styles/fonts';
import {ORANGE, WHITE} from '../styles/colors';
import {calcHeight, calcWidth, moderateScale} from '../utils/dimensions';

function MButton({main, onPress, title, buttonExtraStyles, disabled = false}) {
  const buttonStyle = main
    ? styles.buttonStyle
    : [styles.buttonStyle, styles.secondaryButton];
  const titleStyle = main
    ? styles.titleStyle
    : [styles.titleStyle, styles.titleSecondary];
  return (
    <Button
      onPress={onPress}
      title={title}
      titleStyle={titleStyle}
      buttonStyle={[buttonStyle, buttonExtraStyles]}
      disabled={disabled}
    />
  );
}

function NextButton({onPress, disabled = false, buttonExtraStyles = {}}) {
  return (
    <Button
      disabled={disabled}
      onPress={onPress}
      buttonStyle={[
        styles.nextButtonStyle,
        {backgroundColor: !disabled ? ORANGE : '#BFBFBF'},
        Platform.OS === 'android' && styles.nextButtonContainer,
        buttonExtraStyles,
      ]}
      style={Platform.OS === 'ios' && styles.nextButtonContainer}
      icon={<Icon name="arrow-forward" size={25} color="white" />}
      title=""
      hitSlop={styles.hitSlop}
    />
  );
}

const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 18,
    fontFamily: MAIN_TITLE_FONT,
    color: '#FFFFFF',
  },
  titleSecondary: {
    color: ORANGE,
  },
  buttonStyle: {
    borderRadius: 25,
    height: 50,
    backgroundColor: ORANGE,
    width: 350,
    marginTop: 30,
    marginBottom: 20,
  },
  secondaryButton: {
    backgroundColor: '#FFFFFF',
    borderWidth: 2,
    borderColor: ORANGE,
  },
  nextButtonStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: WHITE,
    borderRadius: 50,
    height: calcWidth(50),
    width: calcWidth(50),
  },
  nextButtonContainer: {
    position: 'absolute',
    zIndex: 100,
    bottom: moderateScale(20),
    right: moderateScale(20),
  },
  hitSlop: {
    top: calcHeight(10),
    bottom: calcHeight(10),
    right: calcWidth(10),
    left: calcWidth(10),
  },
});

export {MButton, NextButton};
