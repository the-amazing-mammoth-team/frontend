import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import {calcFontSize, moderateScale} from '../../utils/dimensions';
import {ORANGE} from '../../styles/colors';
import CartIcon from '../../assets/icons/cartIcon.svg';
import {SUB_TITLE_FONT} from '../../styles/fonts';

const OrangeButtonCart = ({text, onPress = () => {}}) => {
  return (
    <TouchableOpacity style={styles.component} onPress={onPress}>
      <CartIcon
        fill={ORANGE}
        width={moderateScale(24)}
        height={moderateScale(21)}
      />
      <Text style={styles.textStyle}>{text}</Text>
    </TouchableOpacity>
  );
};

OrangeButtonCart.propTypes = {
  text: PropTypes.string,
  onPress: PropTypes.func,
};

const styles = StyleSheet.create({
  component: {
    width: '100%',
    paddingVertical: moderateScale(15),
    borderRadius: moderateScale(10),
    borderColor: ORANGE,
    borderWidth: moderateScale(1),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  textStyle: {
    color: ORANGE,
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_FONT,
    marginLeft: moderateScale(10),
  },
});

export default OrangeButtonCart;
