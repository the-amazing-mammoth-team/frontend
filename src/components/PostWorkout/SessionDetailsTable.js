import React, {useContext, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {LocalizationContext} from '../../localization/translations';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY,
  LIGHT_ORANGE,
  ORANGE,
} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {calcFontSize, calcHeight, calcWidth} from '../../utils/dimensions';
import Svg, {Path} from 'react-native-svg';

const SessionDetailsTable = ({data}) => {
  const [showAllDetails, setShowDetails] = useState(true);

  const {translations} = useContext(LocalizationContext);

  return (
    <View style={styles.container}>
      {data.map((item, index) => {
        if (index > 1 && showAllDetails) {
          return null;
        }
        return (
          <View key={item.id} style={styles.table}>
            <View style={[styles.tableRow, styles.noBorderBottom]}>
              <View style={styles.tableFirstColumn}>
                <Text style={styles.subHeaderText}>
                  {translations.postWorkoutScreen.roundCapitalize} {item.round}
                </Text>
              </View>
              <View style={styles.tableSecondColumn}>
                <Text style={[styles.lightGrayColor, styles.tableText]}>
                  Reps
                </Text>
              </View>
              <View style={styles.tableThirdColumn}>
                <Text style={[styles.tableText, styles.lightGrayColor]}>
                  Min
                </Text>
              </View>
              <View style={styles.tableFourthColumn}>
                <Text style={[styles.greenColor, styles.tableText]}>
                  Reps/min
                </Text>
              </View>
            </View>
            {item.exercises.map(exercise => (
              <View key={exercise.id} style={styles.tableRow}>
                <View style={styles.tableFirstColumn}>
                  <Text style={[styles.tableText, styles.darkGrayColor]}>
                    {exercise.name}
                  </Text>
                </View>
                <View style={styles.tableSecondColumn}>
                  <Text style={styles.tableText}>{exercise.repsCount}</Text>
                </View>
                <View style={styles.tableThirdColumn}>
                  <Text style={styles.tableText}>{exercise.timeMin}</Text>
                </View>
                <View style={styles.tableFourthColumn}>
                  <Text style={styles.tableText}>{exercise.repsMin}</Text>
                </View>
              </View>
            ))}
          </View>
        );
      })}
      <TouchableOpacity
        style={styles.showMoreLessView}
        onPress={() => setShowDetails(!showAllDetails)}>
        <Text style={styles.greenUnderLineText}>
          {!showAllDetails
            ? translations.workoutScreen.seeLess
            : translations.workoutScreen.seeMore}
        </Text>
        <TouchableOpacity
          onPress={() => setShowDetails(!showAllDetails)}
          style={{
            transform: [{rotate: showAllDetails ? '-90deg' : '90deg'}],
            marginLeft: calcWidth(10),
          }}>
          <Svg
            width={calcWidth(11)}
            height={calcHeight(11)}
            viewBox="0 0 9 13"
            fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <Path
              d="M7.5 12L2 6.5L7.5 1"
              stroke={LIGHT_ORANGE}
              stroke-width="1.5"
            />
          </Svg>
        </TouchableOpacity>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  table: {
    marginBottom: calcHeight(20),
  },
  headerText: {
    fontSize: 18,
    marginTop: 25,
    color: BLACK,
    textAlign: 'center',
    fontFamily: MAIN_TITLE_FONT,
  },
  subHeaderText: {
    color: GRAY,
    fontSize: 14,
    fontFamily: SUB_TITLE_FONT,
    //marginBottom: calcHeight(5)
  },
  tableRow: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5',
    paddingBottom: 8,
    paddingTop: 8,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  tableFirstColumn: {
    flexBasis: '43%',
  },
  tableSecondColumn: {
    flexBasis: '17%',
  },
  tableThirdColumn: {
    flexBasis: '20%',
  },
  tableFourthColumn: {
    flexBasis: '23%',
  },
  tableText: {
    fontSize: 16,
    fontFamily: SUB_TITLE_FONT,
  },
  noBorderBottom: {
    borderBottomWidth: 0,
  },
  subHeadermb: {
    marginBottom: 10,
    marginTop: 20,
  },
  greenColor: {
    color: LIGHT_ORANGE,
  },
  purpleColor: {
    color: ORANGE,
  },
  lightGrayColor: {
    color: GRAY,
  },
  darkGrayColor: {
    color: BUTTON_TITLE,
  },
  blackColor: {
    color: BLACK,
  },
  grayText: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
  },
  trainingDaysLabel: {
    textAlign: 'right',
  },
  bottomSeparation: {
    marginBottom: 15,
  },
  nextButtonStyle: {
    borderRadius: 50,
    height: 50,
    width: 50,
    backgroundColor: ORANGE,
  },
  greenUnderLineText: {
    textDecorationLine: 'underline',
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
  showMoreLessView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: calcHeight(15),
  },
});

export default SessionDetailsTable;
