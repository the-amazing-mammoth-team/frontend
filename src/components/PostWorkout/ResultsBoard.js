import React, {useCallback, useContext, useMemo} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import PropTypes from 'prop-types';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../../utils/dimensions';
import {BLACK, GRAY_BORDER, WHITE} from '../../styles/colors';
import {LocalizationContext} from '../../localization/translations';
import TimerIcon from '../../assets/icons/timerIcon.svg';
import ManRunningIcon from '../../assets/icons/manRun.svg';
import LikeFinger from '../../assets/icons/likeFinger.svg';
import StarIcon from '../../assets/icons/star.svg';
import {SUB_TITLE_FONT} from '../../styles/fonts';

const ResultBoard = ({
  effort,
  kcal,
  reps,
  intensity,
  difficulty,
  exersion,
  points,
}) => {
  const {translations} = useContext(LocalizationContext);

  const results = useMemo(() => {
    return [
      {
        id: 1,
        title: translations.postWorkoutScreen.effort,
        icon: (
          <ManRunningIcon
            width={moderateScale(20)}
            height={moderateScale(20)}
          />
        ),
        value: effort,
        prefix: '',
      },
      {
        id: 2,
        title: translations.postWorkoutScreen.enjoyment,
        icon: (
          <LikeFinger width={moderateScale(18)} height={moderateScale(18)} />
        ),
        value: exersion,
        prefix: '',
      },
      {
        id: 3,
        title: translations.postWorkoutScreen.points,
        icon: <StarIcon width={moderateScale(18)} height={moderateScale(18)} />,
        value: points,
        prefix: '',
      },
    ];
  }, [
    effort,
    exersion,
    points,
    translations.postWorkoutScreen.effort,
    translations.postWorkoutScreen.enjoyment,
    translations.postWorkoutScreen.points,
  ]);

  const renderRowResult = useCallback(item => {
    return (
      <View>
        <View style={styles.rowResult}>
          <View style={styles.row}>
            {item.icon}
            <Text
              style={[
                styles.valuesText,
                styles.grayColorText,
                styles.iconTextDistance,
              ]}>
              {item.title}
            </Text>
          </View>
          <Text style={styles.valuesText}>
            {item.value} {item.prefix}
          </Text>
        </View>
        {item.id !== 3 ? (
          <View style={styles.line} />
        ) : (
          <View style={styles.emptyView} />
        )}
      </View>
    );
  }, []);

  return (
    <View style={styles.component}>
      {results.map(item => (
        <View key={item.id}>{renderRowResult(item)}</View>
      ))}
    </View>
  );
};

ResultBoard.propTypes = {
  time: PropTypes.string,
  kcal: PropTypes.string,
  reps: PropTypes.string,
  intensity: PropTypes.string,
  difficulty: PropTypes.any,
  exersion: PropTypes.any,
  points: PropTypes.string,
};

const styles = StyleSheet.create({
  component: {
    width: '100%',
    paddingHorizontal: calcWidth(20),
    paddingTop: calcHeight(20),
    elevation: 5,
    borderRadius: 10,
    backgroundColor: WHITE,
  },
  rowResult: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  valuesText: {
    fontFamily: SUB_TITLE_FONT,
    color: BLACK,
    fontSize: calcFontSize(14),
  },
  line: {
    width: '100%',
    height: calcHeight(1),
    backgroundColor: GRAY_BORDER,
    marginVertical: calcHeight(15),
  },
  grayColorText: {
    color: '#616362',
  },
  iconTextDistance: {
    marginLeft: calcWidth(10),
  },
  emptyView: {
    marginVertical: calcHeight(10),
  },
});

export default ResultBoard;
