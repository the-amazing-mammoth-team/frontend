import React from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import {styles} from '../../scenes/PostWorkout/summaryStyles';
import ResumeFeedCard from './ResumeFeedCard';

const TrySomething = ({data, description, buttonTitle, callback}) => {
  return (
    <View style={styles.backgroundRectangle}>
      <View style={[styles.generalPaddingHorizontal]}>
        <TouchableWithoutFeedback onPress={callback}>
          <View>
            {data.map(item => (
              <ResumeFeedCard
                key={item.id}
                title={item.title}
                description={item.description}
                colors={item.colors}
                idNum={item.id}
                icon={item.icon}
                onPress={callback}
              />
            ))}
          </View>
        </TouchableWithoutFeedback>
        <View style={styles.underFeedCardsView}>
          <Text style={[styles.headerText, styles.fontTitleTwenty]}>
            {description}
          </Text>
        </View>
        <TouchableOpacity style={styles.tryButton} onPress={callback}>
          <Text style={styles.buttonTextWhite}>{buttonTitle}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

TrySomething.propTypes = {
  data: PropTypes.array,
  description: PropTypes.string,
  buttonTitle: PropTypes.string,
};

export default TrySomething;
