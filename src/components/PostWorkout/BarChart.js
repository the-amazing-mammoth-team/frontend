import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {GRAY, ORANGE} from '../../styles/colors';
import {moderateScale} from '../../utils/dimensions';

const styles = StyleSheet.create({
  bar: {
    backgroundColor: ORANGE,
    width: 13,
    borderRadius: 40,
    marginTop: 5,
    marginBottom: 5,
  },
  container: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'flex-end',
    padding: 15,
    justifyContent: 'center',
  },
  barContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    marginLeft: 5,
    marginRight: 15,
  },
  text: {
    color: GRAY,
  },
});

function BarChart({data, color, index}) {
  console.log('data from barchar', data);
  if (!data) {
    return null;
  }
  console.log({data});
  return (
    <View style={styles.container}>
      {Object.keys(data).map((setIndex, index) => {
        const allReps = Object.keys(data).map(set => data[set].count);
        const maxRepsPerSet = Math.max(...allReps);
        const set = data[setIndex];
        const barHeight = (set.count / maxRepsPerSet) * moderateScale(110);
        return (
          <View style={styles.barContainer}>
            <Text style={styles.text}>{Math.round(set.count)}</Text>
            <View
              style={[
                styles.bar,
                {height: barHeight || 0, backgroundColor: color},
              ]}
            />
            <Text style={styles.text}>{index + 1}</Text>
          </View>
        );
      })}
    </View>
  );
}

export default BarChart;
