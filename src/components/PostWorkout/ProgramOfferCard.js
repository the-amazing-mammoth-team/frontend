import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';
import {BLACK, GRAY, LIGHT_ORANGE, ORANGE, WHITE} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_BOLD_FONT} from '../../styles/fonts';

const ProgramOfferCard = ({
  title,
  isCustom,
  tipText,
  costInWeek,
  extraInfoAboutPrice,
  onPress,
  selectedProgram,
}) => {
  const isSelected = selectedProgram === title;

  return (
    <>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => onPress(title)}
        style={[styles.component, styles.selectedComponent]}>
        <View style={styles.row}>
          <View style={styles.titleView}>
            <Text style={styles.title}>{title}</Text>
          </View>
          <View>
            <Text style={[styles.title, styles.titleMain]}>
              {costInWeek} € /<Text style={styles.weeksText}> week</Text>
            </Text>
            <Text style={styles.extraInfoPrice}>{extraInfoAboutPrice}</Text>
          </View>
        </View>
        {isCustom && tipText && (
          <View style={styles.customTip}>
            <Text style={styles.tipText}>{tipText}</Text>
          </View>
        )}
      </TouchableOpacity>
    </>
  );
};

ProgramOfferCard.propTypes = {
  title: PropTypes.string,
  tipText: PropTypes.string,
  isCustom: PropTypes.bool,
  costInWeek: PropTypes.string || PropTypes.number,
  extraInfoAboutPrice: PropTypes.string,
  isSelected: PropTypes.bool,
  onPress: PropTypes.func,
  selectedProgram: PropTypes.any,
};

const styles = StyleSheet.create({
  component: {
    borderRadius: 10,
    backgroundColor: WHITE,
    elevation: 4,
    paddingVertical: calcHeight(28),
    paddingHorizontal: calcWidth(18),
    zIndex: -20,
    marginBottom: calcHeight(20),
  },
  selectedComponent: {
    borderWidth: 1,
    borderColor: ORANGE,
  },
  title: {
    color: BLACK,
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  titleMain: {
    fontSize: calcFontSize(22),
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  customTip: {
    backgroundColor: LIGHT_ORANGE,
    alignItems: 'center',
    justifyContent: 'center',
    height: calcHeight(30),
    width: deviceWidth * 0.44,
    zIndex: 99999,
    left: calcWidth(20),
    position: 'absolute',
    top: -calcHeight(15),
  },
  tipText: {
    color: WHITE,
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(14),
  },
  weeksText: {
    color: BLACK,
    fontSize: calcFontSize(18),
  },
  extraInfoPrice: {
    color: GRAY,
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(12),
    fontWeight: '400',
  },
  titleView: {
    maxWidth: calcWidth(140),
  },
});

export default ProgramOfferCard;
