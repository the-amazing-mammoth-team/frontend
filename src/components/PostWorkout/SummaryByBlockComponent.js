import React, {useCallback, useContext, useState} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {SceneMap, TabBar, TabView} from 'react-native-tab-view';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
  YELLOW,
} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {SUB_TITLE_BOLD_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import {styles as tabViewStyles} from './TabViewResults';
import BarChart from './BarChart';

const SummaryByBlockComponent = ({blockNumber, blockName, summary}) => {
  const {translations} = useContext(LocalizationContext);

  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'first', title: translations.postWorkoutScreen.laps},
    {key: 'second', title: translations.postWorkoutScreen.exercises},
  ]);

  const genericShadowComponent = useCallback(
    item => (
      <View key={item.id} style={styles.rootShadowContainer}>
        <Text style={styles.cardTitle}>{item.title}</Text>
        <BarChart data={item.data} color={item.color} />
        <Text style={styles.barLegend}>{item.barLegend}</Text>
      </View>
    ),
    [],
  );

  console.log('summ', summary);

  let repsSetPerBlock = {};
  if (summary?.getSessionExecutionSummary?.repsSetPerBlock[blockNumber]) {
    Object.keys(
      summary.getSessionExecutionSummary.repsSetPerBlock[blockNumber],
    ).forEach(set => {
      repsSetPerBlock[set] = {
        count:
          summary.getSessionExecutionSummary.repsSetPerBlock[blockNumber][set]
            .reps,
      };
    });
  }

  let timeSetPerBlock = {};
  if (summary?.getSessionExecutionSummary?.timeSetPerBlock[blockNumber]) {
    Object.keys(
      summary.getSessionExecutionSummary.timeSetPerBlock[blockNumber],
    ).forEach(set => {
      timeSetPerBlock[set] = {
        count:
          summary.getSessionExecutionSummary.timeSetPerBlock[blockNumber][set]
            .execution_time,
      };
    });
  }

  let averageRepsMinSetPerBlock = {};
  if (
    summary?.getSessionExecutionSummary?.averageRepsMinSetPerBlock[blockNumber]
  ) {
    Object.keys(
      summary.getSessionExecutionSummary.averageRepsMinSetPerBlock[blockNumber],
    ).forEach(set => {
      averageRepsMinSetPerBlock[set] = {
        count:
          summary.getSessionExecutionSummary.averageRepsMinSetPerBlock[
            blockNumber
          ][set],
      };
    });
  }

  const parseExercisesBlock = (data = {}, key) => {
    return Object.keys(data).reduce((acc, exerciseId) => {
      acc[exerciseId] = {
        count: data[exerciseId][key],
      };
      return acc;
    }, {});
  };

  const FirstRoute = useCallback(
    () => (
      <View style={styles.rootCarousel}>
        <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
          {[
            {
              data: averageRepsMinSetPerBlock,
              title: translations.postWorkoutScreen.pacePerLap,
              id: 3,
              barLegend: 'reps/min',
              color: ORANGE,
            },
            {
              data: timeSetPerBlock,
              title: translations.postWorkoutScreen.timePerLap,
              id: 2,
              barLegend: 'seconds',
              color: LIGHT_ORANGE,
            },
            {
              data: repsSetPerBlock,
              title: translations.postWorkoutScreen.repsPerLap,
              id: 1,
              barLegend: 'reps',
              color: YELLOW,
            },
          ].map(item => {
            const arrValuesMoreZero = Object.values(item.data).filter(
              item => item.count > 0,
            );
            return arrValuesMoreZero.length > 0 && genericShadowComponent(item);
          })}
        </ScrollView>
      </View>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      averageRepsMinSetPerBlock,
      genericShadowComponent,
      repsSetPerBlock,
      timeSetPerBlock,
    ],
  );

  const SecondRoute = useCallback(
    () => (
      <View style={styles.rootCarousel}>
        <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
          {[
            {
              data: parseExercisesBlock(
                summary?.getSessionExecutionSummary?.repsPerMinPerExercise,
                'value',
              ),
              title: translations.postWorkoutScreen.pacePerExercise,
              id: 3,
              barLegend: 'reps/min',
              color: ORANGE,
            },
            {
              data: parseExercisesBlock(
                summary?.getSessionExecutionSummary?.minsPerExercise,
                'execution_time',
              ),
              title: translations.postWorkoutScreen.timePerExercise,
              id: 2,
              barLegend: 'seconds',
              color: LIGHT_ORANGE,
            },
            {
              data: parseExercisesBlock(
                summary?.getSessionExecutionSummary?.repsPerExercise,
                'reps',
              ),
              title: translations.postWorkoutScreen.repsPerExercise,
              id: 1,
              barLegend: 'reps',
              color: YELLOW,
            },
          ].map(item => genericShadowComponent(item))}
        </ScrollView>
      </View>
    ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [
      averageRepsMinSetPerBlock,
      genericShadowComponent,
      repsSetPerBlock,
      timeSetPerBlock,
    ],
  );

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });

  const initialLayout = deviceWidth;
  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={tabViewStyles.indicatorStyle}
      style={tabViewStyles.tabBarStyle}
      renderLabel={({route, focused, color}) => (
        <Text
          style={[
            tabViewStyles.renderLabelText,
            focused && tabViewStyles.renderLabelTextFocused,
          ]}>
          {route.title}
        </Text>
      )}
    />
  );

  return (
    <View style={styles.root}>
      <View style={styles.blockNumberView}>
        <Text style={styles.blockNumberText}>
          {translations.postWorkoutScreen.block} {blockNumber} - {blockName}
        </Text>
      </View>

      {FirstRoute()}
      {/* <TabView
        swipeEnabled={false}
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
        renderTabBar={renderTabBar}
      /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    marginBottom: calcHeight(25),
  },
  blockNumberView: {
    backgroundColor: WHITE,
    paddingVertical: moderateScale(5),
    alignItems: 'flex-start',
    marginBottom: moderateScale(10),
  },
  blockNumberText: {
    color: ORANGE,
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(14),
  },
  indicatorStyle: {
    backgroundColor: LIGHT_ORANGE,
  },
  tabBarStyle: {
    backgroundColor: 'white',
    elevation: 0,
  },
  renderLabelText: {
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
    fontSize: calcFontSize(16),
  },
  renderLabelTextFocused: {
    color: BLACK,
  },
  rootShadowContainer: {
    shadowColor: BLACK,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    marginLeft: moderateScale(5),
    marginRight: moderateScale(20),
    //borderWidth: 1,
    elevation: 4,
    borderRadius: moderateScale(10),
    marginVertical: moderateScale(10),
    paddingVertical: moderateScale(15),
    backgroundColor: WHITE,
    minWidth: deviceWidth * 0.8,
    height: moderateScale(247),
  },
  rootCarousel: {
    marginTop: moderateScale(20),
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(10),
    backgroundColor: 'white',
  },
  cardTitle: {
    color: BUTTON_TITLE,
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
  },
  barLegend: {
    textAlign: 'center',
    color: GRAY,
  },
});

export default SummaryByBlockComponent;
