import React, {useCallback, useContext, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {SceneMap, TabBar, TabView} from 'react-native-tab-view';
import Time from '../../components/Time';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {BLACK, GRAY, LIGHT_ORANGE, ORANGE, YELLOW} from '../../styles/colors';
import {SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';

const colors = [ORANGE, LIGHT_ORANGE, YELLOW];

const TabViewResults = ({summary}) => {
  const {translations, appLanguage} = useContext(LocalizationContext);
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: 'first', title: translations.postWorkoutScreen.repetitions},
    {key: 'second', title: translations.postWorkoutScreen.time},
    {key: 'third', title: translations.postWorkoutScreen.pace},
  ]);
  console.log('SUmmary', summary);
  const repsData =
    (summary?.getSessionExecutionSummary?.repsPerExercise &&
      Object.keys(summary.getSessionExecutionSummary.repsPerExercise).map(
        exId => {
          return {
            id: exId,
            repsCount:
              summary.getSessionExecutionSummary.repsPerExercise[exId].reps,
            color: colors[Math.floor(Math.random() * colors.length)],
            name:
              summary.getSessionExecutionSummary.repsPerExercise[exId][
                appLanguage === 'es' ? 'name_es' : 'name_en'
              ],
            totalTime:
              summary.getSessionExecutionSummary.minsPerExercise[exId]
                .execution_time,
            repsPerMin:
              summary.getSessionExecutionSummary.repsPerMinPerExercise[exId]
                .value,
          };
        },
      )) ||
    [];

  console.log({repsData});

  const handleChartData = useCallback(
    ({count, color, totalTime, mode, repsPerMin, maxValue}) => {
      const widthPercent = (90 * count) / 100;

      let totalCount = count;
      let label = 'reps';
      if (mode === 'time') {
        totalCount = <Time count={totalTime} />;
        label = 'minutes';
      }
      if (mode === 'reps/min') {
        totalCount = Math.round(repsPerMin);
        label = 'reps/min';
      }

      console.log(111, widthPercent);

      const calcFlex = (mode !== 'time' ? totalCount : totalTime) / maxValue;

      return (
        <View style={styles.chartRow}>
          <View
            style={[
              styles.chartRowColumn,
              {
                flex: !isNaN(calcFlex) ? calcFlex : 0,
                backgroundColor: color,
              },
            ]}
          />
          <Text style={[styles.blackText, {marginLeft: calcWidth(8)}]}>
            {totalCount} {label}
          </Text>
        </View>
      );
    },
    [],
  );

  const calcMaxValue = useCallback(
    key => {
      return repsData?.reduce(function(a, b) {
        return Math.max(a, b[key]);
      }, 0);
    },
    [repsData],
  );

  const FirstRoute = useCallback(() => {
    return (
      <View style={{marginTop: calcHeight(25)}}>
        {repsData.map(item => (
          <View style={{marginVertical: calcHeight(6)}} key={item.id}>
            <Text style={styles.grayText}>{item.name}</Text>
            {handleChartData({
              count: item.repsCount,
              color: item.color,
              mode: 'reps',
              totalTime: item.totalTime,
              repsPerMin: item.repsPerMin,
              maxValue: calcMaxValue('repsCount'),
            })}
          </View>
        ))}
      </View>
    );
  }, [calcMaxValue, handleChartData, repsData]);

  const SecondRoute = useCallback(() => {
    return (
      <View style={{marginTop: calcHeight(25)}}>
        {repsData.map(item => (
          <View style={{marginVertical: calcHeight(6)}} key={item.id}>
            <Text style={styles.grayText}>{item.name}</Text>
            {handleChartData({
              count: item.repsCount,
              color: item.color,
              mode: 'time',
              totalTime: item.totalTime,
              repsPerMin: item.repsPerMin,
              maxValue: calcMaxValue('totalTime'),
            })}
          </View>
        ))}
      </View>
    );
  }, [calcMaxValue, handleChartData, repsData]);

  const ThirdRoute = useCallback(() => {
    return (
      <View style={{marginTop: calcHeight(25)}}>
        {repsData.map(item => (
          <View style={{marginVertical: calcHeight(6)}} key={item.id}>
            <Text style={styles.grayText}>{item.name}</Text>
            {handleChartData({
              count: item.repsCount,
              color: item.color,
              mode: 'reps/min',
              totalTime: item.totalTime,
              repsPerMin: item.repsPerMin,
              maxValue: calcMaxValue('repsPerMin'),
            })}
          </View>
        ))}
      </View>
    );
  }, [calcMaxValue, handleChartData, repsData]);

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute,
  });
  const initialLayout = deviceWidth;

  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={styles.indicatorStyle}
      style={styles.tabBarStyle}
      renderLabel={({route, focused, color}) => (
        <Text
          style={[
            styles.renderLabelText,
            focused && styles.renderLabelTextFocused,
          ]}>
          {route.title}
        </Text>
      )}
    />
  );

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
      renderTabBar={renderTabBar}
    />
  );
};

export const styles = StyleSheet.create({
  indicatorStyle: {
    backgroundColor: LIGHT_ORANGE,
  },
  tabBarStyle: {
    backgroundColor: 'white',
    elevation: 0,
  },
  renderLabelText: {
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
    fontSize: calcFontSize(16),
  },
  renderLabelTextFocused: {
    color: BLACK,
  },
  grayText: {
    color: GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    marginBottom: calcHeight(5),
  },
  blackText: {
    color: BLACK,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  chartRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  chartRowColumn: {
    height: moderateScale(10),
    borderRadius: moderateScale(10) / 2,
  },
});

export default TabViewResults;
