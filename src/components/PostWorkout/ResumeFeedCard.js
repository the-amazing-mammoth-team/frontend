import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {WHITE} from '../../styles/colors';

const ResumeFeedCard = ({colors, title, description, idNum, icon: Icon}) => {
  const isParity = idNum % 2 === 0;

  return (
    <View>
      <LinearGradient
        style={styles.component}
        colors={colors}
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}>
        <View
          style={[
            styles.titleView,
            idNum % 2 === 0 && styles.titleViewReversed,
          ]}>
          <Text style={styles.title}>{title}</Text>
        </View>
        <View
          style={[
            styles.descriptionView,
            isParity && styles.descriptionViewReversed,
          ]}>
          <Text
            style={[
              styles.description,
              isParity && styles.descriptionReversed,
            ]}>
            {description}
          </Text>
        </View>
      </LinearGradient>
      <View
        style={[
          styles.absoluteIconView,
          isParity && styles.absoluteIconViewReversed,
        ]}>
        <Icon width={moderateScale(110)} height={moderateScale(110)} />
      </View>
    </View>
  );
};

ResumeFeedCard.propTypes = {
  color: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  idNum: PropTypes.number,
  icon: PropTypes.any,
};

const styles = StyleSheet.create({
  component: {
    paddingTop: calcHeight(15),
    paddingBottom: calcHeight(25),
    paddingHorizontal: calcWidth(15),
    borderRadius: 15,
    width: '100%',
    marginBottom: calcHeight(35),
  },
  title: {
    fontFamily: MAIN_TITLE_FONT,
    color: WHITE,
    fontSize: calcFontSize(20),
  },
  titleView: {
    alignSelf: 'flex-start',
  },
  titleViewReversed: {
    alignSelf: 'flex-end',
  },
  description: {
    fontFamily: SUB_TITLE_FONT,
    color: WHITE,
    fontSize: calcFontSize(14),
    marginTop: calcHeight(5),
  },
  descriptionReversed: {
    textAlign: 'right',
  },
  descriptionView: {
    maxWidth: '60%',
    alignSelf: 'flex-start',
  },
  descriptionViewReversed: {
    alignSelf: 'flex-end',
  },
  absoluteIconView: {
    position: 'absolute',
    bottom: moderateScale(20),
    right: moderateScale(10),
  },
  absoluteIconViewReversed: {
    position: 'absolute',
    bottom: moderateScale(20),
    left: moderateScale(10),
  },
});

export default ResumeFeedCard;
