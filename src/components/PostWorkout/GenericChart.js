import React from 'react';
import {calcHeight} from '../../utils/dimensions';
import {SUB_TITLE_FONT} from '../../styles/fonts';
import {BarChart} from 'react-native-charts-wrapper';

const GenericChart = ({data}) => {
  return (
    <BarChart
      style={{flex: 1, height: calcHeight(255), width: '100%'}}
      drawBorders={false}
      legend={{
        fontFamily: SUB_TITLE_FONT,
        horizontalAlignment: 'CENTER',
      }}
      chartDescription={{text: ''}}
      xAxis={{
        drawGridLines: false,
        drawAxisLine: false,
        drawLabels: false,
      }}
      yAxis={{
        left: {
          drawAxisLine: false,
        },
        right: {
          drawAxisLine: false,
          drawLabels: false,
        },
      }}
      scaleEnabled={false}
      dragEnabled={false}
      pinchZoom={false}
      doubleTapToZoomEnabled={false}
      noDataText={'Opps... no data available!'}
      data={data}
    />
  );
};

export default GenericChart;
