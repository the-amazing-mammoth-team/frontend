import React from 'react';
import {Text} from 'react-native';

export default ({count, styles}) => {
  const minutes =
    Math.floor(count / 60) > 9
      ? Math.floor(count / 60)
      : `0${Math.floor(count / 60)}`;
  const seconds = count % 60 > 9 ? count % 60 : `0${count % 60}`;
  return <Text style={styles}>{`${minutes}:${seconds}`}</Text>;
};
