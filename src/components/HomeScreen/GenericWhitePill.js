import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import styles from '../../scenes/Home/styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import PropTypes from 'prop-types';
import {moderateScale} from '../../utils/dimensions';

const GenericWhitePill = ({
  title,
  description,
  children,
  isTouch = false,
  onPress = () => {},
  extraStyles,
  extraStylesTitle,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={isTouch ? 0.2 : 1}
      style={[styles.pill, extraStyles]}>
      <View>
        <Text style={[styles.pillText, extraStylesTitle]}>{title}</Text>
        <Text style={[styles.pillTextGray, localStyles.descriptionView]}>
          {description}
        </Text>
      </View>
      <View style={localStyles.childView}>{children}</View>
    </TouchableOpacity>
  );
};

GenericWhitePill.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  onPress: PropTypes.func,
  isTouch: PropTypes.bool,
  children: PropTypes.any,
};

const localStyles = StyleSheet.create({
  childView: {
    marginTop: moderateScale(15),
  },
  descriptionView: {
    marginTop: moderateScale(5),
  },
});

export default GenericWhitePill;
