import React from 'react';
import {Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import PropTypes from 'prop-types';
import {LIGHT_ORANGE, ORANGE} from '../../styles/colors';
import styles from '../../scenes/Home/styles';

const NewsGradientCard = ({title, description}) => {
  return (
    <LinearGradient
      colors={[LIGHT_ORANGE, ORANGE]}
      useAngle={true}
      angle={255}
      style={styles.linearGradient}>
      <Text style={styles.buttonText}>{title}</Text>
      <Text style={styles.mainPillParagraph}>{description}</Text>
      {/*<TouchableOpacity*/}
      {/*  hitSlop={styles.hitSlopFifteen}*/}
      {/*  style={styles.understandTextView}>*/}
      {/*  <Text style={styles.understandText}>It is understood</Text>*/}
      {/*</TouchableOpacity>*/}
    </LinearGradient>
  );
};

NewsGradientCard.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
};

export default NewsGradientCard;
