import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {calcWidth, calcHeight} from '../../utils/dimensions';
import {WHITE} from '../../styles/colors';
import {SUB_TITLE_FONT_SEMI_BOLD} from '../../styles/fonts';
import FireIconBlack from '../../assets/icons/fireIconBlack.svg';
import FireIcon from '../../assets/icons/fireIcon.svg';
import RepsIconBlack from '../../assets/icons/repeatIconBlack.svg';
import ClockIcon from '../../assets/icons/clockIcon.svg';
import styles from '../../scenes/Home/styles';

const GenericSessionFooter = ({calories, timeDuration, reps, isWhiteText}) => {
  const textStyle = isWhiteText
    ? localStyles.descriptionWhite
    : [styles.programCardDescription, styles.genericComponentTextBlack];
  return (
    <>
      {calories && (timeDuration || reps) && (
        <View style={styles.programCardFooter}>
          {timeDuration ? (
            <>
              {isWhiteText ? (
                <ClockIcon width={calcWidth(15)} height={calcHeight(15)} />
              ) : (
                //here needed to add black clock Icon
                <></>
              )}
              <Text style={textStyle}>{` ${Math.round(
                timeDuration / 60,
              )} min`}</Text>
            </>
          ) : (
            <>
              {isWhiteText ? (
                //here needed to add white reps Icon
                <></>
              ) : (
                <RepsIconBlack width={calcWidth(15)} height={calcHeight(15)} />
              )}
              <Text style={textStyle}>{` ${reps} reps`}</Text>
            </>
          )}
          <Text style={textStyle}>{'   |   '}</Text>
          {isWhiteText ? (
            <FireIcon width={calcWidth(15)} height={calcHeight(15)} />
          ) : (
            <FireIconBlack width={calcWidth(15)} height={calcHeight(15)} />
          )}
          <Text style={textStyle}>{` ${calories} Kcal`}</Text>
        </View>
      )}
    </>
  );
};

export default GenericSessionFooter;

const localStyles = StyleSheet.create({
  descriptionWhite: {
    fontFamily: SUB_TITLE_FONT_SEMI_BOLD,
    color: WHITE,
  },
});
