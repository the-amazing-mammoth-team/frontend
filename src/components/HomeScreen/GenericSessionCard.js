import React from 'react';
import {Text, StyleSheet, View, ImageBackground} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import PropTypes from 'prop-types';
import {
  moderateScale,
  calcFontSize,
  calcWidth,
  deviceWidth,
} from '../../utils/dimensions';
import {
  LIGHT_GRAY,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
  YELLOW,
} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT_SEMI_BOLD} from '../../styles/fonts';
import CheckedIcon from '../../assets/icons/checkIcon.svg';
import GenericSessionFooter from '../../components/HomeScreen/GenericSessionFooter';

export const Body = ({
  title,
  description,
  bodyPartsFocused,
  calories,
  timeDuration,
  reps,
  footerShow,
}) => {
  const focused = bodyPartsFocused?.reduce((acc, item, index) => {
    if (item.value > 10 && item.name) {
      console.log(item.name);
      if (acc.length === 0) {
        acc.push(item.name);
      } else {
        acc.push(`, ${item.name}`);
      }
    }
    return acc;
  }, []);
  const subTitle = focused?.map(el => el) || description;
  return (
    <>
      <View>
        <Text style={styles.title}>{title || '-'}</Text>
        <Text style={styles.description}>{subTitle}</Text>
      </View>
      {footerShow && (
        <GenericSessionFooter
          calories={calories}
          timeDuration={timeDuration}
          reps={reps}
          isWhiteText={true}
        />
      )}
    </>
  );
};

const GenericSessionCard = ({
  title,
  isTouch,
  onPress,
  backgroundImage,
  description,
  bodyPartsFocused,
  calories,
  timeDuration,
  reps,
  footerShow,
  extraStyles,
  discarded,
}) => {
  const linerGradient = discarded
    ? [LIGHT_GRAY, '#616362']
    : isTouch
    ? [YELLOW, LIGHT_ORANGE]
    : [LIGHT_ORANGE, ORANGE];
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={isTouch ? 0.2 : 1}
      disabled={discarded}
      style={[styles.container, extraStyles]}>
      {backgroundImage ? (
        <ImageBackground source={backgroundImage} style={styles.background}>
          <Body
            title={title}
            description={description}
            bodyPartsFocused={bodyPartsFocused}
            calories={calories}
            timeDuration={timeDuration}
            reps={reps}
            footerShow={footerShow}
          />
        </ImageBackground>
      ) : (
        <LinearGradient
          colors={linerGradient}
          start={{x: 0, y: 0}}
          end={{x: 0, y: 1}}
          style={styles.background}>
          {!isTouch && !discarded && (
            <>
              <View style={styles.checkIconBackground} />
              <View style={styles.checkIcon}>
                <CheckedIcon
                  width={moderateScale(12)}
                  height={moderateScale(12)}
                />
              </View>
            </>
          )}
          {discarded && <Text style={styles.discarded}>discarded</Text>}
          <Body
            title={title}
            description={description}
            bodyPartsFocused={bodyPartsFocused}
            calories={calories}
            timeDuration={timeDuration}
            reps={reps}
            footerShow={footerShow}
          />
        </LinearGradient>
      )}
    </TouchableOpacity>
  );
};

GenericSessionCard.propTypes = {
  title: PropTypes.string,
  onPress: PropTypes.func,
  isTouch: PropTypes.bool,
  description: PropTypes.string,
  timeDuration: PropTypes.number,
  calories: PropTypes.number,
  reps: PropTypes.number,
  footerShow: PropTypes.bool,
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: moderateScale(15),
    marginBottom: moderateScale(15),
  },
  background: {
    paddingHorizontal: moderateScale(20),
    paddingVertical: moderateScale(15),
    borderRadius: moderateScale(10),
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    overflow: 'hidden',
    minHeight: calcWidth(deviceWidth * 0.32),
  },
  title: {
    fontSize: calcFontSize(25),
    color: WHITE,
    backgroundColor: 'transparent',
    fontFamily: MAIN_TITLE_FONT,
  },
  description: {
    fontFamily: SUB_TITLE_FONT_SEMI_BOLD,
    fontSize: calcFontSize(14),
    color: WHITE,
    backgroundColor: 'transparent',
    textAlignVertical: 'center',
  },
  checkIcon: {
    position: 'absolute',
    right: moderateScale(8),
    top: moderateScale(8),
  },
  checkIconBackground: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: moderateScale(50),
    backgroundColor: 'transparent',
    borderLeftWidth: moderateScale(50),
    borderTopWidth: moderateScale(40),
    borderLeftColor: 'transparent',
    borderTopColor: '#25D366',
  },
  discarded: {
    position: 'absolute',
    right: moderateScale(8),
    top: moderateScale(3),
    fontFamily: SUB_TITLE_FONT_SEMI_BOLD,
    fontSize: calcFontSize(12),
    color: WHITE,
    backgroundColor: 'transparent',
    textAlignVertical: 'center',
  },
  genericComponentBottomView: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'transparent',
    marginTop: moderateScale(10),
  },
});

export default GenericSessionCard;
