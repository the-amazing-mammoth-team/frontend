import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';
import styles from '../../scenes/Home/styles';
import {TouchableOpacity} from 'react-native-gesture-handler';
import PropTypes from 'prop-types';
import {moderateScale} from '../../utils/dimensions';

const GenericImagePill = ({
  title,
  description,
  children,
  backgroundImage,
  onPress = () => {},
  isTouch = false,
  extraStyles,
  extraStylesTitle,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={isTouch ? 0.2 : 1}
      style={[styles.pill, extraStyles, localStyles.padding0]}>
      <ImageBackground
        source={backgroundImage}
        style={localStyles.imageContainer}
        borderRadius={styles.pill.borderRadius}
        resizeMode="cover">
        <View>
          <Text style={[styles.pillText, extraStylesTitle]}>{title}</Text>
          <Text style={[styles.pillTextGray, localStyles.descriptionView]}>
            {description}
          </Text>
        </View>
        <View style={localStyles.childView}>{children}</View>
      </ImageBackground>
    </TouchableOpacity>
  );
};

GenericImagePill.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  backgroundImag: PropTypes.string,
  onPress: PropTypes.func,
  isTouch: PropTypes.bool,
  children: PropTypes.any,
  extraStyles: PropTypes.object,
  extraStylesTitle: PropTypes.object,
};

const localStyles = StyleSheet.create({
  padding0: {
    paddingHorizontal: 0,
    paddingVertical: 0,
  },
  imageContainer: {
    flex: 1,
    paddingHorizontal: styles.pill.paddingHorizontal,
    paddingVertical: styles.pill.paddingVertical,
  },
  childView: {
    marginTop: moderateScale(15),
  },
  descriptionView: {
    marginTop: moderateScale(5),
  },
});

export default GenericImagePill;
