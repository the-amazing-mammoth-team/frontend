import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {BLACK} from '../../styles/colors';
import {calcFontSize, moderateScale} from '../../utils/dimensions';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_BOLD_FONT,
  SUB_TITLE_FONT,
} from '../../styles/fonts';

const FrequentQuestionsSection = ({title, translations}) => {
  const mockQuestions = [
    {
      id: 'ds',
      name: translations.programBuyScreen.whatIncludesPro,
      answer: translations.programBuyScreen.whatIncludesProText
    },
    {
      id: 'rhdr',
      name: translations.programBuyScreen.howCanICancel,
      answer: translations.programBuyScreen.howCanICancelText
    },
    {
      id: 'sge',
      name: translations.programBuyScreen.howMuchSpace,
      answer: translations.programBuyScreen.howMuchSpaceText
    },
  ];

  return (
    <View>
      <Text style={styles.sectionTitle}>{title}</Text>
      {mockQuestions.map(question => (
        <View key={question.id}>
          <Text style={styles.questionTitle}>{question.name}</Text>
          <Text style={styles.questionText}>{question.answer}</Text>
        </View>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  sectionTitle: {
    color: BLACK,
    fontSize: calcFontSize(22),
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: moderateScale(15),
  },
  questionTitle: {
    color: BLACK,
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_BOLD_FONT,
    marginBottom: moderateScale(7),
  },
  questionText: {
    color: BLACK,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    marginBottom: moderateScale(15),
  },
});

export default FrequentQuestionsSection;
