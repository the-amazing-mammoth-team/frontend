import React, {useContext} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {levelCircles} from './CurrentProgramCard';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../../utils/dimensions';
import {BLACK, GRAY, LIGHT_ORANGE, ORANGE, WHITE} from '../../styles/colors';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_BOLD_FONT,
  SUB_TITLE_FONT,
} from '../../styles/fonts';
import CheckedIcon from '../../assets/icons/checkIcon.svg';
import {LocalizationContext} from '../../localization/translations';

const ProgramCardPaymentSuccess = ({
  title,
  infoRows,
  onStartPress,
  programAttributes,
}) => {
  const {translations} = useContext(LocalizationContext);

  return (
    <View style={styles.root}>
      <View style={styles.container}>
        <View style={styles.titleView}>
          <Text style={styles.titleText}>{title}</Text>
        </View>
        <View style={styles.attributesContainer}>
          <View>
            {programAttributes?.map(item => {
              return (
                <View style={styles.attributeView}>
                  <Text style={styles.attributeText}>{item?.key}</Text>
                  {levelCircles(item?.value)}
                </View>
              );
            })}
          </View>
          <View style={{flex: 1}} />
        </View>
        {infoRows.map((value, index) => (
          <View key={index} style={styles.rowField}>
            <View style={styles.circle}>
              <CheckedIcon width={calcWidth(12)} height={calcHeight(8)} />
            </View>
            <Text style={styles.infoRowText}>{value}</Text>
          </View>
        ))}
      </View>
      <TouchableOpacity onPress={onStartPress} style={styles.buttonPurple}>
        <Text style={styles.buttonText}>
          {translations.programsScreen.startMyChange}
        </Text>
      </TouchableOpacity>
      <View style={styles.absoluteGreenTip}>
        <Text style={styles.tipText}>
          {translations.programsScreen.yourProgram}
        </Text>
      </View>
    </View>
  );
};

ProgramCardPaymentSuccess.propTypes = {
  title: PropTypes.string,
  infoRows: PropTypes.array,
  onStartPress: PropTypes.func,
  programAttributes: PropTypes.array,
};

const styles = StyleSheet.create({
  root: {
    borderRadius: 10,
    backgroundColor: WHITE,
    elevation: 6,
    shadowColor: BLACK,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    paddingHorizontal: moderateScale(15),
  },
  titleText: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(30),
    color: BLACK,
  },
  titleView: {
    marginTop: moderateScale(30),
  },
  rowField: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: moderateScale(12),
  },
  container: {
    marginLeft: moderateScale(10),
    marginRight: moderateScale(20),
  },
  infoRowText: {
    color: BLACK,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
  circle: {
    width: moderateScale(17),
    height: moderateScale(17),
    borderRadius: moderateScale(17) / 2,
    backgroundColor: ORANGE,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: moderateScale(11),
  },
  doWorkoutTextView: {
    marginTop: moderateScale(25),
    marginBottom: moderateScale(15),
  },
  doWorkoutText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: GRAY,
    textAlign: 'center',
  },
  buttonPurple: {
    backgroundColor: ORANGE,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: moderateScale(13),
    marginVertical: moderateScale(30),
    borderRadius: moderateScale(25),
    width: '100%',
  },
  buttonText: {
    color: WHITE,
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  greenText: {
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
    marginVertical: moderateScale(15),
  },
  laterTextView: {
    marginVertical: moderateScale(8),
  },
  absoluteGreenTip: {
    backgroundColor: LIGHT_ORANGE,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: moderateScale(8),
    paddingHorizontal: moderateScale(30),
    position: 'absolute',
    right: moderateScale(15),
    top: -moderateScale(20),
  },
  tipText: {
    color: WHITE,
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(14),
  },
  attributesContainer: {
    flexDirection: 'row',
    marginTop: moderateScale(10),
    marginBottom: moderateScale(20),
  },
  attributeView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  attributeText: {
    color: BLACK,
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(14),
    marginRight: moderateScale(15),
  },
});

export default ProgramCardPaymentSuccess;
