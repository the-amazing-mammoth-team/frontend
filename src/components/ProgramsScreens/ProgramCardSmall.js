import React, {useContext} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import {levelCircles} from './CurrentProgramCard';
import ProIcon from '../../assets/icons/pro.svg';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_FONT,
  SUB_TITLE_FONT_SEMI_BOLD,
} from '../../styles/fonts';
import {BLACK, WHITE} from '../../styles/colors';

const ProgramCardSmall = ({
  item,
  navigation,
  isCirclesLevel = true,
  callback,
  prevScreen,
}) => {
  const {translations} = useContext(LocalizationContext);
  const program = item;

  return (
    <TouchableOpacity
      activeOpacity={1}
      style={styles.programCard}
      onPress={
        callback
          ? () => callback(program)
          : () => navigation.navigate('ProgramDetail', {program, prevScreen})
      }>
      {Boolean.parse(item?.pro) && (
        <ProIcon
          width={styles.proIcon.width}
          height={styles.proIcon.height}
          style={styles.proIcon}
        />
      )}
      <ImageBackground
        source={{
          uri: item.imageUrl,
        }}
        imageStyle={styles.programImage}
        style={styles.programSmall}
      />
      <View style={styles.headerTextViewSmall}>
        <Text style={styles.durationText}>
          {item.sessions.length} {translations.programsScreen.sessions}
        </Text>
        <Text style={[styles.titleText, styles.titleTextSmall]}>
          {item.name}
        </Text>
      </View>
      {isCirclesLevel && (
        <View style={styles.infoViewSmall}>
          <View style={styles.commonFieldView}>
            <Text style={styles.infoText}>{item.maxAttribute.key}</Text>
            {levelCircles(item.maxAttribute.value)}
          </View>
          <View style={styles.commonFieldView}>
            <Text style={styles.infoText}>{item.maxAttribute2.key}</Text>
            {levelCircles(item.maxAttribute2.value)}
          </View>
        </View>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  programCard: {
    height: calcHeight(180),
    width: '48%',
    borderRadius: moderateScale(10),
    marginBottom: moderateScale(25),
    backgroundColor: BLACK,
  },
  renderCurrentProgramStyles: {
    marginBottom: moderateScale(35),
    marginTop: moderateScale(30),
  },
  programSmall: {
    width: '100%',
    height: '100%',
    borderRadius: 10,
    flex: 1,
    resizeMode: 'cover',
  },
  programImage: {
    borderRadius: 10,
  },
  headerTextViewSmall: {
    position: 'absolute',
    paddingTop: moderateScale(12),
    paddingHorizontal: calcWidth(5),
  },
  commonFieldView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  durationText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: WHITE,
  },
  infoViewSmall: {
    position: 'absolute',
    bottom: calcHeight(10),
    left: calcWidth(5),
  },
  titleText: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(25),
    color: WHITE,
  },
  titleTextSmall: {
    fontSize: calcFontSize(20),
  },
  infoText: {
    fontFamily: SUB_TITLE_FONT_SEMI_BOLD,
    fontSize: calcFontSize(14),
    color: WHITE,
    marginRight: calcWidth(7),
  },
  proIcon: {
    position: 'absolute',
    top: moderateScale(5),
    right: moderateScale(7),
    width: moderateScale(25),
    height: moderateScale(25),
    zIndex: 999,
  },
});

export default ProgramCardSmall;
