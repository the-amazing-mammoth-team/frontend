import React, {useState, useCallback} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import PropTypes from 'prop-types';
import {BLACK, GRAY, LIGHT_ORANGE, ORANGE, WHITE} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_BOLD_FONT} from '../../styles/fonts';

const ProgramOfferCard = ({
  title,
  currency,
  local,
  price,
  onPress,
  pro,
  isTrial,
  trialDays,
  translations,
  coloredBorder,
  disabled,
  extraStyle,
  extraStyleTip,
  isAnnual,
}) => {
  const [pressed, setPressed] = useState(false); //isPressed
  const handlePressed = () => {
    console.log('pressed');
    setPressed(true);
    const result = () => true;
    setPressed(false);
  };
  const year = translations.year;
  const quarter = translations.quarter;
  const week = translations.week;
  const free = translations.free;
  const limitedFunctionality = translations.limitedFeatures;
  const buttonStyle =
    coloredBorder === true
      ? [styles.component, styles.coloredBorder]
      : styles.component;
  return (
    <>
      <TouchableOpacity
        activeOpacity={0.2}
        onPress={onPress}
        style={[buttonStyle, extraStyle]}
        disabled={disabled}>
        <View style={styles.row}>
          <View style={styles.titleView}>
            <Text style={styles.title}>{title}</Text>
          </View>
          <View>
            <Text style={[styles.title]}>
              {pro ? `${price} € / ${isAnnual ? year : quarter}` : free}
            </Text>
            <Text style={styles.extraInfoPrice}>
              {pro ? Number(price / (isAnnual ? 52 : 12.8)).toFixed(2) : ''}
              {pro ? ` € / ${week} ` : limitedFunctionality}
            </Text>
          </View>
        </View>
        {isTrial && (
          <View style={[styles.customTip, extraStyleTip]}>
            <Text style={styles.tipText}>{trialDays}</Text>
          </View>
        )}
      </TouchableOpacity>
    </>
  );
};

ProgramOfferCard.propTypes = {
  title: PropTypes.string,
  onPress: PropTypes.func,
  price: PropTypes.string || PropTypes.number,
  pro: PropTypes.bool,
  storeReference: PropTypes.string,
  isTrial: PropTypes.bool,
  isPressed: PropTypes.bool,
};

const styles = StyleSheet.create({
  component: {
    borderRadius: 10,
    backgroundColor: WHITE,
    elevation: 4,
    paddingVertical: calcHeight(15),
    paddingHorizontal: calcWidth(18),
    zIndex: -20,
    marginBottom: calcHeight(20),
    shadowColor: '#000',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 5,
  },
  coloredBorder: {
    borderWidth: 1,
    borderColor: ORANGE,
  },
  title: {
    color: BLACK,
    fontSize: calcFontSize(17),
    fontFamily: MAIN_TITLE_FONT,
  },
  titleMain: {
    fontSize: calcFontSize(22),
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  customTip: {
    backgroundColor: LIGHT_ORANGE,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 99999,
    left: calcWidth(20),
    position: 'absolute',
    top: -moderateScale(15),
  },
  tipText: {
    color: WHITE,
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(14),
    paddingHorizontal: moderateScale(25),
    paddingVertical: moderateScale(4),
  },
  weeksText: {
    color: BLACK,
    fontSize: calcFontSize(18),
  },
  extraInfoPrice: {
    color: GRAY,
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(12),
    fontWeight: '400',
  },
  titleView: {
    maxWidth: calcWidth(140),
  },
});

export default ProgramOfferCard;
