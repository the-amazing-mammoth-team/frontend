import React from 'react';
import {TouchableOpacity, Image, Text, StyleSheet} from 'react-native';
import {calcFontSize, calcWidth, moderateScale} from '../../utils/dimensions';
import {GRAY_BORDER, BLACK, ORANGE} from '../../styles/colors';
import {SUB_TITLE_FONT} from '../../styles/fonts';

const Equipment = ({item, onPress, disabled}) => {
  disabled = disabled || false;
  return (
    <TouchableOpacity
      style={[
        styles.equipmentView,
        {borderColor: item.isActive ? ORANGE : GRAY_BORDER},
      ]}
      disabled={disabled}
      onPress={() => onPress(item)}>
      <Image
        source={{uri: item?.imageUrl}}
        style={styles.equipmentImage}
        resizeMode="contain"
      />
      <Text style={styles.equipmentName} numberOfLines={2} ellipsizeMode="tail">
        {item.name}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  equipmentView: {
    borderRadius: moderateScale(25),
    borderWidth: moderateScale(1),
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: calcWidth(150),
    width: calcWidth(140),
    marginRight: moderateScale(15),
    paddingHorizontal: moderateScale(10),
    paddingVertical: moderateScale(13),
    marginBottom: moderateScale(12),
  },
  equipmentImage: {
    width: calcWidth(100),
    height: calcWidth(100),
  },
  equipmentName: {
    color: BLACK,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
    marginTop: moderateScale(-10),
  },
});

export default Equipment;
