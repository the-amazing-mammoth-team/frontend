import React, {useRef, useState} from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import SwiperFlatList from 'react-native-swiper-flatlist';
import Animated from 'react-native-reanimated';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY,
  MEDIUM_GRAY,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import {calcFontSize, deviceWidth, moderateScale} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import DietWomanResult from '../../assets/images/dietWomanResult.png';

const PersonalExperienceSection = ({translations}) => {
  const [index, setIndex] = useState(0);
  const swiperRef = useRef();
  const animation = useRef(new Animated.Value(0)).current;
  const manImage = 'https://stage-dev-new.s3-eu-west-1.amazonaws.com/assets/hombre-testimonial.png';
  const womanImage = 'https://stage-dev-new.s3-eu-west-1.amazonaws.com/assets/mujer-testimonial.png';

  const mockExperience = [
    {
      id: '1',
      user: {
        name: 'Dory Arrebato',
        image: womanImage,
        experience: translations.programBuyScreen.doryExperience,
      },
    },
    {
      id: '2',
      user: {
        name: 'Mario Redondo',
        image: manImage,
        experience: translations.programBuyScreen.marioExperience
      },
    },
  ];

  return (
    <View>
      <View style={[styles.titleView, styles.genericPadding]}>
        <Text style={styles.titleText}>{translations.programBuyScreen.personalExperienceTitle}</Text>
      </View>
      <View style={styles.containerCopy}>
        <SwiperFlatList
          paginationActiveColor={ORANGE}
          paginationDefaultColor={GRAY}
          paginationStyleItem={styles.paginationDot}
          paginationStyle={styles.paginationStyle}
          ref={swiperRef}
          onViewableItemsChanged={() => {
            setIndex(swiperRef.current?.getCurrentIndex());
          }}
          index={index}
          showPagination>
          {mockExperience.map(item => (
            <View key={item.id} style={[styles.child]}>
              <View>
                <View style={styles.mockImageView}>
                  <ImageBackground
                    source={{uri: item.user.image}}
                    imageStyle={styles.imageView}
                    style={styles.imageView}
                  />
                </View>
                <View style={styles.commentView}>
                  <Text style={styles.commentText}>{item.user.experience}</Text>
                </View>
                <Text style={styles.userNameAgeText}>{item.user.name}</Text>
              </View>
            </View>
          ))}
        </SwiperFlatList>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  titleView: {
    marginBottom: moderateScale(15),
  },
  titleText: {
    color: BLACK,
    fontSize: calcFontSize(22),
    fontFamily: MAIN_TITLE_FONT,
  },
  userNameAgeText: {
    color: BLACK,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
    marginBottom: moderateScale(10),
  },
  mockRect: {
    backgroundColor: MEDIUM_GRAY,
    width: '45%',
    height: moderateScale(219),
    marginRight: moderateScale(10),
  },
  imageView: {
    width: '100%',
    height: moderateScale(219),
  },
  commentView: {
    marginTop: moderateScale(15),
    marginBottom: moderateScale(10),
    width: deviceWidth * 0.9,
  },
  commentText: {
    color: BUTTON_TITLE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
  generalPadding: {
    paddingHorizontal: moderateScale(20),
  },
  mockImageView: {
    width: deviceWidth * 0.9,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerCopy: {
    flex: 1,
  },
  child: {
    width: deviceWidth,
    justifyContent: 'center',
    alignItems: 'center',
  },
  paginationDot: {
    width: moderateScale(9),
    height: moderateScale(9),
    borderRadius: moderateScale(9) / 2,
  },
  paginationStyle: {
    bottom: moderateScale(-25),
  },
  genericPadding: {
    paddingHorizontal: moderateScale(20),
  },
});

export default PersonalExperienceSection;
