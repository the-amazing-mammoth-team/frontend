import React, {useContext} from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import PropTypes from 'prop-types';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceHeight,
  moderateScale,
} from '../../utils/dimensions';
import ProIcon from '../../assets/icons/pro.svg';
import {BLACK, GRAY, LIGHT_GRAY, ORANGE, WHITE} from '../../styles/colors';
import {LocalizationContext} from '../../localization/translations';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_BOLD_FONT,
  SUB_TITLE_FONT,
} from '../../styles/fonts';
import {TouchableOpacity} from 'react-native-gesture-handler';

export const levelCircles = (count, maxCount = 5) => {
  return (
    <View style={styles.levelCirclesContainer}>
      {new Array(maxCount).fill(0).map((el, index) => {
        return (
          <View
            style={
              count > index
                ? [styles.circle, styles.filledCircle]
                : styles.circle
            }
          />
        );
      })}
    </View>
  );
};

const CurrentProgramCard = ({
  currentProgram,
  isCirclesLevel = true,
  navigation,
  callback,
  prevScreen,
}) => {
  const {translations} = useContext(LocalizationContext);

  return (
    <View style={styles.currentProgramCard}>
      <TouchableOpacity
        onPress={() =>
          callback
            ? callback(currentProgram)
            : navigation.navigate('ProgramDetail', {
                program: currentProgram,
                prevScreen,
              })
        }>
        {Boolean.parse(currentProgram?.pro) && (
          <ProIcon
            width={styles.proIcon.width}
            height={styles.proIcon.height}
            style={styles.proIcon}
          />
        )}
        <ImageBackground
          source={{uri: currentProgram?.imageUrl}}
          imageStyle={styles.programImage}
          style={styles.program}
        />
        <View style={styles.headerTextView}>
          <Text style={styles.durationText}>
            {currentProgram.sessions.length}{' '}
            {translations.programsScreen.sessions}
          </Text>
          <Text style={styles.titleText}>{currentProgram.name}</Text>
        </View>
        {isCirclesLevel && (
          <View style={styles.infoView}>
            <View style={styles.commonFieldView}>
              <Text style={styles.infoText}>
                {currentProgram.maxAttribute.key}
              </Text>
              {levelCircles(currentProgram.maxAttribute.value)}
            </View>
            <View style={styles.commonFieldView}>
              <Text style={styles.infoText}>
                {currentProgram.maxAttribute2.key}
              </Text>
              {levelCircles(currentProgram.maxAttribute2.value)}
            </View>
          </View>
        )}
      </TouchableOpacity>
      <View style={styles.popularProgramTip}>
        <Text style={styles.tipText}>
          {translations.programDetailScreen.recomendForYou}
        </Text>
      </View>
    </View>
  );
};

CurrentProgramCard.propTypes = {
  currentProgram: PropTypes.object,
  isCirclesLevel: PropTypes.bool,
  isDays: PropTypes.bool,
};

const styles = StyleSheet.create({
  currentProgramCard: {
    borderRadius: moderateScale(10),
    width: '100%',
    height: deviceHeight * 0.25,
    backgroundColor: BLACK,
  },
  programCard: {
    height: calcHeight(180),
    width: '48%',
    borderRadius: moderateScale(10),
    marginBottom: moderateScale(25),
  },
  program: {
    width: '100%',
    height: deviceHeight * 0.25,
    borderRadius: 10,
    resizeMode: 'cover',
    alignSelf: 'flex-start',
  },
  programImage: {
    borderRadius: 10,
  },
  circle: {
    width: moderateScale(9),
    height: moderateScale(9),
    borderRadius: moderateScale(9) / 2,
    borderColor: GRAY,
    borderWidth: calcWidth(1.5),
    marginRight: calcWidth(4),
    backgroundColor: 'transparent',
  },
  filledCircle: {
    backgroundColor: '#FF8F00',
    borderWidth: 0,
  },
  headerTextView: {
    position: 'absolute',
    paddingTop: calcHeight(10),
    paddingLeft: calcWidth(20),
  },
  commonFieldView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  durationText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
    color: WHITE,
  },
  titleText: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(25),
    color: WHITE,
  },
  infoText: {
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(14),
    color: WHITE,
    marginRight: calcWidth(7),
  },
  infoView: {
    position: 'absolute',
    bottom: calcHeight(20),
    left: calcWidth(20),
  },
  popularProgramTip: {
    backgroundColor: ORANGE,
    paddingVertical: moderateScale(5),
    paddingHorizontal: moderateScale(20),
    alignItems: 'center',
    justifyContent: 'center',
    bottom: -moderateScale(13),
    right: calcWidth(19),
    position: 'absolute',
  },
  tipText: {
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(14),
    color: WHITE,
  },
  levelCirclesContainer: {
    flexDirection: 'row',
  },
  proIcon: {
    position: 'absolute',
    top: moderateScale(5),
    right: moderateScale(10),
    width: moderateScale(30),
    height: moderateScale(30),
    zIndex: 999,
  },
});

export default CurrentProgramCard;
