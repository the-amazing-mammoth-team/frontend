import {ImageBackground, Text, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import {Path, Svg} from 'react-native-svg';
import styles from '../../scenes/Exercise/styles';
import {calcHeight, calcWidth, moderateScale} from '../../utils/dimensions';
import RepsIcon from '../../assets/icons/repeatIcon.svg';
import RepsIconBlack from '../../assets/icons/repeatIconBlack.svg';
import ClockIcon from '../../assets/icons/clockIcon.svg';
import LockIcon from '../../assets/icons/lockIon.svg';
import {BLACK, WHITE} from '../../styles/colors';
import CheckedIcon from '../../assets/icons/checkIcon.svg';

export const GenericCardComponent = ({
  challengeId,
  activeRestId,
  myProgramId,
  title,
  description,
  repsCount,
  timeCount,
  kcalCount,
  isBlocked = false,
  keyNumber,
  backgroundImage,
}) => {
  const [isLiked, setIsLiked] = useState(false);
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={[
        keyNumber === 0 && styles.genericLeftMargin,
        styles.programItemWhite,
      ]}
      disabled={isBlocked}
      key={myProgramId || challengeId || activeRestId}>
      {backgroundImage && (
        <ImageBackground
          source={
            typeof backgroundImage === 'number'
              ? backgroundImage
              : {uri: backgroundImage}
          }
          style={styles.absoluteBackgroundImageView}
          imageStyle={[styles.programImageSmall]}
        />
      )}

      <View
        style={[
          styles.programCardContent,
          isBlocked && styles.genericComponentDisabledCard,
        ]}>
        <View style={styles.genericComponentLikeView}>
          <TouchableOpacity
            onPress={() => setIsLiked(oldData => !oldData)}
            hitSlop={styles.hitSlopTen}
            disabled={isBlocked}>
            {backgroundImage ? (
              <View style={styles.checkContainer}>
                <CheckedIcon
                  width={moderateScale(12)}
                  height={moderateScale(12)}
                />
              </View>
            ) : isBlocked ? (
              <LockIcon width={calcWidth(17)} height={calcHeight(15)} />
            ) : (
              <View style={{width: calcWidth(17), height: calcHeight(15)}} />
            )}
          </TouchableOpacity>
        </View>
        <Text
          style={
            !myProgramId
              ? styles.programCardTitleBlack
              : styles.programCardTitle
          }>
          {title}
        </Text>
        <Text
          style={
            !myProgramId
              ? styles.programCardDescriptionGray
              : styles.programCardDescription
          }>
          {description}
        </Text>
        <View style={styles.programCardFooter}>
          {repsCount ? (
            <View style={styles.genericComponentRepsView}>
              {!myProgramId ? (
                <RepsIconBlack width={calcWidth(17)} height={calcHeight(15)} />
              ) : (
                <RepsIcon width={calcWidth(17)} height={calcHeight(15)} />
              )}
              <Text
                style={[
                  styles.programCardDescription,
                  styles.genericComponentFooterPadding,
                  !myProgramId && styles.genericComponentTextBlack,
                ]}>
                {repsCount} reps
              </Text>
            </View>
          ) : (
            <View style={styles.genericComponentRepsView}>
              <ClockIcon width={calcWidth(17)} height={calcHeight(15)} />
              <Text
                style={[
                  styles.programCardDescription,
                  styles.genericComponentFooterPadding,
                ]}>
                {timeCount} min
              </Text>
            </View>
          )}
          <Text
            style={[
              styles.programCardDescription,
              styles.genericComponentSeparateLine,
              !myProgramId && styles.genericComponentTextBlack,
            ]}>
            |
          </Text>
          <View style={styles.genericComponentRepsView}>
            <Svg
              width={calcWidth(17)}
              height={calcHeight(15)}
              viewBox="0 0 18 16"
              fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <Path
                d="M12.227 9.52464L12.2156 9.36808C12.0323 6.98212 10.9213 5.4864 9.94124 4.16711C9.03372 2.94547 8.24991 1.89051 8.24991 0.333919C8.24991 0.208924 8.17991 0.0946791 8.06891 0.0373689C7.95757 -0.0202537 7.8238 -0.0108166 7.72258 0.0627742C6.25064 1.11611 5.02251 2.89144 4.59349 4.58537C4.29566 5.7647 4.25626 7.09049 4.25073 7.96611C2.89141 7.67575 2.58348 5.6423 2.58023 5.62017C2.56492 5.51471 2.50048 5.4229 2.40674 5.37278C2.31202 5.32331 2.20068 5.31972 2.10466 5.36725C2.03338 5.40174 0.355041 6.25458 0.257389 9.66004C0.250545 9.77332 0.250233 9.88659 0.250233 10.0002C0.250233 13.3083 2.94184 15.9999 6.24999 15.9999C9.55813 15.9999 12.2497 13.3083 12.2497 10.0002C12.2497 9.83419 12.2384 9.67926 12.227 9.52464ZM6.24999 15.3333C5.14716 15.3333 4.25007 14.3776 4.25007 13.2029C4.25007 13.1628 4.24976 13.1225 4.25266 13.073C4.26601 12.5776 4.3601 12.2394 4.46328 12.0144C4.65665 12.4298 5.00263 12.8116 5.56383 12.8116C5.74807 12.8116 5.89716 12.6625 5.89716 12.4783C5.89716 12.0037 5.90694 11.4562 6.02509 10.962C6.13024 10.5236 6.38151 10.0578 6.69988 9.6841C6.84146 10.1691 7.11752 10.5617 7.38704 10.9448C7.77277 11.493 8.1715 12.0597 8.2415 13.0261C8.24572 13.0834 8.24997 13.141 8.24997 13.2028C8.24991 14.3776 7.35282 15.3333 6.24999 15.3333ZM8.35653 14.8978C8.70251 14.426 8.91657 13.8427 8.91657 13.2029C8.91657 13.1244 8.91169 13.0515 8.90191 12.9151C8.82282 11.827 8.34984 11.1544 7.93223 10.5611C7.57646 10.0559 7.26951 9.61938 7.26951 9.00025C7.26951 8.87363 7.19789 8.75808 7.08461 8.70177C6.97199 8.64511 6.83593 8.65783 6.73534 8.73433C6.09606 9.21737 5.56286 10.0305 5.37668 10.8068C5.28162 11.2049 5.24844 11.6356 5.23672 11.9897C5.0056 11.741 4.93398 11.2801 4.93335 11.2739C4.91839 11.1669 4.85264 11.0734 4.75661 11.0236C4.66124 10.9742 4.547 10.9729 4.45097 11.0223C4.36697 11.0653 3.62906 11.4885 3.58675 13.045C3.58382 13.0978 3.5835 13.1505 3.5835 13.2029C3.5835 13.8428 3.79756 14.426 4.14354 14.8978C2.24787 14.0793 0.916955 12.1928 0.916955 10.0002C0.916955 9.90031 0.916643 9.80072 0.923455 9.68969C0.980734 7.69171 1.62005 6.70088 2.03735 6.26374C2.32834 7.30535 3.09068 8.66695 4.58346 8.66695C4.7677 8.66695 4.91679 8.51787 4.91679 8.33362C4.91679 7.21811 4.94185 5.9291 5.24003 4.7488C5.57855 3.41161 6.50592 1.96767 7.62534 0.990174C7.81121 2.41787 8.58883 3.4647 9.4062 4.56456C10.3779 5.87244 11.3824 7.22492 11.5506 9.41496L11.5621 9.57445C11.5725 9.71313 11.5832 9.85147 11.5832 10.0002C11.5831 12.1928 10.2522 14.0792 8.35653 14.8978Z"
                fill={!myProgramId ? BLACK : WHITE}
              />
            </Svg>
            <Text
              style={[
                styles.programCardDescription,
                !myProgramId && styles.genericComponentTextBlack,
              ]}>
              {kcalCount} Kcal
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};
