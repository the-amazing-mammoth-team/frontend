import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useContext} from 'react';
import PropTypes from 'prop-types';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../../utils/dimensions';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_BOLD_FONT,
  SUB_TITLE_FONT,
} from '../../styles/fonts';
import {
  BLACK,
  LIGHT_GRAY,
  ORANGE,
  LIGHT_ORANGE,
  WHITE,
} from '../../styles/colors';
import {LocalizationContext} from '../../localization/translations';
import {levelCircles} from '../ProgramsScreens/CurrentProgramCard';

const AvailableProgramCard = ({
  item,
  keyNumber,
  onPress = () => {},
  isCirclesLevel = true,
  isDaysDuration = false,
  isDuration = true,
}) => {
  const {
    intensityLevel,
    musculatureLevel,
    title,
    duration,
    background,
    id,
    isTip,
    tipText,
  } = item;
  const BackgroundImage =
    typeof background === 'number' ? background : {uri: background};

  const {translations} = useContext(LocalizationContext);

  return (
    <View>
      <TouchableOpacity
        activeOpacity={1}
        style={[styles.card, keyNumber === 0 && styles.genericLeftMargin]}
        key={id}
        onPress={onPress}>
        <ImageBackground
          source={BackgroundImage}
          style={styles.image}
          imageStyle={styles.image}
        />
        <View style={styles.headerTextView}>
          {isDuration && (
            <Text style={styles.durationText}>
              {duration}{' '}
              {isDaysDuration
                ? translations.nutritionScreen.days
                : translations.programsScreen.sessions}
            </Text>
          )}
          <Text style={styles.titleText}>{title}</Text>
        </View>
        {isCirclesLevel && (
          <View style={styles.infoView}>
            <View style={styles.commonFieldView}>
              <Text style={styles.infoText}>
                {translations.exerciseScreen.intensity}
              </Text>
              {levelCircles(intensityLevel)}
            </View>
            <View style={styles.commonFieldView}>
              <Text style={styles.infoText}>
                {translations.exerciseScreen.musculature}
              </Text>
              {levelCircles(musculatureLevel)}
            </View>
          </View>
        )}
      </TouchableOpacity>
      {isTip && tipText && (
        <View style={styles.popularProgramTip}>
          <Text style={styles.tipText}>{tipText}</Text>
        </View>
      )}
    </View>
  );
};

AvailableProgramCard.propTypes = {
  onPress: PropTypes.func,
  item: PropTypes.object,
  isCirclesLevel: PropTypes.bool,
  isDuration: PropTypes.bool,
  isDaysDuration: PropTypes.bool,
};

const styles = StyleSheet.create({
  durationText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: WHITE,
  },
  titleText: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(30),
    color: WHITE,
  },
  infoText: {
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(14),
    color: WHITE,
    marginRight: calcWidth(7),
  },
  circle: {
    width: moderateScale(8),
    height: moderateScale(8),
    borderRadius: moderateScale(8) / 2,
    marginRight: calcWidth(5),
    borderWidth: 1,
    borderColor: LIGHT_GRAY,
  },
  filledCircle: {
    backgroundColor: LIGHT_ORANGE,
    borderWidth: 0,
  },
  infoView: {
    position: 'absolute',
    bottom: calcHeight(20),
    left: calcWidth(10),
  },
  card: {
    width: calcWidth(250),
    height: calcHeight(320),
    marginRight: calcWidth(30),
    zIndex: -8,
    backgroundColor: BLACK,
    borderRadius: moderateScale(10),
  },
  image: {
    width: calcWidth(250),
    height: calcHeight(320),
    borderRadius: moderateScale(10),
  },
  popularProgramTip: {
    backgroundColor: ORANGE,
    paddingVertical: moderateScale(5),
    paddingHorizontal: moderateScale(13),
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end',
    marginTop: -calcHeight(15),
    marginRight: calcHeight(40),
  },
  tipText: {
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(14),
    color: WHITE,
  },
  headerTextView: {
    position: 'absolute',
    paddingTop: calcHeight(10),
    paddingLeft: calcWidth(10),
    paddingRight: calcWidth(10),
  },
  commonFieldView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  genericLeftMargin: {
    marginLeft: calcWidth(20),
  },
});

export default AvailableProgramCard;
