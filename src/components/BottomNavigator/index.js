import React from 'react';
import {calcHeight, calcWidth} from '../../utils/dimensions';
import {BLACK, ORANGE} from '../../styles/colors';
import HomeIcon from '../../assets/icons/home.svg';
import NutritionIcon from '../../assets/icons/nutrition.svg';
import FireIconBottom from '../../assets/icons/fireBottomNavigator.svg';
import ManRun from '../../assets/icons/manRun.svg';
import FeedIcon from '../../assets/icons/messages.svg';
import ChartIcon from '../../assets/icons/chart.svg';

export const renderIcon = (routeName, isActive) => {
  console.log(routeName, isActive);
  switch (routeName) {
    case 'Nutrition':
      return (
        <NutritionIcon
          fill={isActive ? ORANGE : BLACK}
          width={calcWidth(22)}
          height={calcHeight(22)}
        />
      );
    case 'Exercise':
      return (
        <ManRun
          fill={isActive ? ORANGE : BLACK}
          height={calcHeight(27)}
          width={calcWidth(27)}
        />
      );
    case 'Home':
      return (
        <HomeIcon
          width={calcWidth(27)}
          height={calcHeight(27)}
          fill={isActive ? ORANGE : BLACK}
        />
      );
    case 'Feed':
      return (
        <FeedIcon
          width={calcWidth(22)}
          height={calcHeight(22)}
          fill={isActive ? ORANGE : BLACK}
        />
      );
    case 'Progress':
      return (
        <ChartIcon
          width={calcWidth(27)}
          height={calcHeight(27)}
          fill={isActive ? ORANGE : BLACK}
        />
      );
  }
};
