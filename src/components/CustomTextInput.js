import {TextInput, View, StyleSheet} from 'react-native';
import React, {useContext} from 'react';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../utils/dimensions';
import SearchIcon from '../assets/icons/searchIcon.svg';
import {SUB_TITLE_FONT} from '../styles/fonts';
import {LocalizationContext} from '../localization/translations';
import {GRAY, LIGHT_GRAY} from '../styles/colors';

const CustomTextInput = ({value, onChange}) => {
  const {translations} = useContext(LocalizationContext);
  return (
    <View style={localStyles.mainView}>
      <SearchIcon width={calcWidth(19)} height={calcHeight(19)} />
      <TextInput
        style={localStyles.textInputStyle}
        placeholder={translations.feedScreen.search}
        value={value}
        onChangeText={onChange}
      />
    </View>
  );
};

const localStyles = StyleSheet.create({
  mainView: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: LIGHT_GRAY,
    borderRadius: moderateScale(30),
    paddingHorizontal: calcWidth(11),
    paddingVertical: calcHeight(5),
  },
  textInputStyle: {
    flex: 1,
    marginLeft: calcWidth(10),
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
    color: GRAY,
    paddingVertical: 0,
  },
});

export default CustomTextInput;
