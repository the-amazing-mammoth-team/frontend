export const MAIN_TITLE_FONT = 'Nunito-Bold';
export const MAIN_TITLE_FONT_REGULAR = 'Nunito-Regular';
export const SUB_TITLE_BOLD_FONT = 'OpenSans-Bold';
export const SUB_TITLE_FONT = 'OpenSans-Regular';
export const SUB_TITLE_FONT_CURSIVE = 'OpenSans-Italic';
export const SUB_TITLE_FONT_SEMI_BOLD = 'OpenSans-SemiBold';
