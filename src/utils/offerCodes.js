export const onboardingSalesCodes = ['onboarding_1y', 'onboarding_3m'];
export const onboardingUpSalesCodes = ['onboarding_upsales_1y'];
export const profileFreeCodes = ['profile_no_pro_1y', 'profile_no_pro_3m'];
export const profileProCodes = ['profile_pro_1y'];
export const postWorkoutCodes = ['workout_graphs_1y', 'workout_graphs_3m'];
