import {IAPErrorCode} from 'react-native-iap';

export const paymentError = (errorCode, translations) => {
  switch (errorCode) {
    // case IAPErrorCode.E_IAP_NOT_AVAILABLE:                    //debug error
    //   return '';
    // case IAPErrorCode.E_UNKNOWN:
    //   return '';
    // case IAPErrorCode.E_USER_CANCELLED:
    //   return '';
    case IAPErrorCode.E_USER_ERROR:
      return "The operation couldn't be completed";
    case IAPErrorCode.E_ITEM_UNAVAILABLE:
      return 'Product is not available for purchase';
    // case IAPErrorCode.E_REMOTE_ERROR:
    //   return '';
    case IAPErrorCode.E_NETWORK_ERROR:
      return 'The service is disconnected';
    case IAPErrorCode.E_SERVICE_ERROR:
      return 'Billing is unavailable';
    case IAPErrorCode.E_RECEIPT_FAILED:
      return 'Invalid receipt';
    // case IAPErrorCode.E_RECEIPT_FINISHED_FAILED:
    //   return '';
    // case IAPErrorCode.E_NOT_PREPARED:                          //debug error
    //   return '';
    // case IAPErrorCode.E_NOT_ENDED:
    //   return '';
    case IAPErrorCode.E_ALREADY_OWNED:
      return 'Failure to purchase since item is already owned';
    // case IAPErrorCode.E_DEVELOPER_ERROR:                       //debug error
    //   return '';
    // case IAPErrorCode.E_BILLING_RESPONSE_JSON_PARSE_ERROR:
    //   return '';
    case IAPErrorCode.E_DEFERRED_PAYMENT:
      return 'The payment was deferred';
    default:
      return 'Error purchase';
  }
};

export const subscribeError = (errorMessage, translations) => {
  switch (true) {
    case errorMessage.includes(
      'Active no more than 1 subscription active per user',
    ):
      return translations.paymentErrors.hasActiveSubscription;
    default:
      return errorMessage;
  }
};
