import {LocalizationContext} from '../localization/translations';

const bodyParts = [
  {en: 'Back', es: 'Espalda'},
  {en: 'Shoulders', es: 'Hombros'},
  {en: 'Whole body', es: 'Todo el cuerpo'},
  {en: 'Core', es: 'Core'},
  {en: 'Legs', es: 'Piernas'},
  {en: 'Glutes', es: 'Glúteos'},
  {en: 'Arms', es: 'Brazos'},
  {en: 'Chest', es: 'Pectorales'},
];

const calcOverallSessionStats = (executions, weight) => {
  let totalReps = 0;
  let totalTime = 0;
  let totalKcal = 0;
  let mediumPace = 0;
  let totalTimeRest = 0;

  executions?.forEach(({sessionSetExecutions}) => {
    sessionSetExecutions?.forEach(({exerciseExecutions}) => {
      exerciseExecutions?.forEach(ex => {
        if (!ex?.bodyPartsFocused) {
          totalTimeRest += ex.executionTime;
        }
        totalReps += ex.repsExecuted;
        totalTime += ex.executionTime;
        const calc = ex.metMultiplier * weight * (ex.executionTime / 3600);
        totalKcal += isFinite(calc) ? calc : 0;
      });
    });
  });

  mediumPace = totalReps / ((totalTime - totalTimeRest) / 60);

  return {
    totalReps,
    totalTime,
    totalKcal: Math.round(totalKcal),
    repsPerMin: Math.round(mediumPace),
  };
};

const calcBodyPartFocused = executions => {
  const arr = [];
  executions?.forEach(({sessionSetExecutions}) => {
    sessionSetExecutions?.forEach(({exerciseExecutions}) => {
      exerciseExecutions?.forEach(ex => {
        if (ex?.bodyPartsFocused) {
          ex.bodyPartsFocused.forEach(el =>
            arr.push({name: el, executionTime: ex.executionTime}),
          );
        }
      });
    });
  });
  let totalTime = 0;
  const groupedBodyParts = bodyParts.map(el => {
    const bodyPart = arr.filter(item => item.name === el.es);
    const totalTimeByBodyPart = bodyPart?.reduce((acc, item) => {
      acc += item.executionTime;
      return acc;
    }, 0);
    totalTime += totalTimeByBodyPart;
    return {
      name: el[LocalizationContext._currentValue.appLanguage],
      totalTime: totalTimeByBodyPart,
    };
  });
  const res = groupedBodyParts.map(el => {
    const calc = (el.totalTime * 100) / totalTime;
    return {name: el.name, value: isFinite(calc) ? calc : 0};
  });
  return res;
};

const calcSummaryPerExercises = executions => {
  const appLanguage = LocalizationContext._currentValue.appLanguage;
  const repsPerExercise = {};
  const repsPerMinPerExercise = {};
  const minsPerExercise = {};
  executions?.forEach(({sessionSetExecutions}) => {
    sessionSetExecutions?.forEach(({exerciseExecutions}) => {
      exerciseExecutions?.forEach(ex => {
        if (ex.bodyPartsFocused) {
          //not rest
          if ([ex.exerciseId] in repsPerExercise) {
            repsPerExercise[ex.exerciseId].reps += ex.repsExecuted;
          } else {
            repsPerExercise[ex.exerciseId] = {
              reps: ex.repsExecuted,
              [appLanguage === 'es' ? 'name_es' : 'name_en']: ex.name,
            };
          }
          if ([ex.exerciseId] in minsPerExercise) {
            minsPerExercise[ex.exerciseId].execution_time += ex.executionTime;
          } else {
            minsPerExercise[ex.exerciseId] = {
              execution_time: ex.executionTime,
              [appLanguage === 'es' ? 'name_es' : 'name_en']: ex.name,
            };
          }
        }
      });
    });
  });
  Object.entries(repsPerExercise).forEach(([key, value]) => {
    const pace = (value.reps / minsPerExercise[key].execution_time) * 60;
    repsPerMinPerExercise[key] = {
      value: isFinite(pace) ? pace : 0,
      [appLanguage === 'es' ? 'name_es' : 'name_en']: value[
        appLanguage === 'es' ? 'name_es' : 'name_en'
      ],
    };
  });
  return {repsPerExercise, repsPerMinPerExercise, minsPerExercise};
};

const calcSummaryPerBlocks = executions => {
  const repsSetPerBlock = {};
  const timeSetPerBlock = {};
  const averageRepsMinSetPerBlock = {};
  executions?.forEach(({sessionSetExecutions, blockName}, blockIndex) => {
    ++blockIndex;
    repsSetPerBlock[blockIndex] = {};
    timeSetPerBlock[blockIndex] = {};
    averageRepsMinSetPerBlock[blockIndex] = {};
    sessionSetExecutions?.forEach(({exerciseExecutions}, setIndex) => {
      ++setIndex;
      repsSetPerBlock[blockIndex][setIndex] = {reps: 0, block_name: blockName};
      timeSetPerBlock[blockIndex][setIndex] = {
        execution_time: 0,
        block_name: blockName,
      };
      averageRepsMinSetPerBlock[blockIndex][setIndex] = 0;
      exerciseExecutions?.forEach(ex => {
        if (ex.bodyPartsFocused) {
          //not rest
          repsSetPerBlock[blockIndex][setIndex].reps += ex.repsExecuted;
          timeSetPerBlock[blockIndex][setIndex].execution_time +=
            ex.executionTime;
        }
      });
      const avarageRepsPerSet =
        (repsSetPerBlock[blockIndex][setIndex].reps /
          timeSetPerBlock[blockIndex][setIndex].execution_time) *
        60;
      averageRepsMinSetPerBlock[blockIndex][setIndex] = isFinite(
        avarageRepsPerSet,
      )
        ? avarageRepsPerSet
        : 0;
    });
  });
  return {repsSetPerBlock, timeSetPerBlock, averageRepsMinSetPerBlock};
};

export default (offlineData, weight) => {
  const sessionBlockExecutions =
    offlineData?.sessionExecutions?.sessionBlockExecutions;
  const {
    sessionExecutionId,
    session,
    effort,
    valueOfSession,
    program,
  } = offlineData;
  const {totalReps, totalTime, totalKcal, repsPerMin} = calcOverallSessionStats(
    sessionBlockExecutions,
    weight,
  );
  const bodyPartsFocused = calcBodyPartFocused(sessionBlockExecutions);
  const {
    repsPerExercise,
    repsPerMinPerExercise,
    minsPerExercise,
  } = calcSummaryPerExercises(sessionBlockExecutions);
  const {
    repsSetPerBlock,
    timeSetPerBlock,
    averageRepsMinSetPerBlock,
  } = calcSummaryPerBlocks(sessionBlockExecutions);

  //Summary model
  return {
    getSessionExecutionSummary: {
      id: sessionExecutionId,
      totalKcal,
      totalReps,
      totalTime,
      repsPerMin,
      repsPerExercise,
      repsPerMinPerExercise,
      minsPerExercise,
      repsSetPerBlock,
      timeSetPerBlock,
      averageRepsMinSetPerBlock,
      effort,
      valueOfSession,
      sessionExecution: {
        id: sessionExecutionId,
        sessionId: session.id,
        session: {
          id: session.id,
          name: session.name,
          sessionType: session.sessionType,
          order: session.order,
          bodyPartsFocused,
        },
        userProgram: {
          program,
        },
      },
    },
  };
};
