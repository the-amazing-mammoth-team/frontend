import moment from 'moment';

const idealBMI = {
  male: {
    loss_weight: {
      lean: 23,
      medium: 23,
      strong: 24,
    },
    antiaging: {
      lean: 23,
      medium: 24,
      strong: 25,
    },
    gain_muscle: {
      lean: 24,
      medium: 25,
      strong: 26,
    },
  },
  female: {
    loss_weight: {
      lean: 20,
      medium: 20,
      strong: 21,
    },
    antiaging: {
      lean: 20,
      medium: 21,
      strong: 22,
    },
    gain_muscle: {
      lean: 21,
      medium: 22,
      strong: 23,
    },
  },
};

const idealBF = {
  male: 0.1,
  female: 0.15,
};

const activityLevelCo = {
  sedentary: 1.15,
  medium_active: 1.35,
  very_active: 1.55,
};

const goalMultiplier = {
  loss_weight: 0.8,
  antiaging: 1,
  gain_muscle: 1,
};

export const calculateBMI = ({weight, heightInMeters}) =>
  weight / (heightInMeters * heightInMeters);

export const calculateFatWeight = ({weight, bodyFat}) =>
  weight * (bodyFat / 100);

export default function getDiagnosis({
  activityLevel,
  bodyFat,
  bodyType,
  gender,
  goal,
  height,
  weight,
  dateOfBirth,
}) {
  const heightInMeters = height / 100;
  const BMI = calculateBMI({weight, heightInMeters});
  const fatWeight = calculateFatWeight({weight, bodyFat});
  const leanWeight = weight - fatWeight;
  const targetBMI = idealBMI[gender][goal][bodyType];
  let idealWeight = targetBMI * (heightInMeters * heightInMeters);
  const idealFatWeight = idealWeight * idealBF[gender];
  let idealLeanWeight = idealWeight - idealFatWeight;

  if (leanWeight > idealLeanWeight) {
    idealWeight = idealWeight + (leanWeight - idealLeanWeight);
    idealLeanWeight = leanWeight;
  }

  console.log({idealLeanWeight, idealFatWeight, idealWeight, leanWeight});

  const fatToLose = fatWeight - idealFatWeight;

  const muscleToGain = Math.max(idealLeanWeight - leanWeight, 0);
  const yearsOld = moment().diff(moment(dateOfBirth, 'DD/MM/YYYY'), 'years');

  // Mifflin-St Jeor
  const genderFactor = gender === 'male' ? 5 : -161;
  const BMR = 9.99 * weight + 6.25 * height - 4.92 * yearsOld + genderFactor;
  // activity level coeficient for anything different than fat loss is 1
  // making target calories same as restBMR making dailyTargetDeficit zero
  const restBMR = BMR * activityLevelCo[activityLevel];

  const targetCalories = restBMR * goalMultiplier[goal];

  const dailyTargetDeficit = restBMR - targetCalories;

  // if muscle to gain is zero every result below goes to Infinity
  const timeToGoalWithTraining = () => {
    if (goal === 'gain_muscle') {
      return {
        a: (muscleToGain / (gender === 'male' ? 0.25 : 0.13)).toFixed(1),
        b: (muscleToGain / (gender === 'male' ? 0.5 : 0.25)).toFixed(1),
        c: (muscleToGain / (gender === 'male' ? 0.63 : 0.38)).toFixed(1),
      };
    }
    const getWeeksPerDeficit = deficitPercentage => {
      const totalDailyDeficit = deficitPercentage * dailyTargetDeficit;
      const daysToTarget =
        ((fatWeight - idealFatWeight) * 9000) / totalDailyDeficit;
      const weeksToTarget = Math.abs(Math.floor(daysToTarget / 7));
      return weeksToTarget !== Infinity ? weeksToTarget.toFixed() : null;
    };
    return {
      // 1-2 veces por semana
      a: getWeeksPerDeficit(1.09),
      // 3-4 veces por semana
      b: getWeeksPerDeficit(1.16),
      // 5-6 veces por semana
      c: getWeeksPerDeficit(1.2),
    };
  };

  const daysToTarget = (fatToLose * 9000) / dailyTargetDeficit;
  let weeksToTarget = daysToTarget / 7;

  if (goal === 'gain_muscle') {
    const muscleToGainPerWeekRatio = gender === 'male' ? 0.25 : 0.13;
    weeksToTarget = muscleToGain / muscleToGainPerWeekRatio;
  }
  weeksToTarget =
    Math.abs(weeksToTarget) !== Infinity ? weeksToTarget.toFixed() : null;

  return {
    BMI: BMI.toFixed(2),
    fatWeight: fatWeight.toFixed(1),
    leanWeight: leanWeight.toFixed(1),
    targetBMI: targetBMI.toFixed(),
    idealWeight: idealWeight.toFixed(1),
    idealFatWeight: idealFatWeight.toFixed(1),
    idealLeanWeight: idealLeanWeight.toFixed(1),
    fatToLose: fatToLose.toFixed(1),
    targetCalories: targetCalories.toFixed(),
    weeksToTarget,
    weeksWithTraining: timeToGoalWithTraining(),
    muscleToGain: muscleToGain.toFixed(1),
  };
}
