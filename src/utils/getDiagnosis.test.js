import getDiagnosis, {calculateBMI, calculateFatWeight} from './getDiagnosis';

it('given weight in kgs and height in meters it calculates my body mass index', () => {
  const bmi = +calculateBMI({
    weight: 90,
    heightInMeters: 1.78,
  }).toFixed(2);
  expect(bmi).toEqual(28.41);
});

it('given weight and bodyfat it calculates my fat weight', () => {
  expect(calculateFatWeight({weight: 90, bodyFat: 40})).toEqual(36);
});

it('being a fat male given my diagnosis data it calculates my ideal stats', () => {
  const results = getDiagnosis({
    weight: 90,
    activityLevel: 'sedentary',
    bodyType: 'medium',
    gender: 'male',
    goal: 'loss_weight',
    height: 178,
    dateOfBirth: '02/02/1988',
    bodyFat: 40,
  });
  expect(results.BMI).toEqual('28.41');
  expect(results.idealWeight).toEqual('72.87');
  expect(results.targetBMI).toEqual('23');
  expect(results.idealFatWeight).toEqual('7.29');
  expect(results.idealLeanWeight).toEqual('65.59');
  expect(results.fatWeight).toEqual('36.00');
  expect(results.leanWeight).toEqual('54.00');
  expect(results.idealFatWeight).toEqual('7.29');
  expect(results.fatToLose).toEqual('28.71');
  expect(results.muscleToGain).toEqual('11.59');
  expect(results.targetCalories).toEqual('1710');
  expect(results.weeksToTarget).toEqual('86');
  expect(results.weeksWithTraining.a).toEqual('79');
  expect(results.weeksWithTraining.b).toEqual('74');
  expect(results.weeksWithTraining.c).toEqual('71');
});

it('being a fit male given my diagnosis data it calculates my ideal stats', () => {
  const results = getDiagnosis({
    weight: 72,
    activityLevel: 'medium_active',
    bodyType: 'medium',
    gender: 'male',
    goal: 'gain_muscle',
    height: 178,
    dateOfBirth: '02/02/1988',
    bodyFat: 8,
  });
  expect(results.BMI).toEqual('22.72');
  expect(results.idealWeight).toEqual('79.21');
  expect(results.targetBMI).toEqual('25');
  expect(results.idealFatWeight).toEqual('7.92');
  expect(results.idealLeanWeight).toEqual('71.29');
  expect(results.fatWeight).toEqual('5.76');
  expect(results.leanWeight).toEqual('66.24');
  expect(results.idealFatWeight).toEqual('7.92');
  expect(results.fatToLose).toEqual('2.16');
  expect(results.muscleToGain).toEqual('5.05');
  expect(results.targetCalories).toEqual('2267');
  expect(results.weeksToTarget).toEqual('20');
});

it('being a fat woman given my diagnosis data it calculates my ideal stats', () => {
  const results = getDiagnosis({
    weight: 90,
    activityLevel: 'sedentary',
    bodyType: 'medium',
    gender: 'female',
    goal: 'loss_weight',
    height: 160,
    dateOfBirth: '02/02/1998',
    bodyFat: 40,
  });
  expect(results.BMI).toEqual('35.16');
  expect(results.idealWeight).toEqual('51.20');
  expect(results.targetBMI).toEqual('20');
  expect(results.idealFatWeight).toEqual('7.68');
  expect(results.idealLeanWeight).toEqual('43.52');
  expect(results.fatWeight).toEqual('36.00');
  expect(results.leanWeight).toEqual('54.00');
  expect(results.fatToLose).toEqual('28.32');
  expect(results.muscleToGain).toEqual('10.48');
  expect(results.targetCalories).toEqual('1499');
  expect(results.weeksToTarget).toEqual('97');
});

it('being a very fit and very active male given my diagnosis data it calculates my ideal stats', () => {
  const results = getDiagnosis({
    weight: 72,
    activityLevel: 'very_active',
    bodyType: 'medium',
    gender: 'male',
    goal: 'gain_muscle',
    height: 178,
    dateOfBirth: '02/02/1998',
    bodyFat: 10,
  });
  expect(results.BMI).toEqual('22.72');
  expect(results.idealWeight).toEqual('79.21');
  expect(results.targetBMI).toEqual('25');
  expect(results.idealFatWeight).toEqual('7.92');
  expect(results.idealLeanWeight).toEqual('71.29');
  expect(results.fatWeight).toEqual('7.20');
  expect(results.leanWeight).toEqual('64.80');
  expect(results.fatToLose).toEqual('0.72');
  expect(results.muscleToGain).toEqual('6.49');
  expect(results.targetCalories).toEqual('2679');
  expect(results.weeksToTarget).toEqual('26');
  expect(results.weeksWithTraining.a).toEqual('26.0');
  expect(results.weeksWithTraining.b).toEqual('13.0');
  expect(results.weeksWithTraining.c).toEqual('10.3');
});
