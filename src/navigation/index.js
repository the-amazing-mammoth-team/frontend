import * as React from 'react';
import {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import PropTypes from 'prop-types';
import {useSafeArea} from 'react-native-safe-area-context';
import Workout from '../scenes/Workout';
import WorkoutSettings from '../scenes/Workout/Settings';
import List from '../scenes/Workout/List';
import WarmUp from '../scenes/Workout/WarmUp';
import ChooseCooldown from '../scenes/Workout/ChooseCooldown';
import Countdown from '../scenes/Workout/Countdown';
import Programs from '../scenes/Programs';
import ProgramsScreen from '../scenes/Programs/ProgramsScreen';
import ProgramDetail from '../scenes/Programs/ProgramDetail';
import ProgramBuy from '../scenes/Programs/ProgramBuy';
import ProgramPaymentSuccess from '../scenes/Programs/ProgramPaymentSuccess';
import SplashMenu from '../scenes/SplashMenu';
import Login from '../scenes/Login';
import RecoverPassword from '../scenes/RecoverPassword';
import RecoverPasswordInputCode from '../scenes/RecoverPassword/InputCode';
import RecoverPasswordChooseNewPassword from '../scenes/RecoverPassword/ChooseNewPassword';
import SignupWithEmail from '../scenes/SignupWithEmail';
import Diagnosis from '../scenes/Diagnosis';
import SignupServices from '../scenes/SignupServices';
import Onboarding from '../scenes/Onboarding';
import {calcFontSize, calcHeight, moderateScale} from '../utils/dimensions';
import Home from '../scenes/Home';
import {renderIcon} from '../components/BottomNavigator';
import {SUB_TITLE_FONT} from '../styles/fonts';
import {BLACK, GRAY, ORANGE} from '../styles/colors';
import Exercise from '../scenes/Exercise';
import Calendar from '../scenes/Exercise/Calendar.js';
import {LocalizationContext} from '../localization/translations';
import SettingsDuringWorkout from '../scenes/Workout/SettingsDuringWorkout';
import Feedback from '../scenes/Workout/Feedback';
import Feed from '../scenes/Feed';
import Comments from '../scenes/Feed/Comments';
import PersonalizeWorkout from '../scenes/Workout/Personalize';
import Profile from '../scenes/Profile';
import Celebration from '../scenes/PostWorkout/Celebration';
import Exersion from '../scenes/PostWorkout/Exersion';
import Sales from '../scenes/PostWorkout/Sales';
import ThanksForPayment from '../scenes/PostWorkout/ThanksForPayment';
import Summary from '../scenes/PostWorkout/Summary';
import ChangeExercise from '../scenes/Workout/ChangeExercise';
import PaymentSuccess from '../scenes/Programs/PaymentSuccess';
import PlanWeek from '../scenes/Programs/PlanWeek';
import ProgramDescription from '../scenes/Programs/ProgramDescription';
import ChooseAnotherProgram from '../scenes/Programs/ChooseAnotherProgram';
import MySubscription from '../scenes/Profile/MySubscription';
import SubscriptionFeedback from '../scenes/Profile/SubscriptionFeedback';
import GetProSubscription from '../scenes/Profile/GetProSubscription';
import EditUser from '../scenes/Profile/EditUser';
import DownloadingVideos from '../scenes/Workout/DownloadingVideos';
import Progress from '../scenes/Progress';
import Nutrition from '../scenes/Nutrition';
import ChooseAnotherNutritionProgram from '../scenes/Nutrition/ChooseAnotherNutritionProgram';
import DishDescription from '../scenes/Nutrition/DishDescription';
import Enjoyment from '../scenes/PostWorkout/Enjoyment';
import FinishProgram from '../scenes/PostWorkout/FinishProgram';
import Achievements from '../scenes/Progress/Achievements';
import DeleteScreen from '../scenes/Profile/DeleteScreen';
import ChangePassword from '../scenes/Profile/ChangePassword';
import WelcomeToPro from '../scenes/Profile/WelcomeToPro';
import SuggestedPrograms from '../scenes/SuggestedPrograms';
import ListSessionsProgram from '../scenes/Home/ListSessionsProgram';
import WeeklyGoals from '../scenes/Home/WeeklyGoals';
import ListActiveSessionsProgram from '../scenes/Home/ListActiveSessionsProgram';
import ListChallengeSessionsProgram from '../scenes/Home/ListChallengeSessionsProgram';
import SearchExercise from '../scenes/Workout/SearchExercise';
import ExerciseDetails from '../scenes/Workout/ExerciseDetails';
import NotificationSettings from '../scenes/Profile/NotificationSettings';
import LanguageSettings from '../scenes/Profile/LanguageSettings';
import TrainingDaysSettings from '../scenes/Profile/TrainingDaysSettings';
import EquipmentSettings from '../scenes/Profile/EquipmentSettings';
import ScientificStudies from '../scenes/Profile/ScientificStudies';
import Privacy from '../scenes/SignupServices/Privacy';
import {AuthContext} from '../../App';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const HomeStack = () => {
  const {translations} = useContext(LocalizationContext);
  const insets = useSafeArea();

  return (
    <>
      {/*<LinearGradient*/}
      {/*  colors={[*/}
      {/*    'transparent',*/}
      {/*    'rgba(255,255,255, 0)',*/}
      {/*    'rgba(255,255,255, 0.3)',*/}
      {/*    'rgba(255,255,255, 0.6)',*/}
      {/*    'rgba(255,255,255, 1)',*/}
      {/*    WHITE,*/}
      {/*  ]}*/}
      {/*  style={styles.linearGradient}*/}
      {/*  useAngle={true}*/}
      {/*  angle={180}*/}
      {/*/>*/}
      <Tab.Navigator
        initialRouteName={'Home'}
        tabBarOptions={{
          showIcon: true,
          style: [
            styles.tabBarOptionsStyle,
            {
              height: insets.bottom ? calcHeight(75) : calcHeight(55), //if true it's iPhoneX or more
            },
          ],
        }}
        screenOptions={({route}) => ({
          tabBarIcon: ({focused}) => {
            return renderIcon(route.name, focused);
          },
          tabBarLabel: ({focused}) => {
            return (
              <Text
                style={[
                  styles.underIconText,
                  focused && styles.underIconTextFocused,
                ]}>
                {translations.bottomNavigationRoutes[route.name]}
              </Text>
            );
          },
        })}>
        {/*#TODO add necessary screens*/}
        {/* <Tab.Screen name={'Nutrition'} component={Nutrition} /> */}
        <Tab.Screen name={'Exercise'} component={Exercise} />
        <Tab.Screen name={'Home'} component={Home} />
        {/* <Tab.Screen name={'Feed'} component={Feed} /> */}
        <Tab.Screen name={'Progress'} component={Progress} />
      </Tab.Navigator>
    </>
  );
};

const Navigator = () => {
  const [isReadyRender, setIsReadyRender] = useState(false);
  const {userData} = React.useContext(AuthContext);

  useEffect(() => setIsReadyRender(true), []);

  const blackScreenConditions = !isReadyRender && !userData?.token;

  return (
    <NavigationContainer>
      {blackScreenConditions ? (
        <View style={styles.blackScreen} />
      ) : (
        <Stack.Navigator screenOptions={{headerShown: false}}>
          {userData?.token ? (
            <>
              <Stack.Screen name="HomeStack" component={HomeStack} />
              <Stack.Screen name="Workout" component={Workout} />
              <Stack.Screen
                name="WorkoutSettings"
                component={WorkoutSettings}
              />
              <Stack.Screen
                name="SettingsDuringWorkout"
                component={SettingsDuringWorkout}
              />
              <Stack.Screen name="ChangeExercise" component={ChangeExercise} />
              <Stack.Screen name="Feedback" component={Feedback} />
              <Stack.Screen
                name="WorkoutList"
                component={List}
                options={{gestureEnabled: false}}
              />
              <Stack.Screen name="WarmUp" component={WarmUp} />
              <Stack.Screen name="ChooseCooldown" component={ChooseCooldown} />
              <Stack.Screen name="Countdown" component={Countdown} />
              <Stack.Screen
                name="DownloadingVideos"
                component={DownloadingVideos}
              />
              <Stack.Screen
                name="SuggestedPrograms"
                component={SuggestedPrograms}
              />

              <Stack.Screen name="Programs" component={Programs} />
              <Stack.Screen name="ProgramsScreen" component={ProgramsScreen} />
              <Stack.Screen name="ProgramDetail" component={ProgramDetail} />
              <Stack.Screen name="ProgramBuy" component={ProgramBuy} />
              <Stack.Screen name="PaymentSuccess" component={PaymentSuccess} />

              <Stack.Screen
                name="ProgramPaymentSuccess"
                component={ProgramPaymentSuccess}
              />
              <Stack.Screen
                name="ProgramDescription"
                component={ProgramDescription}
              />
              <Stack.Screen
                name="ChooseAnotherProgram"
                component={ChooseAnotherProgram}
              />
              <Stack.Screen name="PlanWeek" component={PlanWeek} />
              <Stack.Screen name="Calendar" component={Calendar} />
              <Stack.Screen
                name="PersonalizeWorkout"
                component={PersonalizeWorkout}
              />
              <Stack.Screen
                name={'CelebrationPostWorkout'}
                component={Celebration}
                options={{gestureEnabled: false}}
              />
              <Stack.Screen
                name={'Enjoyment'}
                component={Enjoyment}
                options={{gestureEnabled: false}}
              />
              <Stack.Screen
                name={'FinishProgram'}
                component={FinishProgram}
                options={{gestureEnabled: false}}
              />
              <Stack.Screen name={'Exersion'} component={Exersion} />
              <Stack.Screen name={'Sales'} component={Sales} />
              <Stack.Screen
                name={'ThanksForPayment'}
                component={ThanksForPayment}
              />
              <Stack.Screen
                name={'Summary'}
                component={Summary}
                options={{gestureEnabled: false}}
              />
              <Stack.Screen name="Comments" component={Comments} />
              <Stack.Screen name="Profile" component={Profile} />
              <Stack.Screen name="MySubscription" component={MySubscription} />
              <Stack.Screen
                name="SubscriptionFeedback"
                component={SubscriptionFeedback}
              />
              <Stack.Screen
                name="GetProSubscription"
                component={GetProSubscription}
              />
              <Stack.Screen name="EditUser" component={EditUser} />
              <Stack.Screen name="DeleteScreen" component={DeleteScreen} />
              <Stack.Screen name="ChangePassword" component={ChangePassword} />
              <Stack.Screen name="WelcomeToPro" component={WelcomeToPro} />
              <Stack.Screen
                name={'ChooseAnotherNutritionProgram'}
                component={ChooseAnotherNutritionProgram}
              />
              <Stack.Screen
                name={'DishDescription'}
                component={DishDescription}
              />
              <Stack.Screen name={'Achievements'} component={Achievements} />
              <Stack.Screen
                name={'ListSessionsProgram'}
                component={ListSessionsProgram}
              />
              <Stack.Screen name={'WeeklyGoals'} component={WeeklyGoals} />
              <Stack.Screen
                name={'ListActiveSessionsProgram'}
                component={ListActiveSessionsProgram}
              />
              <Stack.Screen
                name={'ListChallengeSessionsProgram'}
                component={ListChallengeSessionsProgram}
              />
              <Stack.Screen name="Diagnosis" component={Diagnosis} />
              <Stack.Screen name="SearchExercise" component={SearchExercise} />
              <Stack.Screen
                name="ExerciseDetails"
                component={ExerciseDetails}
              />
              <Stack.Screen
                name="NotificationSettings"
                component={NotificationSettings}
              />
              <Stack.Screen
                name="LanguageSettings"
                component={LanguageSettings}
              />
              <Stack.Screen
                name="TrainingDaysSettings"
                component={TrainingDaysSettings}
              />
              <Stack.Screen
                name="EquipmentSettings"
                component={EquipmentSettings}
              />
              <Stack.Screen
                name="ScientificStudiesSettings"
                component={ScientificStudies}
              />
            </>
          ) : (
            <>
              <Stack.Screen name="SplashMenu" component={SplashMenu} />
              <Stack.Screen name="Login" component={Login} />
              <Stack.Screen
                name="RecoverPassword"
                component={RecoverPassword}
              />
              <Stack.Screen
                name="RecoverPasswordInputCode"
                component={RecoverPasswordInputCode}
              />
              <Stack.Screen
                name="RecoverPasswordChooseNewPassword"
                component={RecoverPasswordChooseNewPassword}
              />
              <Stack.Screen
                name="SignupWithEmail"
                component={SignupWithEmail}
              />
              <Stack.Screen name="SignupServices" component={SignupServices} />
              <Stack.Screen name="Onboarding" component={Onboarding} />
              <Stack.Screen name="Diagnosis" component={Diagnosis} />
              <Stack.Screen name="Privacy" component={Privacy} />
            </>
          )}
        </Stack.Navigator>
      )}
    </NavigationContainer>
  );
};

Navigator.propTypes = {
  token: PropTypes.string,
};

const styles = StyleSheet.create({
  underIconText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
    color: GRAY,
    marginTop: moderateScale(-5),
  },
  underIconTextFocused: {
    color: ORANGE,
  },
  linearGradient: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: calcHeight(55),
    height: calcHeight(40),
    borderTopWidth: 0,
    zIndex: 999,
  },
  tabBarOptionsStyle: {
    borderTopWidth: 0,
    //height: calcHeight(55),
  },
  blackScreen: {
    backgroundColor: BLACK,
    flex: 1,
  },
});

export default Navigator;
