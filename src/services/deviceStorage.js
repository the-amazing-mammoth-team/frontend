import AsyncStorage from '@react-native-community/async-storage';

const deviceStorage = {
  async saveItem(key, value) {
    try {
      await AsyncStorage.setItem(key, value);
      console.log(`saved item ${key} to local storage`);
    } catch (error) {
      console.log('Error saving to device storage', error);
    }
  },
  async getItem(key) {
    let res;
    try {
      res = await AsyncStorage.getItem(key);
      console.log(res, key);
    } catch (error) {
      console.log('Error getting value from device storage');
    }
    return res;
  },
  async clearAppData() {
    try {
      const keys = await AsyncStorage.getAllKeys();
      await AsyncStorage.multiRemove(keys);
      console.log('cleared app data');
    } catch (error) {
      console.error('Error clearing app data.');
    }
  },
  async removeItem(key) {
    try {
      await AsyncStorage.removeItem(key);
      console.log('item deleted');
    } catch (error) {
      console.error('Error deleting item');
    }
  },
};

export default deviceStorage;
