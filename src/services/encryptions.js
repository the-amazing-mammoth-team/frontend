import CryptoJS from 'crypto-js';

const secretKey = 'VE5kT4xZtMD4@EvJ'; //random password

const Encription = {
  encrypt(data) {
    const ecryptedData = CryptoJS.AES.encrypt(
      JSON.stringify(data),
      secretKey,
    ).toString();
    return ecryptedData;
  },
  decrypt(hash) {
    const bytes = CryptoJS.AES.decrypt(hash, secretKey);
    const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    return decryptedData;
  },
};

export default Encription;
