import RNFetchBlob from 'rn-fetch-blob';
import allSettled from 'promise.allsettled';

let downloadedVideos = [];
let videosToDownload = new Set();
let allVideoUrls = new Set();
let downloadProgress = {};

const pathToVideos = RNFetchBlob.fs.dirs.DocumentDir + '/videos';

const getVideoPathOfCache = videoUrl => {
  const videoName = videoUrl.split('/exercises')[1];
  const path = pathToVideos + videoName;
  return path;
};

const getVideo = async videoUrl => {
  if (!videoUrl) {
    return;
  }
  const isVideoBeingDownloaded = !!downloadedVideos.find(videoObj => {
    return videoObj.videoUrl === videoUrl;
  });

  if (!isVideoBeingDownloaded) {
    downloadedVideos = downloadedVideos.concat({
      path: null,
      videoUrl,
    });

    // check if video has been stored on local storage
    const videoPathInLocalStorage = getVideoPathOfCache(videoUrl);
    const isExistVideo = await RNFetchBlob.fs.exists(videoPathInLocalStorage);

    // console.log({videoPathInLocalStorage, isExistVideo});

    if (isExistVideo) {
      downloadedVideos = downloadedVideos.map(videoObj => {
        if (videoObj.videoUrl === videoUrl) {
          return {
            videoUrl,
            path: videoPathInLocalStorage,
          };
        }
        return videoObj;
      });
      return;
    }

    videosToDownload.add(downloadThumb(videoUrl));
  }
};

const downloadThumb = videoUrl => {
  const videoExt = videoUrl.split('.')?.reverse()[0];
  const path = getVideoPathOfCache(videoUrl);
  return new Promise((resolve, reject) => {
    RNFetchBlob.config({
      fileCache: true,
      appendExt: videoExt,
      path,
    })
      .fetch('GET', videoUrl)
      .progress((received, total) => {
        if (downloadProgress[videoUrl]) {
          downloadProgress[videoUrl].total = Number(total);
          downloadProgress[videoUrl].received = Number(received);
        } else {
          downloadProgress[videoUrl] = {
            total: Number(total),
            received: Number(received),
          };
        }
      })
      .then(async res => {
        downloadedVideos = downloadedVideos.map(videoObj => {
          if (videoObj.videoUrl === videoUrl) {
            return {
              videoUrl,
              path,
            };
          }
          return videoObj;
        });
        resolve();
      })
      .catch(err => {
        console.log('ERROR WHEN GETTING VIDEO:', err);
        reject(err);
      });
  });
};

export const convertSessionBlock = sessions => {
  return sessions.map(block => {
    let allSets = {};
    const allSessionSets = block.sessionSets;
    allSets = allSessionSets.reduce((acc, cur, index) => {
      acc[index + 1] = {
        allExercises: cur.exerciseSets,
      };
      return acc;
    }, {});
    return {
      allSets,
    };
  });
};

export const getVideos = async (currentSessions = [], callback = () => {}) => {
  downloadedVideos = [];
  videosToDownload = new Set();
  allVideoUrls = new Set();
  downloadProgress = {};
  console.log('downloading videos');
  console.log(currentSessions);
  currentSessions.map(sessionBlock => {
    Object.values(sessionBlock.allSets).forEach(el => {
      el.allExercises.forEach(set => {
        allVideoUrls.add(set.exercise.video);
      });
    });
  });
  for (const value of allVideoUrls) {
    await getVideo(value);
  }
  const interval = setInterval(() => {
    let downloadedBytes = 0;
    let bytesToDownload = 0;
    Object.values(downloadProgress).forEach(({total, received}) => {
      downloadedBytes += received;
      bytesToDownload += total;
    });
    callback(downloadedBytes / 1024 / 1024, bytesToDownload / 1024 / 1024);
  }, 100);
  let arrayPromises = [];
  do {
    videosToDownload.forEach(el => arrayPromises.push(el));
    videosToDownload = new Set();
    const res = await allSettled(arrayPromises);
    res.forEach((el, index) => {
      if (el.status === 'rejected') {
        videosToDownload.add(arrayPromises[index]);
      }
    });
    arrayPromises = [];
  } while (videosToDownload.size > 0);

  clearInterval(interval);
  return downloadedVideos;
};
