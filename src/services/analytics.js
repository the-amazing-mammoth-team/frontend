import Mixpanel from 'react-native-mixpanel';
import MoEngage, {MoEGeoLocation, MoEProperties} from 'react-native-moengage';
import * as Sentry from '@sentry/react-native';
import {MIXPANEL_TOKEN} from '../private/credentials';

export const Platforms = Object.freeze({
  moEngage: 'moEngage',
  mixPanel: 'mixPanel',
});

export const reservedPropertiesMoEngage = Object.freeze({
  //key - field name on the backend, value - equivalent of the library
  dateOfBirth: MoEngage.setUserBirthday,
  gender: MoEngage.setUserGender,
  names: MoEngage.setUserName,
  // --------------not used--------------
  // MoEngage.setUserFirstName,
  // MoEngage.setUserLastName,
  // MoEngage.setUserUniqueID,
  // MoEngage.setUserLocation,
  // MoEngage.setUserContactNumber,
  // MoEngage.setUserEmailID,
  // MoEngage.setUserAttributeLocation,
  // MoEngage.setUserAttributeISODateString,
});

export const reservedPropertiesMixPanel = Object.freeze({
  //key - field name on the backend, value - equivalent of the library
  email: '$email',
  names: '$name',
  // --------------not used--------------
  // '$avatar',
  // '$phone',
  // '$distinct_id'
  // '$ios_devices',
  // '$android_devices',
  // '$first_name',
  // '$last_name',
  // '$transactions',
  // '$created',
  // '$city',
  // '$region',
  // '$country_code',
  // '$timezone',
  // '$unsubscribed',
  // '$bucket',
});

export const initAnalytics = async () => {
  try {
    await Mixpanel.sharedInstanceWithToken(MIXPANEL_TOKEN);
    MoEngage.initialize();
  } catch (error) {}
};

export const createAnalytics = email => {
  try {
    //----Mixpanel----
    Mixpanel.createAlias(email.toLowerCase());
    Mixpanel.identify(email.toLowerCase());
    Mixpanel.set({$distinct_id: email.toLowerCase()});
    Mixpanel.set({$email: email.toLowerCase()});
    //----MoEngage----
    MoEngage.setAlias(email.toLowerCase());
  } catch (e) {
    console.log(e);
    Sentry.captureMessage(e);
  }
};

export const loginAnalytics = email => {
  try {
    Mixpanel.identify(email.toLowerCase());
    MoEngage.setUserUniqueID(email.toLowerCase());
  } catch (e) {
    console.log(e);
    Sentry.captureMessage(e);
  }
};

export const logoutAnalytics = () => {
  try {
    Mixpanel.reset();
    MoEngage.logout();
  } catch (e) {
    console.log(e);
    Sentry.captureMessage(e);
  }
};

export const trackEvents = ({
  platforms = Platforms,
  eventName = '',
  properties,
}) => {
  try {
    Object.values(platforms).forEach(el => {
      if (el === Platforms.mixPanel) {
        if (!properties) {
          Mixpanel.track(eventName);
        } else {
          Mixpanel.trackWithProperties(eventName, properties);
        }
      } else {
        let moEProperties = new MoEProperties();
        if (properties) {
          Object.entries(properties).forEach(([key, value]) => {
            moEProperties.addAttribute(key, value);
          });
        }
        MoEngage.trackEvent(eventName, moEProperties);
      }
    });
  } catch (e) {
    console.log(e);
    Sentry.captureMessage(e);
  }
};

export const trackProperties = ({platforms = Platforms, properties = {}}) => {
  try {
    Object.values(platforms).forEach(el => {
      if (el === Platforms.mixPanel) {
        Object.entries(properties).forEach(([key, value]) => {
          if (reservedPropertiesMixPanel.hasOwnProperty(key)) {
            Mixpanel.set({[reservedPropertiesMixPanel[key]]: value});
          } else {
            Mixpanel.set({[key]: value});
          }
        });
      } else {
        Object.entries(properties).forEach(([key, value]) => {
          if (reservedPropertiesMoEngage.hasOwnProperty(key)) {
            MoEngage[reservedPropertiesMoEngage[key]](value);
          } else {
            MoEngage.setUserAttribute(key, value);
          }
        });
      }
    });
  } catch (e) {
    console.log(e);
    Sentry.captureMessage(e);
  }
};

export const trackSuperProperties = ({
  platforms = Platforms,
  properties = {},
}) => {
  try {
    Object.values(platforms).forEach(el => {
      if (el === Platforms.mixPanel) {
        Mixpanel.registerSuperProperties(properties);
      } else {
        Object.entries(properties).forEach(([key, value]) => {
          if (reservedPropertiesMoEngage.hasOwnProperty(key)) {
            MoEngage[reservedPropertiesMoEngage[key]](value);
          } else {
            MoEngage.setUserAttribute(key, value);
          }
        });
      }
    });
  } catch (e) {
    console.log(e);
    Sentry.captureMessage(e);
  }
};

export const incrementProperty = (propertyName, by) => {
  Mixpanel.increment(propertyName, by);
};

export const appendProperty = (propertyName, value = []) => {
  Mixpanel.append(propertyName, value);
};
