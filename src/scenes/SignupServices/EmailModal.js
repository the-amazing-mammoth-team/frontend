import React, {useCallback, useContext} from 'react';
import {Modal, View, Text, StyleSheet} from 'react-native';
import {Input, Icon} from 'react-native-elements';
import {MButton} from '../../components/buttons';
import {LocalizationContext} from '../../localization/translations';
import {WHITE, GRAY_BORDER} from '../../styles/colors';
import {MAIN_TITLE_FONT} from '../../styles/fonts';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
} from '../../utils/dimensions';

const EmailModal = ({
  closeModal,
  isVisible,
  userEmail,
  setEmail,
  accessTokenFacebook,
  initUser,
}) => {
  const {translations} = useContext(LocalizationContext);

  const handleCloseModal = useCallback(() => {
    closeModal();
  }, [closeModal]);

  const handleOnPress = useCallback(() => {
    console.log(666, accessTokenFacebook);
    initUser(accessTokenFacebook);
    handleCloseModal();
  }, [accessTokenFacebook, handleCloseModal, initUser]);

  return (
    <Modal transparent={true} animationType="slide" visible={isVisible}>
      <View style={styles.overlay}>
        <View style={styles.modal}>
          <Text style={styles.modalTitle}>
            {translations.signUpServicesScreen.emailErrorText}
          </Text>
          <Input
            value={userEmail}
            onChangeText={input => setEmail(input)}
            inputContainerStyle={styles.containerStyles}
            containerStyle={styles.containerStyles}
            inputStyle={styles.inputStyles}
            placeholder="Email"
            rightIcon={
              <Icon
                onPress={() => setEmail('')}
                type="font-awesome"
                name="times"
                iconStyle={styles.iconStyle}
              />
            }
            keyboardType={'email-address'}
            autoCapitalize={'none'}
          />
          <View style={styles.buttonContainer}>
            <MButton
              main
              title={translations.repsCounterScreen.send}
              onPress={handleOnPress}
              disabled={userEmail === undefined || !userEmail.length}
              buttonExtraStyles={styles.sendButton}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
  },
  inner: {
    backgroundColor: WHITE,
    flex: 1,
    borderRadius: 10,
    padding: 25,
  },
  modalTitle: {
    textAlign: 'center',
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: calcHeight(25),
  },
  inputStyles: {
    backgroundColor: GRAY_BORDER,
    borderRadius: 25,
    borderWidth: 0,
    borderBottomWidth: 0,
    height: calcHeight(55),
    fontSize: calcFontSize(18),
  },
  containerStyles: {
    backgroundColor: GRAY_BORDER,
    borderRadius: 25,
    borderBottomWidth: 0,
    borderWidth: 0,
    paddingRight: calcWidth(10),
  },
  buttonContainer: {
    alignItems: 'center',
  },
  iconStyle: {
    fontSize: calcFontSize(20),
  },
  modal: {
    backgroundColor: WHITE,
    borderRadius: 10,
    marginTop: calcHeight(50),
    paddingHorizontal: calcWidth(10),
    paddingVertical: calcHeight(20),
    marginHorizontal: calcWidth(10),
  },
  sendButton: {
    width: deviceWidth * 0.3,
  },
});

export default EmailModal;
