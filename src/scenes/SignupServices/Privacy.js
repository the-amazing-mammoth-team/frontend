import React, {
  useContext,
  useState,
  useCallback,
  useMemo,
  useLayoutEffect,
} from 'react';
import {View, Text, SafeAreaView, StyleSheet, ScrollView} from 'react-native';
import {Icon, CheckBox, Button} from 'react-native-elements';
import gql from 'graphql-tag';
import {useMutation} from '@apollo/react-hooks';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {WHITE, GRAY, ORANGE, LIGHT_ORANGE} from '../../styles/colors';
import ErrorModal from '../../components/Modal/ErrorModal';
import {LocalizationContext} from '../../localization/translations';
import {calcFontSize, calcHeight, moderateScale} from '../../utils/dimensions';
import {NextButton} from '../../components/buttons';
import {AuthContext} from '../../../App';
import WebViewModal from '../../components/Modal/WebViewModal';
import LastStepIcon from '../../assets/icons/lastStepIcon.svg';
import {
  trackEvents,
  Platforms,
  createAnalytics,
} from '../../services/analytics';
import MixPanel from 'react-native-mixpanel';

const SIGN_UP = gql`
  mutation SignUp($input: SignUpInput!) {
    signUp(input: $input) {
      email
      id
      jwt
      gender
    }
  }
`;

const Privacy = ({navigation, route}) => {
  const {userInput, measuringSystem, signUpMethod} = route.params || {};
  const {translations, appLanguage} = useContext(LocalizationContext);
  const {signIn: signInInContext} = useContext(AuthContext);

  const handleGoBack = () => {
    navigation.goBack();
  };

  const [formData, setFormData] = useState({
    acceptedNewsletter: true,
    acceptedTerms: false,
    acceptedData: false,
  });

  const [visibleModal, setVisibleModal] = useState(false);
  const [isRepeatEmail, setIsRepeatEmail] = useState(false);
  const [webLink, setWebLink] = useState('');

  const handleVisibleModal = () => setVisibleModal(prev => !prev);

  const [signup] = useMutation(SIGN_UP);

  useLayoutEffect(() => {
    trackEvents({
      eventName: 'Load  Onboarding TOS',
      properties: {signup_method: signUpMethod},
      platforms: {1: Platforms.mixPanel},
    });
  }, [signUpMethod]);

  const handleErrors = useCallback(error => {
    if (
      [
        'Email translation missing: es.activerecord.errors.models.user.attributes.email.taken',
        'Invalid input: Email has already been taken',
      ].some(el => error.toString().includes(el))
    ) {
      setIsRepeatEmail(true);
    }
  }, []);

  const renderEmailErrorModal = useMemo(() => {
    return (
      <ErrorModal
        isModalVisible={isRepeatEmail}
        onDismiss={() => setIsRepeatEmail(false)}
        title={'Error'}
        description={translations.signUpServicesScreen.repeatEmailError}
      />
    );
  }, [isRepeatEmail, translations.signUpServicesScreen.repeatEmailError]);

  const submit = async () => {
    const input = {
      ...userInput,
      newsletterSubscription: formData.acceptedNewsletter,
      scientificDataUsage: formData.acceptedData,
    };
    try {
      console.log(input);
      MixPanel.getDistinctId(async id => {
        try {
          const res = await signup({
            variables: {
              input: {
                ...input,
                mixPanelId: id.toLowerCase(),
                moengageId: input.email.toLowerCase(),
              },
            },
          });
          createAnalytics(input.email);
          console.log('response', res);
          if (res.data?.signUp?.id) {
            const id = res.data.signUp.id;
            const token = res.data.signUp.jwt;
            navigation.navigate('Diagnosis', {
              diagnosisData: input,
              token,
              id,
              measuringSystem,
            });
            await signInInContext({
              id,
              token,
              gender: res.data.signUp.gender,
              email: res.data.signUp.email,
            });
          }
        } catch (err) {
          console.log(err);
          handleErrors(err);
        }
      });
    } catch (err) {
      console.log(err);
      handleErrors(err);
    }
  };

  const toggleCheck = key => {
    setFormData({
      ...formData,
      [key]: !formData[key],
    });
  };

  const requiredKeys = ['acceptedNewsletter', 'acceptedData'];
  const allFilled = Object.keys(formData)
    .filter(key => !requiredKeys.includes(key))
    .every(key => !!formData[key]);

  const BackButton = () => {
    return (
      <View style={styles.backButton}>
        <Button
          buttonStyle={{backgroundColor: 'transparent'}}
          onPress={handleGoBack}
          icon={<Icon name="arrow-back" />}
        />
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.sceneContainer}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={'always'}>
        {renderEmailErrorModal}
        <BackButton />
        <View style={styles.container}>
          <WebViewModal
            isVisible={visibleModal}
            onButtonPress={handleVisibleModal}
            link={webLink}
          />
          <View style={styles.legend}>
            <View style={styles.svgIconContainer}>
              <LastStepIcon
                width={moderateScale(122)}
                height={moderateScale(130)}
              />
            </View>
            <Text style={styles.title}>{translations.privacyScreen.title}</Text>
            <Text style={styles.text}>
              {translations.privacyScreen.description}
            </Text>
          </View>
          <View style={styles.checkboxContainer}>
            <CheckBox
              onPress={() => toggleCheck('acceptedTerms')}
              checked={formData.acceptedTerms}
              checkedIcon={<Icon name="check" color={WHITE} />}
              checkedColor="#FFFFFF"
              containerStyle={
                formData.acceptedTerms
                  ? styles.checkboxContainerStyles
                  : [styles.checkboxContainerStyles, styles.checkboxUnchecked]
              }
            />
            <Text style={styles.checkboxLabel}>
              {
                translations.signUpWithEmailScreen.termsConditionsPrivacyPolicy
                  .fistPrefix
              }{' '}
              <Text
                style={styles.purpleText}
                onPress={() => {
                  setWebLink(
                    `https://mhunters.com/es/${
                      appLanguage === 'es' ? 'cdu' : 'tos'
                    }`,
                  );
                  handleVisibleModal();
                }}>
                {
                  translations.signUpWithEmailScreen
                    .termsConditionsPrivacyPolicy.termsConditions
                }
              </Text>{' '}
              {
                translations.signUpWithEmailScreen.termsConditionsPrivacyPolicy
                  .secondPrefix
              }
              <Text
                style={styles.purpleText}
                onPress={() => {
                  setWebLink(
                    `https://mhunters.com/es/${
                      appLanguage === 'es'
                        ? 'politica-de-privacidad'
                        : 'privacy'
                    }`,
                  );
                  handleVisibleModal();
                }}>
                {' '}
                {
                  translations.signUpWithEmailScreen
                    .termsConditionsPrivacyPolicy.privacyPolicy
                }{' '}
              </Text>
              {
                translations.signUpWithEmailScreen.termsConditionsPrivacyPolicy
                  .mammothHunters
              }
            </Text>
          </View>
          <View style={styles.checkboxContainer}>
            <CheckBox
              onPress={() => toggleCheck('acceptedData')}
              checked={formData.acceptedData}
              checkedIcon={<Icon name="check" color={WHITE} />}
              checkedColor="#FFFFFF"
              containerStyle={
                formData.acceptedData
                  ? styles.checkboxContainerStyles
                  : [styles.checkboxContainerStyles, styles.checkboxUnchecked]
              }
            />
            <Text style={styles.checkboxLabel}>
              {translations.signUpWithEmailScreen.dataAgreement.firstPart}{' '}
              <Text
                style={styles.purpleText}
                onPress={() => {
                  setWebLink(
                    `https://mhunters.com/es/${
                      appLanguage === 'es'
                        ? 'consentimiento-estudio-cientifico'
                        : 'concent-science-study'
                    }`,
                  );
                  handleVisibleModal();
                }}>
                {translations.signUpWithEmailScreen.dataAgreement.secondPart}
              </Text>
            </Text>
          </View>
          <View style={styles.checkboxContainer}>
            <CheckBox
              onPress={() => toggleCheck('acceptedNewsletter')}
              checked={formData.acceptedNewsletter}
              checkedIcon={<Icon name="check" color={WHITE} />}
              checkedColor="#FFFFFF"
              containerStyle={
                formData.acceptedNewsletter
                  ? styles.checkboxContainerStyles
                  : [styles.checkboxContainerStyles, styles.checkboxUnchecked]
              }
            />
            <Text style={styles.checkboxLabel}>
              {translations.signUpWithEmailScreen.newsDissemination}
            </Text>
          </View>
        </View>
      </ScrollView>
      <NextButton onPress={submit} disabled={!allFilled} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sceneContainer: {
    backgroundColor: WHITE,
    flex: 1,
  },
  container: {
    paddingTop: 15,
    paddingLeft: 25,
    paddingRight: 25,
    paddingBottom: 30,
    backgroundColor: WHITE,
  },
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingLeft: 0,
    marginLeft: 0,
    marginBottom: moderateScale(28),
  },
  checkboxLabel: {
    flex: 1,
    flexWrap: 'wrap',
    fontSize: 16,
  },
  checkboxContainerStyles: {
    borderColor: ORANGE,
    backgroundColor: ORANGE,
    borderWidth: 0,
    marginTop: 0,
    marginRight: 20,
    padding: 0,
    borderRadius: moderateScale(3),
  },
  checkboxUnchecked: {
    borderColor: WHITE,
    backgroundColor: WHITE,
  },
  purpleText: {
    color: ORANGE,
    fontWeight: 'bold',
  },
  legend: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  svgIconContainer: {
    marginTop: calcHeight(40),
  },
  title: {
    fontSize: calcFontSize(22),
    marginTop: moderateScale(15),
    fontFamily: MAIN_TITLE_FONT,
  },
  text: {
    flex: 1,
    fontSize: calcFontSize(16),
    color: GRAY,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: moderateScale(50),
    fontFamily: SUB_TITLE_FONT,
  },
  backButton: {
    alignSelf: 'flex-start',
    width: 50,
    height: 50,
    position: 'absolute',
    left: moderateScale(10),
    top: moderateScale(10),
    zIndex: 999,
  },
});

export default Privacy;
