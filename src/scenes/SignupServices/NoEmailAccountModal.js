import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Modal from 'react-native-modal';
import {MButton} from '../../components/buttons';
import {WHITE, GRAY_BORDER} from '../../styles/colors';
import {MAIN_TITLE_FONT} from '../../styles/fonts';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceHeight,
  deviceWidth,
} from '../../utils/dimensions';

const NoEmailAccountModal = ({isVisible, closeModal, title, buttonText}) => {
  return (
    <Modal
      isVisible={isVisible}
      useNativeDriver={true}
      onBackdropPress={closeModal}>
      <View style={styles.container}>
        <View style={styles.modal}>
          <Text style={styles.modalTitle}>{title}</Text>
          <View style={styles.buttonContainer}>
            <MButton
              main
              title={buttonText}
              onPress={closeModal}
              buttonExtraStyles={styles.sendButton}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
  },
  inner: {
    backgroundColor: WHITE,
    flex: 1,
    borderRadius: 10,
    padding: 25,
  },
  modalTitle: {
    textAlign: 'center',
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  inputStyles: {
    backgroundColor: GRAY_BORDER,
    borderRadius: 25,
    borderWidth: 0,
    borderBottomWidth: 0,
    height: calcHeight(55),
    fontSize: calcFontSize(18),
  },
  containerStyles: {
    backgroundColor: GRAY_BORDER,
    borderRadius: 25,
    borderBottomWidth: 0,
    borderWidth: 0,
    paddingRight: calcWidth(10),
  },
  buttonContainer: {
    alignItems: 'center',
  },
  iconStyle: {
    fontSize: calcFontSize(20),
  },
  modal: {
    backgroundColor: WHITE,
    borderRadius: 10,
    marginTop: deviceHeight * 0.3,
    paddingHorizontal: calcWidth(10),
    paddingVertical: calcHeight(20),
    marginHorizontal: calcWidth(10),
  },
  sendButton: {
    width: deviceWidth * 0.7,
  },
});

export default NoEmailAccountModal;
