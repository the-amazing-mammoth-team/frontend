import React, {
  useState,
  useContext,
  useCallback,
  useMemo,
  useLayoutEffect,
} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import {Button, Icon} from 'react-native-elements';
import {AccessToken, LoginManager} from 'react-native-fbsdk-next';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {BLACK, ORANGE} from '../../styles/colors';
import {LocalizationContext} from '../../localization/translations';
import EmailModal from './EmailModal';
import ErrorModal from '../../components/Modal/ErrorModal';
import NoEmailAccountModal from './NoEmailAccountModal';
import {appleAuth} from '@invertase/react-native-apple-authentication';
import HandLikeIcon from '../../assets/icons/onBoardingSvgImage.svg';
import {moderateScale, calcHeight, calcFontSize} from '../../utils/dimensions';
import deviceStorage from '../../services/deviceStorage';
import {WEB_CLIENT_ID} from '../../private/credentials';
import {Platforms, trackEvents} from '../../services/analytics';
import jwt_decode from 'jwt-decode';
import * as Sentry from '@sentry/react-native';

const Login = ({navigation, route}) => {
  const onboardingData = route.params && route.params.onboardingData;

  const [email, setEmail] = useState('');
  const [facebookToken, setFacebookToken] = useState('');
  const [hasLoggedIn, setHasLoggedIn] = useState(false);

  const {translations} = useContext(LocalizationContext);

  const [isFacebookEmail, setIsFacebookEmail] = useState(true);
  const [isRepeatEmail, setIsRepeatEmail] = useState(false);
  const [googleErrorState, setGoogleErrorState] = useState({
    isModalVisible: false,
    errorText: '',
  });

  useLayoutEffect(() => {
    trackEvents({
      eventName: 'Load Signup Method',
      platforms: {1: Platforms.mixPanel},
    });
  }, []);

  const handleGoogleErrors = useCallback(error => {
    switch (true) {
      default:
        return setGoogleErrorState({
          isModalVisible: true,
          errorText: error.message,
        });
    }
  }, []);

  async function handleAppleSignUp() {
    try {
      // performs login request
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });
      // get current authentication state for user
      // /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
      const credentialState = await appleAuth.getCredentialStateForUser(
        appleAuthRequestResponse.user,
      );

      let userEmail = '';
      // use credentialState response to ensure the user is authenticated
      if (credentialState === appleAuth.State.AUTHORIZED) {
        // user is authenticated
        let {fullName, email, identityToken} = appleAuthRequestResponse;
        if (fullName?.givenName) {
          await deviceStorage.saveItem('@user/name', JSON.stringify(fullName));
        } else {
          const res = await deviceStorage.getItem('@user/name');
          fullName = JSON.parse(res);
        }
        if (!email) {
          const {email} = jwt_decode(identityToken);
          userEmail = email;
        } else {
          userEmail = email;
        }
        const {
          activityLevel,
          bodyFat,
          bodyType,
          dateOfBirth,
          gender,
          goal,
          height,
          weight,
          language,
          measuringSystem,
        } = onboardingData;
        const signUpInput = {
          activityLevel,
          bodyFat,
          bodyType,
          dateOfBirth,
          gender,
          goal,
          height,
          weight,
          language,
        };
        console.log({fullName, email});
        const input = {
          appleIdToken: userEmail,
          email: userEmail,
          names: fullName?.givenName || ' ' + ' ' + fullName?.familyName || ' ',
          ...signUpInput,
        };
        navigation.navigate('Privacy', {
          userInput: input,
          measuringSystem,
          signUpMethod: 'Apple',
        });
      }
    } catch (err) {
      if (err.code === appleAuth.Error.CANCELED) {
        console.log('canceled');
      } else {
        Sentry.captureMessage(err);
        console.log(err);
      }
    }
  }

  const handleGoogleSignUp = useCallback(async () => {
    try {
      GoogleSignin.configure({
        webClientId: WEB_CLIENT_ID,
      });
      console.log('WEB_CLIENT_ID', WEB_CLIENT_ID);
      const has_play = await GoogleSignin.hasPlayServices();
      console.log('has pla', has_play);
      //can remove next 4 lines if it necessary (user will be signUp in the app via google account that the user has already logged into)
      const isSignedIn = await GoogleSignin.isSignedIn();
      console.log('isSignedIn', isSignedIn);
      //console.log('key', webClientId);

      if (isSignedIn) {
        await GoogleSignin.signOut();
      }

      const userInfoGoogle = await GoogleSignin.signIn();
      const {id, email, name, photo} = userInfoGoogle.user;
      const {
        activityLevel,
        bodyFat,
        bodyType,
        dateOfBirth,
        gender,
        goal,
        height,
        weight,
        language,
        measuringSystem,
      } = onboardingData;
      const signUpInput = {
        activityLevel,
        bodyFat,
        bodyType,
        dateOfBirth,
        gender,
        goal,
        height,
        weight,
        language,
      };
      const input = {
        googleUid: id,
        email: email,
        names: name,
        ...signUpInput,
      };
      await deviceStorage.saveItem('@user/photo', photo);
      navigation.navigate('Privacy', {
        userInput: input,
        measuringSystem,
        signUpMethod: 'Gmail',
      });
    } catch (error) {
      if (statusCodes.SIGN_IN_CANCELLED !== error.code) {
        setHasLoggedIn(false);
        handleGoogleErrors(error);
      }
    }
  }, [handleGoogleErrors, navigation, onboardingData]);

  const handleMailLogin = useCallback(() => {
    navigation.navigate('SignupWithEmail', {
      onboardingData: onboardingData,
    });
  }, [navigation, onboardingData]);

  const initUser = useCallback(
    token => {
      fetch(
        'https://graph.facebook.com/v8.0/me?fields=email,name,picture.type(large)&access_token=' +
          token,
      )
        .then(response => response.json())
        .then(async json => {
          const {
            activityLevel,
            bodyFat,
            bodyType,
            dateOfBirth,
            gender,
            goal,
            height,
            weight,
            language,
            measuringSystem,
          } = onboardingData;
          const signUpInput = {
            activityLevel,
            bodyFat,
            bodyType,
            dateOfBirth,
            gender,
            goal,
            height,
            weight,
            language,
          };
          setIsRepeatEmail(false);
          console.log(777, json);
          if (!json.email) {
            console.log('No email in json');
            if (email === undefined || !email.length) {
              console.log('No email in local state');
              setIsFacebookEmail(false);
              setHasLoggedIn(false);
              throw new Error('Facebook email error');
            }
          }
          const input = {
            facebookUid: json.id,
            email: json.email ? json.email : email.trim(),
            names: json.name,
            ...signUpInput,
          };
          try {
            console.log('INPUT:', input);
            await deviceStorage.saveItem('@user/photo', json.picture.data.url);
            navigation.navigate('Privacy', {
              userInput: input,
              measuringSystem,
              signUpMethod: 'Facebook',
            });
            setHasLoggedIn(true);
          } catch (err) {
            setHasLoggedIn(false);
            console.log(err);
          }
        })
        .catch(error => console.log(error));
    },
    [email, navigation, onboardingData],
  );

  const handleFacebookLogin = useCallback(() => {
    AccessToken.getCurrentAccessToken().then(data => {
      if (data != null) {
        LoginManager.logOut();
      }
    });
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      function(result) {
        if (result.isCancelled) {
          console.log('Login was cancelled');
        } else {
          setHasLoggedIn(true);
          console.log(
            'Login was successful with permissions: ' +
              result.grantedPermissions.toString(),
          );
          AccessToken.getCurrentAccessToken().then(data => {
            const {accessToken} = data;
            setFacebookToken(accessToken);
            console.log(222, data);
            initUser(accessToken);
          });
        }
      },
      error => {
        console.log('Login failed with error: ' + error);
      },
    );
  }, [initUser]);

  const closeEmailModal = useCallback(() => {
    setHasLoggedIn(true);
  }, []);

  const renderEmailModal = useMemo(() => {
    return (
      <EmailModal
        isVisible={!hasLoggedIn && !isFacebookEmail && !isRepeatEmail}
        closeModal={closeEmailModal}
        userEmail={email}
        accessTokenFacebook={facebookToken}
        setEmail={setEmail}
        initUser={initUser}
      />
    );
  }, [
    closeEmailModal,
    email,
    facebookToken,
    hasLoggedIn,
    initUser,
    isFacebookEmail,
    isRepeatEmail,
  ]);

  const closeGoogleErrorModal = useCallback(() => {
    setGoogleErrorState({...googleErrorState, isModalVisible: false});
  }, [googleErrorState]);

  const renderGoogleErrorModal = useMemo(() => {
    return (
      <ErrorModal
        isModalVisible={googleErrorState.isModalVisible}
        onDismiss={closeGoogleErrorModal}
        title={'Error'}
        description={googleErrorState.errorText}
      />
    );
  }, [
    closeGoogleErrorModal,
    googleErrorState.errorText,
    googleErrorState.isModalVisible,
  ]);

  return (
    <>
      <SafeAreaView style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          {renderEmailModal}
          {renderGoogleErrorModal}
          <View style={styles.legend}>
            <View style={styles.svgIconContainer}>
              <HandLikeIcon
                width={moderateScale(122)}
                height={moderateScale(130)}
              />
            </View>
            <Text style={styles.title}>
              {translations.signUpServicesScreen.header}
            </Text>
            <Text style={styles.text}>
              {translations.signUpServicesScreen.description}
            </Text>
          </View>
          <View style={styles.buttonContainer}>
            {hasLoggedIn && <ActivityIndicator size="large" color="#3B5999" />}
            {/* <Button
              onPress={handleGoogleSignUp}
              title={translations.signUpServicesScreen.accessGoogleButton}
              titleStyle={styles.googleButtonText}
              buttonStyle={[styles.button, styles.google]}
              accessibilityLabel="Learn more about this ORANGE button"
            /> */}
            <Button
              onPress={handleFacebookLogin}
              title={translations.signUpServicesScreen.accessFacebookButton}
              titleStyle={styles.buttonText}
              buttonStyle={[styles.button, styles.facebook]}
              accessibilityLabel="Learn more about this ORANGE button"
            />
            {appleAuth.isSupported && (
              <Button
                onPress={handleAppleSignUp}
                title={translations.signUpServicesScreen.accessAppleButton}
                titleStyle={styles.buttonText}
                buttonStyle={[styles.button, styles.apple]}
                accessibilityLabel="Learn more about this ORANGE button"
              />
            )}
            <Button
              onPress={handleMailLogin}
              title={translations.signUpServicesScreen.accessEmailButton}
              titleStyle={styles.buttonText}
              buttonStyle={styles.button}
              accessibilityLabel="Learn more about this ORANGE button"
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  legend: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  title: {
    fontSize: 25,
    marginTop: 40,
    fontFamily: MAIN_TITLE_FONT,
  },
  text: {
    lineHeight: 22,
    fontSize: 16,
    textAlign: 'center',
    marginTop: 10,
    paddingLeft: 30,
    paddingRight: 30,
    marginBottom: 50,
    fontFamily: SUB_TITLE_FONT,
  },
  container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
  button: {
    borderRadius: 25,
    height: 50,
    backgroundColor: ORANGE,
    width: 350,
    marginTop: 20,
  },
  buttonContainer: {
    alignItems: 'center',
    paddingBottom: calcHeight(25),
  },
  google: {
    borderColor: '#E5E5E5',
    borderWidth: 1,
    borderStyle: 'solid',
    backgroundColor: '#FFFFFF',
  },
  facebook: {
    backgroundColor: '#3B5999',
  },
  apple: {
    backgroundColor: 'black',
  },
  googleButtonText: {
    fontSize: 18,
    color: BLACK,
    fontFamily: MAIN_TITLE_FONT,
  },
  buttonText: {
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  errorEmail: {
    color: ORANGE,
    paddingLeft: 30,
    paddingRight: 30,
    textAlign: 'center',
  },
  repeatEmail: {
    alignSelf: 'center',
    color: ORANGE,
  },
  svgIconContainer: {
    marginTop: calcHeight(70),
  },
});

export default Login;
