import React, {useCallback, useContext, useState, useEffect} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useMutation} from '@apollo/react-hooks';
import {useFocusEffect} from '@react-navigation/native';
import gql from 'graphql-tag';
import * as Sentry from '@sentry/react-native';
import RNIap, {purchaseErrorListener, IAPErrorCode} from 'react-native-iap';
import {BLACK, GRAY, ORANGE, WHITE, LIGHT_ORANGE} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import ImageBackground from '../../assets/images/postWorkouSales.png';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import CheckedIcon from '../../assets/icons/checkIcon.svg';
import ProgramOfferCard from '../../components/ProgramsScreens/ProgramOfferCard';
import ErrorModal from '../../components/Modal/ErrorModal';
import deviceStorage from '../../services/deviceStorage';
import {paymentError} from '../../utils/errorMessages';
import {trackEvents} from '../../services/analytics';
import {AuthContext} from '../../../App';
import {Loading} from '../../components/Loading';

let myPurchaseErrorListener = () => {};

const SUBSCRIBE = gql`
  mutation subscribe($input: SubscribeInput!) {
    subscribe(input: $input) {
      id
      status
      startDate
      transactionBody
      user {
        email
        names
      }
    }
  }
`;

const Sales = ({route, navigation}) => {
  const {
    imageHeader = 'https://stage-dev-new.s3-eu-west-1.amazonaws.com/assets/Upsale-image.png',
    program = {},
    offerCodes,
    paymentPlatform,
    products = {},
    user,
  } = route?.params || {};

  console.log(111, program.id);
  console.log('products sales', products);

  const {translations} = useContext(LocalizationContext);
  const {userData: userInfo} = useContext(AuthContext);

  const [selectedProgram, setSelectedProgram] = useState(null);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalTextError, setModalTextError] = useState({code: '', message: ''});
  const [isButtonPress, setIsButtonPress] = useState(false);

  const handleDismissModal = useCallback(() => {
    setIsModalVisible(oldData => !oldData);
    setIsButtonPress(false);
  }, []);

  const [subscribe] = useMutation(SUBSCRIBE);

  const handlePurchaseErrors = useCallback(
    err => {
      if (err.code !== IAPErrorCode.E_USER_CANCELLED) {
        Sentry.captureMessage(err);
        setModalTextError({message: paymentError(err.message, translations)});
        setIsModalVisible(true);
      } else {
        setIsButtonPress(false);
      }
    },
    [translations],
  );

  useFocusEffect(
    useCallback(() => {
      trackEvents({
        eventName: 'Load UpSales',
        properties: {
          offer: offerCodes,
          products: products?.products?.map(el => el.storeReference),
          prices: products?.products?.map(el => el.price),
          payment_platform: paymentPlatform,
        },
      });
    }, [offerCodes, paymentPlatform, products]),
  );

  useEffect(() => {
    myPurchaseErrorListener = purchaseErrorListener(err =>
      handlePurchaseErrors(err),
    );
    return () => {
      myPurchaseErrorListener.remove();
    };
  }, [handlePurchaseErrors, navigation]);

  const onSubscribe = useCallback(
    async (productId, receipt, userInfo) => {
      try {
        console.log('user info at subscribe', userInfo);
        const res = await subscribe({
          variables: {
            input: {
              programId: program.id,
              productId: productId,
              transactionBody: receipt.toString(),
            },
          },
          context: {
            headers: {
              authorization: userInfo?.token,
            },
          },
          //refetchQueries: ['user'],
        });
        if (res.data) {
          console.log('Response:', res.data.subscribe);
          await deviceStorage.saveItem(
            '@programs/currentProgram',
            JSON.stringify(program),
          );
          return true;
        }
      } catch (err) {
        Sentry.captureMessage(err);
        setModalTextError({code: err.code, message: err.message});
        setIsModalVisible(true);
        throw new Error(err.message);
      }
    },
    [program, subscribe],
  );

  const buyProgram2 = useCallback(
    async product => {
      setIsButtonPress(true);
      const requestPurchase = async sku => {
        try {
          await RNIap.initConnection();
          const products = await RNIap.getSubscriptions([sku]);
          // await deviceStorage.saveItem(
          //   '@purchase/product',
          //   JSON.stringify(products[0]),
          // );
          const res = await RNIap.requestPurchase(sku, false);
          if (res) {
            await onSubscribe(product.id, res.transactionReceipt, userInfo);
            await deviceStorage.saveItem('@purchase/isrSuccessRequest', 'true');
            setIsButtonPress(false);
            !user?.currentProgram
              ? //first user program, asking to choose training days settings
                navigation.navigate('PlanWeek', {program})
              : navigation.navigate('PaymentSuccess', {
                  program,
                  weeklyGoal: user?.trainingDaysSetting,
                });
          }
          setIsButtonPress(false);
        } catch (err) {}
      };
      await requestPurchase(product.storeReference);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [navigation, program, userInfo],
  );

  const salesAbilities = [
    {
      id: 1,
      title: translations.postWorkoutScreen.learnGetInShape,
    },
    {
      id: 2,
      title: translations.postWorkoutScreen.saveData,
    },
    {
      id: 3,
      title: translations.postWorkoutScreen.trainingNeeds,
    },
    {
      id: 4,
      title: translations.postWorkoutScreen.nutritionOption,
    },
  ];

  const onPressProgram = useCallback(program => {
    setSelectedProgram(program);
  }, []);

  const onPressStartFreeTrial = useCallback(() => {
    !user?.currentProgram
      ? //first user program, asking to choose training days settings
        navigation.navigate('PlanWeek', {
          program,
        })
      : navigation.navigate('PaymentSuccess', {
          program,
          weeklyGoal: user?.trainingDaysSetting,
        });
  }, [navigation, program, user]);

  const renderFooterButton = useCallback(
    () => (
      <TouchableOpacity
        style={styles.renderFooterButton}
        onPress={onPressStartFreeTrial}>
        <Text style={styles.renderFooterButtonText}>
          {translations.programsScreen.startFreeTrial}
        </Text>
      </TouchableOpacity>
    ),
    [onPressStartFreeTrial, translations.programsScreen.startFreeTrial],
  );

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <ErrorModal
          isModalVisible={isModalVisible}
          onDismiss={handleDismissModal}
          title={'Error'}
          description={modalTextError.message}
        />
        <View style={styles.rect}>
          <Image
            source={
              typeof imageHeader === 'number' ? imageHeader : {uri: imageHeader}
            }
            style={styles.image}
          />
          {/* <TouchableOpacity
            style={styles.xView}
            hitSlop={styles.hitSlopFifteen}>
            <Text style={styles.xText}>X</Text>
          </TouchableOpacity> */}
        </View>
        <View style={styles.container}>
          <View style={styles.titleView}>
            <Text style={styles.title}>
              {translations.postWorkoutScreen.salesTitle}
            </Text>
          </View>
          <View style={styles.salesAbilitiesView}>
            {salesAbilities.map(item => (
              <View key={item.id} style={styles.abilityRow}>
                <View style={styles.circle}>
                  <CheckedIcon width={calcWidth(12)} height={calcHeight(9)} />
                </View>
                <Text style={styles.abilityText}>{item.title}</Text>
              </View>
            ))}
          </View>
          {products ? (
            <ProgramOfferCard
              id={products?.products[0].id}
              title={products?.products[0].name}
              onPress={() => buyProgram2(products?.products[0])}
              price={products?.products[0].price}
              pro={true}
              isTrial={products?.products[0].hasTrial}
              trialDays={
                products?.products[0].trialDays +
                ' ' +
                translations.programBuyScreen.trialDays
              }
              currency={'eur'}
              local="es"
              translations={translations.programBuyScreen}
              disabled={isButtonPress}
              isAnnual={true}
            />
          ) : (
            <View style={{marginBottom: moderateScale(40)}}>
              <Loading noMargin />
            </View>
          )}

          <View style={styles.footerTextView}>
            <Text style={styles.footerText}>
              {translations.programBuyScreen.upsaleSubscriptionMessage}
            </Text>
          </View>

          <Text style={styles.benefitsOfPro} onPress={onPressStartFreeTrial}>
            {translations.postWorkoutScreen.linkText}
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  benefitsOfPro: {
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
    marginBottom: calcHeight(30),
  },
  rect: {
    width: deviceWidth,
    height: deviceHeight * 0.3,
    zIndex: -8,
  },
  image: {
    width: deviceWidth,
    height: deviceHeight * 0.3,
    resizeMode: 'stretch',
  },
  xView: {
    position: 'absolute',
    right: calcWidth(10),
    top: calcHeight(15),
  },
  xText: {
    fontFamily: MAIN_TITLE_FONT,
    color: BLACK,
    fontSize: calcFontSize(18),
  },
  hitSlopFifteen: {
    top: calcHeight(15),
    bottom: calcHeight(15),
    right: calcWidth(15),
    left: calcWidth(15),
  },
  container: {
    paddingHorizontal: calcWidth(30),
  },
  titleView: {
    marginVertical: calcHeight(25),
  },
  title: {
    fontFamily: MAIN_TITLE_FONT,
    color: BLACK,
    fontSize: calcFontSize(25),
  },
  abilityRow: {
    marginBottom: calcHeight(15),
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  circle: {
    width: moderateScale(17),
    height: moderateScale(17),
    borderRadius: moderateScale(17) / 2,
    backgroundColor: ORANGE,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  abilityText: {
    flex: 1,
    flexWrap: 'wrap',
    color: BLACK,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    marginLeft: moderateScale(15),
  },
  salesAbilitiesView: {
    marginBottom: calcHeight(30),
  },
  footerTextView: {
    alignSelf: 'center',
    marginBottom: calcHeight(30),
  },
  footerText: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    textAlign: 'left',
  },
  renderFooterButton: {
    width: '100%',
    backgroundColor: ORANGE,
    paddingVertical: moderateScale(13),
    borderRadius: moderateScale(25),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: moderateScale(20),
  },
  renderFooterButtonText: {
    color: WHITE,
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
  },
});

export default Sales;
