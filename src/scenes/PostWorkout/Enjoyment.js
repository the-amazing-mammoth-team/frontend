import React, {useCallback, useContext, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from 'react-native';
import gql from 'graphql-tag/src';
import {useMutation} from '@apollo/react-hooks';
import {useNetInfo} from '@react-native-community/netinfo';
import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY_BORDER,
  GRAY,
  ORANGE,
  WHITE,
  LIGHT_ORANGE,
} from '../../styles/colors';
import CustomSlider from '../../components/CustomSlider';
import DiscardWorkoutModal from '../../components/Workout/DiscardWorkoutModal';
import {calcFontSize, calcHeight, moderateScale} from '../../utils/dimensions';
import {trackEvents} from '../../services/analytics';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import EnjoymentImage from '../../assets/images/enjoymentImage.svg';
import {LocalizationContext} from '../../localization/translations';
import {AuthContext} from '../../../App';
import deviceStorage from '../../services/deviceStorage';
import {CommonActions} from '@react-navigation/routers';

const SAVE_SESSION_EXECUTION = gql`
  mutation saveSessionExecution($input: SaveSessionExecutionInput!) {
    saveSessionExecution(input: $input) {
      id
      difficultyFeedback
      enjoymentFeedback
      userProgram {
        program {
          name
        }
      }
    }
  }
`;

const Enjoyment = ({navigation, route}) => {
  const {sessionExecutions = {}, programCodeName, session} = route.params || {};
  const [sliderValue, setSliderValue] = useState(3);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const {translations} = useContext(LocalizationContext);
  const {userData: userInfo} = useContext(AuthContext);

  const [saveSessionExecution, {loading}] = useMutation(SAVE_SESSION_EXECUTION);

  const netInfo = useNetInfo();

  const handleSliderValue = useCallback(e => setSliderValue(e), []);

  const saveSessionExecutionToStorage = useCallback(
    async newSession => {
      try {
        await deviceStorage.saveItem(
          'completedSessions',
          JSON.stringify(newSession),
        );
        navigation.navigate('HomeStack', {
          screen: 'Home',
          params: {doRefetch: true},
        });
      } catch (err) {
        console.log(err);
      }
    },
    [navigation],
  );

  const handleOnPressSave = useCallback(async () => {
    trackEvents({eventName: 'Save Workout'});
    const uniqueFrontEndId = uuidv4();
    const {
      sessionId,
      programId,
      difficultyFeedback,
      sessionBlockExecutions,
    } = sessionExecutions;
    const newSessionExecution = {
      frontEndId: uniqueFrontEndId,
      sessionId: sessionId,
      programId,
      difficultyFeedback,
      enjoymentFeedback: sliderValue,
      sessionBlockExecutions: sessionBlockExecutions.map(set => {
        const {blockName, ...block} = set;
        return {
          ...block,
          sessionSetExecutions: set.sessionSetExecutions.map(ex => {
            return {
              ...ex,
              exerciseExecutions: ex.exerciseExecutions.map(el => {
                const {metMultiplier, bodyPartsFocused, name, ...rest} = el;
                return rest;
              }),
            };
          }),
        };
      }),
    };
    try {
      if (netInfo) {
        const res = await saveSessionExecution({
          variables: {
            input: {
              sessionExecution: newSessionExecution,
            },
          },
          context: {
            headers: {
              authorization: userInfo.token,
            },
          },
        });
        console.log('Response:', res);
        if (res?.data?.saveSessionExecution) {
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [
                {
                  name: 'Summary',
                  params: {
                    data: {
                      sessionExecutionId: res.data.saveSessionExecution.id,
                    },
                    offlineData: {
                      sessionExecutions,
                      session,
                      sessionExecutionId: res.data.saveSessionExecution.id,
                      effort: res.data.saveSessionExecution.difficultyFeedback,
                      valueOfSession:
                        res.data.saveSessionExecution.enjoymentFeedback,
                      program: {
                        id: programId,
                        codeName: programCodeName,
                        name:
                          res.data.saveSessionExecution.userProgram.program
                            .name,
                      },
                    },
                    prevScreen: 'Enjoyment',
                  },
                },
              ],
            }),
          );
        } else {
          await saveSessionExecutionToStorage(newSessionExecution);
        }
      } else {
        await saveSessionExecutionToStorage(newSessionExecution);
      }
    } catch (error) {
      await saveSessionExecutionToStorage(newSessionExecution);
      console.log(error);
    }
  }, [
    navigation,
    netInfo,
    programCodeName,
    saveSessionExecution,
    saveSessionExecutionToStorage,
    session,
    sessionExecutions,
    sliderValue,
    userInfo,
  ]);

  const toggleModal = useCallback(() => {
    if (!isModalVisible) {
      //on open
      trackEvents({
        eventName: 'Load Discard Workout Popup',
        properties: {
          training_program: programCodeName,
          session_id: session?.id,
          session_name: session?.name,
          session_type: session?.sessionType,
          session_order: session?.order,
          discard_placement: 'Enjoyment Evaluation',
        },
      });
    }
    setIsModalVisible(!isModalVisible);
  }, [isModalVisible, programCodeName, session]);

  const handleLaterButtonModal = useCallback(() => {
    toggleModal();
    trackEvents({
      eventName: 'Discard Workout',
      properties: {
        training_program: programCodeName,
        session_id: session?.id,
        session_name: session?.name,
        session_type: session?.sessionType,
        session_order: session?.order,
        placement: 'Enjoyment Evaluation',
      },
    });
    navigation.navigate('HomeStack', {
      screen: 'Home',
      params: {doRefetch: true},
    });
  }, [navigation, programCodeName, session, toggleModal]);

  const handleDeleteButtonModal = useCallback(() => {
    toggleModal();
    const {sessionId, programId} = sessionExecutions;
    navigation.navigate('Feedback', {
      sessionId: sessionId,
      programId: programId,
      programCodeName,
      session,
      prevScreen: 'Enjoyment Evaluation',
    });
  }, [navigation, programCodeName, session, sessionExecutions, toggleModal]);

  const getSliderDesc = () => {
    let value = {};
    switch (sliderValue) {
      case 1:
        value = translations.enjoymentScreen.sliderValues.veryUnpleasan;
        break;
      case 2:
        value = translations.enjoymentScreen.sliderValues.unpleasant;
        break;
      case 3:
        value = translations.enjoymentScreen.sliderValues.normal;
        break;
      case 4:
        value = translations.enjoymentScreen.sliderValues.satisfied;
        break;
      case 5:
        value = translations.enjoymentScreen.sliderValues.verySatisfied;
        break;
      default:
        break;
    }
    return value;
  };

  const sliderDesc = getSliderDesc();

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView
        style={styles.root}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={'always'}>
        <DiscardWorkoutModal
          title={translations.settingsScreen.discardWorkout}
          subTitle={translations.settingsScreen.discardWorkoutDesc}
          goBack={handleLaterButtonModal}
          goBackText={translations.settingsScreen.repeatLater}
          goNext={handleDeleteButtonModal}
          goNextText={translations.settingsScreen.deleteTraining}
          visible={isModalVisible}
          closeModal={toggleModal}
        />
        <View style={styles.header}>
          <Text style={styles.headerTitle}>
            {translations.enjoymentScreen.header}
          </Text>
        </View>
        <View style={styles.container}>
          <View style={styles.titleView}>
            <Text style={styles.titleText}>
              {translations.enjoymentScreen.mainQuestion}
            </Text>
          </View>
          {/* <Text style={styles.genericGrayText}>
            {translations.enjoymentScreen.mainDescribe}
          </Text> */}
          <View style={styles.imageContainer}>
            <EnjoymentImage width={'100%'} />
          </View>
          <View style={styles.valueView}>
            <Text style={[styles.titleText, styles.valueDigit]}>
              {sliderValue}
            </Text>
            <Text style={[styles.titleText, styles.valueText]}>
              {sliderDesc?.title}
            </Text>
            <Text style={[styles.genericGrayText, styles.valueDescription]}>
              {sliderDesc?.desc}
            </Text>
          </View>
          <CustomSlider
            maxValue={5}
            value={sliderValue}
            onChange={handleSliderValue}
            minValueText={translations.enjoymentScreen.iHateIt}
            maxValueText={translations.enjoymentScreen.iLoveIt}
          />
        </View>
      </ScrollView>
      <TouchableOpacity
        onPress={handleOnPressSave}
        disabled={loading}
        style={styles.bottomButton}>
        <Text style={styles.bottomButtonText}>
          {translations.enjoymentScreen.save}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={toggleModal}>
        <Text style={styles.bottomOrangeText}>
          {translations.enjoymentScreen.discardWorkout}
        </Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  header: {
    borderBottomWidth: moderateScale(1),
    borderBottomColor: GRAY_BORDER,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: moderateScale(6),
    marginBottom: moderateScale(5),
  },
  headerTitle: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
    textAlign: 'center',
    color: BLACK,
  },
  container: {
    paddingHorizontal: moderateScale(20),
  },
  genericGrayText: {
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
    fontSize: calcFontSize(14),
  },
  titleText: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(20),
    color: BLACK,
  },
  titleView: {
    marginTop: moderateScale(15),
  },
  valueView: {
    alignSelf: 'center',
    alignItems: 'center',
    marginBottom: moderateScale(10),
    marginTop: moderateScale(-40),
  },
  valueDigit: {
    fontSize: calcFontSize(60),
  },
  valueDescription: {
    color: BUTTON_TITLE,
    textAlign: 'center',
  },
  valueText: {
    fontSize: calcFontSize(22),
    marginTop: -moderateScale(10),
    marginBottom: moderateScale(5),
  },
  imageContainer: {
    marginTop: moderateScale(-20),
  },
  bottomButton: {
    alignSelf: 'center',
    marginBottom: calcHeight(20),
    marginTop: calcHeight(20),
    borderRadius: moderateScale(25),
    backgroundColor: ORANGE,
    width: '90%',
    paddingVertical: moderateScale(13),
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomButtonText: {
    color: WHITE,
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  bottomOrangeText: {
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    alignSelf: 'center',
    marginBottom: moderateScale(20),
  },
});

export default Enjoyment;
