import React, {useCallback, useContext} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {BLACK, WHITE} from '../../styles/colors';
import ThanksForPaymentImage from '../../assets/images/thanksForPayment.png';
import {MButton} from '../../components/buttons';
import {calcFontSize, deviceWidth, moderateScale} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';

const ThanksForPayment = ({navigation, route}) => {
  const {translations} = useContext(LocalizationContext);

  const handleNext = useCallback(
    () => navigation.navigate('Home', {refetch: true}),
    [navigation],
  );

  return (
    <SafeAreaView style={styles.root}>
      <Image source={ThanksForPaymentImage} style={styles.imageContainer} />
      <View style={styles.container}>
        <View>
          <Text style={styles.title}>
            {translations.postWorkoutScreen.thanksForPaymentTitle}
          </Text>
          <Text style={styles.subTitle}>
            {translations.postWorkoutScreen.thanksForPaymentSubTitle}
          </Text>
        </View>
        <MButton
          main
          onPress={handleNext}
          title={translations.postWorkoutScreen.startNow}
          buttonExtraStyles={{maxWidth: deviceWidth * 0.75}}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flexGrow: 1,
    backgroundColor: WHITE,
  },
  container: {
    flex: 1,
    paddingVertical: moderateScale(20),
    paddingHorizontal: moderateScale(20),
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  imageContainer: {
    width: deviceWidth,
  },
  title: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(25),
    color: BLACK,
    textAlign: 'center',
  },
  subTitle: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
    color: BLACK,
    marginVertical: moderateScale(10),
    textAlign: 'center',
  },
});

export default ThanksForPayment;
