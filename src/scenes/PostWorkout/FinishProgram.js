import React, {useCallback, useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  TextInput,
  BackHandler,
} from 'react-native';
import gql from 'graphql-tag/src';
import {useMutation} from '@apollo/react-hooks';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY_BORDER,
  GRAY,
  ORANGE,
  WHITE,
  LIGHT_ORANGE,
} from '../../styles/colors';
import CustomSlider from '../../components/CustomSlider';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../../utils/dimensions';
import {trackEvents} from '../../services/analytics';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import MHuntersIcon from '../../assets/icons/MHuntersIcon.svg';
import {LocalizationContext} from '../../localization/translations';
import {AuthContext} from '../../../App';

const SAVE_PROGRAM_FEEDBACK = gql`
  mutation programFeedback($input: ProgramFeedbackInput!) {
    programFeedback(input: $input) {
      id
      enjoyment
      enjoymentNotes
    }
  }
`;

const FinishProgram = ({navigation, route}) => {
  const {userProgramId, programName, codeName} = route.params || {};

  const [sliderValue, setSliderValue] = useState(3);
  const [feedbackText, setFeedbackText] = useState('');

  const {translations} = useContext(LocalizationContext);
  const {userData: userInfo} = useContext(AuthContext);

  const isFocused = useIsFocused();

  const [programFeedback, {loading}] = useMutation(SAVE_PROGRAM_FEEDBACK);

  useFocusEffect(
    useCallback(() => {
      if (isFocused) {
        BackHandler.addEventListener('hardwareBackPress', () => {
          return true;
        });
        navigation.addListener('blur', () =>
          BackHandler.removeEventListener('hardwareBackPress'),
        );
        trackEvents({
          eventName: 'Load Finish Program Evaluation',
          properties: {training_program: codeName},
        });
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]),
  );

  const handleSliderValue = useCallback(e => setSliderValue(e), []);

  const mutation = useCallback(
    async callback => {
      try {
        const res = await programFeedback({
          variables: {
            input: {
              userProgramId,
              enjoyment: sliderValue.toString(),
              enjoymentNotes: feedbackText,
            },
          },
          context: {
            headers: {
              authorization: userInfo.token,
            },
          },
        });
        console.log('Response:', res);
        if (res?.data?.programFeedback) {
          callback();
        }
      } catch (error) {
        console.log(error);
      }
    },
    [feedbackText, programFeedback, sliderValue, userInfo.token, userProgramId],
  );

  const handleChooseAnother = useCallback(() => {
    mutation(() => navigation.navigate('SuggestedPrograms'));
  }, [mutation, navigation]);

  const handleLater = useCallback(() => {
    mutation(() =>
      navigation.navigate('HomeStack', {
        screen: 'Home',
        params: {doRefetch: true},
      }),
    );
  }, [mutation, navigation]);

  const getSliderDesc = () => {
    let value = {};
    switch (sliderValue) {
      case 1:
        value = translations.finishProgramScreen.sliderValues.hate;
        break;
      case 2:
        value = translations.finishProgramScreen.sliderValues.dontLike;
        break;
      case 3:
        value = translations.finishProgramScreen.sliderValues.ok;
        break;
      case 4:
        value = translations.finishProgramScreen.sliderValues.like;
        break;
      case 5:
        value = translations.finishProgramScreen.sliderValues.love;
        break;
      default:
        break;
    }
    return value;
  };

  const sliderDesc = getSliderDesc();

  const renderSubTitle = () => {
    const name = programName || ' - ';
    const splited = translations.finishProgramScreen.subTitle.split(
      '*programName*',
    );
    return (
      <Text style={[styles.genericGrayText, styles.subTitle]}>
        {splited[0] + name + splited[1]}
      </Text>
    );
  };

  return (
    <SafeAreaView style={styles.root}>
      <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <MHuntersIcon width={styles.iconMHunters.width} />
          <Text style={styles.title}>
            {translations.finishProgramScreen.title}
          </Text>
          {renderSubTitle()}
          <View style={styles.line} />
          <View style={styles.valueView}>
            <Text style={[styles.title, styles.valueDigit]}>{sliderValue}</Text>
            <Text style={[styles.title, styles.valueText]}>
              {sliderDesc?.title}
            </Text>
            <Text style={[styles.genericGrayText, styles.valueDescription]}>
              {sliderDesc?.desc}
            </Text>
          </View>
          <View style={{flex: 1}}>
            <CustomSlider
              maxValue={5}
              value={sliderValue}
              onChange={handleSliderValue}
              minValueText={
                translations.finishProgramScreen.sliderValues.hate.title
              }
              maxValueText={
                translations.finishProgramScreen.sliderValues.love.title
              }
            />
          </View>
          <View style={styles.inputView}>
            <TextInput
              multiline={true}
              value={feedbackText}
              placeholder={translations.finishProgramScreen.comments}
              placeholderTextColor={GRAY}
              style={styles.textInputStyle}
              onChangeText={e => setFeedbackText(e)}
            />
          </View>
        </View>
      </KeyboardAwareScrollView>
      <View style={styles.containerButtons}>
        <TouchableOpacity
          onPress={handleChooseAnother}
          disabled={loading}
          style={styles.bottomButton}>
          <Text style={styles.bottomButtonText}>
            {translations.finishProgramScreen.selectAnotherProgram}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={handleLater} disabled={loading}>
          <Text style={styles.bottomOrangeText}>
            {translations.finishProgramScreen.notNow}
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  container: {
    alignItems: 'center',
    paddingHorizontal: moderateScale(20),
    paddingVertical: moderateScale(20),
    marginBottom: moderateScale(100),
  },
  iconMHunters: {
    width: calcWidth(100),
  },
  title: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(22),
    color: BLACK,
    marginTop: moderateScale(15),
  },
  subTitle: {
    fontSize: calcFontSize(16),
    marginVertical: moderateScale(10),
    textAlign: 'center',
    paddingHorizontal: moderateScale(10),
  },
  line: {
    width: '100%',
    borderColor: GRAY_BORDER,
    borderWidth: calcHeight(0.5),
    marginTop: moderateScale(15),
  },
  genericGrayText: {
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
    fontSize: calcFontSize(14),
  },
  valueView: {
    alignSelf: 'center',
    alignItems: 'center',
  },
  valueDigit: {
    fontSize: calcFontSize(60),
  },
  valueDescription: {
    color: BUTTON_TITLE,
    textAlign: 'center',
    height: moderateScale(60),
    paddingHorizontal: moderateScale(10),
  },
  valueText: {
    fontSize: calcFontSize(22),
    marginTop: -moderateScale(10),
    marginBottom: moderateScale(5),
  },
  inputView: {
    width: '100%',
    height: calcHeight(100),
    backgroundColor: '#F2F2F2',
    borderRadius: 10,
    marginTop: moderateScale(40),
    marginBottom: moderateScale(20),
    paddingHorizontal: moderateScale(10),
    paddingVertical: moderateScale(15),
  },
  textInputStyle: {
    flex: 1,
    marginLeft: calcWidth(10),
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    //lineHeight: calcHeight(22),
    color: GRAY,
    textAlignVertical: 'top',
  },
  bottomButton: {
    alignSelf: 'center',
    marginBottom: calcHeight(20),
    borderRadius: moderateScale(25),
    backgroundColor: ORANGE,
    width: '80%',
    paddingVertical: moderateScale(13),
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomButtonText: {
    color: WHITE,
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  bottomOrangeText: {
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    alignSelf: 'center',
    marginBottom: moderateScale(20),
  },
  containerButtons: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
  },
});

export default FinishProgram;
