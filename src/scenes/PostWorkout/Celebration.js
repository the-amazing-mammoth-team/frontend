import React, {useCallback, useContext, useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  BackHandler,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Icon} from 'react-native-elements';
import {useFocusEffect} from '@react-navigation/native';
import {useLazyQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import CustomSlider from '../../components/CustomSlider';
import {NextButton} from '../../components/buttons';
import {LocalizationContext} from '../../localization/translations';
import {AuthContext} from '../../../App';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {
  trackProperties,
  incrementProperty,
  Platforms,
} from '../../services/analytics';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {
  BLACK,
  GRAY,
  GRAY_BORDER,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
} from '../../styles/colors';

const GET_USER = gql`
  query user($id: ID!) {
    user(id: $id) {
      id
      weeklyExecutions {
        id
      }
      trainingDaysSetting
      bestWeeklyStreak
      currentWeeklyStreak
    }
  }
`;

const Celebration = ({navigation, route}) => {
  const {sessionExecutions, programCodeName, session} = route.params;
  const {userData: userInfo} = useContext(AuthContext);
  const {translations} = useContext(LocalizationContext);

  const [weekStreak, setWeekStreak] = useState('-');

  const [getUserData, {data, loading = true, error}] = useLazyQuery(GET_USER, {
    variables: {id: userInfo?.id},
    fetchPolicy: 'network-only',
  });

  const user = data?.user;
  const weeklyExecutions = user?.weeklyExecutions;
  const trainingDaysSetting = user?.trainingDaysSetting;
  const bestStreak = user?.bestWeeklyStreak;
  const currentWeeklyStreak = user?.currentWeeklyStreak;

  useFocusEffect(
    useCallback(() => {
      console.log('refetching');
      getUserData();
    }, [getUserData]),
  );

  useEffect(() => {
    //disable go back android
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      () => true,
    );
    () => {
      backHandler.remove();
    };
  }, [navigation]);

  useEffect(() => {
    if (weekStreak !== '-') {
      trackProperties({
        properties: {
          weekly_streak: weekStreak,
        },
        platforms: {1: Platforms.mixPanel},
      });
      incrementProperty('completed_workouts_count', 1);
    }
  }, [weekStreak]);

  useEffect(() => {
    if (!isNaN(weeklyExecutions?.length)) {
      if (currentWeeklyStreak >= 0 && weeklyExecutions.length === 0) {
        setWeekStreak(currentWeeklyStreak + 1);
      } else if (currentWeeklyStreak > 0 && weeklyExecutions.length > 0) {
        setWeekStreak(currentWeeklyStreak);
      }
    }
  }, [currentWeeklyStreak, weeklyExecutions]);

  const goNext = useCallback(
    () =>
      navigation.navigate('Exersion', {
        sessionExecutions,
        programCodeName,
        session,
      }),
    [navigation, programCodeName, session, sessionExecutions],
  );

  let bestWeeklyStreak = 1;
  if (weekStreak > bestStreak) {
    bestWeeklyStreak = weekStreak;
  } else {
    bestWeeklyStreak = bestStreak;
  }

  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView style={styles.root} showsVerticalScrollIndicator={false}>
        <View style={styles.titleCelebrationView}>
          <Text style={styles.titleCelebration}>
            {translations.celebrationScreen.congratulationsText}
          </Text>
        </View>
        <View style={styles.line} />
        <View style={styles.genericTitleView}>
          <Text style={styles.goalTitle}>
            {translations.celebrationScreen.weeklyGoal}
          </Text>
        </View>
        <View style={styles.progressContainer}>
          {!isNaN(weeklyExecutions?.length) && trainingDaysSetting && (
            <CustomSlider
              value={weeklyExecutions?.length + 1}
              maxValue={trainingDaysSetting}
              disabled={true}
            />
          )}
        </View>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={[ORANGE, LIGHT_ORANGE]}
          style={styles.linearGradient}>
          <View style={styles.genericTitleView}>
            <Text style={styles.cardTitle}>
              {translations.celebrationScreen.weeklyStreak}
            </Text>
          </View>
          <View style={styles.row}>
            <View style={styles.cardView}>
              <View style={styles.genericCenterAlign}>
                <Text style={styles.cardDigits}>{weekStreak}</Text>
                <Text style={styles.underDigitsText}>
                  {translations.celebrationScreen.weeks}
                </Text>
              </View>
            </View>
            <View style={styles.cardSeparateLine} />
            <View style={styles.cardView}>
              <View style={styles.genericCenterAlign}>
                <Text style={styles.cardDigits}>{bestWeeklyStreak || '-'}</Text>
                <Text style={styles.underDigitsText}>
                  {translations.celebrationScreen.yourBestStreak}
                </Text>
              </View>
            </View>
          </View>
        </LinearGradient>
        <View
          style={[
            styles.line,
            {marginBottom: calcHeight(deviceHeight * 0.025)},
          ]}
        />
        {/* <View style={styles.recordTextView}>
          <Text style={styles.descriptionText}>
            {translations.celebrationScreen.questionFinishedSession}
          </Text>
          <Text style={[styles.descriptionText, styles.resumeText]}>
            {translations.celebrationScreen.resume}
          </Text>
        </View>*/}
      </ScrollView>
      <NextButton onPress={goNext} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: WHITE,
  },
  root: {
    flex: 1,
    backgroundColor: WHITE,
    paddingHorizontal: calcWidth(20),
  },
  titleCelebration: {
    fontSize: calcFontSize(22),
    fontFamily: MAIN_TITLE_FONT,
    textAlign: 'center',
    color: BLACK,
  },
  titleCelebrationView: {
    marginTop: calcHeight(deviceHeight * 0.05),
    maxWidth: deviceWidth * 0.78,
    alignSelf: 'center',
  },
  descriptionText: {
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
    fontSize: calcFontSize(14),
    textAlign: 'center',
  },
  resumeText: {
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
  },
  line: {
    width: '100%',
    height: calcHeight(1),
    backgroundColor: GRAY_BORDER,
    marginVertical: calcHeight(deviceHeight * 0.05),
  },
  genericTitleView: {
    alignSelf: 'center',
  },
  goalTitle: {
    fontSize: calcFontSize(20),
    fontFamily: MAIN_TITLE_FONT,
    color: BLACK,
  },
  progressContainer: {
    marginTop: calcHeight(20),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  progressTextContainer: {
    marginLeft: calcWidth(15),
  },
  progressText: {
    fontSize: calcFontSize(14),
    color: GRAY,
  },
  linearGradient: {
    borderRadius: moderateScale(10),
    paddingTop: calcHeight(10),
    paddingBottom: calcHeight(15),
    paddingHorizontal: calcWidth(20),
    marginVertical: calcHeight(deviceHeight * 0.05),
  },
  cardTitle: {
    color: WHITE,
    fontSize: calcFontSize(20),
    fontFamily: MAIN_TITLE_FONT,
  },
  cardView: {
    flex: 1,
    flexDirection: 'row',
    marginTop: calcHeight(5),
    alignSelf: 'center',
  },
  cardDigits: {
    color: WHITE,
    fontSize: calcFontSize(40),
    fontFamily: MAIN_TITLE_FONT,
  },
  underDigitsText: {
    color: WHITE,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  genericCenterAlign: {
    flex: 1,
    alignItems: 'center',
  },
  cardSeparateLine: {
    flex: 0,
    width: calcWidth(1),
    backgroundColor: GRAY_BORDER,
    alignSelf: 'stretch',
  },
  recordTextView: {
    marginTop: calcHeight(12),
  },
  row: {
    flexDirection: 'row',
  },
});

export default Celebration;
