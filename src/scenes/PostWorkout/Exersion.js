import React, {useCallback, useContext, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from 'react-native';
import {Icon} from 'react-native-elements';
import CustomSlider from '../../components/CustomSlider';
import {NextButton} from '../../components/buttons';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY,
  GRAY_BORDER,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  deviceHeight,
  moderateScale,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import RangeImage from '../../assets/images/rangePostWorkout.svg';

const Exersion = ({navigation, route}) => {
  const {sessionExecutions = {}, programCodeName, session} = route.params || {};
  const {translations} = useContext(LocalizationContext);

  const [sliderValue, setSliderValue] = useState(5);

  const onSave = useCallback(() => {
    navigation.navigate('Enjoyment', {
      sessionExecutions: {
        ...sessionExecutions,
        difficultyFeedback: sliderValue,
      },
      programCodeName,
      session,
    });
  }, [navigation, programCodeName, session, sessionExecutions, sliderValue]);

  const handleSliderValue = useCallback(e => setSliderValue(e), []);

  const getSliderDesc = () => {
    let value = {};
    switch (true) {
      case sliderValue === 1:
        value = translations.exersionScreen.sliderValues.veryLight;
        break;
      case sliderValue <= 3:
        value = translations.exersionScreen.sliderValues.light;
        break;
      case sliderValue <= 6:
        value = translations.exersionScreen.sliderValues.moderate;
        break;
      case sliderValue <= 8:
        value = translations.exersionScreen.sliderValues.vigorous;
        break;
      case sliderValue === 9:
        value = translations.exersionScreen.sliderValues.veryIntense;
        break;
      case sliderValue === 10:
        value = translations.exersionScreen.sliderValues.maximumEffort;
        break;
      default:
        break;
    }
    return value;
  };

  const sliderDesc = getSliderDesc();

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView
        style={styles.root}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={'always'}>
        <View style={styles.header}>
          <Text style={styles.headerTitle}>
            {translations.postWorkoutScreen.exersion}
          </Text>
        </View>
        <View style={styles.container}>
          <View style={styles.titleView}>
            <Text style={styles.titleText}>
              {translations.postWorkoutScreen.exersionMainQuestion}
            </Text>
          </View>
          <RangeImage width={'100%'} />
          <View style={styles.valueView}>
            <Text style={[styles.titleText, styles.valueDigit]}>
              {sliderValue}
            </Text>
            <Text style={[styles.titleText, styles.valueText]}>
              {sliderDesc?.title}
            </Text>
            <Text style={[styles.genericGrayText, styles.valueDescription]}>
              {sliderDesc?.desc}
            </Text>
          </View>
          <CustomSlider
            maxValue={10}
            value={sliderValue}
            onChange={handleSliderValue}
            minValueText={translations.feedbackScreen.tooEasy}
            maxValueText={translations.feedbackScreen.tooHard}
          />
        </View>
      </ScrollView>
      <NextButton onPress={onSave} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  header: {
    borderBottomWidth: moderateScale(1),
    borderBottomColor: GRAY_BORDER,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: moderateScale(6),
    marginBottom: moderateScale(5),
  },
  headerTitle: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(16),
    textAlign: 'center',
    color: BLACK,
  },
  container: {
    paddingHorizontal: moderateScale(20),
  },
  genericGrayText: {
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
    fontSize: calcFontSize(14),
  },
  titleText: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(20),
    color: BLACK,
  },
  titleView: {
    marginTop: moderateScale(15),
  },
  valueView: {
    alignSelf: 'center',
    alignItems: 'center',
    marginBottom: moderateScale(20),
    marginTop: moderateScale(-20),
  },
  valueDigit: {
    fontSize: calcFontSize(60),
  },
  valueDescription: {
    color: BUTTON_TITLE,
    textAlign: 'center',
    height: moderateScale(60),
  },
  valueText: {
    fontSize: calcFontSize(20),
    marginTop: -moderateScale(10),
    marginBottom: moderateScale(5),
  },
  button: {
    position: 'absolute',
    bottom: deviceHeight + 0,
  },
});

export default Exersion;
