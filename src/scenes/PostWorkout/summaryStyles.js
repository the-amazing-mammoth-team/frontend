import {StyleSheet} from 'react-native';
import {
  BLACK,
  GRAY,
  GRAY_BORDER,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';

export const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerText: {
    fontFamily: MAIN_TITLE_FONT,
    color: BLACK,
    fontSize: calcFontSize(18),
  },
  fontTitleTwenty: {
    fontSize: calcFontSize(20),
    textAlign: 'center',
  },
  hitSlopFifteen: {
    top: calcHeight(15),
    bottom: calcHeight(15),
    right: calcWidth(15),
    left: calcWidth(15),
  },
  line: {
    height: calcHeight(1),
    width: deviceWidth,
    backgroundColor: GRAY_BORDER,
    marginTop: -20,
    marginBottom: calcHeight(20),
  },
  grayText: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
  blackText: {
    color: BLACK,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
  container: {
    paddingHorizontal: calcWidth(20),
  },
  verticalDistanceHeaderLittle: {
    marginTop: calcHeight(7),
    marginBottom: calcHeight(16),
  },
  inputView: {
    width: '100%',
    flex: 1,
    backgroundColor: '#F2F2F2',
    borderRadius: 10,
    paddingHorizontal: moderateScale(10),
    paddingVertical: moderateScale(20),
  },
  textInputStyle: {
    flex: 1,
    marginLeft: calcWidth(10),
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
    lineHeight: calcHeight(22),
  },
  editIconView: {
    position: 'absolute',
    right: calcWidth(9),
    bottom: calcHeight(9),
  },
  boardView: {
    marginTop: calcHeight(30),
    marginBottom: calcHeight(45),
  },
  underFeedCardsView: {
    marginTop: calcHeight(5),
  },
  buttonTextWhite: {
    color: WHITE,
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
  },
  tryButton: {
    backgroundColor: ORANGE,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: moderateScale(25),
    paddingVertical: calcHeight(12),
    width: deviceWidth * 0.8,
    marginTop: calcHeight(12),
    alignSelf: 'center',
  },
  bottomButton: {
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    marginBottom: calcHeight(20),
  },
  nextButtonStyleActive: {
    width: moderateScale(50),
    height: moderateScale(50),
    borderRadius: moderateScale(50) / 2,
    backgroundColor: ORANGE,
    alignItems: 'center',
    justifyContent: 'center',
  },
  sectionText: {
    fontSize: calcFontSize(18),
    color: BLACK,
    fontFamily: MAIN_TITLE_FONT,
  },
  sectionTextView: {
    marginBottom: calcHeight(20),
  },
  bodyPartView: {
    backgroundColor: GRAY,
    borderRadius: 5,
    paddingVertical: calcHeight(5),
    paddingHorizontal: calcWidth(13),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: calcHeight(16),
    marginRight: calcWidth(13),
  },
  bodyPartViewSelected: {
    backgroundColor: ORANGE,
  },
  bodyPartText: {
    color: WHITE,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  generalPaddingHorizontal: {
    paddingHorizontal: calcWidth(20),
    width: '100%',
  },
  bodyPartsViewRoot: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  discardView: {
    marginTop: calcHeight(20),
    marginBottom: calcHeight(12),
    alignSelf: 'center',
  },
  discardText: {
    color: LIGHT_ORANGE,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    textDecorationLine: 'underline',
  },
  equipmentView: {
    borderRadius: moderateScale(25),
    borderWidth: moderateScale(1),
    borderColor: GRAY_BORDER,
    alignItems: 'center',
    justifyContent: 'center',
    height: moderateScale(148),
    width: moderateScale(140),
    marginRight: calcWidth(15),
    paddingHorizontal: calcWidth(10),
    paddingVertical: calcWidth(13),
  },
  equipmentImage: {
    width: calcWidth(100),
    height: calcHeight(100),
  },
  equipmentName: {
    color: BLACK,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
  },
  noEquipment: {
    color: GRAY,
  },
  equipmentRowView: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
    marginBottom: calcHeight(35),
  },
  tryConnectionView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: calcWidth(5),
    marginTop: calcHeight(5),
  },
  backgroundRectangle: {
    backgroundColor: '#F2F2F2',
    width: deviceWidth,
    alignSelf: 'center',
    alignItems: 'center',
    marginBottom: calcHeight(50),
    paddingVertical: moderateScale(30),
  },
  backgroundImageChartView: {
    marginTop: calcHeight(230),
    backgroundColor: '#F2F2F2',
    width: deviceWidth,
    alignSelf: 'center',
    alignItems: 'center',
  },
  absoluteImage: {
    position: 'absolute',
    top: calcHeight(40),
  },
  noConnectionRect: {
    backgroundColor: '#F2F2F2',
    width: deviceWidth,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: calcHeight(50),
    marginBottom: calcHeight(50),
  },
  absolutePosition: {
    position: 'absolute',
  },
  underChartsSpace: {
    marginBottom: calcHeight(60),
  },
  sectionDescriptionText: {
    color: GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    marginTop: -calcHeight(15),
  },
  lineSection: {
    height: calcHeight(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginTop: calcHeight(38),
    marginBottom: calcHeight(28),
    alignSelf: 'center',
  },
  greenUnderLineText: {
    textDecorationLine: 'underline',
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
  showMoreLessView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: calcHeight(10),
  },
});
