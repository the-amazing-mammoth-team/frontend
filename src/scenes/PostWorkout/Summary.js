import React, {useCallback, useContext, useEffect, useState} from 'react';
import {
  Image,
  processColor,
  SafeAreaView,
  ScrollView,
  Text,
  Platform,
  TouchableOpacity,
  View,
  BackHandler,
} from 'react-native';
import Svg, {Path} from 'react-native-svg';
import gql from 'graphql-tag/src';
import {useLazyQuery, useQuery} from '@apollo/react-hooks';
import {
  useFocusEffect,
  useIsFocused,
  useNavigationState,
} from '@react-navigation/native';
import {capitalize} from 'lodash';
import {
  GRAY,
  MEDIUM_GRAY,
  LIGHT_GRAY,
  LIGHT_ORANGE,
  ORANGE,
  YELLOW,
} from '../../styles/colors';
import Header from '../../components/header';
import Time from '../../components/Time';
import {calcHeight, calcWidth} from '../../utils/dimensions';
import getOfflineSummary from '../../utils/getSummary';
import {LocalizationContext} from '../../localization/translations';
import EditIcon from '../../assets/icons/editIcon.svg';
import ResultBoard from '../../components/PostWorkout/ResultsBoard';
import {Icon} from 'react-native-elements';
import {styles} from './summaryStyles';
import MagentoEquipment from '../../assets/images/pullUpBar.png';
import TabViewResults from '../../components/PostWorkout/TabViewResults';
import SessionDetailsTable from '../../components/PostWorkout/SessionDetailsTable';
import GenericChart from '../../components/PostWorkout/GenericChart';
import RadarChartComponent from '../../components/ProgressScreen/RadarChart';
import TrySomething from '../../components/PostWorkout/TrySomething';
import SummaryByBlockComponent from '../../components/PostWorkout/SummaryByBlockComponent';
import GradientCard from '../../components/ProfileScreen/GradientCard';
import DiscardWorkoutModal from '../../components/Workout/DiscardWorkoutModal';
import SearchAbsoluteIcon from '../../assets/icons/searchImagePostWorkout.svg';
import DocumentAbsoluteIcon from '../../assets/icons/documentImagePostWorkout.svg';
import TargetAbsoluteIcon from '../../assets/icons/targetImagePostWorkout.svg';
import {useInterval} from '../../services/customHooks';
import {trackEvents} from '../../services/analytics';
import {postWorkoutCodes} from '../../utils/offerCodes';
import {getPreviousRouteFromState} from '../../services/navigation';
import {Loading} from '../../components/Loading';
import {NextButton} from '../../components/buttons';
import {AuthContext} from '../../../App';

const SESSION_SUMMARY_QUERY = gql`
  query getSessionExecutionSummary($sessionExecutionId: ID!, $locale: String!) {
    getSessionExecutionSummary(
      sessionExecutionId: $sessionExecutionId
      locale: $locale
    ) {
      id
      totalKcal
      totalReps
      totalTime
      repsPerMin
      repsPerExercise
      repsPerMinPerExercise
      minsPerExercise
      repsSetPerBlock
      timeSetPerBlock
      averageRepsMinSetPerBlock
      effort
      valueOfSession
      points
      sessionExecution {
        id
        sessionId
        session {
          id
          name
          sessionType
          order
          bodyPartsFocused
        }
        userProgram {
          id
          program {
            id
            name
            codeName
          }
        }
      }
    }
  }
`;

const GET_USER = gql`
  query user($id: ID!, $locale: String!) {
    user(id: $id, locale: $locale) {
      id
      isPro
      weight
      currentProgram {
        id
      }
      userProgram {
        id
        progress
      }
    }
  }
`;

function getOs() {
  return Platform.OS === 'ios' ? 'apple' : 'google_play';
}

const Summary = ({navigation, route}) => {
  const {data, offlineData, prevScreen = ''} = route.params || {};
  const {inputValue, sessionExecutionId} = data;
  const {userData: userInfo} = useContext(AuthContext);
  const {translations, appLanguage} = useContext(LocalizationContext);
  const isFocused = useIsFocused();
  // const inputValue = 3;
  // const difficulty = '1'; // replace back to data
  let [getSummary, {data: summary, error, loading = false}] = useLazyQuery(
    SESSION_SUMMARY_QUERY,
    {
      variables: {sessionExecutionId, locale: appLanguage},
      fetchPolicy: 'network-only',
    },
  );
  const {data: userData, error: errorUser} = useQuery(GET_USER, {
    variables: {id: userInfo.id, locale: appLanguage},
  });

  console.log('sum');
  console.log('err', error);

  console.log('user', userData);
  console.log('err', errorUser);

  if (offlineData && userData) {
    summary = getOfflineSummary(offlineData, userData?.user?.weight);
  }
  const executionSummary = summary?.getSessionExecutionSummary;
  const sessionExecution = executionSummary?.sessionExecution;
  const user = userData?.user;
  const userProgram = user?.userProgram;
  const program = sessionExecution?.userProgram?.program;
  const sessionId = sessionExecution?.sessionId;
  const sessionBodyPartsFocused = sessionExecution?.session?.bodyPartsFocused;
  const session = sessionExecution?.session;
  const currentProgram = user?.currentProgram;
  const isPro = user?.isPro; // replace to test

  useFocusEffect(
    useCallback(() => {
      if (isFocused && program && session) {
        trackEvents({
          eventName: 'Load Post Workout Graphs',
          properties: {
            training_program: program?.codeName,
            session_id: session?.id,
            session_name: session?.name,
            session_type: session?.sessionType,
            session_order: session?.order,
            duration_seconds: executionSummary?.totalTime,
            total_reps: executionSummary?.totalReps,
            block_list: Object.entries(
              executionSummary?.repsSetPerBlock || {},
            )?.map(([key, value]) => {
              return value && value[1]?.block_name;
            }),
            exercise_list: Object.entries(
              executionSummary?.repsPerExercise || {},
            )?.map(([key, value]) => {
              return value.name_en;
            }),
            session_enjoyment: executionSummary?.valueOfSession,
            session_exertion: executionSummary?.effort,
          },
        });
      }
    }, [executionSummary, isFocused, program, session]),
  );

  const [isEditableInput, setIsEditableInput] = useState(false);
  const [feedbackText, setFeedbackText] = useState(
    inputValue ? inputValue : '',
  );
  const [showAllBlocks, setShowAllBlocks] = useState(true);
  const [timer, setTimer] = useState(false);
  const [waitingTime, setWaitingTime] = useState(0);
  const [isModalVisible, setIsModalVisible] = useState(false);

  useEffect(() => {
    //disable go back android
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      () => true,
    );
    () => {
      backHandler.remove();
    };
  }, [navigation]);

  const onEditPress = useCallback(
    () => setIsEditableInput(oldData => !oldData),
    [],
  );

  useInterval(
    () => {
      if (!offlineData) {
        setWaitingTime(prev => ++prev);
        !loading &&
          sessionExecutionId &&
          !summary?.getSessionExecutionSummary &&
          getSummary();
        if (waitingTime > 30) {
          setTimer(false);
          setWaitingTime(0);
        }
      }
    },
    timer ? 1000 : null,
  );

  useEffect(() => {
    if (
      !loading &&
      summary?.getSessionExecutionSummary &&
      timer &&
      waitingTime
    ) {
      setTimer(false);
      setWaitingTime(0);
    }
  }, [loading, navigation, summary, timer, waitingTime]);

  const startTimer = useCallback(() => {
    setWaitingTime(0);
    setTimer(true);
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      startTimer();
    });
    return unsubscribe;
  }, [navigation, startTimer]);

  const toggleModal = useCallback(() => {
    if (!isModalVisible) {
      //on open
      trackEvents({
        eventName: 'Load Discard Workout Popup',
        properties: {
          training_program: program?.codeName,
          session_id: session?.id,
          session_name: session?.name,
          session_type: session?.sessionType,
          session_order: session?.order,
          discard_placement: 'Post Workout Graphs',
        },
      });
    }
    setIsModalVisible(!isModalVisible);
  }, [isModalVisible, program, session]);

  const handleLaterButtonModal = useCallback(() => {
    toggleModal();
    trackEvents({
      eventName: 'Discard Workout',
      properties: {
        training_program: program?.codeName,
        session_id: session?.id,
        session_name: session?.name,
        session_type: session?.sessionType,
        session_order: session?.order,
        placement: 'Post Workout Graphs',
      },
    });
    navigation.navigate('HomeStack', {
      screen: 'Home',
      params: {doRefetch: true},
    });
  }, [navigation, program, session, toggleModal]);

  const handleDeleteButtonModal = useCallback(() => {
    toggleModal();
    navigation.navigate('Feedback', {
      sessionId: sessionId,
      programId: program?.id,
      programCodeName: program?.codeName,
      session,
      prevScreen: 'Post Workout Graphs',
    });
  }, [navigation, program, session, sessionId, toggleModal]);

  const mockInfoForPRO = [
    {
      id: 1,
      title: translations.postWorkoutScreen.summaryExercises,
      description: translations.postWorkoutScreen.summaryExercicesDescription,
      colors: [LIGHT_ORANGE, ORANGE],
      icon: SearchAbsoluteIcon,
    },
    {
      id: 2,
      title: translations.postWorkoutScreen.summaryBlock,
      description: translations.postWorkoutScreen.summaryBlockDescription,
      colors: [YELLOW, LIGHT_ORANGE],
      icon: DocumentAbsoluteIcon,
    },
    {
      id: 3,
      title: translations.postWorkoutScreen.yourTribe,
      description: translations.postWorkoutScreen.yourTribeDescription,
      colors: [MEDIUM_GRAY, '#616362'],
      icon: TargetAbsoluteIcon,
    },
  ];

  const renderSectionTitle = useCallback(
    title => (
      <View style={styles.sectionTextView}>
        <Text style={styles.sectionText}>{title}</Text>
      </View>
    ),
    [],
  );

  const renderEquipment = useCallback(
    (name, image) => {
      return (
        <View style={styles.equipmentView}>
          {name && image ? (
            <>
              <Image source={image} style={styles.equipmentImage} />
              <Text style={styles.equipmentName}>{name}</Text>
            </>
          ) : (
            <Text style={[styles.equipmentName, styles.noEquipment]}>
              {translations.postWorkoutScreen.noEquipment}
            </Text>
          )}
        </View>
      );
    },
    [translations.postWorkoutScreen.noEquipment],
  );

  const goToPreviousScreen = useCallback(() => {
    navigation.pop();
  }, [navigation]);

  const onPressNext = useCallback(() => {
    userProgram?.progress >= 100
      ? navigation.navigate('FinishProgram', {
          userProgramId: userProgram.id,
          programName: program.name,
          codeName: program.codeName,
        })
      : navigation.navigate('HomeStack', {
          screen: 'Home',
          params: {doRefetch: true},
        });
  }, [navigation, program, userProgram]);

  const startAsPro = useCallback(() => {
    isPro
      ? navigation.navigate('HomeStack', {
          screen: 'Home',
          params: {doRefetch: true},
        })
      : navigation.navigate('ProgramBuy', {
          program: currentProgram,
          prevScreen: 'Post Workout Graphs',
          offerCodes: postWorkoutCodes,
        });
  }, [currentProgram, isPro, navigation]);

  let totalKcal, totalReps, totalTime, repsPerMin, effort, exersion, points;
  if (sessionExecution && summary.getSessionExecutionSummary && user) {
    totalKcal = summary.getSessionExecutionSummary.totalKcal;
    totalReps = summary.getSessionExecutionSummary.totalReps;
    totalTime = summary.getSessionExecutionSummary.totalTime;
    repsPerMin = summary.getSessionExecutionSummary.repsPerMin;
    effort = summary.getSessionExecutionSummary.effort;
    exersion = summary.getSessionExecutionSummary.valueOfSession;
    points = summary.getSessionExecutionSummary.points;
  }

  const radarChartData = sessionBodyPartsFocused?.map(item => {
    return {
      name: `${capitalize(item.name || 'other')}`,
      value: item.value,
    };
  });

  const isLegacySession =
    !summary?.getSessionExecutionSummary?.repsPerMin &&
    !summary?.getSessionExecutionSummary?.repsPerExercise &&
    !summary?.getSessionExecutionSummary?.repsPerMinPerExercise &&
    !summary?.getSessionExecutionSummary?.minsPerExercise &&
    !summary?.getSessionExecutionSummary?.repsSetPerBlock &&
    !summary?.getSessionExecutionSummary?.timeSetPerBlock &&
    !summary?.getSessionExecutionSummary?.averageRepsMinSetPerBlock;

  console.log({summary});

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView
        keyboardShouldPersistTaps={'always'}
        showsVerticalScrollIndicator={false}>
        <DiscardWorkoutModal
          title={translations.settingsScreen.discardWorkout}
          subTitle={translations.settingsScreen.discardWorkoutDesc}
          goBack={handleLaterButtonModal}
          goBackText={translations.settingsScreen.repeatLater}
          goNext={handleDeleteButtonModal}
          goNextText={translations.settingsScreen.deleteTraining}
          visible={isModalVisible}
          closeModal={toggleModal}
        />
        <Header
          title={translations.postWorkoutScreen.summary}
          onPressBackButton={
            prevScreen !== 'Enjoyment' ? goToPreviousScreen : undefined
          }
        />
        <View style={styles.line} />
        {summary?.getSessionExecutionSummary ? (
          <View style={styles.container}>
            <Text style={styles.grayText}>
              {program.name} |{' '}
              {session?.name ||
                translations.exerciseScreen.session + ' ' + session?.order}
            </Text>
            {/* <View style={styles.verticalDistanceHeaderLittle}>
              <Text style={styles.blackText}>Session 3, block 4</Text>
            </View> */}
            {/* <View style={styles.inputView}>
              <TextInput
                editable={isEditableInput}
                multiline={true}
                value={feedbackText}
                style={styles.textInputStyle}
                onChangeText={e => setFeedbackText(e)}
              />
              <TouchableOpacity
                hitSlop={styles.hitSlopFifteen}
                onPress={onEditPress}
                style={styles.editIconView}>
                <EditIcon
                  width={calcWidth(17)}
                  height={calcHeight(17)}
                  fill={LIGHT_GRAY}
                />
              </TouchableOpacity>
            </View> */}

            <GradientCard
              totalKcal={totalKcal || '='}
              totalReps={totalReps || '='}
              mediumPace={repsPerMin || '='}
              totalTime={<Time count={totalTime} /> || '='}
              noGradient={true}
            />

            <View style={styles.boardView}>
              <ResultBoard
                effort={effort ? `${effort}/10` : '='}
                exersion={exersion ? `${exersion}/5` : '='}
                points={points || '='}
              />
            </View>

            {!isLegacySession && (
              <>
                {renderSectionTitle(
                  translations.postWorkoutScreen.muscleGroupsTrained,
                )}
                <Text style={styles.grayText}>
                  {
                    translations.postWorkoutScreen
                      .muscleGroupsTrainedDescription
                  }
                </Text>
                <RadarChartComponent data={radarChartData} />

                {!isPro ? (
                  <TrySomething
                    data={mockInfoForPRO}
                    description={translations.postWorkoutScreen.advancedAccess}
                    buttonTitle={translations.postWorkoutScreen.startNow}
                    callback={startAsPro}
                  />
                ) : (
                  <>
                    {renderSectionTitle(
                      translations.postWorkoutScreen.summaryExercices,
                    )}
                    <View style={styles.underChartsSpace}>
                      <TabViewResults summary={summary} />
                    </View>
                    {renderSectionTitle(
                      translations.postWorkoutScreen.summaryBlock,
                    )}
                    {Object.keys(
                      summary?.getSessionExecutionSummary?.repsSetPerBlock ||
                        {},
                    ).map((key, index) => {
                      if (index > 1 && showAllBlocks) {
                        return null;
                      }
                      return (
                        <>
                          <SummaryByBlockComponent
                            key={key}
                            blockNumber={key}
                            blockName={
                              summary?.getSessionExecutionSummary
                                ?.repsSetPerBlock[key][1]?.block_name
                            }
                            summary={summary}
                          />
                        </>
                      );
                    })}
                    {Object.keys(
                      summary?.getSessionExecutionSummary?.repsSetPerBlock ||
                        {},
                    ).length > 1 && (
                      <TouchableOpacity
                        style={styles.showMoreLessView}
                        onPress={() => setShowAllBlocks(!showAllBlocks)}>
                        <Text style={styles.greenUnderLineText}>
                          {!showAllBlocks
                            ? translations.workoutScreen.seeLess
                            : translations.workoutScreen.seeMore}
                        </Text>
                        <TouchableOpacity
                          onPress={() => setShowAllBlocks(!showAllBlocks)}
                          style={{
                            transform: [
                              {rotate: showAllBlocks ? '-90deg' : '90deg'},
                            ],
                            marginLeft: calcWidth(10),
                          }}>
                          <Svg
                            width={calcWidth(11)}
                            height={calcHeight(11)}
                            viewBox="0 0 9 13"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <Path
                              d="M7.5 12L2 6.5L7.5 1"
                              stroke={LIGHT_ORANGE}
                              stroke-width="1.5"
                            />
                          </Svg>
                        </TouchableOpacity>
                      </TouchableOpacity>
                    )}
                    <View style={styles.lineSection} />
                    {/* {renderGenericCompareResultsComponent(
                  translations.postWorkoutScreen.historical,
                  translations.postWorkoutScreen.historicalDescription,
                  true,
                )}
                {renderGenericCompareResultsComponent(
                  translations.postWorkoutScreen.yourTribe,
                  translations.postWorkoutScreen.yourTribeSectionDescription,
                )} */}
                    {/* {renderSectionTitle(
                  translations.postWorkoutScreen.sessionDetail,
                )}
                <View style={styles.underChartsSpace}>
                  <SessionDetailsTable data={mockSessionDetails} />
                </View>
                {renderSectionTitle(
                  translations.postWorkoutScreen.equipmentUsed,
                )}
                <View style={styles.equipmentRowView}>
                  {renderEquipment('Pull-up bar', MagentoEquipment)}
                </View> */}
                  </>
                )}
              </>
            )}

            <TouchableOpacity
              hitSlop={styles.hitSlopFifteen}
              style={styles.discardView}
              onPress={toggleModal}>
              <Text style={styles.discardText}>
                {translations.postWorkoutScreen.discardWorkout}
              </Text>
            </TouchableOpacity>
          </View>
        ) : (
          <Loading />
        )}
      </ScrollView>
      {prevScreen === 'Enjoyment' && <NextButton onPress={onPressNext} />}
    </SafeAreaView>
  );
};

export default Summary;
