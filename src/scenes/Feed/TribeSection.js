import {FlatList, StyleSheet, View} from 'react-native';
import React, {useCallback, useState} from 'react';
import TrackingOptionsModal from '../../components/FeedScreen/TrackingOptionsModal';
import mockTribeFeeds from './mockTribeFeeds';
import FeedCard from '../../components/FeedScreen/FeedCard';
import {WHITE} from '../../styles/colors';

const TribeSection = ({navigation}) => {
  const [info, setInfo] = useState({isModalVisible: false});

  const showModal = useCallback(
    options =>
      setInfo({
        isModalVisible: true,
        trackingOptions: options,
      }),
    [],
  );

  const hideModal = useCallback(() => setInfo({isModalVisible: false}), []);

  const onPressItem = useCallback(
    options => {
      showModal(options);
    },
    [showModal],
  );

  const renderItem = useCallback(
    ({item}) => (
      <FeedCard
        item={item}
        isThreeDots={true}
        onPressThreeDots={onPressItem}
        isFriendsSection={true}
        onPressComment={() => {
          navigation.navigate('Comments', {
            author: item.author,
            date: item.date,
            comments: item.comments,
          });
        }}
      />
    ),
    [navigation, onPressItem],
  );

  return (
    <View style={styles.thirdRouteView}>
      {info.isModalVisible && (
        <TrackingOptionsModal
          isModalVisible={info.isModalVisible}
          trackingOptions={info.trackingOptions}
          onDismiss={hideModal}
        />
      )}
      <FlatList
        data={mockTribeFeeds}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  thirdRouteView: {
    flex: 1,
    backgroundColor: WHITE,
  },
});

export default TribeSection;
