const mockTribeFeeds = [
  {
    id: 1,
    author: 'Juan Carlos Fernandez',
    date: '25/09/2020',
    typeOfTraining: 'Interval with pauses',
    generalTrainingInfo: {
      week: 1,
      typeOfProgram: 'Unbreakable',
      companyName: 'Mammoth Hunters',
    },
    trackingOptions: [],
    detailTrainingInfo: {
      minutesCount: 12,
      repsCount: 85,
      kcalCount: 105,
    },
    comments: [
      {
        id: 1,
        author: 'Pedro Perez',
        date: '25/10/2020',
        text: 'Muy cool!',
      },
      {
        id: 2,
        author: 'Marta Navarro',
        date: '27/10/2020',
        text: 'wow.',
      },
      {
        id: 3,
        author: 'Pedro Perez',
        date: '25/10/2020',
        text: 'Muy cool!',
      },
      {
        id: 4,
        author: 'Marta Navarro',
        date: '27/10/2020',
        text: 'wow.',
      },
      {
        id: 5,
        author: 'Pedro Perez',
        date: '25/10/2020',
        text: 'Muy cool!',
      },
      {
        id: 6,
        author: 'Marta Navarro',
        date: '27/10/2020',
        text: 'wow.',
      },
    ],
  },
  {
    id: 2,
    author: 'Pedro Perez',
    date: '23/07/2020',
    typeOfTraining: 'Hurricane',
    generalTrainingInfo: {
      week: 1,
      typeOfProgram: 'Unbreakable',
      companyName: 'Mammoth Hunters',
    },
    trackingOptions: [],
    detailTrainingInfo: {
      minutesCount: 12,
      repsCount: 85,
      kcalCount: 105,
    },
    comments: [
      {
        id: 1,
        author: 'Pedro Perez',
        date: '25/10/2020',
        text: 'Muy cool!',
      },
      {
        id: 2,
        author: 'Marta Navarro',
        date: '27/10/2020',
        text: 'wow.',
      },
    ],
  },
  {
    id: 3,
    author: 'Marta Navarro',
    date: '1/07/2020',
    typeOfTraining: 'Tabata',
    generalTrainingInfo: {
      week: 2,
      typeOfProgram: 'Unbreakable',
      companyName: 'Revolutionary Fitness',
    },
    trackingOptions: [],
    detailTrainingInfo: {
      minutesCount: 12,
      repsCount: 85,
      kcalCount: 105,
    },
    comments: [
      {
        id: 1,
        author: 'Pedro Perez',
        date: '25/10/2020',
        text: 'Muy cool!',
      },
      {
        id: 2,
        author: 'Marta Navarro',
        date: '27/10/2020',
        text: 'wow.',
      },
    ],
  },
  {
    id: 4,
    author: 'Marta Navarro',
    date: '1/07/2020',
    typeOfTraining: 'Active rest',
    trackingOptions: ['Silence'],
    detailTrainingInfo: {
      minutesCount: 12,
      repsCount: 85,
      kcalCount: 105,
    },
    comments: [
      {
        id: 1,
        author: 'Pedro Perez',
        date: '25/10/2020',
        text: 'Muy cool!',
      },
      {
        id: 2,
        author: 'Marta Navarro',
        date: '27/10/2020',
        text: 'wow.',
      },
    ],
  },
];

export default mockTribeFeeds;
