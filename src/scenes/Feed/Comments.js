import {
  FlatList,
  Keyboard,
  processColor,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useCallback, useContext, useState} from 'react';
import moment from 'moment';
import {BarChart} from 'react-native-charts-wrapper';
import PropTypes from 'prop-types';
import Header from '../../components/header';
import {
  GRAY,
  GRAY_BORDER,
  LIGHT_GRAY,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import {SafeAreaContext, SafeAreaView} from 'react-native-safe-area-context';

const Comments = ({navigation, route}) => {
  const handleGoBack = useCallback(() => navigation.goBack(), [navigation]);
  const {translations} = useContext(LocalizationContext);

  const {author, date, comments} = route.params;

  console.log(999, route.params);

  const [input, setInput] = useState('');

  const renderItem = useCallback(comment => {
    const {item} = comment;
    return (
      <View key={item.id} style={styles.commentDistance}>
        <View style={styles.headerRoot}>
          <View style={styles.noImageCircle} />
          <View>
            <View style={styles.rowView}>
              <Text style={styles.commentAuthorText}>{item.author} - </Text>
              <Text style={styles.commentAuthorText}>{item.date}</Text>
            </View>
            <View style={styles.commentTexView}>
              <Text style={styles.commentText}>{item.text}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }, []);

  const onSend = useCallback(() => {
    comments.push({
      id: Math.random(),
      date: moment(new Date()).format('L'),
      author: 'Juan Carlos Fernandez',
      text: input,
    });
    setInput('');
    Keyboard.dismiss();
  }, [comments, input]);

  return (
    <SafeAreaView style={styles.root}>
      <Header onPressBackButton={handleGoBack} title={'Comments'} />
      <View style={styles.headerLine} />
      <View style={styles.headerRoot}>
        <View style={styles.noImageCircle} />
        <View style={styles.infoView}>
          <Text style={styles.authorText}>{author}</Text>
          <Text style={styles.dateText}>{date}</Text>
        </View>
      </View>
      <View style={styles.chartView}>
        <BarChart
          style={{height: calcHeight(230)}}
          drawBorders={false}
          legend={{
            fontFamily: SUB_TITLE_FONT,
            horizontalAlignment: 'CENTER',
          }}
          chartDescription={{text: ''}}
          xAxis={{
            drawGridLines: false,
            drawAxisLine: false,
            drawLabels: false,
          }}
          yAxis={{
            left: {
              drawAxisLine: false,
            },
            right: {
              drawAxisLine: false,
              drawLabels: false,
            },
          }}
          scaleEnabled={false}
          dragEnabled={false}
          pinchZoom={false}
          doubleTapToZoomEnabled={false}
          noDataText={'Opps... no data available!'}
          data={{
            dataSets: [
              {
                label: 'Series 1',
                values: [
                  {x: 1, y: 8},
                  {x: 5, y: 5},
                  {x: 9, y: 8},
                  {x: 13, y: 5},
                  {x: 17, y: 8},
                  {x: 21, y: 5},
                  {x: 25, y: 8},
                  {x: 29, y: 5},
                ],
                config: {
                  color: processColor(ORANGE),
                  valueTextColor: processColor('transparent'),
                },
              },
              {
                label: 'Series 2',
                values: [
                  {x: 2, y: 20},
                  {x: 6, y: 15},
                  {x: 10, y: 22},
                  {x: 14, y: 20},
                  {x: 18, y: 30},
                  {x: 22, y: 25},
                  {x: 26, y: 60},
                  {x: 30, y: 60},
                ],
                config: {
                  color: processColor(LIGHT_ORANGE),
                  valueTextColor: processColor('transparent'),
                },
              },
              {
                label: 'Series 3',
                values: [
                  {x: 3, y: 23},
                  {x: 7, y: 23},
                  {x: 11, y: 22},
                  {x: 15, y: 18},
                  {x: 19, y: 16},
                  {x: 23, y: 15},
                  {x: 27, y: 8},
                  {x: 31, y: 5},
                ],
                config: {
                  color: processColor(GRAY),
                  valueTextColor: processColor('transparent'),
                },
              },
            ],
          }}
        />
      </View>
      <View style={styles.line} />
      <FlatList
        data={comments}
        renderItem={renderItem}
        showsVerticalScrollIndicator={false}
        keyExtractor={item => item.id.toString()}
        style={styles.flatListStyle}
      />

      <View style={styles.inputView}>
        <TextInput
          value={input}
          onChangeText={value => setInput(value)}
          style={styles.inputStyles}
          placeholder={translations.feedScreen.addComment}
        />
        <TouchableOpacity onPress={onSend}>
          <Text style={styles.sendText}>{translations.feedScreen.send}</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

Comments.propTypes = {
  navigation: PropTypes.object,
  route: PropTypes.object,
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerRoot: {
    flexDirection: 'row',
    alignItems: 'stretch',
    paddingHorizontal: calcWidth(20),
  },
  noImageCircle: {
    backgroundColor: '#C4C4C4',
    width: moderateScale(40),
    height: moderateScale(40),
    borderRadius: moderateScale(40) / 2,
    marginRight: calcWidth(13),
  },
  infoView: {
    justifyContent: 'space-around',
  },
  authorText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    fontWeight: '600',
  },
  dateText: {
    fontSize: calcFontSize(12),
    fontFamily: SUB_TITLE_FONT,
    color: LIGHT_GRAY,
  },
  line: {
    backgroundColor: GRAY_BORDER,
    width: deviceWidth,
    marginTop: calcHeight(15),
    //marginBottom: calcHeight(15),
    height: calcHeight(1),
  },
  commentAuthorText: {
    color: LIGHT_GRAY,
    fontSize: calcFontSize(12),
    fontFamily: SUB_TITLE_FONT,
  },
  commentText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  sendText: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
  },
  inputView: {
    width: '100%',
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: calcWidth(20),
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 999,
    borderWidth: 1,
    borderColor: GRAY_BORDER,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    backgroundColor: WHITE,
  },
  inputStyles: {
    flex: 1,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
  },
  flatListStyle: {
    paddingTop: calcHeight(15),
  },
  commentTexView: {
    maxWidth: calcWidth(250),
  },
  commentDistance: {
    marginBottom: calcHeight(20),
  },
  headerLine: {
    height: moderateScale(1),
    backgroundColor: GRAY_BORDER,
    width: '100%',
    marginTop: -20,
    marginBottom: moderateScale(20),
  },
  chartView: {
    width: deviceWidth * 0.95,
    alignSelf: 'center',
  },
});

export default Comments;
