import React, {useCallback, useContext, useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import UsersPlusIcon from '../../assets/icons/usersPlusIcon.svg';
import {
  calcHeight,
  calcWidth,
  calcFontSize,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {BLACK, LIGHT_GRAY, LIGHT_ORANGE, WHITE} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import YourSection from './YourSection';
import TribeSection from './TribeSection';
import LearnSection from './LearnSection';
import {Loading} from '../../components/Loading';

const Feed = ({navigation}) => {
  const {translations} = useContext(LocalizationContext);
  const [index, setIndex] = useState(0);
  const [isLoaderVisible, setIsLoaderVisible] = useState(true);
  const [routes] = useState([
    {key: 'first', title: translations.feedScreen.friends},
    {key: 'second', title: translations.feedScreen.your},
    {key: 'third', title: translations.feedScreen.learn},
  ]);

  const FirstRoute = useCallback(
    () => <TribeSection navigation={navigation} />,
    [navigation],
  );

  const SecondRoute = useCallback(() => <YourSection />, []);

  const ThirdRoute = useCallback(() => <LearnSection />, []);

  useEffect(() => {
    setIsLoaderVisible(false);
  }, []);

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
    third: ThirdRoute,
  });
  const initialLayout = deviceWidth;

  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={styles.indicatorStyle}
      style={styles.tabBarStyle}
      renderLabel={({route, focused, color}) => (
        <Text
          style={[
            styles.renderLabelText,
            focused && styles.renderLabelTextFocused,
          ]}>
          {route.title}
        </Text>
      )}
    />
  );

  return (
    <SafeAreaView>
      <Text>Under development</Text>
    </SafeAreaView>
  );

  // return (
  //   <SafeAreaView style={styles.safeAreaRoot}>
  //     <View style={styles.header}>
  //       <View style={styles.userPlusIconView}>
  //         <TouchableOpacity onPress={() => {}} hitSlop={styles.hitSlopTen}>
  //           <UsersPlusIcon width={calcWidth(26)} height={calcHeight(26)} />
  //         </TouchableOpacity>
  //       </View>
  //       <Text style={styles.titleScreen}>Feed</Text>
  //     </View>
  //     {!isLoaderVisible ? (
  //       <TabView
  //         navigationState={{index, routes}}
  //         renderScene={renderScene}
  //         onIndexChange={setIndex}
  //         initialLayout={initialLayout}
  //         renderTabBar={renderTabBar}
  //       />
  //     ) : (
  //       <Loading />
  //     )}
  //   </SafeAreaView>
  // );
};

const styles = StyleSheet.create({
  header: {
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: calcWidth(20),
    backgroundColor: WHITE,
  },
  titleScreen: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
    paddingVertical: moderateScale(17),
    textAlign: 'center',
  },
  hitSlopTen: {
    top: calcHeight(10),
    bottom: calcHeight(10),
    right: calcWidth(10),
    left: calcWidth(10),
  },
  indicatorStyle: {
    backgroundColor: LIGHT_ORANGE,
  },
  tabBarStyle: {
    backgroundColor: 'white',
    elevation: 0,
  },
  renderLabelText: {
    fontFamily: SUB_TITLE_FONT,
    color: LIGHT_GRAY,
    fontSize: calcFontSize(16),
  },
  renderLabelTextFocused: {
    color: BLACK,
  },
  safeAreaRoot: {
    flex: 1,
    backgroundColor: WHITE,
  },
  userPlusIconView: {
    position: 'absolute',
    left: moderateScale(20),
  },
});

export default Feed;
