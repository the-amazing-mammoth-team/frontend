import React from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import FeedCard from '../../components/FeedScreen/FeedCard';
import mockFeeds from './mockFeeds';
import CustomTextInput from '../../components/CustomTextInput';
import {calcHeight, calcWidth, moderateScale} from '../../utils/dimensions';
import {WHITE} from '../../styles/colors';

const LearnSection = () => {
  const renderItem = ({item}) => <FeedCard item={item} isChart={false} />;

  return (
    <View style={styles.scrollView}>
      <FlatList
        data={mockFeeds}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        initialNumToRender={2}
        showsVerticalScrollIndicator={false}
        ListHeaderComponent={() => {
          return (
            <View style={[styles.genericHorizontalPadding, styles.inputView]}>
              <CustomTextInput />
            </View>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: WHITE,
  },
  genericHorizontalPadding: {
    paddingHorizontal: calcWidth(20),
  },
  inputView: {
    marginTop: moderateScale(20),
  },
});

export default LearnSection;
