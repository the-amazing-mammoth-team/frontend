import MeatImage from '../../assets/images/meatImage.png';
import SweetFoodImage from '../../assets/images/sweeFood.png';
import NightFoodImage from '../../assets/images/nightFood.png';
const mockFeeds = [
  {
    id: 1,
    author: 'Nestor Sanchez',
    authorImage: false,
    date: '5 august 2020',
    image: MeatImage,
    feedTitle: 'Proteins on the ketogenic diet',
    feedDescription:
      'In the ketogenic diet, protein is changing prominence according to',
  },
  {
    id: 2,
    author: 'Nestor Sanchez',
    authorImage: false,
    date: '5 august 2020',
    image: SweetFoodImage,
    feedTitle: 'Prohibited foods on the ketogenic diet',
    feedDescription:
      'In the ketogenic diet, protein is changing prominence according to',
  },
  {
    id: 3,
    author: 'Nestor Sanchez',
    authorImage: false,
    date: '5 august 2020',
    image: NightFoodImage,
    feedTitle: 'Is it good to eat carbohydrates at night?',
    feedDescription:
      'For weeks there has been an intense debate about whether it is good',
  },
];

export default mockFeeds;
