import {ScrollView, StyleSheet} from 'react-native';
import React from 'react';
import FeedCard from '../../components/FeedScreen/FeedCard';
import mockTribeFeeds from './mockTribeFeeds';
import {WHITE} from '../../styles/colors';

const YourSection = () => {
  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={styles.secondRouteView}>
      <FeedCard item={mockTribeFeeds[0]} isChart={true} isYourSection={true} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  secondRouteView: {
    flex: 1,
    backgroundColor: WHITE,
  },
});

export default YourSection;
