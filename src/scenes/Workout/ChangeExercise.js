import React, {useState, useContext, useEffect} from 'react';
import {SafeAreaView, StyleSheet, View, FlatList} from 'react-native';
import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import Header from '../../components/header';
import {Loading} from '../../components/Loading';
import Exercise from '../../components/Workout/Exercise';
import {WHITE, GRAY_BORDER} from '../../styles/colors';
import {calcWidth, moderateScale} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import {AuthContext} from '../../../App';

const GET_EXERCISES = gql`
  query getExerciseData($id: ID!, $locale: String!) {
    getExerciseData(id: $id, locale: $locale) {
      id
      name
      thumbnail
      easierVariation {
        id
        name
        thumbnail
        video
      }
      harderVariation {
        id
        name
        thumbnail
        video
      }
    }
  }
`;

const ChangeExercise = ({navigation, route}) => {
  const {
    exerciseId,
    workoutSessions,
    setWorkoutSessions,
    warmUpSessions,
    setWarmUpSessions,
    currentWarmUpSession,
    setCurrentWarmUpSession,
    typeSession,
  } = route.params;
  console.log('params', workoutSessions);
  const {translations, appLanguage} = useContext(LocalizationContext);
  const {userData: userInfo} = useContext(AuthContext);
  const [currentExerciseId, setCurrentExerciseId] = useState(exerciseId);
  const {data, error, loading} = useQuery(GET_EXERCISES, {
    variables: {id: exerciseId, locale: appLanguage},
    context: {
      headers: {
        authorization: userInfo?.token,
      },
    },
  });
  const res = data?.getExerciseData;
  const exercises = res
    ? [
        res.easierVariation,
        {id: res.id, name: res.name, thumbnail: res.thumbnail},
        res.harderVariation,
      ].filter(Boolean)
    : [];

  const updateSessionsBlock = (block, newExercise) => {
    block.forEach(el => {
      Object.values(el.allSets).forEach((set, index) => {
        set.allExercises.forEach(el => {
          if (el.exercise.id === exerciseId) {
            setCurrentExerciseId(newExercise.id);
            el.exercise = {...newExercise};
          }
        });
      });
    });
  };

  const updateWorkoutSessions = newExercise => {
    const cloneArray = JSON.parse(JSON.stringify(workoutSessions));
    updateSessionsBlock(cloneArray, newExercise);
    setWorkoutSessions(cloneArray);
  };

  const updateWarmUpSessions = newExercise => {
    const cloneArray = JSON.parse(JSON.stringify(warmUpSessions));
    cloneArray.forEach(warmUpSession => {
      updateSessionsBlock(warmUpSession, newExercise);
    });
    setWarmUpSessions(cloneArray);
  };

  const updateCurrentWarmUpSessions = newExercise => {
    const cloneArray = JSON.parse(JSON.stringify(currentWarmUpSession));
    updateSessionsBlock(cloneArray, newExercise);
    setCurrentWarmUpSession(cloneArray);
  };

  const handleChange = newExercise => {
    switch (typeSession) {
      case 'workout': {
        updateWorkoutSessions(newExercise);
        setTimeout(() => navigation.goBack(), 500);
        break;
      }
      case 'warmUp': {
        updateCurrentWarmUpSessions(newExercise);
        updateWarmUpSessions(newExercise);
        setTimeout(() => navigation.goBack(), 500);
        break;
      }
      default:
        break;
    }
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <>
        <Header
          title={translations.workoutScreen.changeExercise}
          onPressBackButton={() => {
            navigation.goBack();
          }}
        />
        <View style={styles.line} />
        <View style={styles.container}>
          {!loading ? (
            <FlatList
              data={exercises}
              renderItem={({item}) => (
                <Exercise
                  item={item}
                  navigation={navigation}
                  currentExerciseId={currentExerciseId}
                  onChangeExercise={handleChange}
                />
              )}
              extraData={exercises}
              keyExtractor={item => item.id}
              style={styles.listExercises}
              showsVerticalScrollIndicator={false}
            />
          ) : (
            <Loading />
          )}
        </View>
      </>
    </SafeAreaView>
  );
};

export default ChangeExercise;

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: WHITE,
  },
  container: {
    paddingBottom: calcWidth(20),
    marginHorizontal: calcWidth(20),
    backgroundColor: WHITE,
  },
  line: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginBottom: moderateScale(15),
    marginTop: moderateScale(-15),
  },
  listExercises: {
    backgroundColor: WHITE,
    marginVertical: moderateScale(15),
  },
});
