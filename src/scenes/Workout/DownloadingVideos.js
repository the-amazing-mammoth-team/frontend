import React, {useEffect, useState, useCallback, useContext} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
// import Mixpanel from 'react-native-mixpanel';
import {MAIN_TITLE_FONT} from '../../styles/fonts';
import {WHITE, ORANGE} from '../../styles/colors';
import {Loading} from '../../components/Loading';
import {getVideos} from '../../services/downloadingVideos';
import {LocalizationContext} from '../../localization/translations';

// const MIXPANEL_TOKEN = 'bf47184b8fc05bef8c88adb01de19387';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE,
  },
  innerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  text: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: 20,
    marginTop: 30,
  },
  content: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: 25,
  },
  bottomButton: {
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    marginTop: 15,
    marginBottom: 35,
    marginRight: 20,
    backgroundColor: WHITE,
  },
  nextButtonStyleActive: {
    borderRadius: 50,
    height: 50,
    width: 50,
    backgroundColor: ORANGE,
    borderColor: WHITE,
  },
  remainingTime: {
    fontSize: 80,
  },
  loadingVideos: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  loadingText: {
    fontSize: 20,
    fontFamily: MAIN_TITLE_FONT,
    textAlign: 'center',
  },
  loadingPhrases: {
    textAlign: 'center',
    marginTop: 40,
    justifyContent: 'center',
  },
  loadingTextContainer: {
    marginBottom: 30,
  },
});

const DownloadingVideos = ({navigation, route}) => {
  const {translations} = useContext(LocalizationContext);
  const phrases = translations.DownloadingVideosScreen.phrases;
  const phrase = phrases[Math.floor(Math.random() * phrases.length)];
  const [downloadedVideos, setDownloadedVideos] = useState([]);
  const [downloadedBytes, setDownloadedBytes] = useState(0);
  const [bytesToDownload, setBytesToDownload] = useState(0);
  const {
    wo,
    warmups,
    cooldowns,
    isWarmUps,
    isCoolDown = false,
    workoutSettingSound,
    sessionId,
    programId,
    programCodeName,
    session,
    saveSession,
  } = route?.params || {};
  let currentSessions;
  if (isWarmUps) {
    currentSessions = warmups;
  } else if (isCoolDown) {
    currentSessions = cooldowns;
  } else {
    currentSessions = wo;
  }
  const isFocused = useIsFocused();

  const getWoWithDownloadedVideos = useCallback(() => {
    const findVideo = videoUrl => {
      const video = downloadedVideos.find(videoObj => {
        return videoUrl === videoObj.videoUrl;
      });
      return video && video.path;
    };

    const result = currentSessions.map(sessionBlock => {
      const allSetsWithDownloadedVideos = {};
      Object.keys(sessionBlock.allSets).map(index => {
        allSetsWithDownloadedVideos[index] = {
          ...sessionBlock.allSets[index],
          allExercises: sessionBlock.allSets[index].allExercises.map(
            exercise => {
              return {
                ...exercise,
                exercise: {
                  ...exercise.exercise,
                  video: findVideo(exercise.exercise.video),
                  isWarmUp: isWarmUps,
                  isCoolDown: isCoolDown,
                },
              };
            },
          ),
        };
      });
      return {
        ...sessionBlock,
        allSets: allSetsWithDownloadedVideos,
      };
    });
    return result;
  }, [currentSessions, downloadedVideos, isCoolDown, isWarmUps]);

  const setProgressDownloading = useCallback((downloaded, all) => {
    setDownloadedBytes(downloaded);
    setBytesToDownload(all);
  }, []);

  useEffect(() => {
    (async () => {
      if (isFocused) {
        const res = await getVideos(currentSessions, setProgressDownloading);
        setDownloadedVideos(res);
      }
    })();
  }, [currentSessions, isFocused, setProgressDownloading]);

  useEffect(() => {
    const allVideosReady =
      downloadedVideos.length &&
      downloadedVideos.every(videoObj => {
        return !!videoObj.path && !!videoObj.videoUrl;
      });
    if (allVideosReady) {
      const updatedWorkout = getWoWithDownloadedVideos();
      setDownloadedVideos([]);
      navigation.navigate('Countdown', {
        wo: updatedWorkout,
        sessionId,
        programId,
        nextWorkout: isWarmUps ? wo : cooldowns,
        workoutSettingSound,
        isWarmUps,
        isCoolDown,
        session,
        programCodeName,
        saveSession,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [downloadedVideos, currentSessions]);

  const progress = ((downloadedBytes * 100) / bytesToDownload).toFixed(1);

  return (
    <View style={styles.loadingVideos}>
      <View style={styles.loadingTextContainer}>
        <Text style={styles.loadingText}>
          {translations.DownloadingVideosScreen.downloading}
        </Text>
        <Text style={styles.loadingText}>
          {downloadedBytes.toFixed(1)}/{bytesToDownload.toFixed(1)}Mb{' '}
          {!isNaN(progress) ? progress : 0}%
        </Text>
        <Loading noMargin />
      </View>
    </View>
  );
};

export default DownloadingVideos;
