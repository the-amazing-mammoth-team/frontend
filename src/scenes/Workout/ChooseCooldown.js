import React, {useContext, useState} from 'react';
import {ScrollView, View, Text, StyleSheet, SafeAreaView} from 'react-native';
import {Button} from 'react-native-elements';
import {TouchableOpacity} from 'react-native-gesture-handler';
import CooldownModalCancel from '../../components/Workout/CooldownModalCancel';
import {
  WHITE,
  GRAY,
  GRAY_BORDER,
  RED_BORDER,
  BLACK,
  LIGHT_ORANGE,
} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import {moderateScale} from '../../utils/dimensions';
import {trackEvents} from '../../services/analytics';

const Cooldown = ({navigation, route}) => {
  const {
    cooldowns,
    sessionId,
    session,
    programId,
    programCodeName,
    workoutSettingSound,
    saveSession,
  } = route.params;
  const [modalVisible, setModalVisible] = useState(false);
  const [activityId, setActivityId] = useState(-1);

  const toggleModal = () => {
    setModalVisible(!modalVisible);
  };
  const cancelCooldowns = async () => {
    trackEvents({eventName: 'Skip Cooldown'});
    toggleModal();
    saveSession();
  };

  const handleChangeCooldown = async id => {
    setActivityId(id);
    const cooldownIndex = cooldowns.findIndex(item => item.id == id);
    trackEvents({eventName: 'Start Cooldown'});
    navigation.navigate('DownloadingVideos', {
      cooldowns: cooldowns[cooldownIndex].wo,
      sessionId: sessionId,
      programId: programId,
      isCoolDown: true,
      workoutSettingSound,
      programCodeName,
      session,
      saveSession,
    });
  };

  const {translations} = useContext(LocalizationContext);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        style={styles.scrollView}
        contentContainerStyle={{justifyContent: 'space-between', flex: 1}}>
        <CooldownModalCancel
          isVisible={modalVisible}
          actionOk={toggleModal}
          actionCancel={cancelCooldowns}
        />
        <View>
          <Text style={styles.title}>{translations.cooldown.title}</Text>
          <View style={{marginBottom: 80}} />
          {cooldowns.map((item, index) => (
            <TouchableOpacity
              style={[
                styles.block,
                {
                  borderColor:
                    item.id === activityId ? RED_BORDER : GRAY_BORDER,
                },
              ]}
              key={item.id}
              onPress={() => handleChangeCooldown(item.id)}>
              <Text style={styles.blockTitle}>{item.name}</Text>
            </TouchableOpacity>
          ))}
        </View>
        <Button
          buttonStyle={styles.button}
          titleStyle={styles.buttonText}
          onPress={toggleModal}
          title={translations.cooldown.skipCoolDown}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default Cooldown;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE,
  },
  scrollView: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 45,
  },
  title: {
    fontSize: 22,
    fontFamily: MAIN_TITLE_FONT,
  },
  block: {
    height: 85,
    borderWidth: 1,
    paddingTop: 20,
    paddingLeft: 25,
    paddingBottom: 25,
    marginBottom: 15,
    borderRadius: 5,
  },
  blockTitle: {
    color: BLACK,
    fontSize: 18,
    fontFamily: MAIN_TITLE_FONT,
  },
  button: {
    backgroundColor: WHITE,
  },
  buttonText: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: 14,
    marginBottom: moderateScale(30),
    textDecorationLine: 'underline',
  },
});
