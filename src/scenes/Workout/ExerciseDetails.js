import React, {useState, useEffect, useContext} from 'react';
import {SafeAreaView, StyleSheet, ScrollView, Text, View} from 'react-native';
import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {Loading} from '../../components/Loading';
import {WHITE, BLACK, GRAY} from '../../styles/colors';
import Header from '../../components/header';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_FONT,
  SUB_TITLE_FONT_CURSIVE,
} from '../../styles/fonts';
import {
  calcWidth,
  moderateScale,
  calcFontSize,
  deviceHeight,
} from '../../utils/dimensions';
import Video from 'react-native-video';
import {LocalizationContext} from '../../localization/translations';
import {AuthContext} from '../../../App';

const GET_EXERCISE = gql`
  query getExerciseData($id: ID!, $locale: String!) {
    getExerciseData(id: $id, locale: $locale) {
      id
      name
      description
      video
      harderVariation {
        id
        name
        description
        video
      }
      easierVariation {
        id
        name
        description
        video
      }
    }
  }
`;

const ExerciseDetails = ({navigation, route}) => {
  const {exerciseId} = route.params;
  const [currentExerciseId, setCurrentExerciseId] = useState();
  const {userData: userInfo} = useContext(AuthContext);
  const {translations, appLanguage} = useContext(LocalizationContext);
  const {data, error, loading} = useQuery(GET_EXERCISE, {
    variables: {id: currentExerciseId, locale: appLanguage},
    context: {
      headers: {
        authorization: userInfo?.token,
      },
    },
  });
  useEffect(() => {
    setCurrentExerciseId(exerciseId);
  }, [exerciseId]);

  const exercise = data?.getExerciseData;

  return (
    <SafeAreaView style={styles.safeArea}>
      <Header
        title={exercise?.name}
        onPressBackButton={() => {
          navigation.goBack();
        }}
      />
      {!loading ? (
        <>
          <ScrollView
            style={styles.container}
            showsVerticalScrollIndicator={false}>
            <Video
              poster="https://doctod-dev-testing.s3.sa-east-1.amazonaws.com/white-cover.png"
              repeat={true}
              onBuffer={buffer => console.log({buffer})}
              onError={error => console.log({error})}
              source={{
                uri: exercise?.video,
              }}
              style={styles.videoBackground}
              resizeMode={'cover'}
              rate={1.0}
              playWhenInactive={true}
            />
            <View style={styles.textContainer}>
              <Text style={styles.title}>{exercise?.name}</Text>
              <Text style={styles.description}>{exercise?.description}</Text>
              {exercise?.harderVariation && (
                <>
                  <Text style={styles.subTitle}>
                    {translations.exerciseDetails.alternativeExercise}
                  </Text>
                  <Text
                    style={[styles.description, {fontFamily: SUB_TITLE_FONT}]}
                    onPress={() =>
                      setCurrentExerciseId(exercise?.harderVariation?.id)
                    }>
                    {exercise?.harderVariation?.name}
                  </Text>
                </>
              )}
            </View>
          </ScrollView>
        </>
      ) : (
        <Loading />
      )}
    </SafeAreaView>
  );
};

export default ExerciseDetails;

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: WHITE,
  },
  textContainer: {
    marginHorizontal: calcWidth(20),
  },
  container: {
    backgroundColor: WHITE,
    paddingBottom: 0,
  },
  videoBackground: {
    height: deviceHeight / 1.55,
    marginTop: 0,
    borderBottomColor: GRAY,
    borderBottomWidth: 1,
  },
  title: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(22),
    color: BLACK,
    marginVertical: moderateScale(5),
  },
  description: {
    fontFamily: SUB_TITLE_FONT_CURSIVE,
    fontSize: calcFontSize(14),
    color: BLACK,
    marginVertical: moderateScale(10),
  },
  subTitle: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
    color: BLACK,
    marginVertical: moderateScale(10),
  },
});
