import React, {useCallback, useContext, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import Slider from '@react-native-community/slider';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY,
  GRAY_BORDER,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import Header from '../../components/header';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
} from '../../utils/dimensions';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_BOLD_FONT,
  SUB_TITLE_FONT,
} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';

const PersonalizeWorkout = ({navigation}) => {
  const handlePressBack = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const [sliderValue, setSliderValue] = useState(5);
  const [personalizedValues, setPersonalizedValues] = useState([]);
  const MIN_SLIDER_VALUE = 5;
  const MAX_SLIDER_VALUE = 20;

  const {translations} = useContext(LocalizationContext);

  const personalizeValues = [
    {
      id: 1,
      title: translations.personalizeWorkoutScreen.wholeBody,
    },
    {
      id: 2,
      title: translations.personalizeWorkoutScreen.upperBody,
    },
    {
      id: 3,
      title: translations.personalizeWorkoutScreen.lowerBody,
    },
    {
      id: 4,
      title: translations.personalizeWorkoutScreen.absCore,
    },
  ];

  const handleOnPressValue = useCallback(
    data => {
      if (!personalizedValues.includes(data)) {
        setPersonalizedValues([...personalizedValues, data]);
      } else {
        const index = personalizedValues.indexOf(data);
        if (index > -1) {
          personalizedValues.splice(index, 1);
          setPersonalizedValues([...personalizedValues]);
        }
      }
    },
    [personalizedValues],
  );

  return (
    <SafeAreaView style={styles.safeArea}>
      <Header
        title={translations.personalizeWorkoutScreen.header}
        onPressBackButton={handlePressBack}
      />
      <View style={styles.sliderView}>
        <View style={styles.valueView}>
          <Text style={styles.sliderValueText}>{sliderValue} </Text>
          <View style={styles.minView}>
            <Text style={styles.sliderValueMin}> min</Text>
          </View>
        </View>

        <View style={styles.sliderRow}>
          <Text style={styles.nearSliderValue}>{MIN_SLIDER_VALUE}</Text>
          <Slider
            minimumValue={5}
            maximumValue={20}
            minimumTrackTintColor={LIGHT_ORANGE}
            maximumTrackTintColor={'gray'}
            onValueChange={e => setSliderValue(e)}
            thumbTintColor={LIGHT_ORANGE}
            step={1}
            style={styles.sliderStyle}
          />
          <Text style={styles.nearSliderValue}>{MAX_SLIDER_VALUE}</Text>
        </View>
      </View>
      <View style={styles.line} />
      <View style={styles.pillsView}>
        {personalizeValues.map((item, key) => {
          return (
            <TouchableOpacity
              key={item.id}
              style={
                !personalizedValues.includes(item.title)
                  ? styles.pill
                  : [styles.pill, styles.pillSelected]
              }
              onPress={() => handleOnPressValue(item.title)}>
              <Text
                style={
                  !personalizedValues.includes(item.title)
                    ? styles.pillText
                    : [styles.pillText, styles.pillTextSelected]
                }>
                {item.title}
              </Text>
            </TouchableOpacity>
          );
        })}
      </View>
      <TouchableOpacity style={styles.footerButton} onPress={() => {}}>
        <Text style={styles.submitButtonText}>
          {translations.personalizeWorkoutScreen.startTraining}
        </Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: WHITE,
  },
  sliderView: {
    paddingHorizontal: calcWidth(20),
    marginTop: calcHeight(5),
  },
  sliderValueText: {
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(70),
    color: BLACK,
    lineHeight: calcHeight(75),
  },
  sliderValueMin: {
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(20),
    color: BLACK,
  },
  nearSliderValue: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: BLACK,
  },
  line: {
    marginTop: calcHeight(30),
    height: calcHeight(0.5),
    backgroundColor: GRAY,
    width: deviceWidth,
  },
  pillsView: {
    paddingHorizontal: calcWidth(10),
    flexWrap: 'wrap',
    alignSelf: 'center',
    marginTop: calcHeight(30),
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  pillText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
    color: BUTTON_TITLE,
  },
  pillTextSelected: {
    color: WHITE,
  },
  pill: {
    borderRadius: 25,
    paddingHorizontal: calcWidth(15),
    paddingVertical: calcHeight(10),
    backgroundColor: WHITE,
    borderWidth: 1,
    borderColor: GRAY_BORDER,
    marginVertical: calcHeight(10),
    marginHorizontal: calcWidth(5),
  },
  pillSelected: {
    backgroundColor: ORANGE,
  },
  footerButton: {
    position: 'absolute',
    bottom: calcHeight(20),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: ORANGE,
    paddingVertical: calcHeight(15),
    paddingHorizontal: calcWidth(75),
    alignSelf: 'center',
    borderRadius: 25,
  },
  submitButtonText: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
    color: WHITE,
  },
  valueView: {
    alignSelf: 'center',
    flexDirection: 'row',
    marginBottom: calcHeight(20),
  },
  minView: {
    position: 'absolute',
    right: -calcWidth(20),
    top: calcHeight(33),
  },
  sliderStyle: {
    width: '90%',
    alignSelf: 'center',
    marginHorizontal: calcWidth(5),
  },
  sliderRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default PersonalizeWorkout;
