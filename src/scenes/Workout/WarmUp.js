import React, {useContext, useState} from 'react';
import {ScrollView, View, Text, StyleSheet, SafeAreaView} from 'react-native';
import {Button} from 'react-native-elements';
import {TouchableOpacity} from 'react-native-gesture-handler';
import WarmUpsModal from '../../components/Workout/WorkoutModal';
import {
  WHITE,
  GRAY,
  GRAY_BORDER,
  RED_BORDER,
  BLACK,
  LIGHT_ORANGE,
} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import {trackEvents} from '../../services/analytics';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE,
  },
  scrollView: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 45,
  },
  title: {
    fontSize: 22,
    fontFamily: MAIN_TITLE_FONT,
  },
  text: {
    marginTop: 15,
    marginBottom: 30,
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
  },
  block: {
    height: 85,
    borderWidth: 1,
    paddingTop: 20,
    paddingLeft: 25,
    paddingBottom: 25,
    marginBottom: 15,
    borderRadius: 5,
  },
  blockTitle: {
    color: BLACK,
    fontSize: 18,
    fontFamily: MAIN_TITLE_FONT,
  },
  blockDescription: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
  },
  button: {
    backgroundColor: WHITE,
  },
  buttonText: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: 14,
  },
});

const WarmUp = ({navigation, route}) => {
  const {
    wo,
    sessionId,
    session,
    programId,
    programCodeName,
    warmups,
    cooldowns,
    workoutSettingSound,
  } = route.params;
  const [modalVisible, setModalVisible] = useState(false);
  const [activityId, setActivityId] = useState(-1);
  const toggleModal = () => {
    if (!modalVisible) {
      //on open
      trackEvents({
        eventName: 'Load Warmup Warning Popup',
        properties: {
          training_program: programCodeName,
          session_id: session?.id,
          session_name: session?.name,
          session_type: session?.sessionType,
          session_order: session?.order,
        },
      });
    } else {
      //on close
      trackEvents({eventName: 'Skip Warmup'});
    }
    setModalVisible(!modalVisible);
  };
  const cancelWarmUp = async () => {
    toggleModal();
    navigation.navigate('DownloadingVideos', {
      wo,
      sessionId,
      programId,
      isWarmUps: false,
      workoutSettingSound,
      programCodeName,
      session,
      cooldowns,
    });
  };

  const handleChangeActivity = async id => {
    setActivityId(id);
    const warmupIndex = route.params.warmups.findIndex(item => item.id == id);
    trackEvents({
      eventName: 'Start Warmup',
      properties: {
        training_program: programCodeName,
        session_id: session?.id,
        session_name: session?.name,
        session_type: session?.sessionType,
        session_order: session?.order,
        warmup_session_name: warmups[warmupIndex]?.name,
        warmups_session_id: warmups[warmupIndex]?.id,
      },
    });
    navigation.navigate('DownloadingVideos', {
      wo,
      sessionId: sessionId,
      programId: programId,
      isWarmUps: true,
      warmups: warmups[warmupIndex].wo,
      cooldowns,
      workoutSettingSound,
      programCodeName,
      session,
    });
  };

  const {translations} = useContext(LocalizationContext);

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scrollView}>
        <WarmUpsModal
          isVisible={modalVisible}
          isDone={false}
          actionOk={toggleModal}
          actionCancel={cancelWarmUp}
        />
        <View>
          <Text style={styles.title}>
            {translations.warmUpScreen.tabHeader}
          </Text>
          <Text style={styles.text}>
            {translations.warmUpScreen.description}
          </Text>
        </View>
        <View style={{marginBottom: 100}}>
          {warmups.map((item, index) => (
            <TouchableOpacity
              style={[
                styles.block,
                {
                  borderColor:
                    item.id === activityId ? RED_BORDER : GRAY_BORDER,
                },
              ]}
              key={item.id}
              onPress={() => handleChangeActivity(item.id)}>
              <Text style={styles.blockTitle}>{item.name}</Text>
              <Text style={styles.blockDescription}>
                {item.description}
                {/* {translations.warmUpScreen.descriptionWord} */}
              </Text>
            </TouchableOpacity>
          ))}
          <Button
            buttonStyle={styles.button}
            titleStyle={styles.buttonText}
            onPress={toggleModal}
            title={translations.warmUpScreen.toggleModalButton}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default WarmUp;
