import React, {useState, useContext, useCallback} from 'react';
import {SafeAreaView, StyleSheet, View, FlatList} from 'react-native';
import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';
import Header from '../../components/header';
import {Loading} from '../../components/Loading';
import Exercise from '../../components/Workout/Exercise';
import {WHITE, GRAY_BORDER} from '../../styles/colors';
import {calcWidth, moderateScale} from '../../utils/dimensions';
import {trackEvents} from '../../services/analytics';
import {LocalizationContext} from '../../localization/translations';
import CustomTextInput from '../../components/CustomTextInput';
import {AuthContext} from '../../../App';

const GET_EXERCISES = gql`
  query searchExercise($name: String!, $locale: String!) {
    searchExercise(name: $name, locale: $locale) {
      id
      name
      thumbnail
    }
  }
`;

const SearchExercise = ({navigation, route}) => {
  const {userData: userInfo} = useContext(AuthContext);
  const {translations, appLanguage} = useContext(LocalizationContext);
  const [searchName, setSearchName] = useState('');
  const {data, error, loading} = useQuery(GET_EXERCISES, {
    variables: {name: searchName, locale: appLanguage},
    context: {
      headers: {
        authorization: userInfo?.token,
      },
    },
  });
  const isFocused = useIsFocused();
  console.log('applanguage', appLanguage);
  const exercises = data?.searchExercise;
  console.log('data', data);

  useFocusEffect(
    useCallback(() => {
      isFocused && trackEvents({eventName: 'Load Exercise List'});
    }, [isFocused]),
  );

  return (
    <SafeAreaView style={styles.safeArea}>
      <>
        <Header
          title={translations.searchExerciseScreen.header}
          onPressBackButton={() => {
            navigation.goBack();
          }}
        />
        <View style={styles.line} />
        <View style={styles.container}>
          <CustomTextInput value={searchName} onChange={setSearchName} />
          {!loading ? (
            <FlatList
              data={exercises}
              renderItem={({item}) => (
                <Exercise item={item} navigation={navigation} />
              )}
              keyExtractor={item => item.id}
              style={styles.listExercises}
              showsVerticalScrollIndicator={false}
            />
          ) : (
            <Loading />
          )}
        </View>
      </>
    </SafeAreaView>
  );
};

export default SearchExercise;

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: WHITE,
  },
  container: {
    paddingBottom: calcWidth(20),
    marginHorizontal: calcWidth(20),
    backgroundColor: WHITE,
  },
  line: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginBottom: moderateScale(15),
    marginTop: moderateScale(-15),
  },
  listExercises: {
    backgroundColor: WHITE,
    marginVertical: moderateScale(15),
  },
});
