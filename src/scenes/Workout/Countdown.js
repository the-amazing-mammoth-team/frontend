import React, {useContext} from 'react';
import {
  Animated,
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  Platform,
} from 'react-native';
// import Mixpanel from 'react-native-mixpanel';
import {CountdownCircleTimer} from 'react-native-countdown-circle-timer';
import Sound from 'react-native-sound';
import {MAIN_TITLE_FONT} from '../../styles/fonts';
import {Button, Icon} from 'react-native-elements';
import {useIsFocused} from '@react-navigation/native';
import {WHITE, ORANGE} from '../../styles/colors';
import {LocalizationContext} from '../../localization/translations';
import {deviceWidth, moderateScale} from '../../utils/dimensions';
import {trackEvents} from '../../services/analytics';

// const MIXPANEL_TOKEN = 'bf47184b8fc05bef8c88adb01de19387';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE,
  },
  innerContainer: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flex: 1,
  },
  text: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: 20,
    marginBottom: moderateScale(35),
    marginTop: moderateScale(-20),
  },
  content: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: 25,
    paddingHorizontal: moderateScale(20),
    textAlign: 'center',
  },
  bottomButton: {
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    marginTop: 15,
    marginBottom: 35,
    marginRight: 20,
    backgroundColor: WHITE,
  },
  nextButtonStyleActive: {
    borderRadius: 50,
    height: 50,
    width: 50,
    backgroundColor: ORANGE,
    borderColor: WHITE,
  },
  remainingTime: {
    marginTop: moderateScale(50),
    fontSize: 80,
  },
  loadingVideos: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  loadingText: {
    fontSize: 20,
    fontFamily: MAIN_TITLE_FONT,
    textAlign: 'center',
  },
  loadingPhrases: {
    textAlign: 'center',
  },
  image: {
    width: deviceWidth * 0.5,
    height: deviceWidth * 0.5,
    marginTop: moderateScale(30),
  },
});

Sound.setCategory(Platform.OS === 'ios' ? 'Ambient' : 'Playback', true);

const beep = new Sound('beep.mp3', Sound.MAIN_BUNDLE, error => {
  if (error) {
    console.log('failed to load the sound', error);
    return;
  }
});

const Countdown = ({navigation, route}) => {
  const {
    sessionId,
    wo,
    nextWorkout,
    programId,
    workoutSettingSound,
    isWarmUps,
    isCoolDown,
    session,
    programCodeName,
    saveSession,
  } = route?.params || {};

  const isFocused = useIsFocused();

  const startWorkout = () => {
    if (workoutSettingSound && isFocused) {
      beep.play(success => {
        if (success) {
          console.log('successfully finished playing');
        } else {
          console.log('playback failed due to audio decoding errors');
        }
      });
    }
    !isWarmUps &&
      !isCoolDown &&
      trackEvents({
        eventName: 'Start Workout',
        properties: {
          training_program: programCodeName,
          session_id: session?.id,
          session_name: session?.name,
          session_type: session?.sessionType,
          session_order: session?.order,
        },
      });
    navigation.navigate('WorkoutList', {
      wo: wo,
      sessionId,
      programId,
      nextWorkout,
      workoutSettingSound,
      isWarmUps,
      isCoolDown,
      session,
      programCodeName,
      saveSession,
    });
  };
  const {translations} = useContext(LocalizationContext);

  const ex = wo[0].allSets[1].allExercises[0];
  const exName = wo[0].allSets[1].allExercises[0].exercise.name;
  let exDuration = '';
  if (ex.reps) {
    exDuration = `${ex.reps} reps`;
  } else {
    console.log(ex);
    const minutes =
      Math.floor(ex.timeDuration / 60) > 9
        ? Math.floor(ex.timeDuration / 60)
        : `0${Math.floor(ex.timeDuration / 60)}`;
    const seconds =
      ex.timeDuration % 60 > 9
        ? ex.timeDuration % 60
        : `0${ex.timeDuration % 60}`;
    exDuration = `${minutes}:${seconds}`;
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.innerContainer}>
        <View>
          <CountdownCircleTimer
            onComplete={startWorkout}
            isPlaying={isFocused}
            strokeWidth={0}
            duration={5}
            colors={[['#000000', 1]]}>
            {({remainingTime, animatedColor}) => (
              <Animated.Text
                style={[styles.remainingTime, {color: animatedColor}]}>
                {remainingTime}
              </Animated.Text>
            )}
          </CountdownCircleTimer>
        </View>
        <Text style={styles.text}>{translations.countDownScreen.getReady}</Text>
        <Text style={styles.content}>{exDuration}</Text>
        <Text style={styles.content}>{exName}</Text>
        <Image source={{uri: ex.exercise.thumbnail400}} style={styles.image} />
      </View>
      <View style={styles.bottomButton}>
        <Button
          onPress={startWorkout}
          buttonStyle={styles.nextButtonStyleActive}
          icon={<Icon name="arrow-forward" size={25} color="white" />}
          title=""
        />
      </View>
    </SafeAreaView>
  );
};

export default Countdown;
