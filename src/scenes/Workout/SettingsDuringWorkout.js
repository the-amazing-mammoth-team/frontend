import React, {useContext, useCallback, useState} from 'react';
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import {useMutation, useQuery} from '@apollo/react-hooks';
import ToggleSwitch from 'toggle-switch-react-native';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';
import {AuthContext} from '../../../App';
import Header from '../../components/header';
import {LIGHT_GRAY, LIGHT_ORANGE, ORANGE} from '../../styles/colors';
import {LocalizationContext} from '../../localization/translations';
import styles from './settingsStyles';
import {moderateScale} from '../../utils/dimensions';
import DiscardWorkoutModal from '../../components/Workout/DiscardWorkoutModal';
import {trackEvents} from '../../services/analytics';

const SettingsBlock = ({title, options, updateUserDataFunc}) => {
  const toggleSwitch = useCallback(
    async (value, option) =>
      await updateUserDataFunc(option.backendString, value),
    [updateUserDataFunc],
  );

  return (
    <View style={[styles.blockContainer, {paddingBottom: moderateScale(35)}]}>
      <Text style={styles.blockTitle}>{title}</Text>
      {options.map(option => {
        return (
          <View key={option.title} style={styles.optionContainer}>
            <View style={styles.optionContent}>
              <Text style={styles.settingTitle}>{option.title}</Text>
              <Text style={styles.settingsDescription}>
                {option.description}
              </Text>
            </View>
            <View>
              <ToggleSwitch
                isOn={option.status}
                onColor={LIGHT_ORANGE}
                offColor={LIGHT_GRAY}
                onToggle={isOn => toggleSwitch(isOn, option)}
                animationSpeed={100}
                thumbOnStyle={styles.thumbStyle}
                thumbOffStyle={styles.thumbStyle}
              />
            </View>
          </View>
        );
      })}
    </View>
  );
};

SettingsBlock.propTypes = {
  title: PropTypes.string,
  options: PropTypes.array,
  updateUserDataFunc: PropTypes.func,
};

const SettingsDuringWorkout = ({navigation, route}) => {
  const {
    exercise,
    sessionId,
    programId,
    session,
    programCodeName,
  } = route?.params;
  const currentExercise = exercise?.exercise;

  const {userData: userInfo} = useContext(AuthContext);
  const {translations} = useContext(LocalizationContext);

  const isFocused = useIsFocused();

  const [isModalVisible, setIsModalVisible] = useState(false);
  const toggleModal = useCallback(() => {
    if (!isModalVisible) {
      //on open
      trackEvents({
        eventName: 'Load Discard Workout Popup',
        properties: {
          training_program: programCodeName,
          session_id: session?.id,
          session_name: session?.name,
          session_type: session?.sessionType,
          session_order: session?.order,
          discard_placement: 'Workout Interactive Settings',
        },
      });
    }
    setIsModalVisible(!isModalVisible);
  }, [isModalVisible, programCodeName, session]);

  const GET_USER = gql`
    query user($id: ID!) {
      user(id: $id) {
        id
        workoutSettingCardioWarmup
        workoutSettingCountdown
        workoutSettingMobility
        workoutSettingSound
        names
        email
      }
    }
  `;

  const UPDATE_USER = gql`
    mutation updateUserData($input: UpdateUserDataInput!) {
      updateUserData(input: $input) {
        id
      }
    }
  `;

  const {data, loading} = useQuery(GET_USER, {
    variables: {id: userInfo.id},
  });

  const [update] = useMutation(UPDATE_USER);

  const {workoutSettingSound} = data?.user || {};

  useFocusEffect(
    useCallback(() => {
      if (isFocused) {
        trackEvents({
          eventName: 'Load Workout Interactive Pause',
          properties: {
            training_program: programCodeName,
            session_id: session?.id,
            session_name: session?.name,
            session_type: session?.sessionType,
            session_order: session?.order,
          },
        });
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]),
  );

  const onPressBackButton = useCallback(() => {
    navigation.navigate('WorkoutList', {
      workoutSettingSound: workoutSettingSound,
    });
  }, [navigation, workoutSettingSound]);

  const updateUserDataFunc = useCallback(
    async (title, value) => {
      try {
        const input = {
          [title]: value,
        };
        console.log('INPUT:', input);
        console.log('JWT:', userInfo.token);
        const res = await update({
          variables: {
            input: {
              userData: {
                ...input,
              },
            },
          },
          context: {
            headers: {
              authorization: userInfo.token,
            },
          },
          refetchQueries: ['user'],
        });
        console.log('RESULT', {res});
        if (res?.data?.updateUserData) {
          console.log('Success');
        }
      } catch (error) {
        console.log(error);
      }
    },
    [update, userInfo],
  );

  const settingsItems = [
    {
      title: '',
      options: [
        // {
        //   title: translations.settingsScreen.voiceOptionTitle,
        //   description: translations.settingsScreen.voiceOptionDescription,
        //   status: data && data.user.workoutSettingVoiceCoach,
        //   backendString: 'workoutSettingVoiceCoach',
        // },
        {
          title: translations.settingsScreen.soundsOptionTitle,
          description: translations.settingsScreen.soundsOptionDescription,
          status: data && data.user.workoutSettingSound,
          backendString: 'workoutSettingSound',
        },
        // {
        //   title: translations.settingsScreen.vibrationOptionTitle,
        //   description: translations.settingsScreen.vibrationOptionDescription,
        //   status: data && data.user.workoutSettingVibration,
        //   backendString: 'workoutSettingVibration',
        // },
      ],
    },
  ];

  const handleLaterButtonModal = useCallback(() => {
    toggleModal();
    navigation.navigate('Home');
    trackEvents({
      eventName: 'Discard Workout',
      properties: {
        training_program: programCodeName,
        session_id: session?.id,
        session_name: session?.name,
        session_type: session?.sessionType,
        session_order: session?.order,
        placement: 'Workout Interactive',
      },
    });
  }, [navigation, programCodeName, session, toggleModal]);

  const handleDeleteButtonModal = useCallback(() => {
    toggleModal();
    navigation.navigate('Feedback', {
      sessionId: sessionId,
      programId: programId,
      programCodeName,
      session,
      prevScreen: 'Workout Interactive',
    });
  }, [navigation, programCodeName, programId, session, sessionId, toggleModal]);

  const goToExerciseDetails = useCallback(
    ex => {
      console.log(currentExercise);
      navigation.navigate('ExerciseDetails', {
        exerciseId: ex.id,
      });
    },
    [currentExercise, navigation],
  );

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.container}>
        <DiscardWorkoutModal
          title={translations.settingsScreen.discardWorkout}
          subTitle={translations.settingsScreen.discardWorkoutDesc}
          goBack={handleLaterButtonModal}
          goBackText={translations.settingsScreen.doLater}
          goNext={handleDeleteButtonModal}
          goNextText={translations.settingsScreen.deleteTraining}
          visible={isModalVisible}
          closeModal={toggleModal}
        />
        {userInfo && data ? (
          <>
            <View>
              <Header
                title={translations.settingsScreen.sessionPaused}
                onPressBackButton={onPressBackButton}
              />
              <View style={styles.headerLine} />
              <View style={styles.allSettings}>
                {settingsItems.map(setting => {
                  return (
                    <SettingsBlock
                      key={setting.title}
                      title={setting.title}
                      options={setting.options}
                      updateUserDataFunc={updateUserDataFunc}
                    />
                  );
                })}
              </View>
              <Text style={styles.duringWorkoutScreenTitle}>
                {currentExercise?.name}
              </Text>
              <TouchableWithoutFeedback
                onPress={() => goToExerciseDetails(currentExercise)}>
                <Image
                  style={styles.image}
                  source={{uri: currentExercise?.thumbnail}}
                />
              </TouchableWithoutFeedback>
            </View>
            <TouchableOpacity
              style={styles.duringWorkoutScreenFooterButton}
              hitSlop={styles.hitSlopTen}
              onPress={toggleModal}>
              <Text style={styles.deleteWorkout}>
                {translations.settingsScreen.finishTraining}
              </Text>
            </TouchableOpacity>
          </>
        ) : (
          <View style={styles.activityIndicator}>
            <ActivityIndicator size={'large'} color={ORANGE} />
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default SettingsDuringWorkout;
