//this should be called discard session feedback
import React, {useCallback, useContext, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
} from 'react-native';
import {useMutation} from '@apollo/react-hooks';
import {
  useFocusEffect,
  useIsFocused,
  useNavigationState,
} from '@react-navigation/native';
import gql from 'graphql-tag';
import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';
import {getPreviousRouteFromState} from '../../services/navigation';
import Header from '../../components/header';
import {
  GRAY,
  GRAY_BORDER,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
  BLACK,
  LIGHT_GRAY,
} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import {trackEvents} from '../../services/analytics';
import CheckedIcon from '../../assets/icons/checkIcon.svg';
import {AuthContext} from '../../../App';

const Feedback = ({navigation, route}) => {
  const {translations} = useContext(LocalizationContext);
  const [selectedOption, setSelectedOption] = useState(null);
  const {userData: userInfo} = useContext(AuthContext);
  const {
    sessionId,
    programId,
    programCodeName,
    session,
    prevScreen,
  } = route?.params;

  const navState = useNavigationState(state => state);
  const isFocused = useIsFocused();

  console.log('data s y p', sessionId, programId);

  useFocusEffect(
    useCallback(() => {
      if (isFocused) {
        console.log({programCodeName});
        trackEvents({
          eventName: 'Load Giving Up',
          properties: {
            training_program: programCodeName,
            session_id: session?.id,
            session_name: session?.name,
            session_type: session?.sessionType,
            session_order: session?.order,
          },
        });
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]),
  );

  const goBack = useCallback(() => {
    if (getPreviousRouteFromState(navState).name === 'WorkoutSettings') {
      navigation.pop(2);
    } else {
      navigation.pop(1);
    }
  }, [navState, navigation]);

  const feedbackOptions = [
    {
      id: 1,
      titleView: translations.feedbackScreen.tooEasy,
      titleForBackend: 'too_easy',
    },
    {
      id: 2,
      titleView: translations.feedbackScreen.tooHard,
      titleForBackend: 'too_hard',
    },
    {
      id: 3,
      titleView: translations.feedbackScreen.enoughTime,
      titleForBackend: 'no_time',
    },
    {
      id: 4,
      titleView: translations.feedbackScreen.notLike,
      titleForBackend: 'no_like',
    },
    {
      id: 5,
      titleView: translations.feedbackScreen.other,
      titleForBackend: 'other',
    },
  ];

  const DISCARD_SESSION = gql`
    mutation discardSession($input: DiscardSessionInput!) {
      discardSession(input: $input) {
        id
        discarded
        discardReason
      }
    }
  `;

  const [saveSession] = useMutation(DISCARD_SESSION);

  const onSaveSession = useCallback(
    async option => {
      if (!option) {
        return null;
      }
      console.log(222, option);
      try {
        const res = await saveSession({
          variables: {
            input: {
              sessionId: sessionId,
              programId: programId,
              discardReason: option.titleForBackend,
            },
          },
          context: {
            headers: {
              authorization: userInfo.token,
            },
          },
        });
        console.log('res:', {res});
        if (res?.data?.discardSession) {
          trackEvents({
            eventName: 'Delete Workout',
            properties: {
              training_program: programCodeName,
              session_id: session?.id,
              session_name: session?.name,
              session_type: session?.sessionType,
              session_order: session?.order,
              reason: res?.data?.discardSession?.discardReason,
              placement: prevScreen,
            },
          });
          console.log('Success');
          navigation.navigate('Home', {doRefetch: true});
        }
      } catch (error) {
        console.log('error:', error);
      }
    },
    [
      navigation,
      prevScreen,
      programCodeName,
      programId,
      saveSession,
      session,
      sessionId,
      userInfo,
    ],
  );

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView style={styles.root} showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <Header
            title={translations.feedbackScreen.title}
            onPressBackButton={goBack}
          />
          <View style={styles.headerLine} />
          <View style={styles.descriptionView}>
            <Text style={styles.descriptionText}>
              {translations.feedbackScreen.descriptionText}
            </Text>
          </View>
          <View>
            {feedbackOptions.map(option => {
              return (
                <TouchableOpacity
                  onPress={() => {
                    setSelectedOption(option);
                  }}
                  style={styles.optionView}
                  key={option.id}>
                  <Text style={styles.optionText}>{option.titleView}</Text>
                  <View
                    style={[
                      styles.circle,
                      selectedOption?.id === option.id && styles.circleChecked,
                    ]}>
                    {selectedOption?.id === option.id && (
                      <CheckedIcon
                        width={calcWidth(12)}
                        height={calcHeight(8)}
                      />
                    )}
                  </View>
                </TouchableOpacity>
              );
            })}
          </View>
          <TouchableOpacity
            hitSlop={styles.hitSlopTwenty}
            style={styles.skipView}>
            <Text
              style={styles.skipText}
              onPress={() => onSaveSession(selectedOption)}>
              {translations.settingsScreen.deleteTraining}
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  container: {
    marginHorizontal: moderateScale(20),
  },
  headerLine: {
    height: moderateScale(1),
    width: deviceWidth,
    alignSelf: 'center',
    backgroundColor: GRAY_BORDER,
    marginTop: -20,
    marginBottom: moderateScale(20),
  },
  descriptionText: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
  descriptionView: {
    marginTop: calcHeight(10),
    marginBottom: calcHeight(35),
  },
  optionView: {
    borderWidth: 1,
    borderColor: GRAY_BORDER,
    borderRadius: 5,
    paddingHorizontal: calcWidth(25),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: calcHeight(17),
    marginBottom: calcHeight(20),
  },
  optionText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
  },
  circle: {
    width: moderateScale(17),
    height: moderateScale(17),
    borderRadius: moderateScale(17) / 2,
    backgroundColor: LIGHT_GRAY,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleChecked: {
    backgroundColor: ORANGE,
  },
  skipText: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
  skipView: {
    marginVertical: calcHeight(30),
  },
  hitSlopTwenty: {
    top: calcHeight(20),
    bottom: calcHeight(20),
    right: calcWidth(20),
    left: calcWidth(20),
  },
});

export default Feedback;
