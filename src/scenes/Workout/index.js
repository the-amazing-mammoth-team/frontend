import React, {useContext, useState, useEffect, useCallback} from 'react';
import {
  ScrollView,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  BackHandler,
} from 'react-native';
import {Icon, Button} from 'react-native-elements';
import FastImage from 'react-native-fast-image';
import {capitalize} from 'lodash';
import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';
import {WHITE, GRAY, ORANGE, LIGHT_ORANGE} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {MButton} from '../../components/buttons';
import InfoIcon from '../../assets/icons/warning.svg';
import ArrowDown from '../../assets/icons/arrowDown.svg';
import {LocalizationContext} from '../../localization/translations';
import {AuthContext} from '../../../App';
import {renderEquipment} from '../Programs/ProgramDescription';
import Header from '../../components/header';
import {
  calcFontSize,
  deviceHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {Loading} from '../../components/Loading';
import RadarChartComponent from '../../components/ProgressScreen/RadarChart';
import MagentoEquipment from '../../assets/images/pullUpBar.png';
import EnumBlockName from './enumNameBlocks';
import {trackEvents} from '../../services/analytics';

const GET_SESSION = gql`
  query getSession($sessionId: ID!, $locale: String!) {
    getSession(sessionId: $sessionId, locale: $locale) {
      id
      name
      imageUrl
      description
      bodyPartsFocused
      order
      sessionType
      sessionBlocks {
        blockType
        sessionSets {
          exercises {
            name
            id
          }
          timeDuration
          exerciseSets {
            id
            reps
            order
            trackReps
            timeDuration
            exercise {
              id
              name
              video
              thumbnail
              thumbnail400
              notes
              metMultiplier
              bodyPartsFocused
            }
          }
        }
      }
      warmup {
        id
        name
        description
        sessionBlocks {
          blockType
          sessionSets {
            exercises {
              name
              id
            }
            timeDuration
            exerciseSets {
              id
              reps
              order
              trackReps
              timeDuration
              exercise {
                id
                name
                video
                thumbnail
                thumbnail400
                notes
              }
            }
          }
        }
      }
      warmups {
        id
        name
        description
        sessionBlocks {
          blockType
          sessionSets {
            exercises {
              name
              id
            }
            timeDuration
            exerciseSets {
              id
              reps
              order
              trackReps
              timeDuration
              exercise {
                id
                name
                video
                thumbnail
                thumbnail400
                notes
              }
            }
          }
        }
      }
      cooldown {
        id
        name
        description
        sessionBlocks {
          blockType
          sessionSets {
            exercises {
              name
              id
            }
            timeDuration
            exerciseSets {
              id
              reps
              order
              trackReps
              timeDuration
              exercise {
                id
                name
                video
                thumbnail
                thumbnail400
                notes
              }
            }
          }
        }
      }
      cooldowns {
        id
        name
        description
        sessionBlocks {
          blockType
          sessionSets {
            exercises {
              name
              id
            }
            timeDuration
            exerciseSets {
              id
              reps
              order
              trackReps
              timeDuration
              exercise {
                id
                name
                video
                thumbnail
                thumbnail400
                notes
              }
            }
          }
        }
      }
    }
  }
`;

const GET_USER = gql`
  query user($id: ID!, $locale: String!) {
    user(id: $id, locale: $locale) {
      id
      workoutSettingSound
    }
  }
`;

const infoStyles = StyleSheet.create({
  infoContainer: {
    flexDirection: 'row',
  },
  text: {
    marginLeft: 10,
    flex: 1,
    fontFamily: SUB_TITLE_FONT,
  },
});

const InfoBlock = ({description}) => {
  return (
    <View style={infoStyles.infoContainer}>
      <InfoIcon height={25} width={25} fill={GRAY} />
      <Text style={infoStyles.text}>{description}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  scrollViewcontainer: {
    flex: 1,
    backgroundColor: WHITE,
  },
  container: {
    flex: 1,
    backgroundColor: WHITE,
  },
  badgesContainer: {
    flexDirection: 'row',
    marginTop: 15,
    flexWrap: 'wrap',
  },
  bottomBadgeContainer: {
    paddingBottom: 100,
  },
  blockContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  badge: {
    color: WHITE,
    backgroundColor: ORANGE,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 20,
    paddingRight: 20,
    marginRight: 15,
    borderRadius: 5,
    fontFamily: SUB_TITLE_FONT,
    marginBottom: 10,
  },
  block_text: {
    color: ORANGE,
    paddingVertical: moderateScale(15),
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(16),
  },
  block: {
    color: WHITE,
    backgroundColor: ORANGE,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 20,
    paddingRight: 20,
    marginRight: 15,
    borderRadius: 5,
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: 10,
  },
  badgeInactive: {
    color: WHITE,
    backgroundColor: '#9F9F9F',
  },
  body: {
    marginLeft: 20,
    marginRight: 20,
  },
  programInfo: {
    marginLeft: 30,
    marginTop: 20,
  },
  programLink: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
  },
  genericGrayText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
  },
  orangeUnderlineText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
  },
  equipmentTextView: {
    marginTop: -moderateScale(70),
    marginBottom: moderateScale(25),
  },
  muscleTitleView: {
    marginBottom: moderateScale(10),
  },
  muscleInfoView: {
    marginBottom: -moderateScale(25),
    marginTop: moderateScale(25),
  },
  routineView: {
    marginTop: moderateScale(10),
    marginBottom: moderateScale(120),
  },
  changeButtonsContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    backgroundColor: '#F2F2F2',
    marginBottom: moderateScale(5),
    marginTop: moderateScale(-5),
  },
  changeButton: {
    backgroundColor: ORANGE,
    borderWidth: 1,
    borderRadius: moderateScale(20),
    overflow: 'hidden',
    borderColor: ORANGE,
    margin: moderateScale(10),
  },
  changeButtonText: {
    color: WHITE,
    fontSize: calcFontSize(16),
    paddingVertical: moderateScale(5),
    paddingHorizontal: moderateScale(20),
    fontFamily: SUB_TITLE_FONT,
  },
});

const wu = StyleSheet.create({
  header: {
    paddingTop: 20,
    paddingBottom: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  container: {
    borderWidth: 1,
    borderColor: '#E5E5E5',
    marginTop: 20,
  },
  text: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
  },
  chevron: {
    alignSelf: 'flex-end',
    marginRight: 15,
  },
  exPlaceholder: {
    height: 40,
    width: 45,
    backgroundColor: GRAY,
    borderRadius: 10,
    marginRight: 10,
  },
  exContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: moderateScale(10),
    paddingTop: 5,
    paddingBottom: 5,
  },
  exText: {
    flex: 1,
    fontSize: 16,
    fontFamily: MAIN_TITLE_FONT,
  },
  lapText: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    marginBottom: 10,
  },
  buttonContainer: {
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
  workoutContent: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  pl20: {
    paddingLeft: 20,
  },
  showAllWorkoutButton: {
    backgroundColor: WHITE,
  },
  showAllWorkoutButtonTitle: {
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
  },
  workoutListContainer: {
    backgroundColor: '#FFFFFF',
    overflow: 'hidden',
  },
});

const Workout = ({navigation, route}) => {
  function toUnderscores(str) {
    return str.replace(/ /g, '_').toLowerCase();
  }

  const {userData: {token} = userInfo} = useContext(AuthContext);
  const {translations, appLanguage: locale} = useContext(LocalizationContext);

  console.log('localesss', locale);
  const {
    programId,
    programCodeName,
    sessionId,
    sessionDescription,
    sessionName,
    afterPayment,
    prevScreen,
  } = route.params;
  const [showAllWorkout, setShowAllWorkout] = useState(false);
  const [showWarmupList, setShowWarmupList] = useState(false);
  const [showChangeExerciseKey, setShowChangeExerciseKey] = useState(false);
  const [workoutSessions, setWorkoutSessions] = useState([]);
  const [warmUpSessions, setWarmUpSessions] = useState([]);
  const [currentWarmUpSession, setCurrentWarmUpSession] = useState(null);
  const [cooldownSessions, setCooldownSessions] = useState([]);
  const [currentCooldownSession, setCurrentCooldownSession] = useState([]);
  const {data: userData, refetch: refetchUser} = useQuery(GET_USER, {
    variables: {id: route.params?.userId, locale: locale},
  });

  console.log(1717, userData);
  console.log('workoutSessions', workoutSessions);
  console.log('warmUpSessions', warmUpSessions);
  console.log('currentWarmUpSession', currentWarmUpSession);
  console.log('cooldownSessions', cooldownSessions);

  const {data: d, error, loading, refetch: refetchSession} = useQuery(
    GET_SESSION,
    {
      variables: {locale: locale, sessionId: sessionId},
      context: {
        headers: {
          authorization: token,
        },
      },
    },
  );

  const isFocused = useIsFocused();

  useEffect(() => {
    refetchUser();
    refetchSession();
  }, [locale, refetchSession, refetchUser]);

  console.log({d, error, loading, programId});

  const session = d?.getSession;
  const userInfo = userData?.user;
  const sessionBodyPartsFocused = session?.bodyPartsFocused;
  console.log('parts focs', sessionBodyPartsFocused);
  // [{name: 'upper_body', value: 20}, {name: 'lowwer_body', value: 30}, {name: 'other', value: 22}];//

  //disable go back for ios
  Workout.navigationOptions = screenProps => ({
    gesturesEnabled: afterPayment ? false : true,
  });

  useFocusEffect(
    useCallback(() => {
      if (isFocused && session) {
        console.log('Load Workout');
        trackEvents({
          eventName: 'Load Workout',
          properties: {
            training_program: programCodeName,
            session_id: session?.id,
            session_name: session?.name,
            session_type: session?.sessionType,
            placement: prevScreen,
            session_order: session?.order,
          },
        });
      }
    }, [isFocused, prevScreen, programCodeName, session]),
  );

  useEffect(() => {
    //custom go back for android
    let backHandler = () => {};
    afterPayment &&
      (backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        navigation.navigate('HomeStack', {
          screen: 'Home',
          params: {doRefetch: true},
        });
      }));
    () => {
      backHandler.remove();
    };
  }, [navigation, afterPayment]);

  useEffect(() => {
    if (session && userInfo) {
      const convertSessionBlock = sessions => {
        return sessions.map((block, blockIdx) => {
          let allSets = {};
          const allSessionSets = block.sessionSets;
          allSets = allSessionSets.reduce((acc, cur, index) => {
            acc[index + 1] = {
              allExercises: cur.exerciseSets,
              timeDuration: cur.timeDuration,
            };
            return acc;
          }, {});
          console.log(toUnderscores(block.blockType));
          return {
            sessionNumber: blockIdx + 1,
            blockType:
              locale === 'es'
                ? EnumBlockName[toUnderscores(block.blockType)]
                : block.blockType.toLowerCase(),
            allSets,
          };
        });
      };
      setWorkoutSessions(convertSessionBlock(session.sessionBlocks));
      session.warmups &&
        setWarmUpSessions(
          session.warmups.map((warmup, index) =>
            convertSessionBlock(warmup.sessionBlocks),
          ),
        );
      session.warmup &&
        setCurrentWarmUpSession(
          convertSessionBlock(session.warmup.sessionBlocks),
        );
      session.cooldown &&
        setCurrentCooldownSession(
          convertSessionBlock(session.cooldown.sessionBlocks),
        );
      session.cooldowns &&
        setCooldownSessions(
          session.cooldowns.map((cooldown, index) =>
            convertSessionBlock(cooldown.sessionBlocks),
          ),
        );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [session, userInfo]);

  const handleOnPress = () => {
    let warmups = [];
    if (currentWarmUpSession) {
      const {id, name, description} = session.warmup;
      warmups = [{id, name, description, wo: currentWarmUpSession}];
    } else {
      warmups = session.warmups.map((warmup, index) => {
        const {id, name, description} = warmup;
        return {id, name, description, wo: warmUpSessions[index]};
      });
    }
    let cooldowns = [];
    if (currentCooldownSession && currentCooldownSession?.length) {
      const {id, name, description} = session.cooldown;
      cooldowns = [{id, name, description, wo: currentCooldownSession}];
    } else {
      cooldowns = session.cooldowns.map((cooldown, index) => {
        const {id, name, description} = cooldown;
        return {id, name, description, wo: cooldownSessions[index]};
      });
    }
    navigation.navigate('WarmUp', {
      wo: workoutSessions,
      sessionId,
      session,
      programCodeName,
      programId,
      warmups: warmups,
      cooldowns: cooldowns,
      workoutSettingSound: userInfo?.workoutSettingSound,
    });
  };

  const goToWorkoutSettings = () => {
    navigation.navigate('WorkoutSettings', {
      session,
      sessionId,
      programId,
      programCodeName,
    });
  };

  const goToExerciseInfo = () => {
    navigation.navigate('WorkoutSettings'); //  SettingsDuringWorkout for testing screen
  };

  //AsyncStorage.clear();
  const goToWorkout = React.useCallback(
    (program, session) => {
      navigation.navigate('Workout', {
        programId: program?.id,
        sessionId: session?.id,
        sessionDescription: session?.description,
        sessionName: session?.name,
        userId: userInfo?.id,
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [navigation],
  );

  const toggleShowWorkout = React.useCallback(() => {
    setShowAllWorkout(prev => !prev);
  }, []);

  const toggleShowWarmupsList = React.useCallback(() => {
    setShowWarmupList(prev => !prev);
  }, []);

  const workoutListHeight = showAllWorkout ? 'auto' : 400;
  const warmupsListHeight = showWarmupList ? 'auto' : 400;

  //MUSCLE FOCUS DATA HERE
  const radarChartData = sessionBodyPartsFocused?.map(item => {
    return {
      name: `${capitalize(item.name || 'other')}`,
      value: item.value,
    };
  });

  const renderSessionBlock = (sessionBlock, typeSession) => {
    return (
      <View key={sessionBlock.sessionNumber}>
        <View style={styles.blockContainer}>
          <Text style={styles.block_text}>
            {translations.workoutScreen.block} {sessionBlock.sessionNumber} -{' '}
            {sessionBlock.blockType}
          </Text>
        </View>
        {Object.keys(sessionBlock.allSets).map(setLap => {
          return (
            <View key={`${setLap}-${sessionBlock}`}>
              <View>
                <Text style={wu.lapText}>
                  {translations.workoutScreen.round} {setLap}{' '}
                </Text>
              </View>
              {sessionBlock.allSets[setLap].allExercises.map(ex => {
                const exerciseKey = `${ex.exercise.id}-${ex.id}`;
                return (
                  <>
                    <TouchableOpacity
                      style={[wu.exContainer]}
                      key={exerciseKey}
                      onPress={() => {
                        setShowChangeExerciseKey(
                          showChangeExerciseKey !== exerciseKey
                            ? exerciseKey
                            : false,
                        );
                      }}>
                      {/* <View  style={wu.exPlaceholder}> */}
                      <FastImage
                        source={{
                          uri: ex.exercise.thumbnail,
                          priority: FastImage.priority.high,
                        }}
                        style={wu.exPlaceholder}
                      />
                      {/* </View> */}
                      <Text style={wu.exText}>
                        {ex.reps
                          ? `${ex.reps} ${ex.exercise.name} `
                          : `${ex.timeDuration}'' ${ex.exercise.name} `}
                      </Text>
                    </TouchableOpacity>
                    {showChangeExerciseKey === exerciseKey && (
                      <View style={styles.changeButtonsContainer}>
                        <TouchableOpacity
                          style={styles.changeButton}
                          onPress={() =>
                            navigation.navigate('ExerciseDetails', {
                              exerciseId: ex.exercise.id,
                            })
                          }>
                          <Text
                            style={[
                              styles.changeButtonText,
                              {color: ORANGE, backgroundColor: '#F2F2F2'},
                            ]}>
                            {translations.workoutScreen.knowMore}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={styles.changeButton}
                          onPress={() => {
                            setShowChangeExerciseKey(false);
                            navigation.navigate('ChangeExercise', {
                              exerciseId: ex.exercise.id,
                              workoutSessions,
                              setWorkoutSessions,
                              warmUpSessions,
                              setWarmUpSessions,
                              currentWarmUpSession,
                              setCurrentWarmUpSession,
                              typeSession,
                            });
                          }}>
                          <Text style={styles.changeButtonText}>
                            {translations.workoutScreen.change}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    )}
                  </>
                );
              })}
            </View>
          );
        })}
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scrollViewcontainer}>
        <View>
          <Header
            onPressBackButton={() =>
              afterPayment
                ? navigation.navigate('HomeStack', {
                    screen: 'Home',
                    params: {doRefetch: true},
                  })
                : navigation.goBack()
            }
            title={sessionName}
            onPressMoreInfo={goToWorkoutSettings}
          />
        </View>
        <View style={styles.body}>
          <InfoBlock description={sessionDescription} />
          <View style={styles.programInfo}>
            {/* <Text style={styles.programLink}>
              {translations.workoutScreen.whatIs}{' '}
              {translations.workoutScreen.tabHeader}?
            </Text> */}
            <View style={styles.badgesContainer} />
          </View>
        </View>
        {/* {currentWarmUpSession && (
          <View style={wu.container}>
            <TouchableOpacity onPress={toggleShowWarmupsList}>
              <View style={wu.header}>
                <Text style={[wu.text, wu.pl20]}>
                  {translations.workoutScreen.warmUp}
                </Text>
                {!showWarmupList && (
                  <View style={wu.chevron}>
                    <ArrowDown height={17} width={17} />
                  </View>
                )}
              </View>
            </TouchableOpacity>
            {showWarmupList && (
              <View style={wu.workoutContent}>
                <View
                  style={[
                    wu.workoutListContainer,
                    {
                      height: warmupsListHeight,
                    },
                  ]}>
                  {currentWarmUpSession.map(sessionBlock => {
                    return renderSessionBlock(sessionBlock, 'warmUp');
                  })}
                </View>
                <Button
                  buttonStyle={wu.showAllWorkoutButton}
                  titleStyle={wu.showAllWorkoutButtonTitle}
                  title={translations.workoutScreen.seeLess}
                  onPress={toggleShowWarmupsList}
                />
              </View>
            )}
          </View>
        )} */}
        {loading ? (
          <Loading />
        ) : (
          <View style={wu.workoutContent}>
            <View style={wu.header}>
              <Text style={wu.text}>{translations.workoutScreen.training}</Text>
            </View>
            <View
              style={[
                wu.workoutListContainer,
                {
                  height: workoutListHeight,
                },
              ]}>
              {workoutSessions?.map(sessionBlock => {
                return renderSessionBlock(sessionBlock, 'workout');
              })}
            </View>
            <Button
              buttonStyle={wu.showAllWorkoutButton}
              titleStyle={wu.showAllWorkoutButtonTitle}
              title={
                showAllWorkout
                  ? translations.workoutScreen.seeLess
                  : translations.workoutScreen.seeMore
              }
              onPress={toggleShowWorkout}
            />
            {/* <View style={wu.header}>
              <Text style={wu.text}>{translations.workoutScreen.focus}</Text>
            </View> */}
            {/* <View style={[styles.badgesContainer, styles.bottomBadgeContainer]}>
              {sessionBodyPartsFocused.map(bodyPart => {
                return (
                  <Text
                    key={bodyPart}
                    style={[styles.badge, styles.badgeInactive]}>
                    {capitalize(bodyPart)}
                  </Text>
                );
              })}
            </View> */}
            {/* <Text style={[wu.text, styles.equipmentTextView]}>
              {translations.profileScreen.equipment}
            </Text>
            {renderEquipment('Pull-up bar', MagentoEquipment)} */}
            <View style={styles.muscleTitleView}>
              <Text style={[wu.text, styles.muscleInfoView]}>
                {translations.workoutScreen.muscleFocus}
              </Text>
              <View style={{marginTop: moderateScale(25)}}>
                <RadarChartComponent data={radarChartData} />
              </View>
            </View>
            <Text style={[wu.text]}>
              {translations.workoutScreen.breakRoutine}
            </Text>
            <View style={styles.routineView}>
              <Text style={styles.genericGrayText}>
                {translations.workoutScreen.breakRoutineDescription}
                {'\n'}
              </Text>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('ListChallengeSessionsProgram', {
                    userId: userInfo?.id,
                    goToWorkout: goToWorkout,
                  });
                }}>
                <Text style={styles.orangeUnderlineText}>
                  {translations.workoutScreen.takeChallenge}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </ScrollView>
      {!loading && Array.isArray(workoutSessions) && (
        <View style={[wu.buttonContainer, {top: deviceHeight - 120}]}>
          <MButton
            main
            onPress={handleOnPress}
            title={translations.workoutScreen.start}
            accessibilityLabel={translations.workoutScreen.start}
            buttonExtraStyles={{maxWidth: deviceWidth * 0.85}}
          />
        </View>
      )}
    </SafeAreaView>
  );
};

export default Workout;
