import {StyleSheet} from 'react-native';
import {
  GRAY_BORDER,
  GRAY,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
  BLACK,
} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
  deviceWidth,
} from '../../utils/dimensions';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE,
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  blockTitle: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    marginBottom: 10,
  },
  allSettings: {
    padding: 20,
    paddingTop: 0,
    marginTop: moderateScale(-15),
  },
  settingTitle: {
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: 5,
    color: BLACK,
  },
  settingsDescription: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    marginBottom: 10,
  },
  blockContainer: {
    borderBottomWidth: calcHeight(1),
    borderColor: GRAY_BORDER,
    paddingBottom: 15,
    paddingTop: 25,
  },
  optionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // borderWidth: 3,
    // borderColor: 'yellow',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  optionContent: {
    flexBasis: '75%',
  },
  deleteWorkout: {
    color: LIGHT_ORANGE,
    textDecorationStyle: 'solid',
    textDecorationLine: 'underline',
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
  },
  deleteWorkoutContainer: {
    marginBottom: moderateScale(20),
    marginTop: moderateScale(40),
    alignItems: 'center',
  },
  modal: {
    marginTop: moderateScale(80),
    marginHorizontal: moderateScale(10),
    paddingHorizontal: moderateScale(25),
    paddingTop: moderateScale(30),
    paddingBottom: moderateScale(15),
    backgroundColor: WHITE,
    borderRadius: 10,
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
  },
  modalTitle: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: 20,
    marginTop: 10,
    color: BLACK,
  },
  modalText: {
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
    lineHeight: 20,
    color: BLACK,
  },
  buttonExtraStyles: {
    width: 'auto',
  },
  buttonText: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: 14,
    backgroundColor: WHITE,
    textDecorationLine: 'underline',
  },
  button: {
    backgroundColor: WHITE,
  },
  activityIndicator: {
    marginTop: calcHeight(55),
  },
  duringWorkoutScreenTitle: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(22),
    color: BLACK,
    textAlign: 'center',
    marginTop: moderateScale(20),
  },
  duringWorkoutScreenChange: {
    color: LIGHT_ORANGE,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
  duringWorkoutScreenLine: {
    height: calcHeight(1),
    marginHorizontal: calcWidth(20),
    backgroundColor: GRAY_BORDER,
    marginVertical: calcHeight(27),
  },
  duringWorkoutScreenFooterButton: {
    marginTop: moderateScale(70),
    marginBottom: moderateScale(20),
  },
  hitSlopTen: {
    top: calcHeight(10),
    bottom: calcHeight(10),
    right: calcWidth(10),
    left: calcWidth(10),
  },
  thumbStyle: {
    width: moderateScale(19),
    height: moderateScale(19),
  },
  image: {
    width: deviceWidth * 0.4,
    height: deviceWidth * 0.4,
    marginTop: moderateScale(30),
    alignSelf: 'center',
    borderRadius: deviceWidth * 0.02,
  },
  headerLine: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginBottom: moderateScale(15),
    marginTop: moderateScale(-15),
  },
  warmupBlock: {
    borderWidth: 1,
    paddingVertical: moderateScale(25),
    paddingLeft: 25,
    marginBottom: moderateScale(15),
    marginHorizontal: moderateScale(15),
    borderRadius: 5,
  },
  warmupBlockTitle: {
    color: BLACK,
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  warmupBlockDescription: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
});

export default styles;
