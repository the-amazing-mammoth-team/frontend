import {StyleSheet} from 'react-native';
import {
  BLACK,
  GRAY,
  GRAY_BORDER,
  LIGHT_ORANGE,
  MEDIUM_GRAY,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../../utils/dimensions';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_FONT,
  SUB_TITLE_FONT_CURSIVE,
} from '../../styles/fonts';

export const styles = StyleSheet.create({
  root: {
    backgroundColor: WHITE,
    flex: 1,
  },
  content: {
    paddingHorizontal: calcWidth(20),
  },
  exerciseView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  mockRect: {
    backgroundColor: MEDIUM_GRAY,
    width: calcWidth(93),
    height: calcHeight(93),
    borderRadius: 5,
  },
  exerciseTitle: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(22),
    marginBottom: calcHeight(5),
  },
  knowMoreText: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
    textDecorationLine: 'underline',
    marginTop: calcHeight(10),
  },
  infoView: {
    marginLeft: calcWidth(20),
    maxWidth: calcWidth(200),
  },
  line: {
    width: '100%',
    height: calcHeight(1),
    backgroundColor: GRAY_BORDER,
    marginVertical: calcHeight(20),
  },
  rightDistanceFireIcon: {
    marginRight: calcWidth(12),
  },
  fireIconsRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  circle: {
    width: moderateScale(17),
    height: moderateScale(17),
    borderRadius: moderateScale(17) / 2,
    backgroundColor: GRAY,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 0,
  },
  circleChecked: {
    backgroundColor: ORANGE,
  },
  hitSlopTen: {
    top: calcHeight(10),
    bottom: calcHeight(10),
    right: calcWidth(10),
    left: calcWidth(10),
  },
  hitSlopTwenty: {
    top: calcHeight(20),
    bottom: calcHeight(20),
    right: calcWidth(20),
    left: calcWidth(20),
  },
  header: {
    backgroundColor: WHITE,
    paddingVertical: calcHeight(13),
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: calcWidth(46),
    height: calcHeight(3),
    backgroundColor: GRAY,
  },
  container: {
    flex: 1,
    backgroundColor: WHITE,
  },
  panel: {
    backgroundColor: WHITE,
  },
  shadowContainer: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: '#000',
  },
  modalStyle: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  mockRectSheet: {
    backgroundColor: MEDIUM_GRAY,
    alignSelf: 'center',
    width: '100%',
    height: calcHeight(220),
  },
  contentSheet: {
    paddingHorizontal: calcWidth(15),
    paddingVertical: calcHeight(15),
  },
  title: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(22),
  },
  subTitle: {
    fontSize: calcFontSize(18),
    marginBottom: calcHeight(15),
  },
  genericText: {
    fontFamily: SUB_TITLE_FONT,
    color: BLACK,
    fontSize: calcFontSize(14),
    marginBottom: calcHeight(10),
  },
  cursiveText: {
    fontFamily: SUB_TITLE_FONT_CURSIVE,
  },
  genericMarginTop: {
    marginTop: calcHeight(15),
  },
  genericMarginBottom: {
    marginBottom: calcHeight(15),
  },
  flatListView: {
    flex: 1,
    paddingHorizontal: calcWidth(20),
  },
});
