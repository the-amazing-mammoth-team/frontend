import React, {useCallback, useContext, useState} from 'react';
import {FlatList, Text, TouchableOpacity, View} from 'react-native';
import Svg, {Path} from 'react-native-svg';
import BottomSheet from 'reanimated-bottom-sheet';
import Animated from 'react-native-reanimated';
import {BLACK, GRAY} from '../../styles/colors';
import Header from '../../components/header';
import {calcHeight, calcWidth, deviceHeight} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import CheckedIcon from '../../assets/icons/checkIcon.svg';
import {styles} from './changeExericeStyles_old';

const ChangeExercise = ({navigation, route}) => {
  const {mockExercises, currentExerciseTitle} = route.params;
  const [info, setInfo] = useState(null);

  const [currentExercise, setCurrentExercise] = useState(currentExerciseTitle);

  const AnimatedView = Animated.View;

  const handleGoBack = useCallback(() => navigation.goBack(), [navigation]);

  const saveAndGoBack = useCallback(
    params => {
      console.log(666, params);

      navigation.navigate('SettingsDuringWorkout', {
        dataExercise: params,
      });
    },
    [navigation],
  );

  const renderFireIcon = useCallback(color => {
    return (
      <View style={styles.rightDistanceFireIcon}>
        <Svg
          width={calcWidth(14)}
          height={calcHeight(17)}
          viewBox="0 0 15 17"
          fill="none"
          xmlns="http://www.w3.org/2000/svg">
          <Path
            d="M9.86965 16.2772C10.4855 15.0455 11.2195 12.948 10.1942 12.0436C9.24911 12.1855 8.4849 12.1864 7.8631 12.0468C6.29736 11.6952 5.80175 10.4547 6.23282 8.86609C4.12622 10.7912 4.03728 13.6298 4.81823 16.3632C4.94508 16.8072 4.44807 17.1709 4.06321 16.9143C3.93214 16.8269 0.853638 14.7376 0.853638 11.5203C0.853638 6.2495 6.82925 6.02272 6.82925 0.498594C6.82925 0.076616 7.3237 -0.154342 7.64728 0.117084C10.0351 2.12014 14.3802 6.9352 14.1364 11.3637C14.0155 13.5592 12.8227 15.4267 10.5912 16.9143C10.1692 17.1956 9.64211 16.7323 9.86965 16.2772Z"
            fill={color}
          />
        </Svg>
      </View>
    );
  }, []);

  const handleIconFill = useCallback(
    count => {
      let arr = [];
      for (let i = 0; i < count; i++) {
        arr.push(renderFireIcon(BLACK));
      }
      return (
        <View style={styles.fireIconsRow}>
          {arr.map((item, index) => {
            return <View key={index}>{renderFireIcon(BLACK)}</View>;
          })}
          {count < 3 && count > 1 && renderFireIcon(GRAY)}
          {count < 2 && count > 0 && renderFireIcon(GRAY)}
          {count < 2 && count > 0 && renderFireIcon(GRAY)}
        </View>
      );
    },
    [renderFireIcon],
  );

  const {translations} = useContext(LocalizationContext);

  let bottomSheetRef = React.createRef();
  let fall = new Animated.Value(1);

  const renderContent = useCallback(
    () => (
      <View style={styles.panel}>
        <View style={styles.contentSheet}>
          {info ? (
            <Text style={styles.title}>{info.title}</Text>
          ) : (
            <Text style={styles.title}>Default title</Text>
          )}
          <Text style={[styles.genericText, styles.genericMarginTop]}>
            {
              'Lie on your stomach with your palms resting on the floor at chest level, your knees on the floor, and your feet raised. From here push yourself with your hands to raise your torso, keeping your body aligned and parallel to the ground.'
            }
          </Text>
          <Text style={[styles.genericText, styles.cursiveText]}>
            {'Count one rep per pushup'}
          </Text>
          <View style={styles.genericMarginTop}>
            <Text style={[styles.title, styles.subTitle]}>
              {translations.workoutScreen.keepInMind}:
            </Text>
            {[
              'Knees on the ground',
              'Look ahead',
              'Your body must be in a complete straight line',
              'The chest must connect with the ground',
            ].map((value, key) => {
              return (
                <View key={key}>
                  <Text style={styles.genericText}>- {value}.</Text>
                </View>
              );
            })}
          </View>
          <View style={styles.genericMarginTop}>
            <Text style={[styles.title, styles.subTitle]}>
              {translations.workoutScreen.alternativeExerice}:
            </Text>
            {['Decline flexion'].map((value, key) => {
              return (
                <View key={key}>
                  <Text style={styles.genericText}>- {value}.</Text>
                </View>
              );
            })}
          </View>
        </View>
      </View>
    ),
    [
      info,
      translations.workoutScreen.alternativeExerice,
      translations.workoutScreen.keepInMind,
    ],
  );

  const renderHeader = useCallback(
    () => (
      <>
        <View style={styles.header}>
          <View style={styles.panelHeader}>
            <View style={styles.panelHandle} />
          </View>
        </View>
        <View style={[styles.mockRectSheet]} />
      </>
    ),
    [],
  );

  const renderShadow = useCallback(() => {
    const animatedShadowOpacity = Animated.interpolate(fall, {
      inputRange: [0, 1],
      outputRange: [0.8, 0],
    });

    return (
      <AnimatedView
        pointerEvents="none"
        style={[
          styles.shadowContainer,
          {
            opacity: animatedShadowOpacity,
          },
        ]}
      />
    );
  }, [fall]);

  const onKnowMorePress = useCallback(
    item => {
      //setInfo(item);
      bottomSheetRef.current.snapTo(0);
    },
    [bottomSheetRef],
  );

  const onSelectExercise = useCallback(
    item => {
      setCurrentExercise(item.title);
      const timer = setTimeout(() => {
        saveAndGoBack(item);
      }, 150);
      return () => clearTimeout(timer);
    },
    [saveAndGoBack],
  );

  const renderItem = useCallback(
    ({item}) => {
      return (
        <View>
          <View style={styles.exerciseView}>
            <View style={styles.mockRect} />
            <View style={styles.infoView}>
              <Text style={styles.exerciseTitle}>{item.title}</Text>
              <View style={styles.fireIconsRow}>
                {handleIconFill(item.hardLevel)}
              </View>
              <TouchableOpacity
                hitSlop={styles.hitSlopTen}
                onPress={() => onKnowMorePress(item)}>
                <Text style={styles.knowMoreText}>
                  {translations.workoutScreen.knowMore}
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => onSelectExercise(item)}
              hitSlop={styles.hitSlopTwenty}
              style={[
                styles.circle,
                item.title === currentExercise && styles.circleChecked,
              ]}>
              {item.title === currentExercise && (
                <CheckedIcon width={calcWidth(12)} height={calcHeight(8)} />
              )}
            </TouchableOpacity>
          </View>
          <View style={styles.line} />
        </View>
      );
    },

    [
      currentExercise,
      handleIconFill,
      onKnowMorePress,
      onSelectExercise,
      translations.workoutScreen.knowMore,
    ],
  );

  const snapPoints = [deviceHeight * 0.8, 0];

  return (
    <View style={styles.container}>
      <BottomSheet
        ref={bottomSheetRef}
        snapPoints={snapPoints}
        initialSnap={1}
        callbackNode={fall}
        enabledGestureInteraction={true}
        renderContent={renderContent}
        renderHeader={renderHeader}
      />
      <Header
        title={translations.workoutScreen.changeExercise}
        onPressBackButton={handleGoBack}
      />
      <View style={styles.flatListView}>
        <FlatList
          data={mockExercises}
          renderItem={renderItem}
          keyExtractor={item => item.id.toString()}
          showsVerticalScrollIndicator={false}
        />
      </View>
      {renderShadow()}
    </View>
  );
};

export default ChangeExercise;
