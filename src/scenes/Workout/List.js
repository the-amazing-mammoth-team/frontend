//@refresh reset;
import React, {
  useRef,
  useEffect,
  useState,
  useCallback,
  useContext,
} from 'react';
import moment from 'moment';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Dimensions,
  BackHandler,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import {Button, Icon} from 'react-native-elements';
import {CommonActions} from '@react-navigation/native';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import FastImage from 'react-native-fast-image';
import GestureRecognizer from 'react-native-swipe-gestures';
import Video from 'react-native-video';
import Sound from 'react-native-sound';
import LinearGradient from 'react-native-linear-gradient';
import {useSafeArea, SafeAreaView} from 'react-native-safe-area-context';
import {useIsFocused} from '@react-navigation/native';
import {
  MAIN_TITLE_FONT,
  MAIN_TITLE_FONT_REGULAR,
  SUB_TITLE_FONT,
} from '../../styles/fonts';
import {
  WHITE,
  GRAY,
  LIGHT_GRAY,
  BLACK,
  LIGHT_ORANGE,
  ORANGE,
} from '../../styles/colors';
import RepsCounterModal from '../../components/Workout/RepsCounter';
import FinishWarmUpsModal from '../../components/Workout/WorkoutModal';
import CooldownModal from '../../components/Workout/CooldownModal';
import Time from '../../components/Time';
// import RadialGradient from 'react-native-radial-gradient';
import {LocalizationContext} from '../../localization/translations';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {Loading} from '../../components/Loading';
import {AuthContext} from '../../../App';
import KeepAwake from 'react-native-keep-awake';
import PauseIcon from '../../assets/icons/pause.svg';
import {trackEvents} from '../../services/analytics';

Sound.setCategory(Platform.OS === 'ios' ? 'Ambient' : 'Playback', true);

const bottomSheetHeight = moderateScale(110);
const counterContainerHeight = calcHeight(100);
let videoHeightScaled = 0;
let headerHeight = calcHeight(50);

const beep = new Sound('beep.mp3', Sound.MAIN_BUNDLE, error => {
  if (error) {
    console.log('failed to load the sound', error);
    return;
  }
});

const playBeep = () => {
  beep.play(success => {
    if (success) {
      console.log('successfully finished playing');
    } else {
      console.log('playback failed due to audio decoding errors');
    }
  });
};

const GlobalCount = React.memo(
  ({count}) => {
    return <Time count={count} />;
  },
  (prevProps, nextProp) => {
    return prevProps.count === nextProp.count;
  },
);

const Countdown = React.memo(
  ({countInSeconds = 60, callback, paused}) => {
    const [count, setCount] = useState(countInSeconds);

    useEffect(() => {
      if (count > 0) {
        const down = () => {
          if (paused) {
            return;
          }
          setCount(count - 1);
        };
        const interval = setInterval(() => {
          down();
        }, 1000);
        return () => {
          clearInterval(interval);
        };
      }
      if (!count) {
        if (callback) {
          callback();
        }
      }
    }, [callback, count, paused]);
    return <Time count={count} />;
  },
  (prevProps, nextProps) => {
    return prevProps.paused === nextProps.paused;
  },
);

const Item = React.memo(
  ({
    item,
    index,
    isActive,
    blockedVideo,
    fallRef,
    isGlobalCountPaused,

    goToNext,
    currentWidth,
  }) => {
    const {translations} = useContext(LocalizationContext);
    const {bottom: bottomInset, top: topInset} = useSafeArea();
    const insetsVertical = bottomInset + topInset;

    const activeSlide = isActive || index === 0;

    return (
      <View
        style={[
          styles.item,
          {
            width: currentWidth,
            height: deviceHeight - headerHeight - insetsVertical - 10, //10 it's progress indicator height
          },
        ]}>
        {activeSlide && (
          <Animated.View
            style={[
              styles.videoBackground,
              {
                transform: [
                  {
                    translateY: fallRef.current.interpolate({
                      inputRange: [0, 1],
                      outputRange: [deviceHeight * -0.17, 0],
                    }),
                  },
                ],
              },
            ]}>
            {item?.exercise?.video && !blockedVideo && (
              <Video
                poster="https://doctod-dev-testing.s3.sa-east-1.amazonaws.com/white-cover.png"
                repeat={true}
                onBuffer={buffer => console.log({buffer})}
                onError={error => console.log({error})}
                source={{
                  uri: item.exercise.video,
                }}
                style={[
                  styles.videoBackground,
                  {
                    width: deviceWidth,
                    height: videoHeightScaled,
                  },
                ]}
                resizeMode="cover"
                rate={1.0}
                playWhenInactive={true}
                onLoad={response => {
                  const {width, height} = response.naturalSize;
                  videoHeightScaled = height * (deviceWidth / width);
                }}
                paused={isGlobalCountPaused}
              />
            )}
            <LinearGradient
              colors={[
                'transparent',
                'rgba(255,255,255, 0)',
                'rgba(255,255,255, 0.1)',
                'rgba(255,255,255, 0.2)',
                'rgba(255,255,255, 0.4)',
                'rgba(255,255,255, 0.8)',
                'rgba(255,255,255, 0.9)',
                'rgba(255,255,255, 1)',
                WHITE,
              ]}
              style={[
                styles.linearGradient,
                {
                  height: videoHeightScaled * 0.25,
                  top: videoHeightScaled - videoHeightScaled * 0.25,
                },
              ]}
              useAngle={true}
              angle={180}>
              <View />
            </LinearGradient>
          </Animated.View>
        )}
        <View />
        <Animated.View
          style={[
            styles.counterContainer,
            {
              bottom: bottomSheetHeight,
            },
            {
              transform: [
                {
                  translateY: fallRef.current.interpolate({
                    inputRange: [0, 1],
                    outputRange: [deviceHeight * -0.35, 0],
                  }),
                },
              ],
            },
          ]}>
          {isActive && (
            <>
              <Animated.Text
                style={[
                  styles.countdown,
                  {
                    fontSize: fallRef.current.interpolate({
                      inputRange: [0, 1],
                      outputRange: [22, 27],
                    }),
                  },
                ]}>
                {item.reps ? (
                  `${item.reps} reps`
                ) : (
                  <Countdown
                    key={index}
                    paused={isGlobalCountPaused}
                    countInSeconds={item.timeDuration}
                    callback={() => goToNext(index + 1)}
                  />
                )}
              </Animated.Text>
              <Animated.Text
                style={[
                  styles.exTitle,
                  {
                    fontSize: fallRef.current.interpolate({
                      inputRange: [0, 1],
                      outputRange: [22, 27],
                    }),
                  },
                ]}>
                {item?.exercise?.name}
              </Animated.Text>
              <Animated.Text
                style={[
                  styles.exSubTitle,
                  {
                    fontSize: fallRef.current.interpolate({
                      inputRange: [0, 1],
                      outputRange: [14, 18],
                    }),
                  },
                ]}>
                {item?.exercise?.notes}
              </Animated.Text>
            </>
          )}
        </Animated.View>
      </View>
    );
  },
  (prevProps, nextProps) => {
    prevProps.currentWidth === nextProps.currentWidth &&
      prevProps.isActive === nextProps.isActive &&
      prevProps.isGlobalCountPaused === nextProps.isGlobalCountPaused;
  },
);

const ProgressIndicator = React.memo(
  ({progressPercentage}) => {
    return (
      <View style={[styles.progressIndicator]}>
        <View
          style={[
            styles.progressIndicator,
            styles.progressIndicatorCompleted,
            {
              width: `${progressPercentage * 100}%`,
            },
          ]}
        />
      </View>
    );
  },
  (prevProps, nextProps) => {
    return (
      prevProps.progressPercentage === nextProps.progressPercentage ||
      nextProps.progressPercentage === -Infinity ||
      prevProps.progressPercentage === -Infinity
    );
  },
);

let lastVisibleItemIndex = null;
let expectedItemIndex = null;
let executionTime = 0;
let lastVisibleItemWithReps = null;
let lastLapDuration = 1;

const List = ({route, navigation}) => {
  const fallRef = useRef(new Animated.Value(1));
  const heightRef = useRef(new Animated.Value(1));
  const isFocused = useIsFocused();
  const {
    wo: workout,
    nextWorkout,
    sessionId: SESSION_ID,
    programId: PROGRAM_ID,
    workoutSettingSound,
    isWarmUps,
    isCoolDown,
    session,
    programCodeName,
    saveSession,
  } = route?.params || {};

  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      console.log('using reducer state:', state);
      console.log('using reducer dispatch:', dispatch);
      console.log('using reducer action:', prevState);
      console.log('using reducer dispatch:', action);
      // this reducer kepts sending info

      switch (action.type) {
        case 'SET_EXERCISE':
          const {
            item,
            index,
            repsExecuted,
            executionTime,
            isLastItem = false,
          } = action.value;
          return {
            ...prevState,
            sessionSetExecutions: [
              ...prevState.sessionSetExecutions,
              {
                order: index + 1,
                sessionBlock: item.sessionBlock,
                sessionBlockName: item.sessionBlockName,
                exerciseExecutions: [
                  {
                    order: item.order,
                    exerciseId: item.exercise.id,
                    repsExecuted: repsExecuted || item.reps,
                    executionTime,
                    lapLength: item.lapLength,
                    setIndex: item.setIndex,
                    trackReps: item.trackReps,
                    timeDuration: item.timeDuration,
                    name: item.exercise.name,
                    metMultiplier: item.exercise.metMultiplier,
                    bodyPartsFocused: item.exercise.bodyPartsFocused,
                  },
                ],
                isLastItem,
              },
            ],
          };
      }
    },
    {
      sessionSetExecutions: [],
    },
  );

  const [visibleItemIndex, setVisibleItemIndex] = useState(0);

  const allSessionBlocks = workout || [];
  const listRef = useRef();
  const allLapsNumber = workout?.reduce((count, sessionBlock) => {
    const laps = Math.max(...Object.keys(sessionBlock.allSets).map(Number));
    count = count + laps;
    return count;
  }, 0);
  const [globalCountIsPaused, setGlobalCountIsPaused] = useState(false);
  const [globalCount, setGlobalCount] = useState(0);
  const [blockedVideo, setBlockedVideo] = useState(false);
  const {translations} = useContext(LocalizationContext);
  const {userData: userInfo} = useContext(AuthContext);

  useEffect(() => {
    //disable go back
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      () => {
        return false;
      },
    );
    () => {
      backHandler.remove();
    };
  }, [navigation]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      setGlobalCountIsPaused(true);
      KeepAwake.deactivate();
      lastVisibleItemIndex = null;
      expectedItemIndex = null;
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      console.log('on focus');
      setGlobalCountIsPaused(false);
      KeepAwake.activate();
    });
    return unsubscribe;
  }, [navigation, programCodeName, session]);

  const goToNext = useCallback(
    index => {
      if (flatListData[visibleItemIndex].lapDuration) {
        //go to next exercise from set
        const nextIndex = visibleItemIndex + 1;
        if (
          currentLapOfAllBlocks ===
          flatListData[nextIndex]?.lapNumberOfAllBlocks
        ) {
          expectedItemIndex = nextIndex;
          listRef.current.scrollToIndex({index: nextIndex});
        } else {
          //get first exersice from set
          ++lastLapDuration;
          const firstExIndex = flatListData.findIndex(
            item => item.lapNumberOfAllBlocks === currentLapOfAllBlocks,
          );
          expectedItemIndex = firstExIndex;
          listRef.current.scrollToIndex({index: firstExIndex});
        }
      } else {
        //go to next
        if (flatListData[index]) {
          expectedItemIndex = index;
          listRef.current.scrollToIndex({index});
        } else if (index > flatListData.length - 1) {
          playBeep();
          handleNextWorkout();
        }
      }
    },
    [currentLapOfAllBlocks, flatListData, handleNextWorkout, visibleItemIndex],
  );

  const goToNextSet = useCallback(
    setIndex => {
      const index = flatListData.findIndex(
        item => item.lapNumberOfAllBlocks === setIndex,
      );
      if (index > 0) {
        lastLapDuration = 1;
        expectedItemIndex = index;
        listRef.current.scrollToIndex({index});
      } else {
        handleNextWorkout();
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [flatListData, handleNextWorkout, state.sessionSetExecutions],
  );

  const [showRepsCounterModal, setShowRepsCounterModal] = useState(false);
  const [showFinishWarmUpsModal, setShowFinishWarmUpsModal] = useState(false);
  const [showCoolDownModal, setShowCoolDownModal] = useState(false);

  const onSaveSessionExecution = useCallback(async () => {
    try {
      let sessionSetExecutionsByBlock = state.sessionSetExecutions.reduce(
        (result, ex) => {
          const blockIndex = ex.sessionBlock - 1;
          if (result[blockIndex]) {
            result[blockIndex].push(ex);
          } else {
            result[blockIndex] = [ex];
          }
          return result;
        },
        [],
      ); //grouping sessions to blocks
      sessionSetExecutionsByBlock = sessionSetExecutionsByBlock.map(block => {
        return {
          order: block[0].sessionBlock,
          blockName: block[0].sessionBlockName,
          sessionSetExecutions: block.reduce((acc, item, index) => {
            const ex = item.exerciseExecutions[0];
            const newEx = {
              exerciseId: ex.exerciseId,
              order: ex.order,
              repsExecuted: ex.repsExecuted,
              executionTime: ex.executionTime,
              name: ex.name,
              metMultiplier: ex.metMultiplier,
              bodyPartsFocused: ex.bodyPartsFocused,
            };
            if (acc[+ex.setIndex - 1]) {
              acc[+ex.setIndex - 1].exerciseExecutions.push(newEx);
            } else {
              acc[+ex.setIndex - 1] = {
                order: +ex.setIndex,
                exerciseExecutions: [newEx],
              };
            }
            return acc;
          }, []),
        };
      });
      // TODO: Sum exercise if they are repeteated inside exerciseExecutions
      const sessionExecutions = {
        sessionId: SESSION_ID,
        programId: PROGRAM_ID,
        sessionBlockExecutions: sessionSetExecutionsByBlock,
      };
      console.log('state.sessionSetExecutions', state.sessionSetExecutions);
      console.log(sessionExecutions);
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [
            {
              name: 'CelebrationPostWorkout',
              params: {
                sessionExecutions,
                programCodeName,
                session,
              },
            },
          ],
        }),
      );
    } catch (error) {
      console.log(error);
    }
  }, [PROGRAM_ID, SESSION_ID, navigation, programCodeName, session, state]);

  const renderContent = useCallback(() => {
    const handleGoToNext = id => {
      const index = flatListData?.findIndex(item => item.id === id);
      if (index !== -1) {
        !flatListData[visibleItemIndex]?.lapDuration && goToNext(index);
      }
    };
    let currentExIndex = 0;
    let currentSetIndex = 0;
    const fistLetterToUpperCase = str => {
      return str.charAt(0).toUpperCase() + str.slice(1);
    };
    return (
      <View style={styles.sheetContent}>
        {allSessionBlocks.map(sessionBlock => {
          return (
            <View key={sessionBlock.sessionNumber}>
              {sessionBlock.sessionNumber >
                currentVisibleItem?.sessionBlock && (
                <Text style={styles.blockHeaderText}>
                  {fistLetterToUpperCase(sessionBlock.blockType)}
                </Text>
              )}
              {Object.keys(sessionBlock.allSets).map(index => {
                const set = sessionBlock.allSets[index];
                currentSetIndex = currentSetIndex + 1;
                return (
                  <View key={`sessionBlock-${index}`}>
                    {currentExIndex >= visibleItemIndex + 1 && (
                      <Text style={styles.setText}>
                        {translations.workoutListScreen.lap} {index}
                      </Text>
                    )}
                    {set.allExercises.map(ex => {
                      currentExIndex = currentExIndex + 1;
                      if (
                        flatListData[visibleItemIndex]?.IS_LAST_ITEM &&
                        flatListData[visibleItemIndex]?.lapDuration
                      ) {
                        // for rendering full list of exercises by lap
                      } else if (currentExIndex <= visibleItemIndex + 1) {
                        return;
                      }
                      return (
                        <View>
                          <View style={styles.exContainer}>
                            <FastImage
                              source={{
                                uri: ex.exercise.thumbnail,
                                priority: FastImage.priority.high,
                              }}
                              style={styles.exPlaceholder}
                            />
                            <Text style={styles.sheetText}>
                              {ex.reps
                                ? `${ex.reps} ${ex.exercise.name}`
                                : `${ex.timeDuration}'' ${ex.exercise.name}`}
                            </Text>
                          </View>
                        </View>
                      );
                    })}
                  </View>
                );
              })}
            </View>
          );
        })}
        {/* <View>
          <Text style={styles.sheetText}>Has terminado</Text>
        </View> */}
      </View>
    );
  }, [
    allSessionBlocks,
    currentVisibleItem,
    flatListData,
    goToNext,
    translations,
    visibleItemIndex,
  ]);

  const renderHeader = useCallback(() => {
    let headerName = translations.countDownScreen.next;
    if (
      flatListData[visibleItemIndex]?.IS_LAST_ITEM &&
      !flatListData[visibleItemIndex]?.lapDuration
    ) {
      if (!isWarmUps) {
        headerName = translations.workoutListScreen.workoutCompleted;
      } else {
        headerName = translations.workoutListScreen.warmupCompleted;
      }
    }
    return (
      <View style={styles.sheetHeader}>
        <View style={styles.headerWrapper}>
          <Text style={styles.sheetText}>{headerName}</Text>
        </View>
      </View>
    );
  }, [flatListData, isWarmUps, translations, visibleItemIndex]);

  let lastLap = 1;
  const flatListData = workout?.reduce((arr, sessionBlock, index) => {
    Object.keys(sessionBlock.allSets).map(setIndex => {
      const set = sessionBlock.allSets[setIndex];
      set.allExercises.forEach((ex, index) => {
        arr = arr.concat({
          ...ex,
          setIndex,
          exNumberInLap: index + 1,
          totalLaps: Object.keys(sessionBlock.allSets).length,
          lapLength: set.allExercises.length,
          lapNumberInBlock: parseInt(setIndex),
          lapNumberOfAllBlocks: lastLap,
          lapDuration: set.timeDuration,
          sessionBlock: sessionBlock.sessionNumber,
          sessionBlockName: sessionBlock.blockType,
          sessionBlockLength: workout.length,
        });
      });
      ++lastLap;
    });
    if (index === workout.length - 1) {
      let lastItem = arr[arr.length - 1];
      arr[arr.length - 1] = {
        IS_LAST_ITEM: true,
        ...lastItem,
      };
      return arr;
    } else {
      return arr;
    }
  }, []);

  const currentVisibleItem = flatListData[visibleItemIndex];
  const currentLapOfAllBlocks = currentVisibleItem?.lapNumberOfAllBlocks || 1;

  const goToSettings = useCallback(() => {
    isWarmUps || isCoolDown
      ? navigation.navigate('WorkoutSettings', {
          sessionId: SESSION_ID,
          programId: PROGRAM_ID,
          isWarmUps,
          isCoolDown,
        })
      : navigation.navigate('SettingsDuringWorkout', {
          exercise: flatListData[visibleItemIndex],
          sessionId: SESSION_ID,
          programId: PROGRAM_ID,
          session,
          programCodeName,
        });
  }, [
    PROGRAM_ID,
    SESSION_ID,
    flatListData,
    isCoolDown,
    isWarmUps,
    navigation,
    programCodeName,
    session,
    visibleItemIndex,
  ]);

  const goToWorkout = () => {
    closeFinishWarmUpsModal();
    navigation.navigate('DownloadingVideos', {
      wo: nextWorkout,
      sessionId: SESSION_ID,
      programId: PROGRAM_ID,
      isWarmUps: false,
    });
  };

  const isGlobalCountPaused =
    globalCountIsPaused ||
    showRepsCounterModal ||
    showCoolDownModal ||
    showFinishWarmUpsModal;
  //console.log('isGlobalCountPaused', isGlobalCountPaused);

  useEffect(() => {
    let interval;
    if (isFocused && !isGlobalCountPaused) {
      const tick = () => {
        if (isGlobalCountPaused) {
          return;
        }
        const delta = (Date.now() - start) / 1000;
        setGlobalCount(parseInt(delta));
        ++executionTime;
      };
      const start = globalCount ? Date.now() - globalCount * 1000 : Date.now();
      interval = setInterval(tick, 1000);
    } else {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isGlobalCountPaused, isFocused]);

  const renderItem = ({item, index, currentWidth}) => {
    const isActive = index === visibleItemIndex;
    return (
      <Item
        currentWidth={currentWidth}
        item={item}
        index={index}
        isActive={isActive}
        blockedVideo={blockedVideo}
        fallRef={fallRef}
        isGlobalCountPaused={isGlobalCountPaused}
        data={flatListData}
        goToNext={goToNext}
      />
    );
  };

  const handleOnChangeVisible = useCallback(item => {
    !item?.item?.exercise?.isWarmUp &&
      !item?.item?.exercise?.isCoolDown &&
      dispatch({type: 'SET_EXERCISE', value: item});
    executionTime = 0;
  }, []);

  const itemsCompletedRef = useRef([]);

  useEffect(() => {
    //console.log(flatListData[lastVisibleItemIndex]?.trackReps);
    if (workoutSettingSound && lastVisibleItemIndex !== null) {
      // console.log('BEEP'); //for test on emulators
      playBeep();
    }
    const lastItem = flatListData[lastVisibleItemIndex];
    if (lastItem?.trackReps) {
      lastVisibleItemWithReps = lastVisibleItemIndex;
      setBlockedVideo(true);
      requestAnimationFrame(() => {
        setShowRepsCounterModal(true);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lastVisibleItemIndex]);

  useEffect(() => {
    if (!showRepsCounterModal) {
      setBlockedVideo(false);
      if (
        //if last item with trackReps = true
        flatListData[visibleItemIndex]?.trackReps &&
        visibleItemIndex === flatListData.length - 1 &&
        lastVisibleItemWithReps === visibleItemIndex
      ) {
        isCoolDown ? saveSession() : openCooldownModal();
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [showRepsCounterModal]);

  const setVisibleItem = useCallback(
    d => {
      const currentViewableItem = Math.max(
        ...d.viewableItems.map(item => item.index),
      );
      if (
        expectedItemIndex === currentViewableItem ||
        expectedItemIndex === null
      ) {
        expectedItemIndex = null;
        if (isFinite(currentViewableItem)) {
          setVisibleItemIndex(prev => {
            if (
              (lastVisibleItemIndex !== null && currentViewableItem >= 0) ||
              (lastVisibleItemIndex === null && currentViewableItem > 0)
            ) {
              lastVisibleItemIndex = prev;
            }
            const item = d.viewableItems[0];
            if (item) {
              if (itemsCompletedRef.current.item) {
                const refItem = itemsCompletedRef.current.item;
                const previousItem = {...refItem, executionTime};
                if (!previousItem.item.trackReps) {
                  handleOnChangeVisible(previousItem);
                }
              }
              itemsCompletedRef.current = {
                item,
              };
            }
            return currentViewableItem;
          });
        }
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [handleOnChangeVisible],
  );

  const handleNextWorkout = useCallback(() => {
    if (flatListData[visibleItemIndex]?.trackReps) {
      lastVisibleItemWithReps = visibleItemIndex;
      setBlockedVideo(true);
      requestAnimationFrame(() => {
        setShowRepsCounterModal(true);
      });
      return;
    }
    if (flatListData[visibleItemIndex]?.exercise?.isWarmUp) {
      openFinishWarmUpsModal();
    } else if (!flatListData[visibleItemIndex]?.exercise?.isCoolDown) {
      if (!flatListData[visibleItemIndex]?.trackReps) {
        handleOnChangeVisible({
          item: flatListData[visibleItemIndex],
          executionTime,
          index: visibleItemIndex,
        });
      }
      openCooldownModal();
    } else if (flatListData[visibleItemIndex]?.exercise?.isCoolDown) {
      saveSession();
    } else {
      handleOnChangeVisible({
        item: flatListData[visibleItemIndex],
        executionTime,
        index: visibleItemIndex,
        isLastItem: true,
      });
    }
  }, [
    flatListData,
    handleOnChangeVisible,
    openCooldownModal,
    saveSession,
    visibleItemIndex,
  ]);

  useEffect(() => {
    if (
      state?.sessionSetExecutions?.length &&
      state.sessionSetExecutions[state.sessionSetExecutions.length - 1]
        .isLastItem
    ) {
      isCoolDown ? saveSession() : openCooldownModal();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state]);

  const openFinishWarmUpsModal = () => {
    setBlockedVideo(true);
    setGlobalCountIsPaused(true);
    setShowFinishWarmUpsModal(true);
  };

  const openCooldownModal = useCallback(() => {
    trackEvents({
      eventName: 'Finish Workout',
      properties: {
        training_program: programCodeName,
        session_id: session?.id,
        session_name: session?.name,
        session_type: session?.sessionType,
        session_order: session?.order,
      },
    });
    setBlockedVideo(true);
    setGlobalCountIsPaused(true);
    setShowCoolDownModal(true);
  }, [programCodeName, session]);

  const closeRepsModal = reps => {
    handleOnChangeVisible({
      item: flatListData[lastVisibleItemWithReps],
      executionTime,
      index: lastVisibleItemWithReps,
      repsExecuted: parseInt(reps), //needed validation
    });
    setShowRepsCounterModal(false);
  };
  const closeFinishWarmUpsModal = () => setShowFinishWarmUpsModal(false);

  const goToCoolDown = () => {
    setShowCoolDownModal(false);
    navigation.navigate('ChooseCooldown', {
      cooldowns: nextWorkout,
      sessionId: SESSION_ID,
      session,
      programId: PROGRAM_ID,
      programCodeName,
      workoutSettingSound,
      saveSession: onSaveSessionExecution.bind(this),
    });
  };

  const [currentWidth, setCurrentWidth] = useState(deviceWidth);

  const handleOnLayout = useCallback(ev => {
    const {width, height} = Dimensions.get('window');
    if (width > height) {
      setCurrentWidth(deviceHeight);
    }
    if (width < height) {
      setCurrentWidth(deviceWidth);
    }
  }, []);

  const config = {
    velocityThreshold: 0,
  };

  // if (!isFocused) {
  //   return null;
  // }
  console.log('render Workout');

  return (
    <SafeAreaView style={[styles.container]}>
      <View style={styles.container} onLayout={handleOnLayout}>
        <RepsCounterModal
          closeModal={closeRepsModal}
          isVisible={showRepsCounterModal}
        />
        <FinishWarmUpsModal
          closeModal={closeFinishWarmUpsModal}
          isVisible={showFinishWarmUpsModal}
          isDone={true}
          actionOk={() => goToWorkout()}
          actionCancel={() => {}}
        />
        <CooldownModal isVisible={showCoolDownModal} callback={goToCoolDown} />
        <View style={styles.globalHeaderStyles}>
          <View>
            <Text
              style={[
                styles.mainTime,
                {color: currentVisibleItem?.lapDuration ? ORANGE : BLACK},
              ]}>
              {currentVisibleItem?.lapDuration ? (
                <Countdown
                  key={currentLapOfAllBlocks}
                  paused={isGlobalCountPaused}
                  countInSeconds={currentVisibleItem.lapDuration}
                  callback={() => goToNextSet(currentLapOfAllBlocks + 1)}
                />
              ) : (
                <GlobalCount count={globalCount} />
              )}
            </Text>
          </View>
          <View>
            <Text style={styles.text}>
              {translations.workoutListScreen.lap}{' '}
              {currentVisibleItem.lapDuration
                ? lastLapDuration
                : currentVisibleItem.lapNumberInBlock}
              /
              {currentVisibleItem.lapDuration
                ? '?'
                : currentVisibleItem.totalLaps}
            </Text>
            <Text style={styles.text}>
              {translations.workoutListScreen.exercise}{' '}
              {`${currentVisibleItem.exNumberInLap}/${
                currentVisibleItem.lapLength
              }`}
            </Text>
          </View>
          <TouchableWithoutFeedback onPress={goToSettings}>
            <PauseIcon
              width={styles.workoutMore.width}
              height={styles.workoutMore.height}
            />
          </TouchableWithoutFeedback>
        </View>
        <ProgressIndicator
          progressPercentage={(visibleItemIndex + 1) / flatListData.length}
        />
        <GestureRecognizer
          onSwipeLeft={() => {
            goToNext(visibleItemIndex + 1);
          }}
          config={config}
          style={{
            flex: 1,
            backgroundColor: 'transparent',
          }}>
          <FlatList
            initialNumToRender={2}
            scrollEnabled={false}
            showsVerticalScrollIndicator={true}
            pagingEnabled={true}
            viewabilityConfig={{viewAreaCoveragePercentThreshold: 100}}
            onViewableItemsChanged={setVisibleItem}
            ref={listRef}
            horizontal={true}
            minimumViewTime={5000}
            renderItem={({item, index}) => {
              return renderItem({item, index, currentWidth});
            }}
            data={flatListData}
            scrollEventThrottle={1}
            // onEndReached={onScrollEnd}
            onEndReachedThreshold={0.01}
            extraData={currentWidth}
            onScrollToIndexFailed={error => {
              listRef.current.scrollToOffset({
                offset: error.averageItemLength * error.index,
                animated: false,
              });
              setTimeout(() => {
                if (flatListData !== 0 && listRef !== null) {
                  listRef.current.scrollToIndex({
                    index: error.index,
                    animated: false,
                  });
                }
              }, 100);
            }}
            keyExtractor={item => {
              return `${item.id}-${item.setIndex}`;
            }}
          />
        </GestureRecognizer>
      </View>
      <BottomSheet
        enabledBottomClamp={true}
        snapPoints={['50%', '10%']}
        initialSnap={1}
        renderContent={renderContent}
        renderHeader={renderHeader}
        callbackNode={fallRef.current}
        contentPosition={heightRef.current}
      />
      {flatListData[visibleItemIndex]?.IS_LAST_ITEM &&
        !flatListData[visibleItemIndex]?.lapDuration && (
          <View style={styles.bottomButton}>
            <TouchableOpacity
              onPress={() => handleNextWorkout()}
              style={styles.nextButtonStyleActive}>
              <Icon name="arrow-forward" size={25} color="white" />
            </TouchableOpacity>
          </View>
        )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  item: {
    justifyContent: 'space-between',
    // zIndex: 3,
    flex: 1,
    // borderWidth: 2,
    // borderColor: 'yellow',
  },
  lastItem: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  itemBlock: {
    padding: 15,
  },
  container: {
    flex: 1,
    // backgroundColor: 'blue',
    backgroundColor: WHITE,
  },
  countdown: {
    fontFamily: MAIN_TITLE_FONT,
    textAlign: 'center',
  },
  mainTime: {
    fontSize: 30,
    fontFamily: MAIN_TITLE_FONT,
  },
  exTitle: {
    fontFamily: MAIN_TITLE_FONT_REGULAR,
    textAlign: 'center',
    zIndex: 99999,
  },
  exSubTitle: {
    fontFamily: MAIN_TITLE_FONT_REGULAR,
    textAlign: 'center',
    zIndex: 99999,
    color: GRAY,
  },
  text: {
    fontSize: 14,
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
  },
  videoBackground: {
    ...StyleSheet.absoluteFillObject,
    zIndex: -1,
    backgroundColor: '#FFFFFF',
  },
  sheetContent: {
    fontSize: 20,
    backgroundColor: WHITE,
    padding: 15,
    height: 'auto',
    minHeight: '100%',
    // zIndex: -1,
  },
  sheetHeader: {
    backgroundColor: WHITE,
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
    justifyContent: 'center',
    paddingLeft: moderateScale(20),
    paddingBottom: moderateScale(5),
  },
  sheetContainer: {
    flex: 1,
    // backgroundColor: 'red',
  },
  sheetText: {
    fontSize: 18,
    fontFamily: MAIN_TITLE_FONT,
    zIndex: 2,
  },
  exPlaceholder: {
    height: 40,
    width: 40,
    backgroundColor: GRAY,
    borderRadius: 10,
    marginRight: 10,
  },
  exContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: moderateScale(10),
    paddingTop: 5,
    paddingBottom: 5,
  },
  counterContainer: {
    position: 'absolute',
    backgroundColor: 'transparent',
    alignSelf: 'center',
    height: counterContainerHeight,
    paddingHorizontal: moderateScale(20),
    width: '100%',
    // borderWidth: 3,
    // height: '100%',
  },
  setText: {
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
    marginBottom: 10,
  },
  pauseWrapper: {
    height: 60,
    width: 60,
    marginTop: 10,
  },
  progressIndicator: {
    height: 10,
    width: '100%',
    backgroundColor: LIGHT_GRAY,
    zIndex: 99999,
  },
  progressIndicatorCompleted: {
    zIndex: 999999,
    backgroundColor: LIGHT_ORANGE,
  },
  globalHeaderStyles: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 10,
    height: headerHeight,
  },
  linearGradient: {
    position: 'absolute',
    borderTopWidth: 0,
    zIndex: -1,
    width: deviceWidth,
  },
  headerWrapper: {
    zIndex: 9999,
  },
  blockHeaderText: {
    color: ORANGE,
    paddingVertical: moderateScale(15),
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(16),
  },
  workoutMore: {
    width: calcWidth(35),
    height: calcHeight(35),
    backgroundColor: 'transparent',
  },
  nextButtonStyleActive: {
    borderRadius: 50,
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: ORANGE,
    borderColor: WHITE,
  },
  bottomButton: {
    backgroundColor: WHITE,
    position: 'absolute',
    zIndex: 100,
    bottom: 50,
    right: 20,
  },
});

export default List;
