import React, {useContext, useState, useCallback} from 'react';
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import {useMutation, useQuery} from '@apollo/react-hooks';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';
// import Mixpanel from 'react-native-mixpanel';
import Header from '../../components/header';
import {GRAY_BORDER, ORANGE, RED_BORDER} from '../../styles/colors';
import SettingBlock from '../../components/Workout/SettingBlock';
import {LocalizationContext} from '../../localization/translations';
import {AuthContext} from '../../../App';
import styles from './settingsStyles';
import {moderateScale} from '../../utils/dimensions';
import DiscardWorkoutModal from '../../components/Workout/DiscardWorkoutModal';
import {trackEvents} from '../../services/analytics';

// const MIXPANEL_TOKEN = 'bf47184b8fc05bef8c88adb01de19387';

const SettingsBlock = ({title, options, updateUserDataFunc, updating}) => {
  const toggleSwitch = useCallback(
    async (value, option) =>
      await updateUserDataFunc(option.backendString, value),
    [updateUserDataFunc],
  );
  return (
    <View style={[styles.blockContainer, {borderBottomWidth: 0}]}>
      {!!title && <Text style={styles.blockTitle}>{title}</Text>}
      {options.map(option => {
        return <SettingBlock data={option} onPress={toggleSwitch} />;
      })}
    </View>
  );
};

SettingsBlock.propTypes = {
  title: PropTypes.string,
  options: PropTypes.array,
  updateUserDataFunc: PropTypes.func,
};

const WarmupsBlock = ({warmups, callback, activityId}) => {
  return (
    <>
      {warmups?.map(item => (
        <TouchableOpacity
          style={[
            styles.warmupBlock,
            {
              borderColor: item.id == activityId ? RED_BORDER : GRAY_BORDER,
            },
          ]}
          key={item.id}
          onPress={() => callback('warmupSessionId', parseInt(item.id))}>
          <Text style={styles.warmupBlockTitle}>{item.name}</Text>
          <Text style={styles.warmupBlockDescription}>{item.description}</Text>
        </TouchableOpacity>
      ))}
    </>
  );
};

const GET_USER = gql`
  query user($id: ID!) {
    user(id: $id) {
      id
      warmupSetting
      warmupSessionId
      workoutSettingSound
    }
  }
`;

const GET_SESSION = gql`
  query getSession($sessionId: ID!, $locale: String!) {
    getSession(sessionId: $sessionId, locale: $locale) {
      id
      warmups {
        id
        name
        description
      }
    }
  }
`;

const UPDATE_USER = gql`
  mutation updateUserData($input: UpdateUserDataInput!) {
    updateUserData(input: $input) {
      id
      workoutSettingSound
    }
  }
`;

const WorkoutSettings = ({navigation, route}) => {
  const {
    sessionId,
    programId,
    isWarmUps,
    isCoolDown,
    programCodeName,
    session,
  } = route.params;
  const [isModalVisible, setIsModalVisible] = useState(false);
  const isFocused = useIsFocused();

  const {translations, appLanguage} = useContext(LocalizationContext);
  const {userData: userInfo} = useContext(AuthContext);

  const {data: userData, loading: userLoading, error: userError} = useQuery(
    GET_USER,
    {
      variables: {id: userInfo?.id},
    },
  );

  const {data, loading, error} = useQuery(GET_SESSION, {
    variables: {sessionId, locale: appLanguage},
  });

  console.log(error);

  const [update, {loading: updating, error: updateError}] = useMutation(
    UPDATE_USER,
  );

  const user = userData?.user;

  useFocusEffect(
    useCallback(() => {
      if (isFocused) {
        trackEvents({
          eventName: 'Load Workout Settings',
          properties: {
            training_program: programCodeName,
            session_id: session?.id,
            session_name: session?.name,
            session_type: session?.sessionType,
            session_order: session?.order,
          },
        });
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]),
  );

  const toggleModal = useCallback(() => {
    setIsModalVisible(!isModalVisible);
  }, [isModalVisible]);

  const onPressBackButton = useCallback(() => {
    isWarmUps || isCoolDown
      ? navigation.navigate('WorkoutList', {
          workoutSettingSound: user?.workoutSettingSound,
        })
      : navigation.navigate('Workout');
  }, [isCoolDown, isWarmUps, navigation, user]);

  const updateUserDataFunc = useCallback(
    async (title, value) => {
      if (updating) {
        return;
      }
      try {
        const input = {
          [title]: value,
        };
        console.log('INPUT:', input);
        const res = await update({
          variables: {
            input: {
              userData: {
                ...input,
              },
            },
          },
          context: {
            headers: {
              authorization: userInfo?.token,
            },
          },
          refetchQueries: ['user'],
        });
        //console.slog('RESULT', res.data);
        if (res?.data?.updateUserData) {
          console.log('Success');
        }
      } catch (error) {
        console.log(error);
      }
    },
    [update, updating, userInfo],
  );

  const handleContinueButtonModal = useCallback(() => {
    toggleModal();
    navigation.goBack();
  }, [navigation, toggleModal]);

  const handleDeleteButtonModal = useCallback(() => {
    toggleModal();
    navigation.navigate('Feedback', {
      sessionId: sessionId,
      programId: programId,
      programCodeName,
      session,
      prevScreen: 'Workout Settings',
    });
  }, [navigation, programCodeName, programId, session, sessionId, toggleModal]);

  console.log(333, data);

  const settingsItems = [
    {
      title: '',
      options: [
        {
          title: translations.settingsScreen.soundsOptionTitle,
          description: translations.settingsScreen.soundsOptionDescription,
          status: user?.workoutSettingSound,
          backendString: 'workoutSettingSound',
        },
      ],
    },
  ];

  const isQuerySuccess = data && userData && !loading;
  const isMutationSuccess = !updating && !updateError;

  return (
    <>
      {isQuerySuccess ? (
        <SafeAreaView style={styles.container}>
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.container}>
            <DiscardWorkoutModal
              icon
              title={translations.settingsScreen.areUSure}
              subTitle={translations.settingsScreen.descriptionModal}
              goBack={handleContinueButtonModal}
              goBackText={translations.settingsScreen.continueProgram}
              goNext={handleDeleteButtonModal}
              goNextText={translations.settingsScreen.deleteTraining}
              visible={isModalVisible}
              closeModal={toggleModal}
            />
            <View>
              <Header
                title={translations.settingsScreen.tabHeader}
                onPressBackButton={onPressBackButton}
              />
              <View style={styles.headerLine} />
              <View style={styles.allSettings}>
                {settingsItems.map((setting, index) => {
                  return (
                    <SettingsBlock
                      key={index}
                      title={setting.title}
                      options={setting.options}
                      updateUserDataFunc={updateUserDataFunc}
                      updating={updating}
                    />
                  );
                })}
              </View>
            </View>
            <View style={styles.deleteWorkoutContainer}>
              <TouchableOpacity onPress={toggleModal}>
                <Text style={styles.deleteWorkout}>
                  {translations.settingsScreen.deleteTraining}
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </SafeAreaView>
      ) : (
        <View style={{flex: 1}}>
          <View style={styles.activityIndicator}>
            <ActivityIndicator size={'large'} color={ORANGE} />
          </View>
        </View>
      )}
    </>
  );
};

export default WorkoutSettings;
