import React, {useCallback, useContext, useLayoutEffect} from 'react';
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  ImageBackground,
  FlatList,
  Image,
} from 'react-native';
import gql from 'graphql-tag';
import {useQuery} from '@apollo/react-hooks';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import WomanPushUps from '../../assets/images/womanPushUps.png';
import AllPrograms from '../../assets/images/allPrograms.png';
import CalendarIcon from '../../assets/icons/calendarIcon.svg';
import styles from './styles';
import GenericSessionCard from '../../components/HomeScreen/GenericSessionCard';
import AvailableProgramCard from '../../components/ExerciseScreen/AvailableProgramCard';
import OrangeCircles from '../../assets/icons/orangeCircles.svg';
import challengesBackground from '../../assets/images/challengesBackground.png';
import {Loading} from '../../components/Loading';
import {AuthContext} from '../../../App';
import GenericWhitePill from '../../components/HomeScreen/GenericWhitePill';
import GenericImagePill from '../../components/HomeScreen/GenericImagePill';
import GenericSessionFooter from '../../components/HomeScreen/GenericSessionFooter';
import {trackEvents} from '../../services/analytics';

const GET_USER = gql`
  query user($id: ID!, $locale: String) {
    user(id: $id, locale: $locale) {
      id
      names
      email
      currentProgram {
        id
        name
        imageUrl
        image2Url
        description
        pro
        user {
          id
          names
        }
        programCharacteristics {
          id
          value
          objective
        }
        sessions {
          id
          name
          description
          order
          calories
          timeDuration
          reps
          bodyPartsFocused
        }
        codeName
        strength
        endurance
        flexibility
        intensity
        technique
        maxAttribute {
          key
          value
        }
        maxAttribute2 {
          key
          value
        }
        implements {
          id
          name
          imageUrl
        }
      }
      currentSession {
        id
        name
        description
        order
        timeDuration
        calories
      }
      userProgram {
        id
        programSessionsCount
        executedSessionsCount
        progress
        sessionExecutions {
          id
          sessionId
          discarded
        }
      }
      updatedAt
      weeklyExecutions {
        id
        sessionId
        createdAt
      }
      trainingDaysSetting
      activeRests {
        id
        sessions {
          id
          order
          name
          description
          timeDuration
          calories
        }
      }
      challenges {
        id
        sessions {
          id
          order
          name
          description
          imageUrl
        }
      }
      isPro
    }
  }
`;

const Exercise = ({navigation}) => {
  //ADD APPLANGUAGE
  const locale = LocalizationContext._currentValue.appLanguage;
  console.log('local', locale);
  const {translations} = useContext(LocalizationContext);
  const {userData: userInfo} = React.useContext(AuthContext);
  console.log('userinfoex', userInfo);
  const {data: getUser, loading, error: errorGetUser, refetch} = useQuery(
    GET_USER,
    {
      variables: {id: userInfo?.id, locale: locale},
    },
  );

  const challenges = getUser?.user?.challenges || [];
  const activeRests = getUser?.user?.activeRests || [];
  const currentProgram = getUser?.user?.currentProgram || {};
  const userProgram = getUser?.user?.userProgram || {};
  const currentSession = getUser?.user?.currentSession || {};
  console.log('cu p', currentProgram);
  console.log('cu ses', currentSession);

  console.log('dsadsa', getUser);
  console.log('challenges', challenges);

  const checkCurrentProgram = async () => {
    console.log('refetching');
    await refetch();
  };

  console.log('dsa ', checkCurrentProgram);

  //navigation callbacks

  const handleOnPressUserIcon = useCallback(
    () => navigation.navigate('Profile'),
    [navigation],
  );
  useLayoutEffect(() => {
    trackEvents({eventName: 'Load Exercice'});
  }, []);

  const sessionExecutions = userProgram?.sessionExecutions;
  const allProgramSessions =
    (sessionExecutions &&
      currentProgram?.sessions?.map(item => {
        const sessionExecutionIndex = sessionExecutions.findIndex(
          el => el.sessionId === item.id,
        );
        return {
          ...item,
          isDone: sessionExecutionIndex >= 0 ? true : false,
          discarded:
            sessionExecutionIndex >= 0 &&
            sessionExecutions[sessionExecutionIndex]?.discarded,
          sessionExecutionId: sessionExecutions[sessionExecutionIndex]?.id, // [executedSessions.findIndex(el => el.id === item.id,)],
        };
      })) ||
    [];

  console.log('allProgramSessions', allProgramSessions);

  const onPressCalendar = useCallback(() => {
    navigation.navigate('Calendar');
  }, [navigation]);

  const goToWorkout = useCallback(
    (program, session, prevScreen = 'Exercise') => {
      navigation.navigate('Workout', {
        programId: program?.id,
        programCodeName: program?.codeName,
        sessionId: session?.id,
        sessionDescription: session?.description,
        sessionName: session?.name,
        userId: userInfo?.id,
        prevScreen,
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [navigation],
  );
  const goToSummary = useCallback(
    session_execution_id => {
      navigation.push('Summary', {
        data: {
          difficultyFeedback: 1,
          sessionExecutionId: session_execution_id,
        },
      });
    },
    [navigation],
  );

  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView
        style={styles.scrollView}
        showsVerticalScrollIndicator={false}>
        <View style={styles.header}>
          <TouchableOpacity
            onPress={onPressCalendar}
            hitSlop={styles.hitSlopTen}>
            <CalendarIcon width={calcWidth(25)} height={calcHeight(25)} />
          </TouchableOpacity>

          <Text style={styles.today}>
            {translations.exerciseScreen.headerExercise}
          </Text>
          <View />
          {/* <TouchableOpacity onPress={() => {}} hitSlop={styles.hitSlopTen}>
            <ThreeDotsIcon
              fill={'#FFC89A'}
              width={calcWidth(23)}
              height={calcHeight(21)}
            />
          </TouchableOpacity> */}
        </View>
        {!loading ? (
          <View style={styles.container}>
            <View style={styles.yourProgramSection}>
              <Text style={styles.sectionText}>
                {translations.exerciseScreen.yourProgram}
              </Text>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('ListSessionsProgram', {
                    userId: userInfo?.id,
                    goToWorkout: goToWorkout,
                  });
                }}
                hitSlop={styles.hitSlopTen}>
                <Text style={styles.seeDetailsText}>
                  {translations.exerciseScreen.viewAll}
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.underProgramSection}>
              <Text style={styles.underSectionText}>
                {translations.exerciseScreen.session} {currentSession.order}
              </Text>
              <Text style={styles.underSectionText}>
                {' '}
                | {currentProgram.name}
              </Text>
              <Text style={styles.underSectionText}>
                {' '}
                | {currentProgram?.user?.names || 'Mammoth Hunters'}{' '}
              </Text>
            </View>
            {/* current program view */}
            <View style={styles.programsCarousel}>
              <FlatList
                data={allProgramSessions}
                renderItem={({item}) => (
                  <GenericSessionCard
                    title={currentProgram.name}
                    description={item.name}
                    isTouch={!item.isDone}
                    onPress={
                      item.isDone
                        ? () => goToSummary(item.sessionExecutionId)
                        : () => goToWorkout(currentProgram, item)
                    }
                    calories={item.calories}
                    timeDuration={item.timeDuration}
                    reps={item.reps}
                    discarded={item.discarded}
                    footerShow={true}
                    extraStyles={{
                      width: deviceWidth * 0.8,
                      marginHorizontal: moderateScale(10),
                    }}
                  />
                )}
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => item.id?.toString()}
                style={{marginHorizontal: moderateScale(10)}}
              />
            </View>
            {/* challenges section */}
            <View style={styles.challengesSection}>
              <View style={styles.challengesHeader}>
                <Text style={styles.sectionText}>
                  {translations.exerciseScreen.challenges}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('ListChallengeSessionsProgram', {
                      userId: userInfo?.id,
                      goToWorkout: goToWorkout,
                    });
                  }}
                  hitSlop={styles.hitSlopTen}>
                  <Text style={styles.seeDetailsText}>
                    {translations.exerciseScreen.viewAll}
                  </Text>
                </TouchableOpacity>
              </View>
              <Text
                style={[styles.underSectionText, {marginTop: calcHeight(10)}]}>
                {translations.exerciseScreen.challengesDescription}
              </Text>
            </View>
            <View style={styles.programsCarousel}>
              <FlatList
                data={challenges?.sessions}
                renderItem={({item}) => {
                  console.log(item);
                  const Item = item?.imageUrl
                    ? GenericImagePill
                    : GenericWhitePill;
                  return (
                    <Item
                      key={item.id}
                      title={item.name ?? '-'}
                      description={
                        item.description &&
                        new String(item.description).substr(0, 20) + ' ...'
                      }
                      extraStyles={styles.activeRestItem}
                      extraStylesTitle={{fontSize: calcFontSize(25)}}
                      onPress={() => goToWorkout(challenges, item)}
                      backgroundImage={challengesBackground}>
                      <GenericSessionFooter
                        calories={item.calories}
                        reps={item.reps}
                        timeDuration={item.timeDuration}
                      />
                    </Item>
                  );
                }}
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => item.id?.toString()}
                contentContainerStyle={{
                  flexGrow: 1,
                  alignItems: 'center',
                }}
                style={{marginHorizontal: moderateScale(10)}}
              />
            </View>

            <TouchableOpacity
              style={styles.manSqueezingsView}
              onPress={() => {
                navigation.navigate('SearchExercise');
              }}>
              <ImageBackground
                source={WomanPushUps}
                imageStyle={styles.manSqueezingsImage}
                style={styles.manSqueezingsImage}>
                <OrangeCircles width={'100%'} height={moderateScale(200)} />
              </ImageBackground>
              <Text style={[styles.programCardTitle, styles.manSqueezingsText]}>
                {translations.exerciseScreen.listExercises}
              </Text>
            </TouchableOpacity>
            {/* active rests section */}
            <View style={styles.challengesSection}>
              <View style={styles.challengesHeader}>
                <Text style={styles.sectionText}>
                  {translations.exerciseScreen.activeRest}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('ListActiveSessionsProgram', {
                      userId: userInfo?.id,
                      goToWorkout: goToWorkout,
                    });
                  }}
                  hitSlop={styles.hitSlopTen}>
                  <Text style={styles.seeDetailsText}>
                    {translations.exerciseScreen.viewMore}
                  </Text>
                </TouchableOpacity>
              </View>
              <Text
                style={[styles.underSectionText, {marginTop: calcHeight(10)}]}>
                {translations.exerciseScreen.activeRestDescription}
              </Text>
            </View>
            <View style={styles.programsCarousel}>
              <FlatList
                data={activeRests?.sessions}
                renderItem={({item}) => (
                  <GenericWhitePill
                    key={item.id}
                    title={item.name}
                    description={''}
                    extraStyles={styles.activeRestItem}
                    onPress={() => goToWorkout(activeRests, item)}>
                    <GenericSessionFooter
                      calories={item.calories}
                      reps={item.reps}
                      timeDuration={item.timeDuration}
                    />
                  </GenericWhitePill>
                )}
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => item.id?.toString()}
                style={{marginHorizontal: moderateScale(10)}}
              />
            </View>
            <TouchableOpacity
              style={styles.manSqueezingsView}
              onPress={() => {
                navigation.navigate('ChooseAnotherProgram', {
                  suggested_program: currentProgram,
                });
              }}>
              <Image
                source={AllPrograms}
                style={styles.manSqueezingsImage}
                imageStyle={styles.manSqueezingsImage}
              />
              <Text style={[styles.programCardTitle, styles.manSqueezingsText]}>
                {translations.exerciseScreen.programs}
              </Text>
            </TouchableOpacity>
          </View>
        ) : (
          <Loading />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Exercise;
