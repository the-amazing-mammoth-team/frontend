import ExerciseImage1 from '../../assets/images/exerciseImage1.png';
import ExerciseImage2 from '../../assets/images/exerciseImage2.png';
import ExerciseImage3 from '../../assets/images/exerciseImage3.png';

export const dumpMyProgramsData = [
  {
    myProgramId: 1,
    title: 'Unnegotiable 1',
    description: 'To the one',
    timeCount: 6,
    KcalCount: 69,
    backgroundImage: ExerciseImage1,
  },
  {
    myProgramId: 2,
    title: 'Unnegotiable 2',
    description: 'Tabata',
    timeCount: 10,
    KcalCount: 80,
    backgroundImage: ExerciseImage2,
  },
  {
    myProgramId: 3,
    title: 'Unnegotiable 3',
    description: 'Hard stop',
    timeCount: 12,
    KcalCount: 70,
    backgroundImage: ExerciseImage3,
  },
  {
    myProgramId: 4,
    title: 'Unnegotiable 4',
    description: 'To the one',
    timeCount: 12,
    KcalCount: 70,
    backgroundImage: ExerciseImage3,
  },
];

export const dumpChallengesData = [
  {
    challengeId: 1,
    title: '0 to 100',
    description: 'Little description',
    repsCount: 43,
    KcalCount: 69,
  },
  {
    challengeId: 2,
    title: 'Rock Star',
    description: 'Little description',
    repsCount: 43,
    KcalCount: 69,
  },
  {
    challengeId: 3,
    title: '6-6-6',
    description: 'Little description',
    repsCount: 43,
    KcalCount: 69,
    isBlocked: true,
  },
  {
    challengeId: 4,
    title: 'Re-core',
    description: 'Little description',
    repsCount: 43,
    KcalCount: 69,
    isBlocked: true,
  },
];
