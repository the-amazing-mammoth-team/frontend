import {StyleSheet} from 'react-native';
import {BLACK, GRAY, LIGHT_ORANGE, ORANGE, WHITE} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: WHITE,
  },
  safeArea: {
    flex: 1,
    backgroundColor: WHITE,
  },
  today: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
    paddingVertical: moderateScale(17),
    textAlign: 'center',
    backgroundColor: WHITE,
  },
  header: {
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: calcWidth(20),
    flex: 1,
  },
  container: {
    paddingBottom: calcWidth(20),
  },
  sectionText: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(20),
  },
  yourProgramSection: {
    marginTop: calcHeight(20),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: calcWidth(20),
  },
  challengesSection: {
    marginTop: calcHeight(40),
    paddingHorizontal: calcWidth(20),
  },
  seeDetailsText: {
    textDecorationLine: 'underline',
    color: LIGHT_ORANGE,
  },
  underSectionText: {
    color: GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  underProgramSection: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: calcHeight(10),
    paddingHorizontal: calcWidth(20),
  },
  programsCarousel: {
    marginTop: calcHeight(20),
  },
  genericLeftMargin: {
    marginLeft: calcWidth(20),
  },
  programItemOrange: {
    width: calcWidth(270),
    borderRadius: 10,
    backgroundColor: ORANGE,
    marginRight: calcWidth(30),
  },
  programItemGray: {
    width: calcWidth(270),
    borderRadius: 10,
    backgroundColor: GRAY,
    marginRight: calcWidth(30),
  },
  programItemWhite: {
    width: deviceWidth * 0.75,
    borderRadius: 10,
    backgroundColor: WHITE,
    elevation: 5,
    marginVertical: calcHeight(10),
    marginRight: calcWidth(30),
  },
  programImageSmall: {
    width: deviceWidth * 0.75,
    borderRadius: 10,
  },
  programCardContent: {
    paddingHorizontal: calcWidth(15),
    paddingVertical: calcHeight(15),
  },
  programCardTitle: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(25),
    color: WHITE,
    marginBottom: moderateScale(5),
  },
  programCardTitleBlack: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(25),
    color: BLACK,
    marginBottom: moderateScale(5),
  },
  programCardDescription: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: WHITE,
  },
  programCardDescriptionGray: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: GRAY,
  },
  programCardFooter: {
    marginTop: calcHeight(30),
    flexDirection: 'row',
    alignItems: 'center',
  },
  challengesHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  availablePrograms: {
    paddingHorizontal: calcWidth(20),
  },
  circleView: {
    width: moderateScale(66),
    height: moderateScale(66),
    borderRadius: moderateScale(66) / 2,
    borderWidth: 1,
    borderColor: GRAY,
    alignItems: 'center',
    justifyContent: 'center',
  },
  somethingDifferentTip: {
    borderRadius: 10,
    backgroundColor: 'rgba(229, 229, 229, 0.4)',
    paddingHorizontal: calcWidth(20),
    paddingVertical: calcHeight(20),
    marginTop: calcHeight(20),
    justifyContent: 'space-between',
  },
  somethingDifferentTipText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: BLACK,
  },
  somethingDifferentTipLightOrange: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
    marginTop: calcHeight(20),
  },
  renderCircleActivity: {
    alignItems: 'center',
    marginRight: calcWidth(20),
  },
  availableProgramCard: {
    borderRadius: 10,
    marginRight: calcWidth(20),
  },
  manSqueezingsView: {
    marginTop: calcHeight(40),
    paddingHorizontal: calcWidth(20),
    paddingVertical: calcWidth(10),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.2,
    elevation: 4,
  },
  manSqueezingsImage: {
    width: '100%',
    height: moderateScale(190),
    borderRadius: moderateScale(10),
    overflow: 'hidden',
  },
  manSqueezingsText: {
    position: 'absolute',
    left: 0,
    top: calcHeight(12),
    paddingLeft: calcWidth(30),
  },
  hitSlopTen: {
    top: calcHeight(10),
    bottom: calcHeight(10),
    right: calcWidth(10),
    left: calcWidth(10),
  },
  genericComponentLikeView: {
    alignSelf: 'flex-end',
  },
  genericComponentRepsView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  genericComponentFooterPadding: {
    marginLeft: calcWidth(4),
  },
  genericComponentTextBlack: {
    color: BLACK,
  },
  genericComponentSeparateLine: {
    marginHorizontal: calcWidth(8),
  },
  genericComponentDisabledCard: {
    opacity: 0.7,
  },
  absoluteBackgroundImageView: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    bottom: 0,
    right: 0,
    top: 0,
  },
  checkContainer: {
    width: moderateScale(17),
    height: moderateScale(17),
    borderRadius: moderateScale(17) / 2,
    backgroundColor: ORANGE,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingText: {
    fontSize: calcFontSize(14),
    color: BLACK,
    fontFamily: SUB_TITLE_FONT,
  },
  loadingView: {
    alignSelf: 'center',
    marginTop: moderateScale(10),
  },
  activeRestItem: {
    //flex: 1,
    width: calcWidth(250),
    height: calcWidth(130),
    marginHorizontal: moderateScale(10),
    marginBottom: moderateScale(5),
    marginTop: moderateScale(5),
  },
});

export default styles;
