import React, {useCallback, useContext, useEffect, useState} from 'react';
import Header from '../../components/header';
import {LocaleConfig, Calendar} from 'react-native-calendars';
import moment from 'moment';
import gql from 'graphql-tag';
import {useLazyQuery} from '@apollo/react-hooks';
import {LocalizationContext} from '../../localization/translations';
import {AuthContext} from '../../../App';
import {SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native';
import {
  BLACK,
  GRAY,
  GRAY_BORDER,
  LIGHT_GRAY,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import ArrowLeft from '../../assets/icons/arrowLeft.svg';
import ArrowRight from '../../assets/icons/arrowRight.svg';
import {Loading} from '../../components/Loading';
import DayComponent from '../../components/Calendar/DayComponent';

LocaleConfig.locales.es = {
  monthNames: [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
  ],
  monthNamesShort: [
    'Enero.',
    'Feb.',
    'Mar.',
    'Abr.',
    'Mayo.',
    'Jun.',
    'Jul.',
    'Agosto.',
    'Sept.',
    'Oct.',
    'Nov.',
    'Dic.',
  ],
  dayNames: [
    'Lunes',
    'Martes',
    'Miércoles',
    'Jueves',
    'Viernes',
    'Sábado',
    'Domingo',
  ],
  dayNamesShort: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
  today: 'hoy',
};
LocaleConfig.locales.en = {
  monthNames: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],
  monthNamesShort: [
    'Jan.',
    'Feb.',
    'Mar.',
    'Apr.',
    'May',
    'Jun.',
    'Jul.',
    'Aug.',
    'Sept.',
    'Oct.',
    'Nov.',
    'Dec.',
  ],
  dayNames: [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  ],
  dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
  today: 'Today',
};

const GET_EXECUTIONS = gql`
  query getExecutions($from: ISO8601DateTime!, $to: ISO8601DateTime!) {
    getExecutions(from: $from, to: $to) {
      id
      sessionId
      createdAt
      session {
        id
        order
      }
      userProgram {
        id
        program {
          id
          codeName
        }
      }
    }
  }
`;

const CalendarScreen = ({navigation, route}) => {
  const handleGoBack = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const {translations} = useContext(LocalizationContext);
  const {userData} = useContext(AuthContext);
  LocaleConfig.defaultLocale = translations.getLanguage();

  const [currentMonth, setCurrentMonth] = useState(new Date());

  const from = moment(currentMonth)
    .startOf('month')
    .toISOString();
  const to = moment(currentMonth)
    .endOf('month')
    .toISOString();

  const [getData, {data, loading, error: err}] = useLazyQuery(GET_EXECUTIONS, {
    variables: {
      from: moment(from).toISOString(),
      to: moment(to).toISOString(),
    },
    context: {
      headers: {
        authorization: userData?.token,
      },
    },
    fetchPolicy: 'network-only',
  });

  const executions = data?.getExecutions;

  useEffect(() => {
    getData();
  }, [currentMonth, getData]);

  const listGoals = (() => {
    let list = executions?.map(item => {
      let newItem = {};
      const res = item?.userProgram?.program?.codeName;
      if (res === 'active rests') {
        newItem = {
          name: translations.homeScreen.activeRest,
          backgroundColor: BLACK,
          createdAt: item.createdAt,
          id: item.sessionId,
          execution_id: item.id,
        };
      } else if (res === 'challenges') {
        newItem = {
          name: translations.homeScreen.challenges,
          backgroundColor: BLACK,
          createdAt: item.createdAt,
          id: item.sessionId,
          execution_id: item.id,
        };
      } else if (res === 'legacy') {
        newItem = {
          name: 'LE',
          backgroundColor: BLACK,
          createdAt: item.createdAt,
          id: item.sessionId,
          execution_id: item.id,
        };
      } else {
        newItem = {
          name: `S${item?.session?.order}`,
          backgroundColor: ORANGE,
          createdAt: item.createdAt,
          id: item.sessionId,
          execution_id: item.id,
        };
      }
      return newItem;
    });
    list = list?.sort((a, b) => {
      return a.createdAt - b.createdAt;
    });
    return list;
  })();

  const renderArrow = direction => {
    if (direction === 'left') {
      return <ArrowLeft width={calcWidth(12)} height={calcHeight(12)} />;
    } else {
      return <ArrowRight width={calcWidth(12)} height={calcHeight(12)} />;
    }
  };

  const renderHeader = date => {
    return (
      <View>
        <Text style={styles.headerText}>
          {moment(date.toString()).format('MMMM yyyy')}
        </Text>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView style={styles.safeArea} showsVerticalScrollIndicator={false}>
        <View>
          <Header
            title={translations.exerciseScreen.calendar}
            onPressBackButton={handleGoBack}
          />
          <View style={styles.headerLine} />
        </View>
        <View style={styles.container}>
          <View style={styles.infoView}>
            <View style={styles.orangeCircle} />
            <Text style={styles.infoText}>
              {translations.weeklyGoalsScreen.workouts}
            </Text>
            <View style={styles.blackCircle} />
            <Text style={styles.infoText}>
              {translations.weeklyGoalsScreen.other}
            </Text>
          </View>
          <Calendar
            current={currentMonth}
            monthFormat={'MMMM yyyy'}
            firstDay={1}
            renderArrow={direction => renderArrow(direction)}
            renderHeader={date => renderHeader(date)}
            onPressArrowLeft={substractMonth => substractMonth()}
            onPressArrowRight={addMonth => addMonth()}
            enableSwipeMonths={false}
            onMonthChange={el => {
              setCurrentMonth(new Date(el.dateString));
            }}
            dayComponent={({date}) => {
              return (
                <DayComponent
                  date={new Date(date.timestamp)}
                  key={date.timestamp}
                  goals={listGoals}
                  navigation={navigation}
                />
              );
            }}
            markedDates={listGoals}
          />
        </View>
        {loading && (
          <View style={styles.loader}>
            <Loading />
          </View>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    width: deviceWidth,
    alignSelf: 'center',
    backgroundColor: GRAY_BORDER,
    marginTop: -20,
    marginBottom: moderateScale(32),
  },
  infoView: {
    alignSelf: 'flex-end',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: calcHeight(10),
  },
  container: {
    flex: 1,
    paddingHorizontal: calcWidth(15),
  },
  infoText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: GRAY,
  },
  orangeCircle: {
    width: moderateScale(8),
    height: moderateScale(8),
    borderRadius: moderateScale(8) / 2,
    backgroundColor: ORANGE,
    marginRight: calcWidth(7),
  },
  blackCircle: {
    width: moderateScale(8),
    height: moderateScale(8),
    borderRadius: moderateScale(8) / 2,
    backgroundColor: BLACK,
    marginRight: calcWidth(7),
    marginLeft: calcWidth(20),
  },
  arrow: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  circleText: {
    fontFamily: SUB_TITLE_FONT,
    color: WHITE,
    fontSize: calcFontSize(12),
  },
  dayText: {
    textAlign: 'center',
    fontFamily: SUB_TITLE_FONT,
    color: LIGHT_GRAY,
    fontSize: calcFontSize(14),
  },
  dayComponentView: {
    alignItems: 'center',
  },
  dayCircleSelected: {
    width: moderateScale(37),
    height: moderateScale(37),
    borderRadius: moderateScale(37) / 2,
    marginTop: calcHeight(5),
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: moderateScale(1),
  },
  dayCircle: {
    width: moderateScale(32),
    height: moderateScale(32),
    borderRadius: moderateScale(32) / 2,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    borderWidth: moderateScale(1),
    borderColor: GRAY_BORDER,
  },
  headerText: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: BLACK,
  },
  hitSlopTen: {
    top: calcHeight(10),
    bottom: calcHeight(10),
    left: calcWidth(10),
    right: calcWidth(10),
  },
  loader: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
  },
});

export default CalendarScreen;
