import React, {useCallback, useContext, useEffect, useState} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY_BORDER,
  LIGHT_GRAY,
  WHITE,
} from '../../styles/colors';
import Header from '../../components/header';
import {LocalizationContext} from '../../localization/translations';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import AvailableProgramCard from '../../components/ExerciseScreen/AvailableProgramCard';
import SlicedAvocado from '../../assets/images/sliced-avocado-fruit-on-white-chopping-board.png';
import VegetablesFruits from '../../assets/images/bowl-of-vegetable-salad-and-fruits.png';
import ChooseAnotherProgramGirlImage from '../../assets/images/pexels-anna-shvets.png';
import PersonalExperienceSection from '../../components/ProgramsScreens/PersonalExperienceSection';
import FrequentQuestionsSection from '../../components/ProgramsScreens/FrequentQuestionsSection';
import {handleStars, mockComments} from '../Programs/ProgramsScreen';
import {Loading} from '../../components/Loading';

const Nutrition = ({navigation}) => {
  const [isLoaderVisible, setIsLoaderVisible] = useState(true);

  const {translations} = useContext(LocalizationContext);

  const handleGoBack = useCallback(() => navigation.goBack(), [navigation]);
  const handleOnProgramPress = useCallback(
    item =>
      navigation.navigate('ProgramDescription', {
        item,
        isCirclesLevel: false,
        isEquipment: false,
        isDays: true,
        isNutrition: true,
      }),
    [navigation],
  );

  const handleChooseAnotherProgram = useCallback(
    () => navigation.navigate('ChooseAnotherNutritionProgram'),
    [navigation],
  );

  useEffect(() => {
    setIsLoaderVisible(false);
  }, []);

  const nutritionProgram = [
    {
      title: 'Ketocourse',
      duration: 28,
      background: SlicedAvocado,
      backgroundWide: SlicedAvocado,
      id: 'gaewgesag',
      isTip: true,
      tipText: 'Recommendation for you',
    },
    {
      title: 'Keto and fasts',
      duration: 28,
      background: VegetablesFruits,
      backgroundWide: VegetablesFruits,
      id: 'esrehserh',
    },
  ];
  return (
    <SafeAreaView>
      <Text>Under development</Text>
    </SafeAreaView>
  );
  // return (
  //   <SafeAreaView style={styles.root}>
  //     <ScrollView showsVerticalScrollIndicator={false}>
  //       <Header
  //         title={translations.nutritionScreen.yourNutritionPlan}
  //         onPressBackButton={handleGoBack}
  //       />
  //       <View style={styles.headerLine} />
  //       {!isLoaderVisible ? (
  //         <>
  //           <View style={styles.titleView}>
  //             <Text style={styles.title}>
  //               {translations.programsScreen.idealPlan}
  //             </Text>
  //             <Text style={styles.description}>
  //               {translations.programsScreen.expertResultTitle}
  //             </Text>
  //           </View>

  //           <View style={styles.programsCarousel}>
  //             <ScrollView
  //               horizontal={true}
  //               showsHorizontalScrollIndicator={false}>
  //               {nutritionProgram.map((item, key) => {
  //                 return (
  //                   <AvailableProgramCard
  //                     item={item}
  //                     id={item.availableProgramId}
  //                     key={key}
  //                     isCirclesLevel={false}
  //                     isDaysDuration={true}
  //                     onPress={() => handleOnProgramPress(item)}
  //                   />
  //                 );
  //               })}
  //               <AvailableProgramCard
  //                 item={{
  //                   title: 'Choose another program',
  //                   background: ChooseAnotherProgramGirlImage,
  //                 }}
  //                 isCirclesLevel={false}
  //                 isDuration={false}
  //                 onPress={handleChooseAnotherProgram}
  //               />
  //             </ScrollView>
  //           </View>

  //           <View style={styles.container}>
  //             <Text
  //               style={[
  //                 styles.title,
  //                 styles.sectionTitle,
  //                 styles.genericPadding,
  //               ]}>
  //               {translations.programsScreen.joinPeople}
  //             </Text>
  //             <View style={styles.genericPadding}>
  //               {mockComments.map(comment => (
  //                 <View key={comment.id}>
  //                   <View style={styles.commentRoot}>
  //                     <View style={styles.commentAuthorTextView}>
  //                       <Text style={styles.commentAuthorText}>
  //                         {comment.author}
  //                       </Text>
  //                     </View>
  //                     <View style={styles.commentTextView}>
  //                       <Text
  //                         style={[styles.description, styles.commentTextColor]}>
  //                         {comment.text}
  //                       </Text>
  //                     </View>
  //                     <View style={styles.starsRow}>
  //                       {handleStars(comment.starsAmount)}
  //                     </View>
  //                   </View>
  //                 </View>
  //               ))}
  //             </View>
  //             <View style={styles.personalExperienceSectionView}>
  //               <PersonalExperienceSection />
  //             </View>
  //             <View
  //               style={[styles.frequentlySectionView, styles.genericPadding]}>
  //               <FrequentQuestionsSection />
  //             </View>
  //           </View>
  //         </>
  //       ) : (
  //         <Loading />
  //       )}
  //     </ScrollView>
  //   </SafeAreaView>
  // );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    backgroundColor: GRAY_BORDER,
    width: '100%',
    marginTop: -20,
    marginBottom: moderateScale(25),
  },
  titleView: {
    paddingHorizontal: calcWidth(20),
    marginBottom: calcHeight(22),
  },
  title: {
    color: BLACK,
    fontSize: calcFontSize(25),
    fontFamily: MAIN_TITLE_FONT,
  },
  description: {
    color: LIGHT_GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    marginTop: moderateScale(3),
  },
  container: {
    //paddingHorizontal: calcWidth(20),
  },
  genericPadding: {
    paddingHorizontal: moderateScale(20),
  },
  programsCarousel: {
    marginBottom: calcHeight(20),
    paddingLeft: calcWidth(20),
  },
  sectionTitle: {
    fontSize: calcFontSize(20),
    marginBottom: moderateScale(30),
  },
  commentTextColor: {
    color: BUTTON_TITLE,
  },
  commentAuthorText: {
    color: BLACK,
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_FONT,
  },
  commentRoot: {
    marginBottom: moderateScale(30),
  },
  commentAuthorTextView: {
    marginBottom: moderateScale(10),
  },
  commentTextView: {
    marginBottom: moderateScale(5),
  },
  starsRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: moderateScale(7),
  },
  personalExperienceSectionView: {
    marginTop: moderateScale(30),
  },
  frequentlySectionView: {
    marginBottom: moderateScale(30),
    marginTop: moderateScale(55),
  },
});

export default Nutrition;
