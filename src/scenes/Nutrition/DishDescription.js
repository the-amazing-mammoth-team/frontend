import React, {useCallback, useContext, useRef} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {PieChart} from 'react-native-svg-charts';
import {Circle, G, Line, Svg, Text as SvgText} from 'react-native-svg';
import {
  BLACK,
  GRAY_BORDER,
  LIGHT_GRAY,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
  YELLOW,
} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import ShareIconUpdated from '../../assets/icons/shareIconUpdated.svg';
import ArrowTop from '../../assets/icons/arrowTop.svg';

const DishDescription = ({route}) => {
  const {dish} = route.params;
  const {image, title, personCount, composition} = dish;

  const {translations} = useContext(LocalizationContext);
  const scrollViewRef = useRef();

  const renderIngredientsSection = useCallback(
    (title, ingredients = []) => (
      <View style={styles.ingredientsViewRoot}>
        <Text style={[styles.title, styles.ingredientsTitle]}>
          {title} ingredients
        </Text>
        <View style={styles.ingredientsView}>
          {ingredients.map((item, key) => (
            <View key={key} style={styles.ingredientRow}>
              <Text style={styles.blackText}>{item}</Text>
            </View>
          ))}
        </View>
      </View>
    ),
    [],
  );

  const handleGoToTop = useCallback(
    () => scrollViewRef.current?.scrollTo({x: 0, y: 0, animated: true}),
    [],
  );

  const renderPreparationStepsSection = useCallback(
    (title, preparationSteps, isTipForPreparation = false) => (
      <View style={styles.preparationStepsViewRoot}>
        <Text style={[styles.title, styles.ingredientsTitle]}>
          {title} preparation
        </Text>
        <View style={styles.preparationStepsView}>
          {preparationSteps.map((item, key) => (
            <View key={key} style={styles.preparationStepItem}>
              <Text style={styles.blackText}>
                {key + 1}. {item}
              </Text>
            </View>
          ))}
        </View>
        {isTipForPreparation && (
          <TouchableOpacity>
            <Text style={styles.preparationTipText}>
              How to make homemade mayonnaise?
            </Text>
          </TouchableOpacity>
        )}
      </View>
    ),
    [],
  );

  const pieData = [
    {
      key: 1,
      amount: 38,
      svg: {fill: ORANGE},
      title: 'Protein',
    },
    {
      key: 2,
      amount: 74,
      svg: {fill: YELLOW},
      title: 'Carbohydrates',
    },
    {
      key: 3,
      amount: 19,
      svg: {fill: LIGHT_ORANGE},
      title: 'Grease',
    },
  ];

  const Labels = ({slices}) => {
    return slices.map((slice, index) => {
      const {labelCentroid, pieCentroid, data} = slice;
      console.log(35345, slice);
      const xMove = labelCentroid[0] > 0 ? 30 : -80;
      return (
        <G key={index}>
          <SvgText
            x={labelCentroid[0] + moderateScale(xMove)}
            y={labelCentroid[1]}
            fill={BLACK}
            fontFamily={SUB_TITLE_FONT}
            fontSize={calcFontSize(12)}>
            {data.title}
          </SvgText>
          <SvgText
            x={labelCentroid[0] + moderateScale(xMove)}
            y={labelCentroid[1] + moderateScale(15)}
            fill={BLACK}
            fontFamily={SUB_TITLE_FONT}
            fontSize={calcFontSize(12)}>
            {data.amount} %
          </SvgText>
        </G>
      );
    });
  };

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView ref={scrollViewRef} showsVerticalScrollIndicator={false}>
        <Image
          source={typeof image === 'number' ? image : {uri: image}}
          style={styles.foodImage}
        />
        <View style={styles.content}>
          <View style={styles.underImageRow}>
            <Text style={styles.grayText}>
              {translations.nutritionScreen.food}
            </Text>
            <TouchableOpacity hitSlop={styles.hitSlopFifteen}>
              <ShareIconUpdated
                width={moderateScale(23)}
                height={moderateScale(24.5)}
              />
            </TouchableOpacity>
          </View>

          <View style={styles.titleView}>
            <Text style={styles.title}>{title}</Text>
          </View>
          <Text style={styles.blackText}>For {personCount} person</Text>
          {renderIngredientsSection(
            title.substr(0, title.indexOf(' ')),
            composition[0].ingredients,
          )}
          {renderPreparationStepsSection(
            title.substr(0, title.indexOf(' ')),
            composition[0].preparationSteps,
          )}
          <View style={styles.separator} />

          {renderIngredientsSection(
            composition[1].name.substr(0, title.indexOf(' ')),
            composition[1].ingredients,
          )}
          {renderPreparationStepsSection(
            composition[1].name.substr(0, title.indexOf(' ')),
            composition[1].preparationSteps,
            composition[1].isTipForPreparation,
          )}
          <View style={styles.separator} />
          <View style={styles.nutritionInfoTitleView}>
            <Text style={[styles.title, styles.ingredientsTitle]}>
              Nutritional information
            </Text>
          </View>
          <PieChart
            style={styles.chart}
            valueAccessor={({item}) => item.amount}
            data={pieData}
            spacing={0}
            innerRadius={'95%'}
            outerRadius={'80%'}
            padAngle={0}>
            <View style={styles.centerValuesChart}>
              <Text
                style={[styles.nutriInfoTitle, {fontSize: calcFontSize(30)}]}>
                897
              </Text>
              <Text style={styles.blackText}>Total calories</Text>
            </View>
            <Labels />
          </PieChart>
          <View style={styles.nutriInfoRow}>
            <View style={styles.nutriInfoItem}>
              <Text style={styles.nutriInfoTitle}>38g</Text>
              <Text style={styles.blackText}>Protein</Text>
            </View>
            <View style={styles.separateNutriLine} />
            <View style={styles.nutriInfoItem}>
              <Text style={styles.nutriInfoTitle}>74g</Text>
              <Text style={styles.blackText}>Carbohydrates</Text>
            </View>
            <View style={styles.separateNutriLine} />
            <View style={styles.nutriInfoItem}>
              <Text style={styles.nutriInfoTitle}>19g</Text>
              <Text style={styles.blackText}>Grease</Text>
            </View>
          </View>
          <TouchableOpacity
            style={styles.scrollToTopView}
            onPress={handleGoToTop}
            hitSlop={styles.hitSlopFifteen}>
            <Text
              style={[
                styles.preparationTipText,
                styles.standardFontSize,
                styles.preparationStepItem,
              ]}>
              Go back up
            </Text>
            <ArrowTop width={calcWidth(11)} height={calcHeight(11)} />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  foodImage: {
    width: '100%',
    height: deviceHeight * 0.3,
    resizeMode: 'stretch',
    //backgroundColor: BLACK,
  },
  content: {
    paddingHorizontal: moderateScale(20),
  },
  underImageRow: {
    flexDirection: 'row',
    marginVertical: moderateScale(15),
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  grayText: {
    fontSize: calcFontSize(14),
    color: LIGHT_GRAY,
    fontFamily: SUB_TITLE_FONT,
  },
  blackText: {
    fontSize: calcFontSize(14),
    color: BLACK,
    fontFamily: SUB_TITLE_FONT,
  },
  titleView: {
    marginBottom: moderateScale(10),
  },
  title: {
    color: BLACK,
    fontSize: calcFontSize(22),
    fontFamily: MAIN_TITLE_FONT,
  },
  ingredientsTitle: {
    fontSize: calcFontSize(18),
  },
  ingredientsViewRoot: {
    marginTop: moderateScale(20),
  },
  hitSlopFifteen: {
    top: calcHeight(15),
    bottom: calcHeight(15),
    left: calcWidth(15),
    right: calcWidth(15),
  },
  ingredientsView: {
    marginTop: moderateScale(9),
    marginBottom: moderateScale(32),
  },
  ingredientRow: {
    paddingVertical: calcHeight(10),
    borderBottomWidth: moderateScale(1),
    borderBottomColor: GRAY_BORDER,
  },
  preparationStepsViewRoot: {
    //marginTop: moderateScale(37),
  },
  preparationStepsView: {
    marginTop: moderateScale(20),
  },
  preparationStepItem: {
    marginBottom: moderateScale(10),
  },
  separator: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginBottom: moderateScale(20),
    marginTop: moderateScale(40),
  },
  preparationTipText: {
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_FONT,
  },
  scrollToTopView: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: moderateScale(30),
  },
  standardFontSize: {
    fontSize: calcFontSize(14),
  },
  nutritionInfoTitleView: {
    marginTop: moderateScale(20),
    marginBottom: moderateScale(40),
  },
  nutriInfoRow: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    width: '100%',
    marginBottom: moderateScale(60),
    marginTop: moderateScale(35),
  },
  nutriInfoTitle: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(25),
    color: BLACK,
    marginBottom: moderateScale(1),
  },
  nutriInfoItem: {
    alignItems: 'center',
    paddingVertical: moderateScale(7),
  },
  separateNutriLine: {
    height: '100%',
    backgroundColor: GRAY_BORDER,
    width: moderateScale(1),
  },
  centerValuesChart: {
    alignItems: 'center',
    marginTop: 65,
  },
  chart: {
    height: 200,
    width: deviceWidth,
    alignSelf: 'center',
  },
});

export default DishDescription;
