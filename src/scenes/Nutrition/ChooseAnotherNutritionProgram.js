import React, {useCallback, useContext} from 'react';
import {FlatList, SafeAreaView, StyleSheet, View} from 'react-native';
import {GRAY_BORDER, WHITE} from '../../styles/colors';
import Header from '../../components/header';
import {calcWidth, moderateScale} from '../../utils/dimensions';
import ManPullUpImage from '../../assets/images/manPullUp.png';
import {LocalizationContext} from '../../localization/translations';
import CurrentProgramCard from '../../components/ProgramsScreens/CurrentProgramCard';
import ProgramCardSmall from '../../components/ProgramsScreens/ProgramCardSmall';
import WomanKitchenImage from '../../assets/images/woman-in-blue-tank-top-standing-near-kitchen-counter.png';
import ManInKitchenGray from '../../assets/images/manInKitchenGray.png';
import SalmonImage from '../../assets/images/salmonEat.png';
import SlicedAvocado from '../../assets/images/sliced-avocado-fruit-on-white-chopping-board.png';

const ChooseAnotherNutritionProgram = ({navigation}) => {
  const {translations} = useContext(LocalizationContext);

  const handleGoBack = useCallback(() => navigation.goBack(), [navigation]);

  const mockPrograms = [
    {
      availableProgramId: 10,
      duration: 28,
      title: 'Hypertrophy',
      background: ManInKitchenGray,
    },
    {
      availableProgramId: 11,
      duration: 28,
      title: 'Ketocourse',
      background: SlicedAvocado,
    },
    {
      availableProgramId: 12,
      duration: 28,
      title: 'Real food with fasts',
      background: SalmonImage,
    },
    {
      availableProgramId: 13,
      duration: 28,
      title: 'Keto hypertrophy',
      background: ManPullUpImage,
    },
  ];

  const renderCurrentProgram = useCallback(() => {
    return (
      <CurrentProgramCard
        isCirclesLevel={false}
        isDays={true}
        currentProgram={{
          background: WomanKitchenImage,
          duration: 28,
          title: 'Real food',
          callback: handleOnProgramPress(),
        }}
      />
    );
  }, []);

  const handleOnProgramPress = useCallback(
    item =>
      navigation.navigate('ProgramDescription', {
        item,
        isEquipment: false,
        isCirclesLevel: false,
        isDays: true,
        isNutrition: true,
      }),
    [navigation],
  );

  const renderItem = useCallback(
    ({item}) => (
      <ProgramCardSmall
        item={item}
        handleOnProgramPress={handleOnProgramPress}
        isCirclesLevel={false}
        callback= {handleOnProgramPress()}
      />
    ),

    [handleOnProgramPress],
  );

  return (
    <SafeAreaView style={styles.root}>
      <Header
        title={translations.nutritionScreen.screenTitleNutriPlanes}
        onPressBackButton={handleGoBack}
      />
      <View style={styles.headerLine} />
      <View style={styles.container}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={mockPrograms}
          renderItem={renderItem}
          ListHeaderComponent={renderCurrentProgram}
          ListHeaderComponentStyle={styles.renderCurrentProgramStyles}
          keyExtractor={item => item.availableProgramId}
          horizontal={false}
          numColumns={2}
          columnWrapperStyle={styles.contentFlatlist}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    backgroundColor: GRAY_BORDER,
    width: '100%',
    marginTop: -20,
    //marginBottom: moderateScale(20),
  },
  container: {
    paddingHorizontal: calcWidth(20),
    flex: 1,
  },
  renderCurrentProgramStyles: {
    marginBottom: moderateScale(35),
    marginTop: moderateScale(30),
  },
  contentFlatlist: {
    justifyContent: 'space-between',
  },
});

export default ChooseAnotherNutritionProgram;
