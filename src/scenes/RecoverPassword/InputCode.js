import React, {useContext, useState} from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import {Icon, Input, Button} from 'react-native-elements';
import {LocalizationContext} from '../../localization/translations';
import {MButton} from '../../components/buttons';
import {styles} from './index';
import Header from '../../components/header';

const RecoverPasswordInputCode = ({navigation, route}) => {
  const handleGoBack = () => {
    navigation.goBack();
  };
  const [inputCode, setInputCode] = useState('');

  const {translations} = useContext(LocalizationContext);

  const email = route.params.email;

  const handleInputCode = () => {
    navigation.navigate('RecoverPasswordChooseNewPassword', {
      email,
      inputCode,
    });
  };

  return (
    <SafeAreaView style={styles.safeView}>
      <Header
        title={translations.recoverPasswordScreen.inputCode}
        onPressBackButton={handleGoBack}
      />
      <View style={styles.container}>
        <View>
          <Text style={styles.paragraphText}>
            {translations.recoverPasswordScreen.inputCodeInstruction}
          </Text>
        </View>
        <View style={styles.inputs}>
          <Input
            errorStyle={styles.errors}
            onChangeText={text => setInputCode(text)}
            value={inputCode}
            label={translations.recoverPasswordScreen.inputCodeLabel}
            labelStyle={styles.inputLabelStyle}
            inputStyle={styles.inputStyle}
            inputContainerStyle={styles.inputLabelContainerstyle}
            containerStyle={styles.inputContainerstyle}
          />
        </View>
      </View>
      <MButton
        main
        disabled={!inputCode}
        onPress={handleInputCode}
        title={translations.recoverPasswordScreen.inputCodeButtonText}
        titleStyle={styles.buttonText}
        buttonExtraStyles={styles.button}
        accessibilityLabel="Learn more about this purple button"
      />
    </SafeAreaView>
  );
};

export default RecoverPasswordInputCode;
