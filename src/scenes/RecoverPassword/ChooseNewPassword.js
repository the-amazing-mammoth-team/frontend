import React, {useContext, useState} from 'react';
import {View, Text, SafeAreaView} from 'react-native';
import {Icon, Input, Button} from 'react-native-elements';
import gql from 'graphql-tag';
import {useMutation} from '@apollo/react-hooks';
import {LocalizationContext} from '../../localization/translations';
import {MButton} from '../../components/buttons';
import Header from '../../components/header';
import ErrorModal from '../../components/Modal/ErrorModal';
import {styles} from './index';
import {calcHeight} from '../../utils/dimensions';

const UPDATE_PASSWORD = gql`
  mutation updatePassword($input: UpdatePasswordInput!) {
    updatePassword(input: $input) {
      id
    }
  }
`;

const RecoverPassword = ({navigation, route}) => {
  const handleGoBack = () => {
    navigation.goBack();
  };
  const [formData, setFormData] = useState({
    newPassword: '',
    newPasswordConfirmation: '',
    errors: {},
  });
  const [modalVisible, setModalVisible] = useState(false);

  const code = +route.params.inputCode;
  const email = route.params.email;

  const {translations} = useContext(LocalizationContext);

  const onChangeText = ({key, value}) => {
    const newFormData = {...formData, [key]: value};
    setFormData(newFormData);
  };

  const [update] = useMutation(UPDATE_PASSWORD);

  const handleError = error => {
    switch (true) {
      case error.message.includes('password doesnt match'):
        setFormData({
          ...formData,
          errors: {
            newPasswordConfirmation:
              translations.recoverPasswordScreen.passwordDoesntMatch,
          },
        });
        break;
      case error.message.includes('Password is too short'):
        setFormData({
          ...formData,
          errors: {
            newPassword: translations.loginScreen.tooShort,
          },
        });
        break;
      default: {
        setFormData({
          ...formData,
          errors: {modal: error.message},
        });
        setModalVisible(true);
      }
    }
  };

  const recoverPassword = async () => {
    const input = {
      password: formData.newPassword,
      email,
      code,
    };

    try {
      if (formData.newPassword !== formData.newPasswordConfirmation) {
        throw new Error('password doesnt match');
      } else if (formData.newPassword.length < 6) {
        throw new Error('Password is too short');
      }
      const res = await update({
        variables: {
          input,
        },
      });
      if (res?.data?.updatePassword) {
        navigation.navigate('Login');
      }
    } catch (error) {
      console.log(error);
      handleError(error);
    }
  };

  const {errors} = formData;

  const handleChangePassword = () => {
    recoverPassword();
  };

  const renderErrorModal = () => {
    console.log(modalVisible);
    return (
      <ErrorModal
        isModalVisible={modalVisible}
        onDismiss={() => setModalVisible(false)}
        title={'Error'}
        description={formData.errors.modal}
        textButton={translations.recoverPasswordScreen.goBack}
        onButtonPress={() => setModalVisible(false)}
      />
    );
  };

  return (
    <SafeAreaView style={styles.safeView}>
      <Header
        title={translations.recoverPasswordScreen.chooseNewPassword}
        onPressBackButton={handleGoBack}
      />
      <View style={styles.container}>
        {renderErrorModal()}
        <View style={[styles.inputs, {height: calcHeight(220)}]}>
          <Input
            secureTextEntry
            errorStyle={styles.errors}
            errorMessage={errors.newPassword}
            onChangeText={text =>
              onChangeText({key: 'newPassword', value: text})
            }
            value={formData.newPassword}
            label={translations.recoverPasswordScreen.newPasswordText}
            labelStyle={styles.inputLabelStyle}
            inputStyle={styles.inputStyle}
            inputContainerStyle={styles.inputLabelContainerstyle}
            containerStyle={styles.inputContainerstyle}
            autoCapitalize={'none'}
          />
          <Input
            secureTextEntry
            errorStyle={styles.errors}
            errorMessage={errors.newPasswordConfirmation}
            onChangeText={text =>
              onChangeText({key: 'newPasswordConfirmation', value: text})
            }
            value={formData.newPasswordConfirmation}
            label={
              translations.recoverPasswordScreen.newPasswordConfirmationText
            }
            labelStyle={styles.inputLabelStyle}
            inputStyle={styles.inputStyle}
            inputContainerStyle={styles.inputLabelContainerstyle}
            containerStyle={styles.inputContainerstyle}
            autoCapitalize={'none'}
          />
        </View>
      </View>
      <MButton
        main
        disabled={!formData.newPassword || !formData.newPasswordConfirmation}
        onPress={handleChangePassword}
        title={translations.recoverPasswordScreen.finishChooseNewPassword}
        titleStyle={styles.buttonText}
        buttonExtraStyles={styles.button}
        accessibilityLabel="Learn more about this purple button"
      />
    </SafeAreaView>
  );
};

export default RecoverPassword;
