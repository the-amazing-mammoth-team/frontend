import React, {useContext, useState} from 'react';
import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import {Icon, Input, Button} from 'react-native-elements';
import gql from 'graphql-tag';
import {useMutation} from '@apollo/react-hooks';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {
  BLACK,
  GRAY,
  WHITE,
  LIGHT_GRAY,
  ORANGE,
  LIGHT_ORANGE,
} from '../../styles/colors';
import {LocalizationContext} from '../../localization/translations';
import {MButton} from '../../components/buttons';
import Header from '../../components/header';
import {
  calcFontSize,
  calcHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {validateEmail} from '../../utils/regexes';

const RECOVER_PASSWORD = gql`
  mutation recoverPassword($input: RecoverPasswordInput!) {
    recoverPassword(input: $input)
  }
`;

const RecoverPassword = ({navigation, route}) => {
  const handleGoBack = () => {
    navigation.goBack();
  };
  const [formData, setFormData] = useState({
    email: '',
    errors: {},
  });

  const {translations} = useContext(LocalizationContext);

  const onChangeText = ({key, value}) => {
    const newFormData = {...formData, [key]: value};
    setFormData(newFormData);
  };

  const [recover] = useMutation(RECOVER_PASSWORD);

  const {email} = formData;

  const handleError = error => {
    switch (true) {
      case error.message.includes('GraphQL error: user not found'): {
        setFormData({
          ...formData,
          errors: {
            email: translations.loginScreen.noEmailAccountError,
          },
        });
        break;
      }
      case error.message.includes('Email is invalid'):
        setFormData({
          ...formData,
          errors: {
            email: translations.loginScreen.invalidEmail,
          },
        });
        break;
    }
  };

  const recoverPassword = async () => {
    const input = {
      email,
    };
    console.log({input});
    try {
      if (!validateEmail.test(String(input.email).toLowerCase())) {
        throw new Error('Email is invalid');
      }
      const res = await recover({
        variables: {
          input,
        },
      });
      console.log({res});
      if (res?.data?.recoverPassword) {
        navigation.navigate('RecoverPasswordInputCode', {email});
      }
    } catch (error) {
      console.log(error);
      handleError(error);
    }
  };

  const {errors} = formData;

  const handleForgotPassword = () => {
    recoverPassword();
  };

  return (
    <SafeAreaView style={styles.safeView}>
      <Header
        title={translations.recoverPasswordScreen.headerText}
        onPressBackButton={handleGoBack}
      />
      <View style={styles.container}>
        <View>
          <Text style={styles.paragraphText}>
            {translations.recoverPasswordScreen.paragraphText}
          </Text>
        </View>
        <View style={styles.inputs}>
          <Input
            errorStyle={styles.errors}
            errorMessage={errors?.email}
            onChangeText={text =>
              onChangeText({key: 'email', value: text.toLowerCase()})
            }
            value={formData.email}
            label={translations.recoverPasswordScreen.emailInputLabel}
            labelStyle={styles.inputLabelStyle}
            inputStyle={styles.inputStyle}
            inputContainerStyle={styles.inputLabelContainerstyle}
            containerStyle={styles.inputContainerstyle}
            keyboardType={'email-address'}
            autoCapitalize={'none'}
          />
        </View>
      </View>
      <MButton
        main
        disabled={!formData.email}
        onPress={handleForgotPassword}
        title={translations.recoverPasswordScreen.buttonText}
        titleStyle={styles.buttonText}
        buttonExtraStyles={styles.button}
        accessibilityLabel="Learn more about this purple button"
      />
    </SafeAreaView>
  );
};

export const styles = StyleSheet.create({
  safeView: {
    flex: 1,
    backgroundColor: WHITE,
  },
  container: {
    flex: 1,
    paddingHorizontal: moderateScale(15),
    backgroundColor: WHITE,
    justifyContent: 'flex-start',
  },
  inputStyle: {
    color: BLACK,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
  inputLabelStyle: {
    color: '#BFBFBF',
    fontFamily: SUB_TITLE_FONT,
    fontWeight: 'normal',
    fontSize: calcFontSize(14),
  },
  inputLabelContainerstyle: {
    borderColor: LIGHT_GRAY,
  },
  inputContainerstyle: {
    paddingHorizontal: 0,
  },
  inputs: {
    marginTop: moderateScale(5),
    justifyContent: 'space-between',
  },
  errors: {
    color: LIGHT_ORANGE,
  },
  button: {
    bottom: moderateScale(20),
    borderRadius: moderateScale(30),
    height: calcHeight(55),
    backgroundColor: ORANGE,
    width: deviceWidth * 0.8,
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  paragraphText: {
    color: GRAY,
    height: calcHeight(100),
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
});

export default RecoverPassword;
