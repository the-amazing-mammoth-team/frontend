import React, {useCallback, useContext, useState, useLayoutEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {Icon, Input, CheckBox, Button} from 'react-native-elements';
import gql from 'graphql-tag';
import {useMutation} from '@apollo/react-hooks';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {WHITE, GRAY, ORANGE, LIGHT_ORANGE} from '../../styles/colors';
import {LocalizationContext} from '../../localization/translations';
import {calcFontSize, moderateScale} from '../../utils/dimensions';
import {NextButton} from '../../components/buttons';
import {validateEmail} from '../../utils/regexes';
import EyeHide from '../../assets/icons/eyeHIde.svg';
import EyeShow from '../../assets/icons/eyeShow.svg';
import {AuthContext} from '../../../App';
import WebViewModal from '../../components/Modal/WebViewModal';
import {AbsoluteLoading} from '../../components/Loading';
import {createAnalytics} from '../../services/analytics';
import {trackEvents, Platforms} from '../../services/analytics';
import MixPanel from 'react-native-mixpanel';

const SIGN_UP = gql`
  mutation SignUp($input: SignUpInput!) {
    signUp(input: $input) {
      email
      id
      jwt
      gender
    }
  }
`;

const SignupWithEmail = ({navigation, route}) => {
  const onboardingData = route.params && route.params.onboardingData;
  const {translations, appLanguage} = useContext(LocalizationContext);
  const {signIn: signInInContext} = useContext(AuthContext);
  const [isLoaderVisible, setIsLoaderVisible] = useState(false);

  const handleGoBack = () => {
    navigation.goBack();
  };

  const [formData, setFormData] = useState({
    names: '',
    password: '',
    email: '',
    acceptedNewsletter: true,
    acceptedTerms: false,
    acceptedData: false,
    errors: {},
  });

  const [visibleModal, setVisibleModal] = useState(false);
  const [webLink, setWebLink] = useState('');

  const onChangeText = ({key, value}) => {
    const newFormData = {...formData, [key]: value};
    setFormData(newFormData);
  };

  const handleVisibleModal = () => setVisibleModal(prev => !prev);

  const [signup] = useMutation(SIGN_UP);
  const [isSecureTextEntry, setIsSecureTextEntry] = useState(true);

  useLayoutEffect(() => {
    trackEvents({
      eventName: 'Load Signup with Email',
      properties: {signup_method: 'Email'},
      platforms: {1: Platforms.mixPanel},
    });
  }, []);

  const handleError = error => {
    switch (true) {
      case [
        'Email translation missing: es.activerecord.errors.models.user.attributes.email.taken',
        'Invalid input: Email has already been taken',
      ].some(el => error.message.includes(el)): {
        setFormData({
          ...formData,
          errors: {
            email: {
              message: translations.signUpWithEmailScreen.emailExistsError,
            },
          },
        });
        break;
      }
      case error.message.includes('Password is too short'):
        setFormData({
          ...formData,
          errors: {
            password: {
              message: translations.loginScreen.tooShort,
            },
          },
        });
        break;
      case error.message.includes('Email is invalid'):
        setFormData({
          ...formData,
          errors: {
            email: {
              message: translations.loginScreen.invalidEmail,
            },
          },
        });
        break;
    }
  };

  const submit = async () => {
    const {
      activityLevel,
      bodyFat,
      bodyType,
      dateOfBirth,
      gender,
      goal,
      height,
      weight,
      language,
      measuringSystem,
    } = onboardingData;
    const signUpInput = {
      activityLevel,
      bodyFat,
      bodyType,
      dateOfBirth,
      gender,
      goal,
      height,
      weight,
      language,
    };
    const input = {
      ...signUpInput,
      email: formData.email.toLowerCase(),
      password: formData.password,
      names: formData.names,
      newsletterSubscription: formData.acceptedNewsletter,
      scientificDataUsage: formData.acceptedData,
    };
    try {
      console.log(input);
      if (!validateEmail.test(String(input.email).toLowerCase())) {
        throw new Error('Email is invalid');
      } else if (input.password?.length < 6) {
        throw new Error('Password is too short');
      }
      MixPanel.getDistinctId(async id => {
        try {
          setIsLoaderVisible(true);
          const res = await signup({
            variables: {
              input: {
                ...input,
                mixPanelId: id.toLowerCase(),
                moengageId: input.email.toLowerCase(),
              },
            },
          });
          console.log('response', res);
          setIsLoaderVisible(false);
          if (res.data?.signUp?.id) {
            const id = res.data.signUp.id;
            const token = res.data.signUp.jwt;
            createAnalytics(res.data.signUp.email);
            navigation.navigate('Diagnosis', {
              diagnosisData: input,
              token,
              id,
              measuringSystem,
            });
            await signInInContext({
              id,
              token,
              gender,
              email: res.data.signUp.email,
            });
          }
        } catch (err) {
          console.log(err);
          setIsLoaderVisible(false);
          handleError(err);
        }
      });
    } catch (err) {
      console.log(err);
      handleError(err);
    }
  };

  const toggleCheck = key => {
    setFormData({
      ...formData,
      [key]: !formData[key],
    });
  };

  const {errors} = formData;
  const requiredKeys = ['errors', 'acceptedNewsletter', 'acceptedData'];
  const allFilled = Object.keys(formData)
    .filter(key => !requiredKeys.includes(key))
    .every(key => !!formData[key]);

  const handleEyeIcon = useCallback(() => {
    return (
      <TouchableOpacity
        onPress={() => setIsSecureTextEntry(prevState => !prevState)}
        hitSlop={styles.hitSlopTen}>
        {isSecureTextEntry ? <EyeHide /> : <EyeShow />}
      </TouchableOpacity>
    );
  }, [isSecureTextEntry]);

  return (
    <SafeAreaView style={styles.sceneContainer}>
      {isLoaderVisible && <AbsoluteLoading />}
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={'always'}>
        <View style={styles.container}>
          <WebViewModal
            isVisible={visibleModal}
            onButtonPress={handleVisibleModal}
            link={webLink}
          />
          <View style={styles.header}>
            <View style={styles.arrowBack}>
              <Button
                onPress={handleGoBack}
                buttonStyle={styles.goBackButton}
                icon={<Icon name="arrow-back" />}
              />
            </View>
            <View>
              <Text style={styles.headerText}>
                {translations.signUpWithEmailScreen.tabHeader}
              </Text>
            </View>
          </View>
          <View style={styles.allInputs}>
            <View style={styles.inputs}>
              <Input
                onChangeText={text => onChangeText({key: 'names', value: text})}
                value={formData.nombres}
                label={translations.signUpWithEmailScreen.fullNameField}
                labelStyle={styles.inputLabelStyle}
                inputContainerStyle={styles.inputLabelContainerstyle}
              />
            </View>
            <View style={styles.inputs}>
              <Input
                errorMessage={errors.email && errors.email.message}
                errorStyle={styles.errorText}
                onChangeText={text =>
                  onChangeText({key: 'email', value: text.toLowerCase()})
                }
                value={formData.email}
                label={translations.signUpWithEmailScreen.emailField}
                labelStyle={styles.inputLabelStyle}
                inputContainerStyle={styles.inputLabelContainerstyle}
                keyboardType={'email-address'}
                autoCapitalize={'none'}
              />
            </View>
            <View style={styles.inputs}>
              <Input
                errorMessage={errors.password && errors.password.message}
                errorStyle={styles.errorText}
                onChangeText={text =>
                  onChangeText({key: 'password', value: text})
                }
                value={formData.password}
                label={translations.signUpWithEmailScreen.passwordField}
                inputContainerStyle={styles.inputLabelContainerstyle}
                labelStyle={styles.inputLabelStyle}
                rightIcon={handleEyeIcon}
                secureTextEntry={isSecureTextEntry}
                autoCapitalize={'none'}
              />
            </View>
          </View>
          <View style={styles.checkboxContainer}>
            <CheckBox
              onPress={() => toggleCheck('acceptedTerms')}
              checked={formData.acceptedTerms}
              checkedIcon={<Icon name="check" color={WHITE} />}
              checkedColor="#FFFFFF"
              containerStyle={
                formData.acceptedTerms
                  ? styles.checkboxContainerStyles
                  : [styles.checkboxContainerStyles, styles.checkboxUnchecked]
              }
            />
            <Text style={styles.checkboxLabel}>
              {
                translations.signUpWithEmailScreen.termsConditionsPrivacyPolicy
                  .fistPrefix
              }{' '}
              <Text
                style={styles.purpleText}
                onPress={() => {
                  setWebLink(
                    `https://mhunters.com/es/${
                      appLanguage === 'es' ? 'cdu' : 'tos'
                    }`,
                  );
                  handleVisibleModal();
                }}>
                {
                  translations.signUpWithEmailScreen
                    .termsConditionsPrivacyPolicy.termsConditions
                }
              </Text>{' '}
              {
                translations.signUpWithEmailScreen.termsConditionsPrivacyPolicy
                  .secondPrefix
              }
              <Text
                style={styles.purpleText}
                onPress={() => {
                  setWebLink(
                    `https://mhunters.com/es/${
                      appLanguage === 'es'
                        ? 'politica-de-privacidad'
                        : 'privacy'
                    }`,
                  );
                  handleVisibleModal();
                }}>
                {' '}
                {
                  translations.signUpWithEmailScreen
                    .termsConditionsPrivacyPolicy.privacyPolicy
                }{' '}
              </Text>
              {
                translations.signUpWithEmailScreen.termsConditionsPrivacyPolicy
                  .mammothHunters
              }
            </Text>
          </View>
          <View style={styles.checkboxContainer}>
            <CheckBox
              onPress={() => toggleCheck('acceptedData')}
              checked={formData.acceptedData}
              checkedIcon={<Icon name="check" color={WHITE} />}
              checkedColor="#FFFFFF"
              containerStyle={
                formData.acceptedData
                  ? styles.checkboxContainerStyles
                  : [styles.checkboxContainerStyles, styles.checkboxUnchecked]
              }
            />
            <Text style={styles.checkboxLabel}>
              {translations.signUpWithEmailScreen.dataAgreement.firstPart}{' '}
              <Text
                style={styles.purpleText}
                onPress={() => {
                  setWebLink(
                    `https://mhunters.com/es/${
                      appLanguage === 'es'
                        ? 'consentimiento-estudio-cientifico'
                        : 'concent-science-study'
                    }`,
                  );
                  handleVisibleModal();
                }}>
                {translations.signUpWithEmailScreen.dataAgreement.secondPart}
              </Text>
            </Text>
          </View>
          <View style={styles.checkboxContainer}>
            <CheckBox
              onPress={() => toggleCheck('acceptedNewsletter')}
              checked={formData.acceptedNewsletter}
              checkedIcon={<Icon name="check" color={WHITE} />}
              checkedColor="#FFFFFF"
              containerStyle={
                formData.acceptedNewsletter
                  ? styles.checkboxContainerStyles
                  : [styles.checkboxContainerStyles, styles.checkboxUnchecked]
              }
            />
            <Text style={styles.checkboxLabel}>
              {translations.signUpWithEmailScreen.newsDissemination}
            </Text>
          </View>
        </View>
      </ScrollView>
      <NextButton onPress={submit} disabled={!allFilled || isLoaderVisible} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sceneContainer: {
    backgroundColor: WHITE,
    flex: 1,
  },
  container: {
    paddingTop: 15,
    paddingLeft: 25,
    paddingRight: 25,
    paddingBottom: 30,
    backgroundColor: WHITE,
  },
  inputLabelStyle: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontWeight: 'normal',
  },
  inputLabelContainerstyle: {
    borderColor: GRAY,
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: MAIN_TITLE_FONT,
  },
  arrowBack: {
    alignSelf: 'flex-start',
    width: 50,
    height: 50,
    position: 'absolute',
    top: 5,
    left: 0,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },
  inputs: {
    height: 30,
    marginBottom: 50,
    marginTop: 10,
  },
  allInputs: {
    marginBottom: moderateScale(30),
    marginTop: moderateScale(1),
  },
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    paddingLeft: 0,
    marginLeft: 0,
    marginBottom: moderateScale(28),
  },
  checkboxLabel: {
    flex: 1,
    flexWrap: 'wrap',
    fontSize: 16,
  },
  checkboxContainerStyles: {
    borderColor: ORANGE,
    backgroundColor: ORANGE,
    borderWidth: 0,
    marginTop: 0,
    marginRight: 20,
    padding: 0,
    borderRadius: moderateScale(3),
  },
  checkboxUnchecked: {
    borderColor: WHITE,
    backgroundColor: WHITE,
  },
  purpleText: {
    color: ORANGE,
    fontWeight: 'bold',
  },
  bottomButton: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    flex: 1,
    position: 'absolute',
    bottom: -20,
    right: 0,
    marginRight: 25,
    marginBottom: 20,
  },
  buttonStyle: {
    borderRadius: 50,
    height: 50,
    width: 50,
    backgroundColor: GRAY,
  },
  goBackButton: {
    backgroundColor: WHITE,
  },
  errorText: {
    color: LIGHT_ORANGE,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  hitSlopTen: {
    top: moderateScale(10),
    bottom: moderateScale(10),
    right: moderateScale(10),
    left: moderateScale(10),
  },
});

export default SignupWithEmail;
