import React, {useContext, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  BackHandler,
  SafeAreaView,
} from 'react-native';
import {Button, Icon} from 'react-native-elements';
import {CommonActions} from '@react-navigation/native';
import {NextButton} from '../../components/buttons';
import {BLACK, GRAY, LIGHT_ORANGE, ORANGE} from '../../styles/colors';
import getDiagnosis from '../../utils/getDiagnosis';
import {SUB_TITLE_FONT, MAIN_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import {calcFontSize, moderateScale} from '../../utils/dimensions';

function Diagnosis({route, navigation}) {
  const {
    BMI,
    fatWeight,
    leanWeight,
    idealWeight,
    targetBMI,
    idealFatWeight,
    idealLeanWeight,
    fatToLose,
    targetCalories,
    weeksToTarget,
    weeksWithTraining,
    muscleToGain,
  } = getDiagnosis(route.params.diagnosisData);
  const {token, diagnosisData, id, measuringSystem} = route.params;
  const {weight} = diagnosisData;

  const {translations} = useContext(LocalizationContext);

  const handleOnPress = async () => {
    //navigation.navigate('ProgramsScreen');
    navigation.dispatch(
      //reset stack screens
      CommonActions.reset({
        index: 1,
        routes: [{name: 'SuggestedPrograms'}],
      }),
    );
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', () => {
      return true;
    });
  });

  console.log(7777, measuringSystem);

  return (
    <SafeAreaView style={styles.safeArea}>
      <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
        <View>
          <Text style={styles.headerText}>
            {translations.diagnosisScreen.tabHeader}
          </Text>
        </View>
        <View>
          <Text style={styles.subHeaderText}>
            {translations.diagnosisScreen.physicalEvaluation}
          </Text>
        </View>
        <View style={styles.table}>
          <View style={[styles.tableRow, styles.noBorderBottom]}>
            <View style={styles.tableFirstColumn} />
            <View style={styles.tableSecondColumn}>
              <Text style={styles.lightOrangeColor}>
                {translations.diagnosisScreen.current}
              </Text>
            </View>
            <View style={styles.tableSecondColumn}>
              <Text style={styles.orangeColor}>
                {translations.diagnosisScreen.ideal}
              </Text>
            </View>
          </View>
          <View style={styles.tableRow}>
            <View style={styles.tableFirstColumn}>
              <Text style={[styles.tableText, styles.lightGrayColor]}>
                {translations.diagnosisScreen.weight}
              </Text>
            </View>
            <View style={styles.tableSecondColumn}>
              <Text style={styles.tableText}>
                {measuringSystem === 'imperial'
                  ? (weight * 2.205).toFixed(1)
                  : weight}{' '}
                {measuringSystem === 'imperial' ? 'lbs' : 'Kg'}
              </Text>
            </View>
            <View style={styles.tableSecondColumn}>
              <Text style={styles.tableText}>
                {measuringSystem === 'imperial'
                  ? (idealWeight * 2.205).toFixed(1)
                  : idealWeight}{' '}
                {measuringSystem === 'imperial' ? 'lbs' : 'Kg'}
              </Text>
            </View>
          </View>
          <View style={styles.tableRow}>
            <View style={styles.tableFirstColumn}>
              <Text style={[styles.tableText, styles.lightGrayColor]}>BMI</Text>
            </View>
            <View style={styles.tableSecondColumn}>
              <Text style={styles.tableText}>{BMI}</Text>
            </View>
            <View style={styles.tableSecondColumn}>
              <Text style={styles.tableText}>{targetBMI}</Text>
            </View>
          </View>
          <View style={styles.tableRow}>
            <View style={styles.tableFirstColumn}>
              <Text style={[styles.tableText, styles.lightGrayColor]}>
                {translations.diagnosisScreen.bodyFat}
              </Text>
            </View>
            <View style={styles.tableSecondColumn}>
              <Text style={styles.tableText}>
                {measuringSystem === 'imperial'
                  ? (fatWeight * 2.205).toFixed(1)
                  : fatWeight}{' '}
                {measuringSystem === 'imperial' ? 'lbs' : 'Kg'}
              </Text>
            </View>
            <View style={styles.tableSecondColumn}>
              <Text style={styles.tableText}>
                {measuringSystem === 'imperial'
                  ? (idealFatWeight * 2.205).toFixed(1)
                  : idealFatWeight}{' '}
                {measuringSystem === 'imperial' ? 'lbs' : 'Kg'}
              </Text>
            </View>
          </View>
          <View style={[styles.tableRow, styles.noBorderBottom]}>
            <View style={styles.tableFirstColumn}>
              <Text style={[styles.tableText, styles.lightGrayColor]}>
                {translations.diagnosisScreen.muscleWeight}
              </Text>
            </View>
            <View style={styles.tableSecondColumn}>
              <Text style={styles.tableText}>
                {measuringSystem === 'imperial'
                  ? (leanWeight * 2.205).toFixed(1)
                  : leanWeight}{' '}
                {measuringSystem === 'imperial' ? 'lbs' : 'Kg'}
              </Text>
            </View>
            <View style={styles.tableSecondColumn}>
              <Text style={styles.tableText}>
                {measuringSystem === 'imperial'
                  ? (idealLeanWeight * 2.205).toFixed(1)
                  : idealLeanWeight}{' '}
                {measuringSystem === 'imperial' ? 'lbs' : 'Kg'}
              </Text>
            </View>
          </View>
        </View>
        <View>
          <Text style={[styles.subHeaderText, styles.subHeadermb]}>
            {translations.diagnosisScreen.objectives}
          </Text>
        </View>
        <View style={styles.table}>
          <View style={styles.tableRow}>
            <View style={styles.objectivesFirstColumn}>
              <Text style={[styles.tableText, styles.lightGrayColor]}>
                {Number(fatToLose) > 0
                  ? translations.diagnosisScreen.loseFat
                  : translations.diagnosisScreen.gainFat}
              </Text>
            </View>
            <View style={styles.objectivesSecondColumn}>
              <Text style={styles.tableText}>
                {measuringSystem === 'imperial'
                  ? (Math.abs(Number(fatToLose)) * 2.205).toFixed(1)
                  : Math.abs(Number(fatToLose))}{' '}
                {measuringSystem === 'imperial' ? 'lbs' : 'Kg'}
              </Text>
            </View>
          </View>
          <View style={styles.tableRow}>
            <View style={styles.objectivesFirstColumn}>
              <Text style={[styles.tableText, styles.lightGrayColor]}>
                {translations.diagnosisScreen.gainMuscle}
              </Text>
            </View>
            <View style={styles.objectivesSecondColumn}>
              <Text style={styles.tableText}>
                {Number(muscleToGain) === 0
                  ? 'All Good'
                  : `${
                      measuringSystem === 'imperial'
                        ? (Math.abs(Number(muscleToGain)) * 2.205).toFixed(1)
                        : Math.abs(Number(muscleToGain))
                    } ${measuringSystem === 'imperial' ? 'lbs' : 'Kg'}`}
              </Text>
            </View>
          </View>
          <View style={styles.tableRow}>
            <View style={styles.objectivesFirstColumn}>
              <Text style={[styles.tableText, styles.lightGrayColor]}>
                {translations.diagnosisScreen.dailyIntake}
              </Text>
            </View>
            <View style={styles.objectivesSecondColumn}>
              <Text style={styles.tableText}>{targetCalories} Cal</Text>
            </View>
          </View>
          {weeksToTarget && (
            <View style={[styles.tableRow, styles.noBorderBottom]}>
              <View style={styles.objectivesFirstColumn}>
                <Text
                  style={[
                    styles.tableText,
                    styles.lightGrayColor,
                    styles.bottomSeparation,
                  ]}>
                  {translations.diagnosisScreen.achieveGoal}
                </Text>
              </View>
              <View style={styles.objectivesSecondColumn}>
                <Text style={[styles.tableText, styles.bottomSeparation]}>
                  {weeksToTarget} {translations.diagnosisScreen.weeks}
                </Text>
              </View>
            </View>
          )}
        </View>
      </ScrollView>
      <NextButton onPress={handleOnPress} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingLeft: 20,
    paddingRight: 20,
  },
  headerText: {
    fontSize: calcFontSize(18),
    marginTop: moderateScale(25),
    marginBottom: moderateScale(30),
    color: BLACK,
    textAlign: 'center',
    fontFamily: MAIN_TITLE_FONT,
  },
  subHeaderText: {
    color: GRAY,
    fontSize: 14,
    fontFamily: SUB_TITLE_FONT,
  },
  tableRow: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5',
    paddingVertical: moderateScale(14),
    justifyContent: 'space-between',
  },
  tableFirstColumn: {
    flexBasis: '50%',
  },
  tableSecondColumn: {
    flexBasis: '20%',
  },
  tableThirdColumn: {
    flexBasis: '30%',
    justifyContent: 'center',
  },
  objectivesFirstColumn: {
    flexBasis: '70%',
  },
  objectivesSecondColumn: {
    flexBasis: '30%',
    justifyContent: 'center',
  },
  tableText: {
    fontSize: 16,
    fontFamily: SUB_TITLE_FONT,
  },
  noBorderBottom: {
    borderBottomWidth: 0,
  },
  subHeadermb: {
    marginBottom: 10,
    marginTop: 20,
  },
  lightOrangeColor: {
    color: LIGHT_ORANGE,
  },
  orangeColor: {
    color: ORANGE,
  },
  lightGrayColor: {
    color: '#616362',
  },
  grayText: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    marginBottom: moderateScale(5),
  },
  trainingDaysLabel: {
    textAlign: 'left',
  },
  bottomSeparation: {
    marginBottom: moderateScale(15),
  },
});

export default Diagnosis;
