import {StyleSheet} from 'react-native';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY,
  GRAY_BORDER,
  LIGHT_GRAY,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_BOLD_FONT,
  SUB_TITLE_FONT,
} from '../../styles/fonts';

export const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  line: {
    height: calcHeight(1),
    width: deviceWidth,
    backgroundColor: GRAY_BORDER,
    marginTop: -20,
    marginBottom: calcHeight(20),
  },
  container: {
    paddingHorizontal: calcWidth(20),
    flex: 1,
  },
  imageCircle: {
    borderRadius: moderateScale(72) / 2,
    overflow: 'hidden',
  },
  imageAuthor: {
    width: moderateScale(70),
    height: moderateScale(70),
  },
  noImageCircle: {
    backgroundColor: LIGHT_ORANGE,
    width: moderateScale(72),
    height: moderateScale(72),
    borderRadius: moderateScale(72) / 2,
  },
  tip: {
    backgroundColor: ORANGE,
    paddingVertical: calcHeight(1.5),
    paddingHorizontal: calcWidth(10),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: calcWidth(40),
    top: -calcHeight(10),
  },
  tipText: {
    color: WHITE,
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(14),
  },
  rowProfileInfo: {
    flexDirection: 'row',
    elevation: 3,
    shadowColor: BLACK,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    borderRadius: 10,
    backgroundColor: '#fff',
    paddingVertical: moderateScale(21),
    paddingHorizontal: moderateScale(11),
  },
  profileTitle: {
    color: BLACK,
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  candidateText: {
    color: GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  levelPointsText: {
    color: BLACK,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    marginRight: moderateScale(17),
  },
  minimalInfoView: {
    flex: 1,
    justifyContent: 'space-around',
    marginLeft: calcWidth(25),
  },
  sectionText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
  },
  linearGradient: {
    flexDirection: 'row',
    flex: 1,
    flexWrap: 'wrap',
    marginTop: 30,
  },
  resultsCardTextTitle: {
    color: WHITE,
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(30),
    textAlign: 'center',
  },
  resultsCardTextTitleBlack: {
    color: BLACK,
  },
  resultView: {
    textAlign: 'center',
    flexBasis: '50%',
    paddingTop: 25,
    paddingBottom: 25,
  },
  topLeft: {
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
    borderRightColor: '#E5E5E5',
    borderRightWidth: 1,
  },
  bottomLeft: {
    borderRightColor: '#E5E5E5',
    borderRightWidth: 1,
  },
  topRight: {
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
  },
  bottomRight: {},
  resultsCardTextName: {
    color: WHITE,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
  },
  resultsCardTextNameBlack: {
    color: BLACK,
  },
  crossView: {
    alignSelf: 'center',
  },
  tableRow: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5E5',
    paddingVertical: calcHeight(10),
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  orangeText: {
    color: ORANGE,
  },
  lightOrangeText: {
    color: LIGHT_ORANGE,
  },
  blackText: {
    color: BLACK,
  },
  darkGrayText: {
    color: BUTTON_TITLE,
  },
  tableText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  table: {
    paddingHorizontal: calcWidth(5),
    paddingVertical: calcHeight(10),
    elevation: 3,
    shadowColor: BLACK,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.3,
    shadowRadius: 3,
    borderRadius: 10,
    backgroundColor: WHITE,
    marginTop: calcHeight(45),
  },
  tableTitleText: {
    color: GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  tableTitleView: {
    marginLeft: calcWidth(5),
    marginTop: calcHeight(5),
  },
  tableHeaderView: {
    flexDirection: 'row',
    marginBottom: calcHeight(5),
  },
  editIconView: {
    marginLeft: calcWidth(15),
    marginTop: calcHeight(0),
  },
  hitSlopFifteen: {
    top: calcHeight(15),
    bottom: calcHeight(15),
    right: calcWidth(15),
    left: calcWidth(15),
  },
  hitSlopTwenty: {
    top: calcHeight(20),
    bottom: calcHeight(20),
    right: calcWidth(20),
    left: calcWidth(20),
  },
  tableFirstColumn: {
    flexBasis: '45%',
  },
  tableSecondColumn: {
    flexBasis: '20%',
  },
  tableThirdColumn: {
    flexBasis: '20%',
    alignSelf: 'flex-end',
  },
  noBorderBottom: {
    borderBottomWidth: 0,
  },
  tipTextGray: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    textAlign: 'center',
  },
  indicatorView: {
    alignSelf: 'center',
    marginTop: deviceHeight * 0.3,
  },
  latestResultsSectionTitleView: {
    marginTop: calcHeight(30),
    marginBottom: calcHeight(5),
  },
  objectivesSectionTextView: {
    marginTop: calcHeight(20),
  },
  objectivesSectionText: {
    color: GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  bottomSeparation: {
    marginBottom: calcHeight(10),
  },
  marginLeftTen: {
    marginLeft: calcWidth(10),
  },
  grayText: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
  },
  centerText: {
    textAlign: 'center',
  },
  startTestButton: {
    backgroundColor: ORANGE,
    borderRadius: moderateScale(25),
    paddingVertical: moderateScale(13),
    width: deviceWidth * 0.8,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: calcHeight(20),
  },
  whiteTextButton: {
    color: WHITE,
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
  },
  renderNavigationRow: {
    borderBottomWidth: moderateScale(1),
    borderBottomColor: GRAY_BORDER,
    paddingVertical: moderateScale(15),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  navigationTextGray: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: BUTTON_TITLE,
  },
  extraSectionMarginTop: {
    marginTop: moderateScale(50),
  },
  versionView: {
    alignSelf: 'center',
    marginTop: moderateScale(90),
    marginBottom: moderateScale(25),
  },
  versionText: {
    textAlign: 'center',
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
  },
  signOffView: {
    marginTop: moderateScale(20),
    marginBottom: moderateScale(37),
    alignSelf: 'center',
  },
  signOffText: {
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  centerRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
