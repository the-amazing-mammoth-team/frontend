import React, {useCallback, useContext} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useMutation} from '@apollo/react-hooks';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';
import {
  BLACK,
  GRAY_BORDER,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import Header from '../../components/header';
import {AuthContext} from '../../../App';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import WarningIcon from '../../assets/icons/Caution.svg';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {logoutAnalytics, trackEvents} from '../../services/analytics';

const DELETE_USER = gql`
  mutation deleteUser($input: DeleteUserInput!) {
    deleteUser(input: $input)
  }
`;

const RESTART_PROGRAM = gql`
  mutation restartProgram($input: RestartProgramInput!) {
    restartProgram(input: $input) {
      id
    }
  }
`;

const DeleteScreen = ({navigation, route}) => {
  const {
    title,
    description,
    underDescription = '',
    underLineText,
    buttonText,
    type,
  } = route.params;

  const {translations} = useContext(LocalizationContext);
  const {userData: userInfo, signOut} = useContext(AuthContext);
  const isFocused = useIsFocused();

  const [update] = useMutation(DELETE_USER);
  const [restart] = useMutation(RESTART_PROGRAM);

  useFocusEffect(
    useCallback(() => {
      isFocused &&
        type === 'restart' &&
        trackEvents({eventName: 'Load Restart Program'});
    }, [isFocused, type]),
  );

  const handleGoBack = useCallback(() => navigation.goBack(), [navigation]);

  const handleDelete = useCallback(async () => {
    try {
      const res = await update({
        variables: {
          input: {
            userId: userInfo?.id,
          },
        },
        context: {
          headers: {
            authorization: userInfo?.token,
          },
        },
        refetchQueries: ['user'],
      });
      console.log('RESULT', {res});
      if (res?.data?.deleteUser) {
        console.log('Success');
        trackEvents({eventName: 'Log Out'});
        logoutAnalytics();
        await signOut();
      }
    } catch (error) {
      console.log(error);
    }
  }, [signOut, update, userInfo]);

  const handleRestart = useCallback(async () => {
    try {
      const res = await restart({
        variables: {
          input: {
            userId: userInfo?.id,
          },
        },
        context: {
          headers: {
            authorization: userInfo?.token,
          },
        },
        refetchQueries: ['user'],
      });
      console.log('RESULT', {res});
      if (res?.data?.restartProgram) {
        console.log('Success');
        goHome();
      }
    } catch (error) {
      console.log(error);
    }
  }, [goHome, restart, userInfo]);

  const onPressKeep = useCallback(() => navigation.goBack(), [navigation]);
  const goHome = useCallback(
    () => navigation.navigate('Home', {doRefetch: true}),
    [navigation],
  );

  return (
    <SafeAreaView style={styles.root}>
      <Header title={title} onPressBackButton={handleGoBack} />
      <View style={styles.headerLine} />
      <View style={styles.content}>
        <View style={styles.warningIconView}>
          <WarningIcon width={moderateScale(42)} height={moderateScale(42)} />
        </View>
        <View style={styles.titleView}>
          <Text style={styles.title}>
            {translations.profileScreen.areYouSure}
          </Text>
        </View>
        <View style={styles.descriptionView}>
          <Text style={styles.genericBlack}>{description}</Text>
        </View>
        <Text style={styles.genericBlack}>{underDescription}</Text>
      </View>
      <View style={styles.footerView}>
        <TouchableOpacity style={styles.footerButton} onPress={onPressKeep}>
          <Text style={[styles.title, styles.footerButtonText]}>
            {buttonText}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.footerUnderline}
          hitSlop={styles.hitSlopFifteen}
          onPress={type === 'delete' ? handleDelete : handleRestart}>
          <Text style={[styles.genericBlack, styles.footerTextUnderline]}>
            {underLineText}
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    width: deviceWidth,
    alignSelf: 'center',
    backgroundColor: GRAY_BORDER,
    marginTop: -20,
    marginBottom: moderateScale(32),
  },
  warningIconView: {
    alignSelf: 'center',
    marginBottom: moderateScale(25),
  },
  titleView: {
    alignSelf: 'center',
    marginBottom: moderateScale(12),
  },
  title: {
    fontSize: calcFontSize(18),
    color: BLACK,
    textAlign: 'center',
    fontFamily: MAIN_TITLE_FONT,
  },
  descriptionView: {
    alignSelf: 'center',
    marginBottom: moderateScale(20),
  },
  finalWordsView: {
    alignSelf: 'center',
  },
  genericBlack: {
    fontSize: calcFontSize(14),
    color: BLACK,
    textAlign: 'center',
    fontFamily: SUB_TITLE_FONT,
  },
  content: {
    paddingHorizontal: moderateScale(20),
  },
  footerButton: {
    width: deviceWidth * 0.8,
    paddingVertical: moderateScale(12),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: moderateScale(25),
    backgroundColor: ORANGE,
  },
  footerButtonText: {
    color: WHITE,
  },
  footerTextUnderline: {
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
  },
  footerUnderline: {
    marginVertical: calcHeight(17),
  },
  hitSlopFifteen: {
    top: calcHeight(15),
    bottom: calcHeight(15),
    right: calcWidth(15),
    left: calcWidth(15),
  },
  footerView: {
    position: 'absolute',
    bottom: moderateScale(10),
    right: 0,
    left: 0,
  },
});

export default DeleteScreen;
