import React, {useContext, useState, useCallback, useEffect} from 'react';
import {Icon} from 'react-native-elements';
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  Platform,
  StyleSheet,
} from 'react-native';
import {useLazyQuery, useMutation} from '@apollo/react-hooks';
import {useFocusEffect} from '@react-navigation/native';
import RNIap, {purchaseErrorListener, IAPErrorCode} from 'react-native-iap';
import gql from 'graphql-tag';
import * as Sentry from '@sentry/react-native';
// import Mixpanel from 'react-native-mixpanel';
import deviceStorage from '../../services/deviceStorage';
import {LocalizationContext} from '../../localization/translations';
import Header from '../../components/header';
import ErrorModal from '../../components/Modal/ErrorModal';
import ProgramOfferCard from '../../components/ProgramsScreens/ProgramOfferCard';
import {paymentError} from '../../utils/errorMessages';
import {AuthContext} from '../../../App';
import {calcFontSize, deviceWidth, moderateScale} from '../../utils/dimensions';
import {profileFreeCodes} from '../../utils/offerCodes';
import {BLACK, GRAY, GRAY_BORDER, ORANGE, WHITE} from '../../styles/colors';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_BOLD_FONT,
  SUB_TITLE_FONT,
} from '../../styles/fonts';
import {trackEvents} from '../../services/analytics';
import {Loading} from '../../components/Loading';

let myPurchaseErrorListener = () => {};

const GET_PRODUCTS = gql`
  query products($offerCodes: [String!]!, $device: String!) {
    products(offerCodes: $offerCodes, device: $device) {
      id
      storeReference
      price
      name
      hasTrial
      trialDays
    }
  }
`;

const SUBSCRIBE = gql`
  mutation subscribe($input: SubscribeInput!) {
    subscribe(input: $input) {
      id
      status
      startDate
      transactionBody
      user {
        email
        names
      }
    }
  }
`;

const CheckItem = ({text}) => {
  return (
    <View style={styles.checkContainer}>
      <View style={styles.circleCheck}>
        <Icon name="check" color="#ffffff" size={20} />
      </View>
      <Text style={styles.checkText}>{text}</Text>
    </View>
  );
};

function getOs() {
  return Platform.OS === 'ios' ? 'apple' : 'google_play';
}

const GetProSubscription = ({navigation, route}) => {
  const {program} = route.params;

  const [
    getProducts,
    {data: products, error: err, loading = false},
  ] = useLazyQuery(GET_PRODUCTS, {
    variables: {
      offerCodes: profileFreeCodes,
      device: getOs(),
    },
    fetchPolicy: 'network-only',
  });

  console.log(products);

  const {translations} = useContext(LocalizationContext);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalTextError, setModalTextError] = useState({code: '', message: ''});

  const {userData: userInfo} = useContext(AuthContext);

  const [isButtonPress, setIsButtonPress] = useState(false);

  const [subscribe] = useMutation(SUBSCRIBE);

  useEffect(() => {
    myPurchaseErrorListener = purchaseErrorListener(err =>
      handlePurchaseErrors(err),
    );
    return () => {
      myPurchaseErrorListener.remove();
    };
  }, [handlePurchaseErrors, navigation]);

  useFocusEffect(
    useCallback(() => {
      if (products) {
        trackEvents({
          eventName: 'Load Sales',
          properties: {
            type: 'profile_free',
            offer: profileFreeCodes,
            products: products.products?.map(el => el.storeReference),
            prices: products.products?.map(el => el.price),
            payment_platform: getOs(),
            placement: 'Profile',
          },
        });
      }
    }, [products]),
  );

  useEffect(() => {
    if (!loading && !products) {
      getProducts();
    }
  }, [getProducts, loading, navigation, products]);

  const handlePurchaseErrors = useCallback(
    err => {
      if (err.code !== IAPErrorCode.E_USER_CANCELLED) {
        Sentry.captureMessage(err);
        setModalTextError({message: paymentError(err.code, translations)});
        setIsModalVisible(true);
      } else {
        setIsButtonPress(false);
      }
    },
    [translations],
  );

  const onSubscribe = useCallback(
    async (productId, receipt, userInfo) => {
      try {
        console.log('user info at subscribe', userInfo);
        const res = await subscribe({
          variables: {
            input: {
              programId: program.id,
              productId: productId,
              transactionBody: receipt.toString(),
            },
          },
          context: {
            headers: {
              authorization: userInfo?.token,
            },
          },
          refetchQueries: ['user'],
        });
        if (res.data) {
          console.log('Response:', res.data.subscribe);
          return true;
        }
      } catch (error) {
        if (
          error.message.includes(
            'GraphQL error: Invalid input: Active no more than 1 subscription active per user',
          )
        ) {
          setModalTextError({
            code: error.code,
            message: translations.getProSubscription.subscriptionExist,
          });
          setIsModalVisible(true);
          return;
        }
        Sentry.captureMessage(error);
        setModalTextError({code: error.code, message: error.message});
        setIsModalVisible(true);
        throw new Error(error.message);
      }
    },
    [program, subscribe, translations],
  );

  const buySubscription = useCallback(
    async product => {
      setIsButtonPress(true);
      const requestPurchase = async sku => {
        try {
          await RNIap.initConnection();
          const products = await RNIap.getSubscriptions([sku]);
          // await deviceStorage.saveItem(
          //   '@purchase/product',
          //   JSON.stringify(products[0]),
          // );
          const res = await RNIap.requestPurchase(sku, false);
          if (res) {
            await onSubscribe(product.id, res.transactionReceipt, userInfo);
            await deviceStorage.saveItem('@purchase/isrSuccessRequest', 'true');
            setIsButtonPress(false);
            navigation.navigate('WelcomeToPro');
          }
          setIsButtonPress(false);
        } catch (err) {}
      };
      await requestPurchase(product.storeReference);
    },
    [onSubscribe, userInfo, navigation],
  );

  const handleDismissModal = useCallback(() => {
    setIsModalVisible(oldData => !oldData);
    setIsButtonPress(false);
  }, []);

  const handleGoBack = () => {
    navigation.goBack();
  };

  const renderSubTitle = () => {
    const splited = translations.getProSubscription.desc.split('*planName*');
    return (
      <Text>
        <Text style={styles.subTitle}>{splited[0]}</Text>
        <Text style={styles.subTitleBold}>
          {translations.getProSubscription.freePlan}
        </Text>
        <Text style={styles.subTitle}>{splited[1]}</Text>
      </Text>
    );
  };

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <ErrorModal
          isModalVisible={isModalVisible}
          onDismiss={handleDismissModal}
          title={'Error'}
          description={modalTextError.message}
        />
        <View style={styles.headerView}>
          <Header
            title={translations.getProSubscription.header}
            onPressBackButton={handleGoBack}
          />
          <View style={styles.headerLine} />
        </View>
        <View style={styles.container}>
          {renderSubTitle()}
          <Text style={styles.title}>
            {translations.getProSubscription.title}
          </Text>
          {translations.getProSubscription.benefitsOfPro.map(char => (
            <CheckItem text={char} />
          ))}
          <View style={{marginBottom: moderateScale(20)}} />
          {products ? (
            <>
              <ProgramOfferCard
                id={products?.products[0].id}
                title={products?.products[0].name}
                onPress={() => buySubscription(products?.products[0])}
                price={products?.products[0].price}
                pro={true}
                isTrial={true}
                trialDays={translations.settingsScreen.discount60}
                currency={'eur'}
                local="es"
                translations={translations.programBuyScreen}
                disabled={isButtonPress}
                isAnnual={true}
              />
              <ProgramOfferCard
                id={products?.products[1].id}
                title={products?.products[1].name}
                onPress={() => buySubscription(products?.products[1])}
                price={products?.products[1].price}
                pro={true}
                isTrial={false}
                currency={'eur'}
                local="es"
                translations={translations.programBuyScreen}
                disabled={isButtonPress}
                isAnnual={false}
              />
            </>
          ) : (
            <View style={{marginBottom: moderateScale(40)}}>
              <Loading noMargin />
            </View>
          )}
          <Text style={styles.grayText}>
            {translations.mySubscription.automaticalyRenewed}
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    width: deviceWidth,
    alignSelf: 'center',
    backgroundColor: GRAY_BORDER,
    marginTop: -10,
    marginBottom: moderateScale(20),
  },
  container: {
    marginHorizontal: moderateScale(20),
  },
  title: {
    fontSize: calcFontSize(25),
    fontFamily: MAIN_TITLE_FONT,
    marginVertical: moderateScale(25),
  },
  subTitle: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: BLACK,
  },
  subTitleBold: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_BOLD_FONT,
    color: BLACK,
  },
  circleCheck: {
    borderRadius: 50,
    width: 20,
    height: 20,
    backgroundColor: ORANGE,
    marginRight: 15,
  },
  checkContainer: {
    flexDirection: 'row',
    marginRight: moderateScale(30),
    marginBottom: moderateScale(15),
  },
  checkText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: BLACK,
  },
  grayText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
    marginBottom: moderateScale(20),
  },
});

export default GetProSubscription;
