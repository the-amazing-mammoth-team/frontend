import React, {useCallback, useContext, useState} from 'react';
import {
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  View,
  Text,
  TextInput,
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useMutation} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import SegmentedPicker from 'react-native-segmented-picker';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';
import BodyTypeQuestion from '../Onboarding/BodyTypeQuestion';
import BodyfatPicker from '../Onboarding/BodyfatPicker';
import Question from '../Onboarding/Question';
import {BLACK, GRAY_BORDER, ORANGE, WHITE} from '../../styles/colors';
import Header from '../../components/header';
import {LocalizationContext} from '../../localization/translations';
import {AuthContext} from '../../../App';
import {moderateScale} from '../../utils/dimensions';
import EditIcon from '../../assets/icons/editIcon.svg';
import {trackEvents} from '../../services/analytics';
import {
  heightSystems,
  heightValuesImperial,
  heightValuesMetric,
  weightSystems,
  weightValuesImperial,
  weightValuesMetric,
} from '../Onboarding/weightHeightData';
import ErrorModal from '../../components/Modal/ErrorModal';
import CountriesModal from '../../components/Modal/CountriesModal';
import ModalChange from '../../components/ProfileScreen/ModalChange';
import {styles} from './editProfileStyles';

const UPDATE_USER = gql`
  mutation updateUserData($input: UpdateUserDataInput!) {
    updateUserData(input: $input) {
      id
    }
  }
`;

const EditUser = ({navigation, route}) => {
  const {user, userPhoto} = route.params;
  const [errorModalState, setErrorModalState] = useState({
    isVisible: false,
    errorText: '',
  });
  const [countriesModalState, setCountriesModalState] = useState({
    isVisible: false,
  });
  const [modalChangeVisible, setModalChangeVisible] = useState(false);
  const [inputsValues, setInputValues] = useState({
    names: user.names,
    gender: user.gender,
    email: user.email,
    country: user.country,
    dateOfBirth: user.dateOfBirth,
    weight: user.weight.toString(),
    height: user.height.toString(),
    bodyType: user.bodyType,
    bodyFat: user.bodyFat,
    goal: user.goal,
    activityLevel: user.activityLevel,
  });
  const [wheelsPickersState, setWheelsPickersState] = useState({
    selectedWeightSystem: 'kg',
    selectedHeightSystem: 'cm',
    initialSelectionsWeight: {
      weight_value: user.weight.toString(),
      weight_system: 'kg',
    },
    initialSelectionsHeight: {
      height_value: user.height.toString(),
      height_system: 'cm',
    },
  });

  const {translations, appLanguage} = useContext(LocalizationContext);
  const {userData: userInfo = {}, updateUserData} = useContext(AuthContext);
  const [update] = useMutation(UPDATE_USER);
  const isFocused = useIsFocused();

  useFocusEffect(
    useCallback(() => {
      isFocused && trackEvents({eventName: 'Load Edit Profile'});
    }, [isFocused]),
  );

  const genders = [
    {
      id: '1',
      title: translations.profileScreen.woman,
      value: 'female',
    },
    {
      id: '2',
      title: translations.profileScreen.man,
      value: 'male',
    },
  ];

  const labels = {
    lean: translations.onBoarding.question6.labelSmall,
    medium: translations.onBoarding.question6.labelMedian,
    strong: translations.onBoarding.question6.labelLarge,
  };

  const handleGoBack = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const onChangeInput = useCallback(
    (key, value) => {
      setInputValues({
        ...inputsValues,
        [key]: value,
      });
    },
    [inputsValues],
  );

  const onConfirmWeight = selections => {
    setInputValues({
      ...inputsValues,
      weight: `${selections.weight_value}`,
    });
    setWheelsPickersState({
      ...wheelsPickersState,
      initialSelectionsWeight: {
        ...wheelsPickersState.initialSelectionsWeight,
        weight_system: selections.weight_system,
        weight_value: selections.weight_value,
      },
    });
  };

  const onConfirmHeight = selections => {
    setInputValues({
      ...inputsValues,
      height: `${selections.height_value}`,
    });
    setWheelsPickersState({
      ...wheelsPickersState,
      initialSelectionsHeight: {
        ...wheelsPickersState.initialSelectionsHeight,
        height_system: selections.height_system,
        height_value: selections.height_value,
      },
    });
  };

  const handleDismissErrorModal = useCallback(
    () => setErrorModalState({...errorModalState, isVisible: false}),
    [errorModalState],
  );

  const handleVisibleCountryModal = useCallback(
    () =>
      setCountriesModalState({
        ...countriesModalState,
        isVisible: !countriesModalState.isVisible,
      }),
    [countriesModalState],
  );

  const handleWeight = useCallback(() => {
    if (wheelsPickersState.selectedWeightSystem === 'lbs') {
      setInputValues({
        ...inputsValues,
        weight: Math.round(inputsValues.weight / 2.205),
      });
    }
  }, [inputsValues, wheelsPickersState.selectedWeightSystem]);

  const onSave = useCallback(async () => {
    try {
      // handleWeight();
      if (inputsValues.email === '' || inputsValues.names === '') {
        throw new Error('Enter your data');
      }
      const input = {
        names: inputsValues.names,
        gender: inputsValues.gender,
        email: inputsValues.email,
        country: inputsValues.country,
        dateOfBirth: inputsValues.dateOfBirth,
        weight: inputsValues.weight,
        height: inputsValues.height,
        bodyType: inputsValues.bodyType,
        bodyFat: inputsValues.bodyFat.toString(),
        goal: inputsValues.goal,
        activityLevel: inputsValues.activityLevel,
      };
      console.log('INPUT:', input);
      const res = await update({
        variables: {
          input: {
            userData: {
              ...input,
            },
          },
        },
        context: {
          headers: {
            authorization: userInfo.token,
          },
        },
        refetchQueries: ['user'],
      });
      console.log('RESULT', {res});
      setModalChangeVisible(false);
      if (res?.data?.updateUserData) {
        updateUserData({gender: inputsValues.gender});
        console.log('Success');
        handleGoBack();
      }
    } catch (error) {
      setModalChangeVisible(false);
      setErrorModalState({isVisible: true, errorText: error.message});
      console.log(error);
    }
  }, [
    handleGoBack,
    inputsValues.activityLevel,
    inputsValues.bodyFat,
    inputsValues.bodyType,
    inputsValues.country,
    inputsValues.dateOfBirth,
    inputsValues.email,
    inputsValues.gender,
    inputsValues.goal,
    inputsValues.height,
    inputsValues.names,
    inputsValues.weight,
    update,
    updateUserData,
    userInfo.token,
  ]);

  const weightSegmentedPicker = React.createRef();
  const heightSegmentedPicker = React.createRef();

  const handleValueChangeWheel = useCallback(
    (column, value) => {
      switch (column) {
        case 'weight_system':
          {
            setWheelsPickersState({
              ...wheelsPickersState,
              selectedWeightSystem: value,
            });
          }
          break;
        case 'height_system':
          {
            setWheelsPickersState({
              ...wheelsPickersState,
              selectedHeightSystem: value,
            });
          }
          break;
        default:
          return;
      }
    },
    [wheelsPickersState],
  );

  return (
    <SafeAreaView style={styles.root}>
      <ModalChange
        isVisible={modalChangeVisible}
        actionOk={onSave}
        actionCancel={() => setModalChangeVisible(false)}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps={'always'}>
        <ErrorModal
          title={'Error'}
          isModalVisible={errorModalState.isVisible}
          description={errorModalState.errorText}
          onDismiss={handleDismissErrorModal}
        />
        <CountriesModal
          isModalVisible={countriesModalState.isVisible}
          onDismiss={handleVisibleCountryModal}
          onSelect={onChangeInput}
        />
        <Header
          title={translations.profileScreen.profile}
          onPressBackButton={handleGoBack}
        />
        <View style={styles.headerLine} />
        <View style={styles.container}>
          <View style={styles.headerRoot}>
            <View style={styles.mockCircleImage}>
              <ImageBackground
                source={
                  typeof userPhoto === 'number' ? userPhoto : {uri: userPhoto}
                }
                imageStyle={styles.mockCircleImage}
                style={styles.mockCircleImage}
              />
            </View>
            <View style={styles.inputRoot}>
              <Text style={styles.textInputLabel}>
                {translations.profileScreen.name}
              </Text>
              <TextInput
                value={inputsValues.names}
                style={styles.input}
                onChangeText={e => onChangeInput('names', e)}
              />
            </View>
          </View>
          <View style={styles.gendersViewRoot}>
            {genders.map(gender => (
              <TouchableOpacity
                key={gender.id}
                style={[
                  styles.genderButton,
                  inputsValues.gender === gender.value &&
                    styles.genderButtonSelected,
                ]}
                onPress={() => onChangeInput('gender', gender.value)}>
                <Text
                  style={[
                    styles.genderButtonText,
                    inputsValues.gender === gender.value &&
                      styles.genderButtonTextSelected,
                  ]}>
                  {gender.title}
                </Text>
              </TouchableOpacity>
            ))}
          </View>
          <View style={styles.formsViewRoot}>
            <View style={styles.inputRootForm}>
              <Text style={styles.textInputLabel}>
                {translations.profileScreen.email}
              </Text>
              <TextInput
                value={inputsValues.email}
                onChangeText={e => onChangeInput('email', e)}
                style={[styles.input, styles.inputForm]}
              />
            </View>
            {/* <View style={styles.inputRootForm}>
              <Text style={styles.textInputLabel}>
                {translations.profileScreen.country}
              </Text>
              <TouchableOpacity onPress={handleVisibleCountryModal}>
                <Text style={[styles.genericBlackText, styles.nonInputRow]}>
                  {inputsValues.country
                    ? inputsValues.country
                    : 'Select country'}
                </Text>
              </TouchableOpacity>
              <View style={styles.lineRow} />
            </View> */}
            <View style={styles.inputRootForm}>
              <Text style={styles.textInputLabel}>
                {translations.profileScreen.birthdate}
              </Text>
              <DatePicker
                date={new Date(inputsValues.dateOfBirth)}
                onDateChange={(e, date) => {
                  onChangeInput('dateOfBirth', date);
                }}
                androidMode="spinner"
                format={'DD MMM. YYYY'}
                locale={appLanguage}
                //ref={datePickerRef}
                confirmBtnText="OK"
                cancelBtnText="Cancel"
                maxDate={new Date()}
                showIcon={false}
                customStyles={{
                  dateInput: styles.dateWrapper,
                  dateText: [styles.genericBlackText, styles.nonInputRow],
                  datePicker: {justifyContent: 'center'},
                }}
              />
              <View style={styles.lineRow} />
            </View>
            <View style={styles.inputRootForm}>
              <Text style={styles.textInputLabel}>
                {translations.profileScreen.weight}
              </Text>
              <SegmentedPicker
                defaultSelections={wheelsPickersState.initialSelectionsWeight}
                ref={weightSegmentedPicker}
                onValueChange={({column, value}) =>
                  handleValueChangeWheel(column, value)
                }
                confirmTextColor={ORANGE}
                toolbarBackgroundColor={WHITE}
                toolbarBorderColor={GRAY_BORDER}
                onConfirm={onConfirmWeight}
                options={[
                  wheelsPickersState.selectedWeightSystem === 'kg'
                    ? weightValuesMetric
                    : weightValuesImperial,
                  weightSystems,
                ]}
                size={0.5}
              />
              <TouchableOpacity
                onPress={() => weightSegmentedPicker.current.show()}>
                <Text style={[styles.genericBlackText, styles.nonInputRow]}>
                  {inputsValues.weight
                    ? `${inputsValues.weight} ${
                        wheelsPickersState.selectedWeightSystem
                          ? wheelsPickersState.selectedWeightSystem
                          : 'kg'
                      }`
                    : 'Select weight'}
                </Text>
              </TouchableOpacity>
              <View style={styles.lineRow} />
            </View>
            <View style={styles.inputRootForm}>
              <Text style={styles.textInputLabel}>
                {translations.profileScreen.height}
              </Text>
              <SegmentedPicker
                defaultSelections={wheelsPickersState.initialSelectionsHeight}
                ref={heightSegmentedPicker}
                onValueChange={({column, value}) =>
                  handleValueChangeWheel(column, value)
                }
                confirmTextColor={ORANGE}
                toolbarBackgroundColor={WHITE}
                toolbarBorderColor={GRAY_BORDER}
                pickerItemTextColor={BLACK}
                selectionBorderColor={GRAY_BORDER}
                onConfirm={onConfirmHeight}
                options={[
                  wheelsPickersState.selectedHeightSystem === 'cm'
                    ? heightValuesMetric
                    : heightValuesImperial,
                  heightSystems,
                ]}
                size={0.5}
              />
              <TouchableOpacity
                onPress={() => heightSegmentedPicker.current.show()}>
                <Text style={[styles.genericBlackText, styles.nonInputRow]}>
                  {inputsValues.height
                    ? `${inputsValues.height} ${
                        wheelsPickersState.selectedHeightSystem
                          ? wheelsPickersState.selectedHeightSystem
                          : 'cm'
                      }`
                    : 'Select height'}
                </Text>
              </TouchableOpacity>
              <View style={styles.lineRow} />
            </View>
          </View>

          <View style={styles.wristSectionRoot}>
            <Text style={styles.genericBlackText}>
              {translations.onBoarding.question6.title}
            </Text>
            <Text style={[styles.textInputLabel, styles.gray]}>
              {labels[inputsValues.bodyType]}
            </Text>
            <BodyTypeQuestion
              position={6}
              isFilled={true}
              responses={{
                bodyType: inputsValues.bodyType,
                answeredPositions: [],
              }}
              setResponses={value => {
                value?.bodyType && onChangeInput('bodyType', value.bodyType);
              }}
              question={{
                value: 'bodyType',
              }}
              translations={translations}
              currentValue={inputsValues.bodyType}
            />
            <Text
              style={[
                styles.genericBlackText,
                {marginTop: moderateScale(-70)},
              ]}>
              {translations.onBoarding.question5}
            </Text>
            <Text style={[styles.textInputLabel, styles.gray]}>
              {`${translations.profileScreen.bodyFat}: ${
                inputsValues.bodyFat
              }%`}
            </Text>
            <View style={styles.bodyFatContainer}>
              <BodyfatPicker
                gender={inputsValues.gender}
                setBodyFat={value => {
                  onChangeInput('bodyFat', value);
                }}
                currentValue={inputsValues.bodyFat}
                showValue={true}
              />
            </View>
            <Text style={styles.genericBlackText}>
              {translations.onBoarding.question1.title}
            </Text>
            <View style={{marginLeft: -20}}>
              <Question
                position={1}
                responses={{
                  goal: inputsValues.goal,
                  answeredPositions: [],
                }}
                setResponses={value => {
                  onChangeInput('goal', value.goal);
                }}
                question={{
                  value: 'goal',
                }}
                responseTitles={[
                  {
                    label: translations.onBoarding.question1.option1,
                    value: 'loss_weight',
                  },
                  {
                    label: translations.onBoarding.question1.option2,
                    value: 'gain_muscle',
                  },
                  {
                    label: translations.onBoarding.question1.option3,
                    value: 'antiaging',
                  },
                ]}
              />
            </View>
            <Text style={styles.genericBlackText}>
              {translations.onBoarding.question2.title}
            </Text>
            <View style={{marginLeft: -20}}>
              <Question
                position={2}
                responses={{
                  activityLevel: inputsValues.activityLevel,
                  answeredPositions: [],
                }}
                setResponses={value => {
                  onChangeInput('activityLevel', value.activityLevel);
                }}
                question={{
                  value: 'activityLevel',
                }}
                responseTitles={[
                  {
                    label: translations.onBoarding.question2.option1,
                    value: 'sedentary',
                  },
                  {
                    label: translations.onBoarding.question2.option2,
                    value: 'medium_active',
                  },
                  {
                    label: translations.onBoarding.question2.option3,
                    value: 'very_active',
                  },
                ]}
              />
            </View>
            <TouchableOpacity
              style={styles.saveButton}
              onPress={() => setModalChangeVisible(true)}>
              <Text style={styles.saveButtonText}>
                {translations.profileScreen.save}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default EditUser;
