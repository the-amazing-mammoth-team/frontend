//this should be called discard session feedback
import React, {useCallback, useContext, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  Linking,
  Platform,
} from 'react-native';
import {useMutation} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import 'react-native-get-random-values';
import {
  GRAY,
  GRAY_BORDER,
  LIGHT_GRAY,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import CheckedIcon from '../../assets/icons/checkIcon.svg';
import {AuthContext} from '../../../App';
import {trackEvents} from '../../services/analytics';

const linkingToSubscriptions = () => {
  const link =
    Platform.OS === 'ios'
      ? 'itms-apps://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/manageSubscriptions'
      : 'http://play.google.com/store/account/subscriptions';
  Linking.canOpenURL(link).then(
    supported => {
      supported && Linking.openURL(link);
    },
    err => console.log(err),
  );
};

const Feedback = ({navigation, route}) => {
  const [selectedOption, setSelectedOption] = useState(null);
  const {userData: userInfo} = useContext(AuthContext);
  const {translations} = useContext(LocalizationContext);

  const cancelSubscription = useCallback(async () => {
    if (!selectedOption) {
      return null;
    }
    trackEvents({
      eventName: 'Click Cancel Subscription Link',
      properties: {reason: selectedOption},
    });
    try {
      linkingToSubscriptions();
    } catch (error) {
      console.log('error:', error);
    }
  }, [selectedOption]);

  console.log(translations.cancelSubscription.cancellationReasons);

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <Text style={styles.titleText}>
            {translations.cancelSubscription.title}
          </Text>
          <Text style={styles.descriptionText}>
            {translations.cancelSubscription.subTitle}
          </Text>
          <View>
            {Object.entries(
              translations.cancelSubscription.cancellationReasons,
            ).map(([key, value]) => {
              return (
                <TouchableOpacity
                  onPress={() => setSelectedOption(key)}
                  style={styles.optionView}
                  key={key}>
                  <Text style={styles.optionText}>{value}</Text>
                  <View
                    style={[
                      styles.circle,
                      selectedOption === key && styles.circleChecked,
                    ]}>
                    {selectedOption === key && (
                      <CheckedIcon
                        width={calcWidth(12)}
                        height={calcHeight(8)}
                      />
                    )}
                  </View>
                </TouchableOpacity>
              );
            })}
          </View>
          <Text style={styles.cancelStyle} onPress={cancelSubscription}>
            {translations.mySubscription.cancelSubscription}
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  container: {
    marginHorizontal: moderateScale(20),
  },
  titleText: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(22),
    marginTop: moderateScale(40),
    marginBottom: moderateScale(15),
  },
  descriptionText: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    marginBottom: moderateScale(35),
  },
  optionView: {
    borderWidth: 1,
    borderColor: GRAY_BORDER,
    borderRadius: 5,
    paddingHorizontal: calcWidth(25),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: calcHeight(17),
    marginBottom: calcHeight(20),
  },
  optionText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
  },
  circle: {
    width: moderateScale(17),
    height: moderateScale(17),
    borderRadius: moderateScale(17) / 2,
    backgroundColor: LIGHT_GRAY,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleChecked: {
    backgroundColor: ORANGE,
  },
  cancelStyle: {
    alignSelf: 'center',
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
    marginBottom: moderateScale(20),
  },
});

export default Feedback;
