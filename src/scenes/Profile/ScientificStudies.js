import React, {useContext, useCallback, useState} from 'react';
import {ScrollView, View, Text, SafeAreaView, StyleSheet} from 'react-native';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import {useMutation} from '@apollo/react-hooks';
import ToggleSwitch from 'toggle-switch-react-native';
import {AuthContext} from '../../../App';
import Header from '../../components/header';
import WebViewModal from '../../components/Modal/WebViewModal';
import {trackSuperProperties} from '../../services/analytics';
import {
  GRAY_BORDER,
  GRAY,
  LIGHT_ORANGE,
  WHITE,
  MEDIUM_GRAY,
  BLACK,
} from '../../styles/colors';
import {calcFontSize, calcHeight, moderateScale} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';

const SettingBlock = ({title, options, updateUserDataFunc, index}) => {
  const toggleSwitch = useCallback(
    async (value, option) => await updateUserDataFunc(option.type, value),
    [updateUserDataFunc],
  );

  return (
    <View
      style={[
        styles.blockContainer,
        options.length === index && {borderBottomWidth: 0},
      ]}>
      {options.map(option => {
        return (
          <View key={option.title} style={styles.optionContainer}>
            <View style={styles.optionContent}>
              <Text style={styles.settingTitle}>{option.title}</Text>
              <Text style={styles.settingsDescription}>
                {option.description}
              </Text>
            </View>
            <View>
              <ToggleSwitch
                isOn={option.status}
                onColor={LIGHT_ORANGE}
                offColor={MEDIUM_GRAY}
                onToggle={isOn => toggleSwitch(isOn, option)}
                animationSpeed={100}
                thumbOnStyle={styles.thumbStyle}
                thumbOffStyle={styles.thumbStyle}
              />
            </View>
          </View>
        );
      })}
    </View>
  );
};

SettingBlock.propTypes = {
  title: PropTypes.string,
  options: PropTypes.array,
  updateUserDataFunc: PropTypes.func,
  index: PropTypes.number,
};

const UPDATE_USER = gql`
  mutation updateUserData($input: UpdateUserDataInput!) {
    updateUserData(input: $input) {
      id
      scientificDataUsage
    }
  }
`;

const ScientificStudies = ({navigation, route}) => {
  const {scientificDataUsage: initParamScientific} = route.params;

  const {userData: userInfo} = useContext(AuthContext);
  const {translations, appLanguage} = useContext(LocalizationContext);

  const [update] = useMutation(UPDATE_USER);

  const [visibleModal, setVisibleModal] = useState(false);
  const [scientificDataUsage, setScientificDataUsage] = useState(
    initParamScientific,
  );

  const handleVisibleModal = () => setVisibleModal(prev => !prev);

  const updateUserDataFunc = useCallback(
    async (title, value) => {
      try {
        const input = {
          [title]: value,
        };
        const res = await update({
          variables: {
            input: {
              userData: {
                ...input,
              },
            },
          },
          context: {
            headers: {
              authorization: userInfo.token,
            },
          },
          refetchQueries: ['user'],
        });
        console.log('RESULT', {res});
        if (res?.data?.updateUserData) {
          const newValue = res?.data?.updateUserData?.scientificDataUsage;
          setScientificDataUsage(newValue);
          trackSuperProperties({
            properties: {
              scientific_data_usage: newValue,
            },
          });
          console.log('Success');
        }
      } catch (error) {
        console.log(error);
      }
    },
    [update, userInfo],
  );

  const settingsItems = [
    {
      title: '',
      options: [
        {
          title: translations.profileScreen.participateInScientificStudies,
          description: (
            <>
              {translations.profileScreen.scientificStudiesDesc}
              <Text
                style={[styles.settingsDescription, {color: 'blue'}]}
                onPress={handleVisibleModal}>
                {' ' + translations.profileScreen.link}
              </Text>
            </>
          ),
          status: scientificDataUsage,
          type: 'scientificDataUsage',
        },
      ],
    },
  ];

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Header
          title={translations.profileScreen.scientificStudies}
          onPressBackButton={() => navigation.goBack()}
        />
        <>
          <View style={styles.headerLine} />
          <WebViewModal
            isVisible={visibleModal}
            onButtonPress={handleVisibleModal}
            link={
              appLanguage === 'es'
                ? 'https://mhunters.com/es/consentimiento-estudio-cientifico/'
                : 'https://mhunters.com/en/concent-science-study/'
            }
          />
          <View style={styles.allSettings}>
            {settingsItems.map((setting, index) => {
              return (
                <SettingBlock
                  index={index}
                  key={setting.type}
                  title={setting.title}
                  options={setting.options}
                  updateUserDataFunc={updateUserDataFunc}
                />
              );
            })}
          </View>
        </>
      </ScrollView>
    </SafeAreaView>
  );
};

export default ScientificStudies;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE,
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  allSettings: {
    padding: 20,
    paddingTop: 0,
    marginTop: moderateScale(-15),
  },
  settingTitle: {
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: 5,
    color: BLACK,
  },
  settingsDescription: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    marginBottom: 10,
  },
  blockContainer: {
    borderBottomWidth: calcHeight(1),
    borderColor: GRAY_BORDER,
    paddingBottom: 15,
    paddingTop: 25,
  },
  optionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // borderWidth: 3,
    // borderColor: 'yellow',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  optionContent: {
    flexBasis: '75%',
  },
  headerLine: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginBottom: moderateScale(15),
    marginTop: moderateScale(-15),
  },
});
