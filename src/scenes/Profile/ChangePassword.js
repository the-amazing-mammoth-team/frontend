import React, {useCallback, useContext, useState} from 'react';
import {
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  Platform,
  Keyboard,
} from 'react-native';
import {SafeAreaContext, SafeAreaView} from 'react-native-safe-area-context';
import {Input} from 'react-native-elements';
import {useMutation} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import Header from '../../components/header';
import {LocalizationContext} from '../../localization/translations';
import {
  calcFontSize,
  calcHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {
  BLACK,
  GRAY,
  GRAY_BORDER,
  LIGHT_GRAY,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {AuthContext} from '../../../App';
import EyeHide from '../../assets/icons/eyeHIde.svg';
import EyeShow from '../../assets/icons/eyeShow.svg';

const UPDATE_PASSWORD = gql`
  mutation updatePassword($input: UpdatePasswordInput!) {
    updatePassword(input: $input) {
      id
    }
  }
`;

const ChangePassword = ({navigation, route}) => {
  const {email} = route.params;

  const [formData, setFormData] = useState({
    oldPassword: '',
    newPassword: '',
    newPasswordConfirmation: '',
    errors: {},
  });
  const [isSecureTextEntry, setIsSecureTextEntry] = useState(true);

  const {translations} = useContext(LocalizationContext);
  const {signOut, userInfo} = useContext(AuthContext);

  const [update] = useMutation(UPDATE_PASSWORD);

  const handleGoBack = useCallback(() => navigation.goBack(), [navigation]);

  const handleLogOut = useCallback(async () => {
    await signOut();
  }, [signOut]);

  const onChangeText = useCallback(
    ({key, value}) => {
      const newFormData = {...formData, [key]: value};
      setFormData(newFormData);
    },
    [formData],
  );

  const handleEyeIcon = useCallback(() => {
    return (
      <TouchableOpacity
        onPress={() => setIsSecureTextEntry(prevState => !prevState)}
        hitSlop={styles.hitSlopTen}>
        {isSecureTextEntry ? <EyeHide /> : <EyeShow />}
      </TouchableOpacity>
    );
  }, [isSecureTextEntry]);

  const setError = useCallback(
    errorText => {
      setFormData({
        ...formData,
        errors: {
          passwordDoesntMatch: errorText,
        },
      });
    },
    [formData],
  );

  const {errors} = formData;

  const onSave = async () => {
    if (formData.newPassword !== formData.newPasswordConfirmation) {
      setError(translations.recoverPasswordScreen.passwordDoesntMatch);
      return;
    }
    if (formData.newPassword < 6 || formData.oldPassword < 6) {
      setError(translations.recoverPasswordScreen.passwordAtLeast6Characters);
      return;
    }
    try {
      const res = await update({
        variables: {
          input: {
            email,
            password: formData.newPassword,
            oldPassword: formData.oldPassword,
          },
        },
        context: {
          headers: {
            authorization: userInfo?.token,
          },
        },
        refetchQueries: ['user'],
      });
      console.log('RESULT', {res});
      if (res?.data?.updatePassword) {
        handleGoBack();
        console.log('Success');
      }
    } catch (error) {
      console.log(error.message);
      error.message.includes(
        'GraphQL error: User wrong password',
        setError(
          translations.recoverPasswordScreen.userWrongPassword.toString(),
        ),
      );
    }
    setError('');
  };

  const keyboardVerticalOffset = Platform.OS === 'ios' ? -75 : 0;

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <KeyboardAvoidingView
          style={{flex: 1}}
          keyboardVerticalOffset={keyboardVerticalOffset}
          behavior={'padding'}>
          <Header
            onPressBackButton={handleGoBack}
            title={translations.profileScreen.password}
          />
          <View style={styles.headerLine} />
          <View style={styles.content}>
            <View style={styles.headerDescriptionView}>
              <Text style={styles.genericBlack}>
                {translations.profileScreen.headerChangePasswordFirstPart}
                <Text onPress={handleLogOut} style={styles.orangeUnderlineText}>
                  {translations.profileScreen.logOut}
                </Text>
                {translations.profileScreen.headerChangePasswordSecondPart}
              </Text>
            </View>
            <View>
              <Text style={styles.grayText}>
                {translations.profileScreen.changePassword}
              </Text>
            </View>
          </View>

          <View style={styles.smallPadding}>
            <View style={styles.inputsView}>
              <View style={styles.inputs}>
                <Input
                  errorStyle={styles.errors}
                  errorMessage={errors.email}
                  onChangeText={text =>
                    onChangeText({key: 'oldPassword', value: text})
                  }
                  value={formData.oldPassword}
                  label={translations.profileScreen.currentPassword}
                  labelStyle={styles.inputLabelStyle}
                  inputContainerStyle={styles.inputLabelContainerstyle}
                  rightIcon={handleEyeIcon}
                  secureTextEntry={isSecureTextEntry}
                  autoCapitalize={'none'}
                />
              </View>
              <View style={[styles.inputs, {marginTop: moderateScale(30)}]}>
                <Input
                  secureTextEntry
                  errorStyle={styles.errors}
                  errorMessage={errors.email}
                  onChangeText={text =>
                    onChangeText({key: 'newPassword', value: text})
                  }
                  value={formData.newPassword}
                  label={translations.profileScreen.enterYourNewPassword}
                  labelStyle={styles.inputLabelStyle}
                  inputContainerStyle={styles.inputLabelContainerstyle}
                  autoCapitalize={'none'}
                />
              </View>
              <View style={styles.inputs}>
                <Input
                  secureTextEntry
                  errorStyle={styles.errors}
                  errorMessage={errors.email}
                  onChangeText={text =>
                    onChangeText({key: 'newPasswordConfirmation', value: text})
                  }
                  value={formData.newPasswordConfirmation}
                  label={translations.profileScreen.confirmYourNewPassword}
                  labelStyle={styles.inputLabelStyle}
                  inputContainerStyle={styles.inputLabelContainerstyle}
                  autoCapitalize={'none'}
                />
              </View>
              <View style={styles.serverError}>
                <Text style={styles.errors}>
                  {formData?.errors?.mutation ||
                    formData.errors.passwordDoesntMatch}
                </Text>
              </View>
            </View>
          </View>

          <TouchableOpacity onPress={onSave} style={styles.footerButton}>
            <Text style={styles.footerButtonText}>
              {translations.profileScreen.save}
            </Text>
          </TouchableOpacity>
        </KeyboardAvoidingView>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  headerLine: {
    height: moderateScale(1),
    backgroundColor: GRAY_BORDER,
    width: '100%',
    marginTop: -20,
    marginBottom: moderateScale(20),
  },
  root: {
    backgroundColor: WHITE,
    flex: 1,
  },
  genericBlack: {
    color: BLACK,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  grayText: {
    color: GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  content: {
    paddingHorizontal: moderateScale(20),
  },
  smallPadding: {
    paddingHorizontal: moderateScale(10),
  },
  orangeUnderlineText: {
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  headerDescriptionView: {
    marginBottom: moderateScale(30),
  },
  inputs: {
    height: 30,
    marginBottom: 70,
    marginTop: 10,
  },
  inputLabelStyle: {
    color: '#BFBFBF',
    fontSize: calcFontSize(14),
    fontWeight: 'normal',
    fontFamily: SUB_TITLE_FONT,
  },
  inputLabelContainerstyle: {
    borderColor: GRAY_BORDER,
  },
  inputsView: {
    marginTop: calcHeight(20),
  },
  footerButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: moderateScale(25),
    backgroundColor: ORANGE,
    paddingVertical: moderateScale(13),
    alignSelf: 'center',
    width: deviceWidth * 0.8,
    marginTop: moderateScale(50),
    marginBottom: moderateScale(10),
  },
  footerButtonText: {
    color: WHITE,
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  errors: {
    color: LIGHT_ORANGE,
  },
  hitSlopTen: {
    top: moderateScale(10),
    bottom: moderateScale(10),
    right: moderateScale(10),
    left: moderateScale(10),
  },
});

export default ChangePassword;
