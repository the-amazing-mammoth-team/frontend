import React, {useCallback, useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View, FlatList} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useMutation, useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY_BORDER,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import Header from '../../components/header';
import Equipment from '../../components/ProgramsScreens/Equipment';
import {AuthContext} from '../../../App';
import {calcFontSize, deviceWidth, moderateScale} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';

const GET_USER = gql`
  query user($id: ID!) {
    user(id: $id) {
      id
      implements {
        id
      }
    }
  }
`;

const GET_IMPLEMENTS = gql`
  query implements($locale: String!) {
    implements(locale: $locale) {
      id
      name
      imageUrl
    }
  }
`;

const UPDATE_USER = gql`
  mutation updateUserData($input: UpdateUserDataInput!) {
    updateUserData(input: $input) {
      id
    }
  }
`;

const EquipmentSettings = ({navigation, route}) => {
  const {translations, appLanguage} = useContext(LocalizationContext);
  const {userData: userInfo} = useContext(AuthContext);

  const {data: userData, error} = useQuery(GET_USER, {
    variables: {id: userInfo.id},
  });

  const {data} = useQuery(GET_IMPLEMENTS, {
    variables: {locale: appLanguage},
  });

  const user = userData?.user;
  const equipments = data?.implements;

  const [update] = useMutation(UPDATE_USER);

  const [selectedEquipmentIds, setSelectedEquipmentIds] = useState(new Set());

  useEffect(() => {
    setSelectedEquipmentIds(
      new Set(
        user?.implements?.reduce((acc, item) => {
          acc.push(item.id);
          return acc;
        }, []) || [],
      ),
    );
  }, [user]);

  const handleGoBack = useCallback(() => navigation.goBack(), [navigation]);
  const handeChange = value => {
    const newArray = new Set(selectedEquipmentIds);
    selectedEquipmentIds.has(value)
      ? newArray.delete(value)
      : newArray.add(value);
    setSelectedEquipmentIds(newArray);
  };

  const onSave = useCallback(async () => {
    try {
      const input = {
        userData: {id: parseInt(userInfo.id)},
        implementIds: [...selectedEquipmentIds],
      };
      const res = await update({
        variables: {
          input: {
            ...input,
          },
        },
        context: {
          headers: {
            authorization: userInfo.token,
          },
        },
        refetchQueries: ['user'],
        awaitRefetchQueries: true,
      });
      console.log('RESULT', {res});
      if (res?.data?.updateUserData) {
        console.log('Success');
        handleGoBack();
      }
    } catch (error) {
      console.log(error);
    }
  }, [handleGoBack, selectedEquipmentIds, update, userInfo.id, userInfo.token]);

  console.log(user);

  const renderEquipment = item => {
    const isActive = selectedEquipmentIds.has(item.id);
    return (
      <Equipment
        item={{...item, isActive}}
        onPress={() => handeChange(item.id)}
      />
    );
  };

  return (
    <SafeAreaView style={styles.root}>
      <Header
        title={translations.profileScreen.equipment}
        onPressBackButton={handleGoBack}
      />
      <View style={styles.headerLine} />
      <FlatList
        data={equipments}
        renderItem={({item}) => {
          return renderEquipment(item);
        }}
        keyExtractor={item => item.id.toString()}
        numColumns={2}
        style={{flex: 1}}
        contentContainerStyle={{
          alignItems: 'center',
          paddingBottom: moderateScale(80),
        }}
      />
      <View style={styles.footerView}>
        <TouchableOpacity style={styles.footerButton} onPress={onSave}>
          <Text style={[styles.title, styles.footerButtonText]}>
            {translations.profileScreen.save}
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default EquipmentSettings;

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    width: deviceWidth,
    alignSelf: 'center',
    backgroundColor: GRAY_BORDER,
    marginTop: -20,
    marginBottom: moderateScale(20),
  },
  title: {
    fontSize: calcFontSize(18),
    color: BLACK,
    textAlign: 'center',
    fontFamily: MAIN_TITLE_FONT,
  },
  footerButton: {
    width: deviceWidth * 0.8,
    paddingVertical: moderateScale(12),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: moderateScale(25),
    backgroundColor: ORANGE,
  },
  footerButtonText: {
    color: WHITE,
  },
  footerView: {
    position: 'absolute',
    bottom: moderateScale(50),
    right: 0,
    left: 0,
  },
});
