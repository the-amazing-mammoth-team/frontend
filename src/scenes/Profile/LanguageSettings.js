import React, {useCallback, useContext, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useMutation, useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY_BORDER,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import Header from '../../components/header';
import {AuthContext} from '../../../App';
import {calcFontSize, deviceWidth, moderateScale} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';

const GET_USER = gql`
  query user($id: ID!) {
    user(id: $id) {
      id
      language
    }
  }
`;

const UPDATE_USER = gql`
  mutation updateUserData($input: UpdateUserDataInput!) {
    updateUserData(input: $input) {
      id
    }
  }
`;

const LanguageSettings = ({navigation, route}) => {
  const {translations, appLanguage, setAppLanguage} = useContext(
    LocalizationContext,
  );
  const {userData: userInfo} = useContext(AuthContext);

  const {data, loading} = useQuery(GET_USER, {
    variables: {id: userInfo.id},
  });

  const user = data?.user;

  console.log(user?.language);

  const [update] = useMutation(UPDATE_USER);

  const [language, setLanguage] = useState(appLanguage);

  const handleGoBack = useCallback(() => navigation.goBack(), [navigation]);

  const handleChangeLanguage = value => {
    setLanguage(value);
  };

  const onSave = useCallback(async () => {
    try {
      const input = {
        language: language,
      };
      const res = await update({
        variables: {
          input: {
            userData: {
              ...input,
            },
          },
        },
        context: {
          headers: {
            authorization: userInfo.token,
          },
        },
        refetchQueries: ['user'],
      });
      console.log('RESULT', {res});
      if (res?.data?.updateUserData) {
        console.log('Success');
        setAppLanguage(language);
        handleGoBack();
      }
    } catch (error) {
      console.log(error);
    }
  }, [handleGoBack, language, setAppLanguage, update, userInfo.token]);

  const languages = [
    {
      id: '1',
      title: translations.languageSettingsScreen.english,
      value: 'en',
    },
    {
      id: '2',
      title: translations.languageSettingsScreen.spanish,
      value: 'es',
    },
  ];

  return (
    <SafeAreaView style={styles.root}>
      <Header
        title={translations.languageSettingsScreen.header}
        onPressBackButton={handleGoBack}
      />
      <View style={styles.headerLine} />
      <View style={styles.descriptionView}>
        <Text style={styles.genericBlack}>
          {translations.languageSettingsScreen.selectLanguage}
        </Text>
      </View>
      <View style={styles.languagesContainer}>
        {languages.map(item => (
          <TouchableOpacity
            key={item.id}
            style={[
              styles.languageButton,
              language === item.value && styles.languageButtonSelected,
            ]}
            onPress={() => handleChangeLanguage(item.value)}>
            <Text
              style={[
                styles.languageButtonText,
                language === item.value && styles.languageButtonTextSelected,
              ]}>
              {item.title}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
      <View style={styles.footerView}>
        <TouchableOpacity style={styles.footerButton} onPress={onSave}>
          <Text style={[styles.title, styles.footerButtonText]}>
            {translations.profileScreen.save}
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    width: deviceWidth,
    alignSelf: 'center',
    backgroundColor: GRAY_BORDER,
    marginTop: -20,
    marginBottom: moderateScale(20),
  },
  title: {
    fontSize: calcFontSize(18),
    color: BLACK,
    textAlign: 'center',
    fontFamily: MAIN_TITLE_FONT,
  },
  descriptionView: {
    alignSelf: 'flex-start',
    marginLeft: moderateScale(20),
  },
  genericBlack: {
    fontSize: calcFontSize(14),
    color: BLACK,
    textAlign: 'center',
    fontFamily: SUB_TITLE_FONT,
  },
  footerButton: {
    width: deviceWidth * 0.8,
    paddingVertical: moderateScale(12),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: moderateScale(25),
    backgroundColor: ORANGE,
  },
  footerButtonText: {
    color: WHITE,
  },
  footerView: {
    position: 'absolute',
    bottom: moderateScale(50),
    right: 0,
    left: 0,
  },
  languagesContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: moderateScale(50),
  },
  languageButton: {
    backgroundColor: WHITE,
    width: deviceWidth * 0.42,
    paddingVertical: moderateScale(10),
    marginHorizontal: moderateScale(10),
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: moderateScale(1),
    borderColor: GRAY_BORDER,
    borderRadius: moderateScale(25),
  },
  languageButtonSelected: {
    backgroundColor: ORANGE,
  },
  languageButtonText: {
    color: BUTTON_TITLE,
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_FONT,
  },
  languageButtonTextSelected: {
    color: WHITE,
  },
});

export default LanguageSettings;
