import {StyleSheet} from 'react-native';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY,
  GRAY_BORDER,
  LIGHT_GRAY,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';

export const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    width: deviceWidth,
    alignSelf: 'center',
    backgroundColor: GRAY_BORDER,
    marginTop: -20,
    marginBottom: calcHeight(30),
  },
  container: {
    paddingHorizontal: moderateScale(20),
  },
  headerRoot: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
  },
  mockCircleImage: {
    width: moderateScale(100),
    height: moderateScale(100),
    borderRadius: moderateScale(100) / 2,
    backgroundColor: GRAY_BORDER,
  },
  hitSlopFifteen: {
    top: calcHeight(15),
    bottom: calcHeight(15),
    right: calcWidth(15),
    left: calcWidth(15),
  },
  inputRoot: {
    marginBottom: moderateScale(11),
  },
  inputRootForm: {
    marginBottom: moderateScale(22),
  },
  textInputLabel: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: '#BFBFBF',
  },
  gray: {
    color: GRAY,
    marginTop: moderateScale(5),
  },
  genericBlackText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: BLACK,
  },
  nullOpacity: {
    opacity: 0,
  },
  input: {
    width: deviceWidth * 0.55,
    borderBottomWidth: moderateScale(1),
    borderBottomColor: GRAY_BORDER,
    paddingVertical: 0,
    fontFamily: SUB_TITLE_FONT,
    color: BLACK,
    fontSize: calcFontSize(14),
    marginTop: moderateScale(3),
  },
  inputForm: {
    width: '100%',
  },
  gendersViewRoot: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: moderateScale(35),
    marginBottom: moderateScale(25),
  },
  genderButton: {
    backgroundColor: WHITE,
    width: deviceWidth * 0.42,
    paddingVertical: moderateScale(10),
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: moderateScale(1),
    borderColor: GRAY_BORDER,
    borderRadius: moderateScale(25),
  },
  genderButtonSelected: {
    backgroundColor: ORANGE,
  },
  genderButtonText: {
    color: BUTTON_TITLE,
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_FONT,
  },
  genderButtonTextSelected: {
    color: WHITE,
  },
  formsViewRoot: {
    marginBottom: moderateScale(20),
  },
  wristSectionRoot: {},
  svgContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: moderateScale(20),
  },
  buttonsRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: moderateScale(10),
    marginBottom: moderateScale(50),
  },
  buttonTitleStyle: {
    color: BUTTON_TITLE,
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_FONT,
  },
  selectedButtonTitleStyle: {
    color: WHITE,
  },
  buttonStyle: {
    backgroundColor: WHITE,
    borderWidth: moderateScale(1),
    borderColor: GRAY_BORDER,
    borderRadius: moderateScale(50),
    paddingHorizontal: moderateScale(30),
    paddingVertical: moderateScale(10),
    marginTop: moderateScale(10),
  },
  buttonContainer: {
    marginRight: moderateScale(15),
    marginBottom: moderateScale(5),
  },
  selectedButtonColor: {
    backgroundColor: ORANGE,
    borderColor: WHITE,
  },
  saveButton: {
    backgroundColor: ORANGE,
    borderRadius: moderateScale(25),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: moderateScale(13),
    width: '100%',
    marginVertical: moderateScale(30),
  },
  saveButtonText: {
    color: WHITE,
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  lineRow: {
    backgroundColor: GRAY_BORDER,
    height: moderateScale(1),
    width: '100%',
    marginTop: moderateScale(3),
  },
  nonInputRow: {
    marginTop: moderateScale(8),
  },
  absoluteEditPhotoIcon: {
    position: 'absolute',
    top: '45%',
    left: '45%',
  },
  dateWrapper: {
    flex: 1,
    borderWidth: 0,
    alignItems: 'flex-start',
    marginTop: moderateScale(10),
  },
  bodyFatContainer: {
    marginLeft: moderateScale(20),
    flex: 1,
  },
});
