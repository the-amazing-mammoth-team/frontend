import React, {useCallback, useContext, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useMutation, useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {BLACK, GRAY_BORDER, ORANGE, WHITE} from '../../styles/colors';
import Header from '../../components/header';
import CustomSlider from '../../components/CustomSlider';
import {AuthContext} from '../../../App';
import {
  calcFontSize,
  calcHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';

const GET_USER = gql`
  query user($id: ID!) {
    user(id: $id) {
      id
      trainingDaysSetting
    }
  }
`;

const UPDATE_USER = gql`
  mutation updateUserData($input: UpdateUserDataInput!) {
    updateUserData(input: $input) {
      id
    }
  }
`;

const LanguageSettings = ({navigation, route}) => {
  const {translations} = useContext(LocalizationContext);
  const {userData: userInfo} = useContext(AuthContext);

  const {data, loading} = useQuery(GET_USER, {
    variables: {id: userInfo.id},
  });

  const user = data?.user;

  const [update] = useMutation(UPDATE_USER);

  const [sliderValue, setSliderValue] = useState(user?.trainingDaysSetting);

  const handleGoBack = useCallback(() => navigation.goBack(), [navigation]);

  const handleSliderValue = useCallback(e => setSliderValue(e), []);

  const onSave = useCallback(async () => {
    try {
      const input = {
        trainingDaysSetting: sliderValue,
      };
      const res = await update({
        variables: {
          input: {
            userData: {
              ...input,
            },
          },
        },
        context: {
          headers: {
            authorization: userInfo.token,
          },
        },
        refetchQueries: ['user'],
      });
      console.log('RESULT', {res});
      if (res?.data?.updateUserData) {
        console.log('Success');
        handleGoBack();
      }
    } catch (error) {
      console.log(error);
    }
  }, [handleGoBack, sliderValue, update, userInfo.token]);

  return (
    <SafeAreaView style={styles.root}>
      <Header
        title={translations.trainingDaysSettings.header}
        onPressBackButton={handleGoBack}
      />
      <View style={styles.headerLine} />
      <View style={styles.descriptionView}>
        <Text style={styles.genericBlack}>
          {translations.trainingDaysSettings.title}
        </Text>
      </View>
      <View style={styles.sliderContainer}>
        <CustomSlider
          maxValue={7}
          value={sliderValue}
          onChange={handleSliderValue}
        />
      </View>
      <View style={styles.footerView}>
        <TouchableOpacity style={styles.footerButton} onPress={onSave}>
          <Text style={[styles.title, styles.footerButtonText]}>
            {translations.profileScreen.save}
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    width: deviceWidth,
    alignSelf: 'center',
    backgroundColor: GRAY_BORDER,
    marginTop: -20,
    marginBottom: moderateScale(20),
  },
  title: {
    fontSize: calcFontSize(18),
    color: BLACK,
    textAlign: 'center',
    fontFamily: MAIN_TITLE_FONT,
  },
  descriptionView: {
    alignSelf: 'center',
    marginHorizontal: moderateScale(10),
  },
  genericBlack: {
    fontSize: calcFontSize(14),
    color: BLACK,
    textAlign: 'center',
    fontFamily: SUB_TITLE_FONT,
  },
  footerButton: {
    width: deviceWidth * 0.8,
    paddingVertical: moderateScale(12),
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: moderateScale(25),
    backgroundColor: ORANGE,
  },
  footerButtonText: {
    color: WHITE,
  },
  footerView: {
    position: 'absolute',
    bottom: moderateScale(50),
    right: 0,
    left: 0,
  },
  sliderContainer: {
    paddingHorizontal: moderateScale(40),
    marginTop: moderateScale(80),
    height: calcHeight(60),
  },
});

export default LanguageSettings;
