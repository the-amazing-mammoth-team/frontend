import React, {useContext, useCallback} from 'react';
import {ScrollView, View, Text, SafeAreaView, StyleSheet} from 'react-native';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';
import {useMutation, useQuery} from '@apollo/react-hooks';
import ToggleSwitch from 'toggle-switch-react-native';
import {AuthContext} from '../../../App';
import Header from '../../components/header';
import {Loading} from '../../components/Loading';
import {
  GRAY_BORDER,
  GRAY,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
  MEDIUM_GRAY,
  BLACK,
} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
  deviceWidth,
} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
// const MIXPANEL_TOKEN = 'bf47184b8fc05bef8c88adb01de19387';

const SettingBlock = ({title, options, updateUserDataFunc, index}) => {
  const toggleSwitch = useCallback(
    async (value, option) => await updateUserDataFunc(option.type, value),
    [updateUserDataFunc],
  );

  return (
    <View
      style={[
        styles.blockContainer,
        options.length === index && {borderBottomWidth: 0},
      ]}>
      <Text style={styles.blockTitle}>{title}</Text>
      {options.map(option => {
        return (
          <View key={option.title} style={styles.optionContainer}>
            <View style={styles.optionContent}>
              <Text style={styles.settingTitle}>{option.title}</Text>
              <Text style={styles.settingsDescription}>
                {option.description}
              </Text>
            </View>
            <View>
              <ToggleSwitch
                isOn={option.status}
                onColor={LIGHT_ORANGE}
                offColor={MEDIUM_GRAY}
                onToggle={isOn => toggleSwitch(isOn, option)}
                animationSpeed={100}
                thumbOnStyle={styles.thumbStyle}
                thumbOffStyle={styles.thumbStyle}
              />
            </View>
          </View>
        );
      })}
    </View>
  );
};

SettingBlock.propTypes = {
  title: PropTypes.string,
  options: PropTypes.array,
  updateUserDataFunc: PropTypes.func,
  index: PropTypes.number,
};

const GET_USER = gql`
  query user($id: ID!) {
    user(id: $id) {
      id
      newsletterSubscription
      notificationsSetting
    }
  }
`;

const UPDATE_USER = gql`
  mutation updateUserData($input: UpdateUserDataInput!) {
    updateUserData(input: $input) {
      id
    }
  }
`;

const NotificationSettings = ({navigation, route}) => {
  const {userData: userInfo} = useContext(AuthContext);
  const {translations} = useContext(LocalizationContext);

  const {data, loading} = useQuery(GET_USER, {
    variables: {id: userInfo.id},
  });

  const user = data?.user;

  const [update] = useMutation(UPDATE_USER);

  const updateUserDataFunc = useCallback(
    async (title, value) => {
      try {
        const input = {
          [title]: value,
        };
        const res = await update({
          variables: {
            input: {
              userData: {
                ...input,
              },
            },
          },
          context: {
            headers: {
              authorization: userInfo.token,
            },
          },
          refetchQueries: ['user'],
        });
        console.log('RESULT', {res});
        if (res?.data?.updateUserData) {
          console.log('Success');
        }
      } catch (error) {
        console.log(error);
      }
    },
    [update, userInfo],
  );

  const settingsItems = [
    {
      title: translations.notificationSettingsScreen.woReminders,
      options: [
        {
          title: translations.notificationSettingsScreen.pushNotifications,
          description:
            translations.notificationSettingsScreen.pushNotificationsDesc,
          status: user?.notificationsSetting,
          type: 'notificationsSetting',
        },
      ],
    },
    {
      title: translations.notificationSettingsScreen.otherReminders,
      options: [
        {
          title: translations.notificationSettingsScreen.newsletter,
          description: translations.notificationSettingsScreen.newsletterDesc,
          status: user?.newsletterSubscription,
          type: 'newsletterSubscription',
        },
      ],
    },
  ];

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Header
          title={translations.notificationSettingsScreen.header}
          onPressBackButton={() => navigation.goBack()}
        />
        {user ? (
          <>
            <View style={styles.headerLine} />
            <View style={styles.allSettings}>
              {settingsItems.map((setting, index) => {
                return (
                  <SettingBlock
                    index={index}
                    key={setting.type}
                    title={setting.title}
                    options={setting.options}
                    updateUserDataFunc={updateUserDataFunc}
                  />
                );
              })}
            </View>
          </>
        ) : (
          <Loading />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default NotificationSettings;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: WHITE,
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  blockTitle: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    marginBottom: 10,
  },
  allSettings: {
    padding: 20,
    paddingTop: 0,
    marginTop: moderateScale(-15),
  },
  settingTitle: {
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
    marginBottom: 5,
    color: BLACK,
  },
  settingsDescription: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    marginBottom: 10,
  },
  blockContainer: {
    borderBottomWidth: calcHeight(1),
    borderColor: GRAY_BORDER,
    paddingBottom: 15,
    paddingTop: 25,
  },
  optionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // borderWidth: 3,
    // borderColor: 'yellow',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  optionContent: {
    flexBasis: '75%',
  },
  headerLine: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginBottom: moderateScale(15),
    marginTop: moderateScale(-15),
  },
});
