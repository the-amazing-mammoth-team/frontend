import React, {useCallback, useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {
  BLACK,
  GRAY,
  LIGHT_ORANGE,
  MEDIUM_GRAY,
  ORANGE,
  WHITE,
  YELLOW,
} from '../../styles/colors';
import SearchAbsoluteIcon from '../../assets/icons/searchImagePostWorkout.svg';
import DocumentAbsoluteIcon from '../../assets/icons/documentImagePostWorkout.svg';
import TargetAbsoluteIcon from '../../assets/icons/targetImagePostWorkout.svg';
import ResumeFeedCard from '../../components/PostWorkout/ResumeFeedCard';
import {NextButton} from '../../components/buttons';
import {calcFontSize, moderateScale} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {ScrollView} from 'react-native-gesture-handler';

const LanguageSettings = ({navigation, route}) => {
  const {translations} = useContext(LocalizationContext);

  const handleNext = useCallback(
    () => navigation.navigate('Home', {refetch: true}),
    [navigation],
  );

  const mockInfoForPRO = [
    {
      id: 1,
      title: translations.postWorkoutScreen.summaryExercises,
      description: translations.postWorkoutScreen.summaryExercicesDescription,
      colors: [LIGHT_ORANGE, ORANGE],
      icon: SearchAbsoluteIcon,
    },
    {
      id: 2,
      title: translations.postWorkoutScreen.summaryBlock,
      description: translations.postWorkoutScreen.summaryBlockDescription,
      colors: [YELLOW, LIGHT_ORANGE],
      icon: DocumentAbsoluteIcon,
    },
    {
      id: 3,
      title: translations.postWorkoutScreen.yourTribe,
      description: translations.postWorkoutScreen.yourTribeDescription,
      colors: [MEDIUM_GRAY, '#616362'],
      icon: TargetAbsoluteIcon,
    },
  ];

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView contentContainerStyle={styles.root}>
        <View style={styles.container}>
          <Text style={styles.title}>{translations.welcomeToPro.title}</Text>
          <Text style={styles.subTitle}>
            {translations.welcomeToPro.subTitle}
          </Text>
          <View style={styles.feedsContainer}>
            {mockInfoForPRO.map(item => {
              return (
                <ResumeFeedCard
                  key={item.id}
                  title={item.title}
                  description={item.description}
                  colors={item.colors}
                  idNum={item.id}
                  icon={item.icon}
                />
              );
            })}
            <View />
          </View>
        </View>
      </ScrollView>
      <NextButton onPress={handleNext} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flexGrow: 1,
    backgroundColor: WHITE,
  },
  container: {
    paddingVertical: moderateScale(40),
    paddingHorizontal: moderateScale(20),
    alignItems: 'center',
  },
  title: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(22),
    color: BLACK,
  },
  subTitle: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
    color: GRAY,
    marginVertical: moderateScale(10),
    textAlign: 'center',
  },
  feedsContainer: {
    width: '100%',
    marginTop: moderateScale(20),
  },
});

export default LanguageSettings;
