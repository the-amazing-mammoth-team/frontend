import React, {useContext, useState, useEffect, useCallback} from 'react';
import {
  View,
  Text,
  ScrollView,
  SafeAreaView,
  Platform,
  StyleSheet,
  Linking,
} from 'react-native';
import {useLazyQuery, useMutation} from '@apollo/react-hooks';
import {useFocusEffect} from '@react-navigation/native';
import RNIap, {purchaseErrorListener, IAPErrorCode} from 'react-native-iap';
import * as Sentry from '@sentry/react-native';
// import Mixpanel from 'react-native-mixpanel';
import {LocalizationContext} from '../../localization/translations';
import Header from '../../components/header';
import ProgramOfferCard from '../../components/ProgramsScreens/ProgramOfferCard';
import gql from 'graphql-tag';
import {calcFontSize, deviceWidth, moderateScale} from '../../utils/dimensions';
import deviceStorage from '../../services/deviceStorage';
import ErrorModal from '../../components/Modal/ErrorModal';
import {paymentError, subscribeError} from '../../utils/errorMessages';
import {profileProCodes} from '../../utils/offerCodes';
import {parseIsoDatetime} from '../../utils/dateTime';
import {AuthContext} from '../../../App';
import {
  BLACK,
  GRAY,
  GRAY_BORDER,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_BOLD_FONT,
  SUB_TITLE_FONT,
} from '../../styles/fonts';
import {trackEvents} from '../../services/analytics';
import moment from 'moment';
import {Loading} from '../../components/Loading';

let myPurchaseErrorListener = () => {};

const GET_PRODUCTS = gql`
  query products($offerCodes: [String!]!, $device: String!) {
    products(offerCodes: $offerCodes, device: $device) {
      id
      storeReference
      price
      name
    }
  }
`;

const SUBSCRIBE = gql`
  mutation subscribe($input: SubscribeInput!) {
    subscribe(input: $input) {
      id
      status
      startDate
      transactionBody
      user {
        email
        names
      }
    }
  }
`;

function getOs() {
  return Platform.OS === 'ios' ? 'apple' : 'google_play';
}

const MySubscription = ({navigation, route}) => {
  const {currentSubscription, program} = route.params;

  const {translations} = useContext(LocalizationContext);
  const {userData: userInfo} = useContext(AuthContext);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalTextError, setModalTextError] = useState({code: '', message: ''});
  const [isButtonPress, setIsButtonPress] = useState(false);

  const [getProducts, {data, error: err, loading = false}] = useLazyQuery(
    GET_PRODUCTS,
    {
      variables: {
        offerCodes: profileProCodes,
        device: getOs(),
      },
      fetchPolicy: 'network-only',
    },
  );

  const [subscribe] = useMutation(SUBSCRIBE);

  const handlePurchaseErrors = useCallback(
    err => {
      if (err.code !== IAPErrorCode.E_USER_CANCELLED) {
        Sentry.captureMessage(err);
        setModalTextError({message: paymentError(err.code, translations)});
        setIsModalVisible(true);
      } else {
        setIsButtonPress(false);
      }
    },
    [translations],
  );

  const handleSubscriptionErrors = useCallback(
    err => {
      setModalTextError({message: subscribeError(err.message, translations)});
      setIsModalVisible(true);
    },
    [translations],
  );

  useFocusEffect(
    useCallback(() => {
      if (data) {
        trackEvents({
          eventName: 'Load Sales',
          properties: {
            type: 'profile_pro',
            offer: profileProCodes,
            products: data.products?.map(el => el.storeReference),
            prices: data.products?.map(el => el.price),
            payment_platform: getOs(),
            placement: 'Profile',
          },
        });
      }
    }, [data]),
  );

  useEffect(() => {
    if (!loading && !data) {
      getProducts();
    }
  }, [data, getProducts, loading, navigation]);

  const handleDismissModal = useCallback(() => {
    setIsModalVisible(oldData => !oldData);
    setIsButtonPress(false);
  }, []);

  useEffect(() => {
    if (err) {
      console.log('error mysub', err);
    }
    myPurchaseErrorListener = purchaseErrorListener(err =>
      handlePurchaseErrors(err),
    );
    return () => {
      myPurchaseErrorListener.remove();
    };
  }, [err, handlePurchaseErrors, navigation]);

  const handleGoBack = () => {
    navigation.goBack();
  };

  const cancelSubscription = useCallback(async () => {
    navigation.navigate('SubscriptionFeedback');
  }, [navigation]);

  const onSubscribe = useCallback(
    async (productId, receipt, userInfo) => {
      try {
        console.log('user info at subscribe', userInfo);
        const res = await subscribe({
          variables: {
            input: {
              programId: program.id,
              productId: productId,
              transactionBody: receipt.toString(),
            },
          },
          context: {
            headers: {
              authorization: userInfo?.token,
            },
          },
          refetchQueries: ['user'],
        });
        if (res.data) {
          console.log('Response:', res.data.subscribe);
          return true;
        }
      } catch (err) {
        Sentry.captureMessage(err);
        handleSubscriptionErrors(err);
        throw new Error(err.message);
      }
    },
    [handleSubscriptionErrors, program, subscribe],
  );

  const buySubscription = useCallback(
    async product => {
      setIsButtonPress(true);
      const requestPurchase = async sku => {
        try {
          await RNIap.initConnection();
          const products = await RNIap.getSubscriptions([sku]);
          // await deviceStorage.saveItem(
          //   '@purchase/product',
          //   JSON.stringify(products[0]),
          // );
          const res = await RNIap.requestPurchase(sku, false);
          if (res) {
            await onSubscribe(product.id, res.transactionReceipt, userInfo);
            await deviceStorage.saveItem('@purchase/isrSuccessRequest', 'true');
            setIsButtonPress(false);
            navigation.navigate('Home', {refetch: true});
          }
          setIsButtonPress(false);
        } catch (err) {}
      };
      await requestPurchase(product.storeReference);
    },
    [onSubscribe, userInfo, navigation],
  );

  const isAnualProPlan =
    data?.products &&
    currentSubscription?.product?.price === data.products[0].price
      ? true
      : false;
  const renewDate = currentSubscription?.endDate;

  const renderSubTitle = () => {
    let planName = !isAnualProPlan
      ? translations.mySubscription['3mProPlan']
      : translations.mySubscription.anualProPlan;
    const splited = translations.mySubscription.aboutProPlan.split(
      '*planName*',
    );
    return (
      <Text>
        <Text style={styles.subTitle}>{splited[0]}</Text>
        <Text style={styles.subTitleBold}>{data?.products && planName}</Text>
        <Text style={styles.subTitle}>{splited[1]}</Text>
        <Text style={styles.subTitle}>
          {!currentSubscription?.cancelled
            ? translations.mySubscription.subscriptionRenew
            : translations.mySubscription.subscriptionCanceled}
        </Text>
        <Text style={styles.subTitleBold}>
          {' ' +
            moment(parseIsoDatetime(renewDate))
              .format('DD/MM/yyyy')
              .toString()}
        </Text>
      </Text>
    );
  };

  const product = data?.products && data.products[0];

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.root}>
        <ErrorModal
          isModalVisible={isModalVisible}
          onDismiss={handleDismissModal}
          title={'Error'}
          description={modalTextError.message}
        />
        <View style={styles.headerView}>
          <Header
            title={translations.profileScreen.mySubscription}
            onPressBackButton={handleGoBack}
          />
          <View style={styles.headerLine} />
        </View>
        <View style={styles.container}>
          <View>
            {renderSubTitle()}
            {!isAnualProPlan ? (
              <>
                <Text style={styles.title}>
                  {translations.mySubscription.saveTitle}
                </Text>
                <Text style={styles.subTitle}>
                  {data?.products &&
                    translations.mySubscription.savePriceAnualProPlan}
                </Text>
                <View style={{marginBottom: moderateScale(40)}} />
                {product && (
                  <ProgramOfferCard
                    id={product.id}
                    title={product.name}
                    onPress={() => buySubscription(product)}
                    price={product.price}
                    pro={true}
                    isTrial={!isAnualProPlan}
                    trialDays={translations.settingsScreen.discount60}
                    currency={'eur'}
                    local="es"
                    translations={translations.programBuyScreen}
                    disabled={isButtonPress}
                    isAnnual={isAnualProPlan}
                  />
                )}
                <Text style={styles.grayText}>
                  {translations.mySubscription.automaticalyRenewed}
                </Text>
              </>
            ) : (
              <View style={{marginTop: moderateScale(100)}}>
                <Loading noMargin />
              </View>
            )}
          </View>
          <Text style={styles.cancelStyle} onPress={cancelSubscription}>
            {translations.mySubscription.cancelSubscription}
          </Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    width: deviceWidth,
    alignSelf: 'center',
    backgroundColor: GRAY_BORDER,
    marginTop: -10,
    marginBottom: moderateScale(20),
  },
  container: {
    flex: 1,
    marginHorizontal: moderateScale(20),
    justifyContent: 'space-between',
  },
  title: {
    fontSize: calcFontSize(25),
    fontFamily: MAIN_TITLE_FONT,
    marginVertical: moderateScale(25),
  },
  subTitle: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: BLACK,
  },
  subTitleBold: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_BOLD_FONT,
    color: BLACK,
  },
  grayText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
  },
  buttonStyle: {
    backgroundColor: ORANGE,
    fontSize: calcFontSize(18),
    fontFamily: SUB_TITLE_BOLD_FONT,
    color: WHITE,
    borderRadius: 50,
  },
  cancelStyle: {
    alignSelf: 'center',
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
    marginTop: moderateScale(40),
    marginBottom: moderateScale(20),
  },
});

export default MySubscription;
