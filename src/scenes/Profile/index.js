import React, {
  useCallback,
  useRef,
  useEffect,
  useContext,
  useState,
} from 'react';
import {
  Image,
  SafeAreaView,
  View,
  Text,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag/src';
import Mixpanel from 'react-native-mixpanel';
import {GRAY_BORDER, LIGHT_ORANGE, ORANGE, BLACK} from '../../styles/colors';
import Header from '../../components/header';
import {moderateScale} from '../../utils/dimensions';
import deviceStorage from '../../services/deviceStorage';
import {styles} from './styles';
import {LocalizationContext} from '../../localization/translations';
import ProfileInfoTable from '../../components/ProfileScreen/ProfileInfoTable';
import getDiagnosis from '../../utils/getDiagnosis';
import FollowersCard from '../../components/ProfileScreen/FollowersCard';
import ArrowRight from '../../assets/icons/arrowRight.svg';
import ProfileNoPictureMen from '../../assets/icons/profileNoPictureMen.svg';
import ProfileNoPictureWomen from '../../assets/icons/profileNoPictureWomen.svg';
import {AuthContext} from '../../../App';
import {calcHeight, calcWidth} from '../../utils/dimensions';
import {logoutAnalytics, trackEvents} from '../../services/analytics';
import EditIcon from '../../assets/icons/editIcon.svg';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';

const GET_USER = gql`
  query user($id: ID!, $locale: String) {
    user(id: $id, locale: $locale) {
      id
      names
      email
      country
      dateOfBirth
      weight
      height
      gender
      activityLevel
      bodyType
      bodyFat
      goal
      level {
        id
        name
        points
      }
      nextLevel {
        id
        points
      }
      trainingDaysSetting
      points
      isPro
      currentSession {
        id
      }
      currentSubscription {
        id
        product {
          id
          name
          price
        }
        endDate
        cancelled
      }
      scientificDataUsage
    }
  }
`;

const calcTintCircularProgress = (
  currentLevel = 0,
  currentPoints = 0,
  nextLevel = 0,
) => {
  return ((currentPoints - currentLevel) / (nextLevel - currentLevel)) * 100;
};

const Profile = ({navigation, route}) => {
  const {program} = route.params;
  const {translations, appLanguage} = useContext(LocalizationContext);
  const {userData: userInfo = {}, signOut} = useContext(AuthContext);
  const [userPhoto, setUserPhoto] = useState();
  const isFocused = useIsFocused();
  console.log('LANGUAGE IS ', appLanguage);

  const animatedCircularProgressRef = useRef();

  const {data, loading} = useQuery(GET_USER, {
    variables: {id: userInfo?.id, locale: appLanguage},
    fetchPolicy: 'network-only',
  });

  const user = data?.user;

  console.log({data, loading});

  const [diagnosisData, setDiagnosisData] = useState(null);

  const handleGoBack = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  useEffect(() => {
    (async () => {
      let tempUserPhoto = await deviceStorage.getItem('@user/photo');
      setUserPhoto(tempUserPhoto);
    })();
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      animatedCircularProgressRef.current?.reAnimate(0, 0, 0);
    });
    return unsubscribe;
  }, [navigation]);

  useFocusEffect(
    useCallback(() => {
      if (data && animatedCircularProgressRef.current) {
        animatedCircularProgressRef.current.reAnimate(
          0,
          calcTintCircularProgress(
            data?.user?.level?.points,
            data?.user?.points,
            data?.user?.nextLevel?.points,
          ),
          1500,
        );
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data, animatedCircularProgressRef.current]),
  );

  useFocusEffect(
    useCallback(() => {
      isFocused && trackEvents({eventName: 'Load Profile'});
    }, [isFocused]),
  );

  useEffect(() => {
    if (data) {
      const dataForDiagnosis = {
        activityLevel: data && data.user.activityLevel,
        bodyFat: data && data.user.bodyFat,
        bodyType: data && data.user.bodyType,
        gender: data && data.user.gender,
        goal: data && data.user.goal,
        height: data && data.user.height,
        weight: data && data.user.weight,
        dateOfBirth: data && data.user.dateOfBirth,
      };
      const {
        idealWeight,
        BMI,
        targetBMI,
        fatWeight,
        idealFatWeight,
        leanWeight,
        idealLeanWeight,
      } = getDiagnosis(dataForDiagnosis);
      setDiagnosisData({
        idealWeight,
        BMI,
        targetBMI,
        fatWeight,
        idealFatWeight,
        leanWeight,
        idealLeanWeight,
      });
    }
  }, [data]);

  const renderCircleImage = useCallback(
    (size, image) => {
      return (
        <View style={styles.imageCircle}>
          {typeof image === 'function' ? (
            <>
              {data?.user?.gender === 'male' ? (
                <ProfileNoPictureMen {...styles.imageAuthor} />
              ) : (
                <ProfileNoPictureWomen {...styles.imageAuthor} />
              )}
            </>
          ) : (
            <Image source={{uri: image}} style={styles.imageAuthor} />
          )}
        </View>
      );
    },
    [data],
  );

  const renderTip = useCallback(
    text => (
      <View style={styles.tip}>
        <Text style={styles.tipText}>{text}</Text>
      </View>
    ),
    [],
  );

  const navigateToEditUser = useCallback(
    () =>
      navigation.navigate('EditUser', {
        user: data && data.user,
        userPhoto: userPhoto || ProfileNoPictureMen,
      }),
    [data, navigation, userPhoto],
  );

  const handleSignOff = useCallback(async () => {
    trackEvents({eventName: 'Log Out'});
    logoutAnalytics();
    await signOut();
  }, [signOut]);

  const renderNavigationRow = useCallback(
    (title, value = '', onPress = () => {}) => (
      <TouchableOpacity onPress={onPress} style={styles.renderNavigationRow}>
        <Text style={styles.navigationTextGray}>{title}</Text>
        <View style={styles.centerRow}>
          <Text style={styles.levelPointsText}>{value}</Text>
          <ArrowRight width={moderateScale(14)} height={moderateScale(14)} />
        </View>
      </TouchableOpacity>
    ),
    [],
  );

  const handleNavigate = useCallback(
    (route, params) =>
      navigation.navigate(`${route}`, {
        ...params,
      }),
    [navigation],
  );

  const isReadyToRender =
    userInfo?.id && userInfo?.token && data && diagnosisData;

  const fullName = data?.user && `${data.user?.names || ''}`;

  console.log('diagnosisssss', {diagnosisData});

  const rowsWithoutData = [
    {
      id: 'dwawagf',
      title: translations.profileScreen.changePassword,
      onPress: () =>
        handleNavigate('ChangePassword', {email: data?.user?.email}),
    },
    {
      id: 'grsges',
      title: translations.profileScreen.notifications,
      onPress: () => handleNavigate('NotificationSettings'),
    },
    {
      id: 'awsegh',
      title: translations.profileScreen.language,
      onPress: () => handleNavigate('LanguageSettings'),
    },
    {
      id: 'adwa',
      title: translations.profileScreen.deleteAccount,
      onPress: () =>
        handleNavigate('DeleteScreen', {
          title: translations.profileScreen.deleteAccount,
          description: translations.profileScreen.deleteAccountDescription,
          underDescription: translations.profileScreen.deleteAccountFinal,
          buttonText: translations.profileScreen.keepMyAccaunt,
          underLineText: translations.profileScreen.delete,
          type: 'delete',
        }),
    },
    {
      id: 'aw',
      title: translations.profileScreen.restartProgram,
      onPress: () =>
        handleNavigate('DeleteScreen', {
          title: translations.profileScreen.restartProgram,
          description: translations.profileScreen.restartProgramDescription,
          buttonText: translations.profileScreen.keep,
          underLineText: translations.profileScreen.restart,
          type: 'restart',
        }),
    },
    {
      id: 'gragas',
      title: translations.profileScreen.scientificStudies,
      onPress: () =>
        handleNavigate('ScientificStudiesSettings', {
          scientificDataUsage: user?.scientificDataUsage,
        }),
    },
  ];

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView
        showsVerticalScrollIndicator={true}
        keyboardShouldPersistTaps={'always'}>
        <Header
          title={translations.profileScreen.profile}
          onPressBackButton={handleGoBack}
        />
        <View style={styles.line} />
        {isReadyToRender ? (
          <View style={styles.container}>
            <TouchableOpacity
              onPress={navigateToEditUser}
              style={styles.rowProfileInfo}>
              <View>
                <AnimatedCircularProgress
                  padding={1}
                  size={moderateScale(72 * 1.1)}
                  width={moderateScale(3)}
                  fill={0}
                  tintColor={LIGHT_ORANGE}
                  backgroundColor={GRAY_BORDER}
                  rotation={0}
                  duration={0}
                  ref={animatedCircularProgressRef}>
                  {() =>
                    renderCircleImage(
                      moderateScale(72),
                      userPhoto || ProfileNoPictureMen,
                    )
                  }
                </AnimatedCircularProgress>
                {user?.isPro && renderTip('PRO')}
              </View>
              <View style={styles.minimalInfoView}>
                <Text style={styles.profileTitle} numberOfLines={2}>
                  {fullName}
                </Text>
                <Text style={styles.levelPointsText}>
                  {`${data?.user?.level?.name || ''} | ${data?.user?.points} ${
                    translations.profileScreen.points
                  }`}
                </Text>
              </View>
              <View>
                <View style={styles.editIconView}>
                  <EditIcon
                    fill={BLACK}
                    width={calcWidth(17)}
                    height={calcHeight(17)}
                  />
                </View>
              </View>
            </TouchableOpacity>
            {/* <FollowersCard
              followersCount={2}
              youFollowedCount={2}
              onPressAdd={() => {}}
            /> */}
            <ProfileInfoTable
              weight={data.user ? data.user.weight.toFixed(1) : '...'}
              idealWeight={
                diagnosisData
                  ? Number(diagnosisData.idealWeight).toFixed(1)
                  : '...'
              }
              BMI={diagnosisData ? diagnosisData.BMI : '...'}
              idealBMI={diagnosisData ? diagnosisData.targetBMI : '...'}
              bodyFat={diagnosisData ? diagnosisData.fatWeight : '...'}
              idealBodyFat={
                diagnosisData ? diagnosisData.idealFatWeight : '...'
              }
              muscleWeight={diagnosisData ? diagnosisData.leanWeight : '...'}
              idealMuscleWeight={
                diagnosisData ? diagnosisData.idealLeanWeight : '...'
              }
              goToEditUser={navigateToEditUser}
            />
            <View style={styles.extraSectionMarginTop}>
              <Text style={styles.sectionText}>
                {translations.profileScreen.adjustmentsCaps}
              </Text>
            </View>

            {rowsWithoutData.map(item => (
              <View key={item.id}>
                {renderNavigationRow(item.title, '', item.onPress)}
              </View>
            ))}

            <View style={styles.extraSectionMarginTop}>
              <Text style={styles.sectionText}>
                {translations.profileScreen.subscriptionCaps}
              </Text>
            </View>
            {renderNavigationRow(
              translations.profileScreen.mySubscription,
              user?.isPro ? 'PRO' : 'FREE',
              () => {
                user?.isPro
                  ? navigation.navigate('MySubscription', {
                      currentSubscription: data?.user?.currentSubscription,
                      program,
                    })
                  : navigation.navigate('GetProSubscription', {program});
              },
            )}
            <View style={styles.extraSectionMarginTop}>
              <Text style={styles.sectionText}>
                {translations.profileScreen.trainingAndNutritionCaps}
              </Text>
            </View>
            {renderNavigationRow(
              translations.profileScreen.trainingDays,
              `${data?.user?.trainingDaysSetting} `,
              () => handleNavigate('TrainingDaysSettings'),
            )}
            {renderNavigationRow(
              translations.profileScreen.equipment,
              null,
              () => handleNavigate('EquipmentSettings'),
            )}

            <TouchableOpacity
              style={styles.signOffView}
              hitSlop={styles.hitSlopFifteen}
              onPress={handleSignOff}>
              <Text style={styles.signOffText}>
                {translations.profileScreen.signOff}
              </Text>
            </TouchableOpacity>
          </View>
        ) : (
          <ActivityIndicator
            style={styles.indicatorView}
            size={'large'}
            color={ORANGE}
          />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Profile;
