import React, {useCallback, useState, useContext} from 'react';
import {StyleSheet, Text, View, SafeAreaView, ScrollView} from 'react-native';
import gql from 'graphql-tag';
import {useMutation, useQuery} from '@apollo/react-hooks';
import {useFocusEffect} from '@react-navigation/native';
import {CommonActions} from '@react-navigation/native';
import {BLACK, WHITE} from '../../styles/colors';
import {calcFontSize, moderateScale} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import ProgramCardPaymentSuccess from '../../components/ProgramsScreens/ProgramCardPaymentSuccess';
import ErrorModal from '../../components/Modal/ErrorModal';
import deviceStorage from '../../services/deviceStorage';
import {AuthContext} from '../../../App';
import {LocalizationContext} from '../../localization/translations';
import {
  appendProperty,
  incrementProperty,
  Platforms,
  trackEvents,
  trackProperties,
  trackSuperProperties,
} from '../../services/analytics';

const SET_CURRENT_PROGRAM = gql`
  mutation setCurrentUserProgram($input: SetCurrentUserProgramInput!) {
    setCurrentUserProgram(input: $input)
  }
`;

const GET_USER_PROGRAM = gql`
  query user($id: ID!) {
    user(id: $id) {
      id
      userProgram {
        id
        progress
        enjoyment
        enjoymentNotes
        program {
          name
          codeName
        }
      }
    }
  }
`;

function isObjective(programCharacteristic) {
  return programCharacteristic?.objective;
}

const PaymentSuccess = ({navigation, route}) => {
  const {translations} = useContext(LocalizationContext);

  const {program, weeklyGoal} = route.params;
  const [isModalVisible, setIsModalVisible] = useState(false);
  const programPoints = [
    program?.sessions?.length +
      ' ' +
      translations.programsScreen.sessions.toLowerCase(),
    weeklyGoal + ' ' + translations.programsScreen.sessionsPerWeek,
    ...program?.programCharacteristics?.reduce((acc, item, index) => {
      if (isObjective(item)) {
        console.log(acc);
        return [...acc, item.value];
      }
      return acc;
    }, []),
  ];
  const [modalTextError, setModalTextError] = useState({code: '', message: ''});
  const {userData: userInfo} = useContext(AuthContext);

  const {data, error, loading} = useQuery(GET_USER_PROGRAM, {
    variables: {id: userInfo?.id},
  });

  const userProgram = data?.user?.userProgram;

  const [setCurrentProgram] = useMutation(SET_CURRENT_PROGRAM);

  useFocusEffect(
    useCallback(() => {
      trackEvents({eventName: 'Load Onboarding Summary'});
    }, []),
  );

  console.log({userProgram, error});

  const handleDismissModal = useCallback(
    () => setIsModalVisible(oldData => !oldData),
    [],
  );
  //add set program here too

  const onStartCurrentProgram = useCallback(async () => {
    try {
      const res = await setCurrentProgram({
        variables: {
          input: {
            programId: program.id,
          },
        },
        context: {
          headers: {
            authorization: userInfo.token,
          },
        },
        refetchQueries: ['user'],
      });
      if (res.data.setCurrentUserProgram) {
        appendProperty('programs_completed', [userProgram?.program?.codeName]);
        incrementProperty('programs_completed_count', 1);
        trackProperties({
          platforms: {1: Platforms.mixPanel},
          properties: {previous_program: userProgram?.program?.codeName},
        });
        trackEvents({
          platforms: {1: Platforms.mixPanel},
          eventName: 'Change Program',
          properties: {
            training_program: program?.codeName,
            program_enjoyment: userProgram?.enjoyment,
            program_notes: userProgram?.enjoymentNotes,
            previous_program: userProgram?.program?.codeName,
          },
        });
        console.log('Response:', res.data);
        trackSuperProperties({
          properties: {
            current_training_program: program.codeName,
          },
        });
        await deviceStorage.saveItem(
          '@programs/currentProgram',
          JSON.stringify(program),
        );
        navigation.dispatch(
          CommonActions.reset({
            index: 1,
            routes: [
              {
                name: 'HomeStack',
                params: {screen: 'Home', params: {doRefetch: true}},
              },
            ],
          }),
        );
      }
    } catch (error) {
      setModalTextError({code: error.code, message: error.message});
      setIsModalVisible(true);
      console.log(error);
    }
  }, [navigation, program, setCurrentProgram, userInfo.token, userProgram]);

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <ErrorModal
            isModalVisible={isModalVisible}
            onDismiss={handleDismissModal}
            title={modalTextError.code}
            description={modalTextError.message}
          />
          <View style={styles.titleView}>
            <Text style={styles.titleText}>
              {translations.paymentSuccess.title}
            </Text>
          </View>
          <View style={styles.descriptionView}>
            <Text style={styles.description}>
              {translations.paymentSuccess.description}
            </Text>
          </View>
          <ProgramCardPaymentSuccess
            title={program.name}
            infoRows={programPoints}
            onStartPress={onStartCurrentProgram}
            programAttributes={[program.maxAttribute, program.maxAttribute2]}
          />
          <View style={styles.extraMargin} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  container: {
    paddingHorizontal: moderateScale(20),
  },
  titleText: {
    fontSize: calcFontSize(25),
    textAlign: 'center',
    color: BLACK,
    fontFamily: MAIN_TITLE_FONT,
  },
  titleView: {
    alignSelf: 'center',
    marginTop: moderateScale(moderateScale(40)),
  },
  description: {
    fontSize: calcFontSize(16),
    textAlign: 'center',
    color: BLACK,
    fontFamily: SUB_TITLE_FONT,
  },
  descriptionView: {
    marginTop: moderateScale(10),
    marginBottom: moderateScale(50),
  },
  extraMargin: {
    marginBottom: moderateScale(20),
  },
});
export default PaymentSuccess;
