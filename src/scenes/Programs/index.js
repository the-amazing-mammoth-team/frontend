import React, {useContext, useLayoutEffect} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  ScrollView,
  ImageBackground,
  Button,
} from 'react-native';
import Mixpanel from 'react-native-mixpanel';
import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {URL} from '../../../apollo';
import styles from './styles';
import {LocalizationContext} from '../../localization/translations';

const AVAILABLE_PROGRAMS = gql`
  query availablePrograms($locale: String!) {
    availablePrograms(locale: $locale) {
      id
      description
      name
      pro
      updatedAt
      imageUrl
      programCharacteristics {
        id
        value
        goal
      }
    }
  }
`;

const MIXPANEL_TOKEN = 'bf47184b8fc05bef8c88adb01de19387';

const Programs = ({navigation}) => {
  const {data, error, loading} = useQuery(AVAILABLE_PROGRAMS, {
    variables: {locale: 'es'},
  });

  const {translations} = useContext(LocalizationContext);

  // const clearAsync = async () => {
  //   try {
  //     const r = await AsyncStorage.clear();
  //   } catch (err) {
  //     console.log(err);
  //   }
  // };

  const renderAvailablePrograms = () => {
    if (data?.availablePrograms) {
      return data.availablePrograms.map(program => {
        return (
          <TouchableWithoutFeedback
            key={program.id}
            onPress={() => navigation.navigate('ProgramDetail', {program})}>
            <ImageBackground
              source={{
                uri: `${URL}${program.image}`,
              }}
              imageStyle={styles.programImage}
              style={styles.program}>
              <Text style={styles.programDescription}>10 Semanas</Text>
              <Text style={styles.programName}>
                {program.name || 'From Fat to Fit'}
              </Text>
            </ImageBackground>
          </TouchableWithoutFeedback>
        );
      });
    }
    return null;
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <ScrollView style={styles.scrollView}>
        <View style={styles.container}>
          <View style={styles.innerContent}>
            <Text style={styles.mainTitle}>
              {translations.programsScreen.idealPlan},
            </Text>
            <Text style={styles.text}>
              {translations.programsScreen.expertResultTitle}
            </Text>
          </View>
          <ScrollView horizontal style={styles.programContainer}>
            {renderAvailablePrograms()}
          </ScrollView>
          {/* <Button title="Clear" onPress={clearAsync} /> */}
          <View>
            <Text style={styles.bigText}>
              {translations.programsScreen.joinPeople}
            </Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Programs;
