import React, {useCallback, useContext, useEffect, useState} from 'react';
import {FlatList, SafeAreaView, StyleSheet, View} from 'react-native';
import {useFocusEffect} from '@react-navigation/native';
import {GRAY_BORDER, WHITE} from '../../styles/colors';
import Header from '../../components/header';
import {calcHeight, calcWidth, moderateScale} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import deviceStorage from '../../services/deviceStorage';
import {trackEvents} from '../../services/analytics';
import CurrentProgramCard from '../../components/ProgramsScreens/CurrentProgramCard';
import ProgramCardSmall from '../../components/ProgramsScreens/ProgramCardSmall';
import {Loading} from '../../components/Loading';
import gql from 'graphql-tag';
import {useQuery} from '@apollo/react-hooks';
import {AuthContext} from '../../../App';

const AVAILABLE_PROGRAMS = gql`
  query suggestedPrograms($userId: ID!, $locale: String!) {
    suggestedPrograms(userId: $userId, locale: $locale) {
      id
      description
      name
      pro
      updatedAt
      imageUrl
      image2Url
      user {
        id
        names
      }
      products {
        id
        storeReference
      }
      maxAttribute {
        key
        value
        text
      }
      maxAttribute2 {
        key
        value
        text
      }
      strength
      endurance
      flexibility
      intensity
      technique
      sessions {
        id
      }
      programCharacteristics {
        id
        value
        objective
      }
      implements {
        id
        name
        imageUrl
      }
      codeName
    }
  }
`;

const ChooseAnotherProgram = ({navigation, route}) => {
  const {userData: userInfo} = React.useContext(AuthContext);

  const {data: availablePrograms, error, loading} = useQuery(
    AVAILABLE_PROGRAMS,
    {
      variables: {
        userId: userInfo.id,
        locale: LocalizationContext._currentValue.appLanguage,
      },
    },
  );
  const {
    suggested_program: currentProgram,
    callback,
    prevScreen,
  } = route.params;
  console.log(callback);
  console.log('navigation', navigation);
  console.log('all programs', availablePrograms);
  const handleGoBack = useCallback(() => navigation.goBack(), [navigation]);
  const {translations} = useContext(LocalizationContext);
  const [isLoaderVisible, setIsLoaderVisible] = useState(true);

  // useEffect(() => {
  //   const getItemFromLS = async () => {
  //     const program = await deviceStorage.getItem('@programs/currentProgram');
  //     const parserData = JSON.parse(program);
  //     if (parserData) {
  //       setCurrentProgram({
  //         availableProgramId: parserData.id,
  //         duration: parserData.duration ? parserData.duration : '10',
  //         title: parserData.title,
  //         background: parserData.backgroundWide,
  //         // intensityLevel: parserData.maxAttribute.value,
  //         musculatureLevel: 1,
  //         programCharacteristics: parserData.programCharacteristics,
  //       });
  //     }
  //   };
  //   getItemFromLS();
  // }, []);

  useEffect(() => {
    const timer = setTimeout(() => setIsLoaderVisible(false), 100);
    return () => {
      clearTimeout(timer);
    };
  }, []);

  useFocusEffect(
    useCallback(() => {
      trackEvents({
        eventName: 'Load All Programs',
        properties: {
          placement: prevScreen,
        },
      });
    }, [prevScreen]),
  );

  const parsingDataForProgramDescription = program => {
    return {
      id: program.id,
      duration: program.sessions ? program.sessions.length : '10',
      background: program.imageUrl,
      backgroundWide: program.image2Url,
      intensityLevel: program.maxAttribute.value,
      title: program.name,
      musculatureLevel: 1,
      programCharacteristics: program.programCharacteristics,
      maxAttribute: program.maxAttribute,
      maxAttribute2: program.maxAttribute2,
      description: program.description,
      sessions: program.sessions,
      creator: program.user?.names,
      program: program,
    };
  };

  console.log('Current program:', currentProgram);

  const renderSuggestedProgram = useCallback(() => {
    return (
      <CurrentProgramCard
        currentProgram={currentProgram}
        navigation={navigation}
        callback={
          callback
            ? program => callback(parsingDataForProgramDescription(program))
            : null
        }
        prevScreen={prevScreen}
      />
    );
  }, [callback, currentProgram, navigation, prevScreen]);

  const renderItem = useCallback(
    ({item}) => (
      <ProgramCardSmall
        item={item}
        navigation={navigation}
        callback={
          callback
            ? program => callback(parsingDataForProgramDescription(program))
            : null
        }
        prevScreen={prevScreen}
      />
    ),
    [callback, navigation, prevScreen],
  );

  const showingPrograms = (() => {
    if (currentProgram) {
      return availablePrograms?.suggestedPrograms?.filter(
        el => el.id !== currentProgram.id,
      );
    } else {
      return [];
    }
  })();

  return (
    <SafeAreaView style={styles.root}>
      <Header
        title={translations.programsScreen.allPrograms}
        onPressBackButton={handleGoBack}
      />
      <View style={styles.headerLine} />
      <View style={styles.container}>
        {availablePrograms && !isLoaderVisible ? (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={showingPrograms}
            renderItem={renderItem}
            contentContainerStyle={styles.contentContainerStyle}
            ListHeaderComponent={currentProgram && renderSuggestedProgram} //este componente muestra el sugerido
            ListHeaderComponentStyle={styles.renderCurrentProgramStyles}
            keyExtractor={item => item.id}
            horizontal={false}
            numColumns={2}
            columnWrapperStyle={styles.contentFlatlist}
          />
        ) : (
          <Loading />
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    backgroundColor: GRAY_BORDER,
    width: '100%',
    marginTop: -20,
  },
  container: {
    paddingHorizontal: calcWidth(20),
    flex: 1,
  },
  renderCurrentProgramStyles: {
    marginBottom: moderateScale(35),
  },
  contentFlatlist: {
    justifyContent: 'space-between',
  },
  contentContainerStyle: {
    marginTop: calcHeight(30),
    paddingBottom: calcHeight(30),
  },
});

export default ChooseAnotherProgram;
