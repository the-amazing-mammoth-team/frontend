import React, {useCallback, useContext, useMemo} from 'react';
import {Icon, Button} from 'react-native-elements';
import {FlatList} from 'react-native-gesture-handler';
import {
  View,
  Text,
  ScrollView,
  ImageBackground,
  SafeAreaView,
  Image,
} from 'react-native';
import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {useFocusEffect} from '@react-navigation/native';
import {AuthContext} from '../../../App';
// import Mixpanel from 'react-native-mixpanel';
import styles from './styles';
import {LocalizationContext} from '../../localization/translations';
import {Platforms, trackEvents} from '../../services/analytics';
import {onboardingSalesCodes} from '../../utils/offerCodes';
import ProIcon from '../../assets/icons/pro.svg';
import {Loading} from '../../components/Loading';
import {deviceHeight, moderateScale} from '../../utils/dimensions';
import Equipment from '../../components/ProgramsScreens/Equipment';
import {levelCircles} from '../../components/ProgramsScreens/CurrentProgramCard';
import {BLACK} from '../../styles/colors';

export const renderEquipment = (name, image) => {
  return (
    <View style={styles.equipmentView}>
      {name && image ? (
        <>
          <Image source={image} style={styles.equipmentImage} />
          <Text style={styles.equipmentName}>{name}</Text>
        </>
      ) : (
        <Text style={[styles.equipmentName, styles.noEquipment]}>
          No Equipment
        </Text>
      )}
    </View>
  );
};

const GET_USER = gql`
  query user($id: ID!) {
    user(id: $id) {
      id
      isPro
      trainingDaysSetting
      userPrograms {
        id
        program {
          id
        }
      }
      currentProgram {
        id
      }
    }
  }
`;

const CheckItem = ({text}) => {
  return (
    <View style={styles.checkContainer}>
      <View style={styles.circleCheck}>
        <Icon name="check" color="#ffffff" size={20} />
      </View>
      <Text style={styles.checkText}>{text}</Text>
    </View>
  );
};

function isObjective(programCharacteristic) {
  return programCharacteristic?.objective;
}

function isCharacteristic(programCharacteristic) {
  return !programCharacteristic?.objective;
}

const ProgramDetail = ({navigation, route}) => {
  const {userData: userInfo} = React.useContext(AuthContext);

  //const {user: userInfo} = useContext(AuthContext);

  console.log('user info', userInfo);

  const {data, loading, error} = useQuery(GET_USER, {
    variables: {id: userInfo.id},
  });

  console.log(error);

  const user = data?.user;

  const handleGoBack = () => {
    navigation.goBack();
  };

  console.log('p details start');
  const {program, prevScreen} = route.params;
  const implementsList = program.implements;

  console.log({program});
  const {translations} = useContext(LocalizationContext);
  console.log(LocalizationContext._currentValue.appLanguage);
  console.log('goals', program.programCharacteristics.filter(isObjective));
  console.log('chars', program.programCharacteristics.filter(isCharacteristic));
  const goals = program.programCharacteristics?.filter(isObjective);
  const characteristics = program.programCharacteristics?.filter(
    isCharacteristic,
  );

  useFocusEffect(
    useCallback(() => {
      if (program) {
        trackEvents({
          eventName: 'Load Program Description',
          properties: {
            training_program: program?.codeName,
            placement: prevScreen,
          },
        });
      }
    }, [prevScreen, program]),
  );

  const chooseProgram = () => {
    trackEvents({
      eventName: 'Select Program',
      platforms: {
        1: Platforms.mixPanel,
      },
      properties: {
        training_program: program?.codeName,
      },
    });
    if (
      user?.isPro ||
      user?.userPrograms?.find(item => item.program.id === program.id)
    ) {
      !user?.currentProgram
        ? //first user program, asking to choose training days settings
          navigation.navigate('PlanWeek', {
            program: route.params.program,
          })
        : navigation.navigate('PaymentSuccess', {
            program: program,
            weeklyGoal: user?.trainingDaysSetting,
          });
    } else {
      navigation.navigate('ProgramBuy', {
        program,
        prevScreen,
        offerCodes: onboardingSalesCodes,
        user,
      });
    }
  };

  const renderFocusProgram = () => {
    const keys = [
      'strength',
      'intensity',
      'flexibility',
      'endurance',
      'technique',
    ];
    return (
      <View style={styles.focusContainer}>
        {keys.map((key, index) => (
          <View
            style={[styles.focusRow, index === 4 && {borderBottomWidth: 0}]}>
            <Text style={styles.focusText}>
              {translations.programsScreen.programAttributes[key]}
            </Text>
            <Text style={[styles.focusText, {color: BLACK}]}>
              {program[key]}/5
            </Text>
          </View>
        ))}
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <ScrollView style={styles.mainScrollView}>
        {!loading ? (
          <>
            <Button
              onPress={handleGoBack}
              buttonStyle={{
                backgroundColor: 'transparent',
              }}
              containerStyle={styles.arrowBack}
              icon={<Icon name="arrow-back" />}
            />
            {Boolean.parse(program?.pro) && (
              <ProIcon
                width={styles.proIcon.width}
                height={styles.proIcon.height}
                style={styles.proIcon}
              />
            )}
            <ImageBackground
              source={{
                uri: `${program.image2Url}`,
              }}
              //style={styles.imagePlaceholder}
              style={[
                styles.containerCopy,
                {
                  width: '100%',
                  height: deviceHeight * 0.35,
                },
              ]}
            />
            {/* <View style={styles.recommendedBadgeContainer}>
              <Text style={styles.recommendedProgram}>
                {translations.programDetailScreen.recomendForYou}
              </Text>
            </View> */}
            <View
              style={[
                styles.programDetailInfo,
                {marginTop: moderateScale(20)},
              ]}>
              <Text style={styles.programLength}>
                {program.sessions.length}{' '}
                {translations.programDetailScreen.sessions}
              </Text>
              <Text style={styles.detailProgramName}>
                {program.name || translations.programDetailScreen.undefinedWord}
              </Text>
              <Text style={styles.programCreator}>
                {program.user?.names || 'Mammoth Hunters'}
              </Text>
              <View>
                <View style={styles.rowView}>
                  <Text style={[styles.durationText, styles.circlesText]}>
                    {program.maxAttribute.key}
                  </Text>
                  {levelCircles(program.maxAttribute.value)}
                </View>
                <View style={styles.rowView}>
                  <Text style={[styles.durationText, styles.circlesText]}>
                    {program.maxAttribute2.key}
                  </Text>
                  {levelCircles(program.maxAttribute2.value)}
                </View>
              </View>
              {/* {program.description && (
                <View style={styles.descriptionView}>
                  <Text style={styles.whatFindText}>{program.description}</Text>
                </View>
              )} */}
              {characteristics?.length > 0 && (
                <Text style={styles.programDetailSubtitle}>
                  {translations.programDetailScreen.findQuestion}
                </Text>
              )}
              {characteristics?.map(char => {
                return <CheckItem key={char.id} text={char.value} />;
              })}
              {goals?.length > 0 && (
                <Text style={styles.programDetailSubtitle}>
                  {translations.programDetailScreen.programGoals}
                </Text>
              )}
              {goals?.map(char => {
                return <CheckItem key={char.id} text={char.value} />;
              })}
              <Text style={styles.programDetailSubtitle}>
                {translations.workoutScreen.focus}
              </Text>
              {renderFocusProgram()}
              <Text style={styles.programDetailSubtitle}>
                {translations.profileScreen.equipment}
              </Text>
              <View style={styles.equipmentRowView}>
                <FlatList
                  data={implementsList}
                  renderItem={({item}) => (
                    <Equipment item={item} disabled={true} />
                  )}
                  horizontal
                  keyExtractor={el => el.id}
                />
              </View>
            </View>
          </>
        ) : (
          <Loading />
        )}
      </ScrollView>
      <Button
        onPress={chooseProgram}
        title={translations.programDetailScreen.chooseProgram}
        titleStyle={styles.buttonTitleStyles}
        buttonStyle={styles.button}
        accessibilityLabel="Learn more about this purple button"
      />
    </SafeAreaView>
  );
};

export default ProgramDetail;
