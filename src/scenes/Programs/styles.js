import {StyleSheet} from 'react-native';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_FONT,
  SUB_TITLE_BOLD_FONT,
  SUB_TITLE_FONT_SEMI_BOLD,
  MAIN_TITLE_FONT_REGULAR,
} from '../../styles/fonts';
import {
  WHITE,
  BLACK,
  GRAY,
  GRAY_BORDER,
  BUTTON_TITLE,
  ORANGE,
  LIGHT_ORANGE,
} from '../../styles/colors';
import {
  calcFontSize,
  deviceHeight,
  moderateScale,
  deviceWidth,
  calcWidth,
  calcHeight,
} from '../../utils/dimensions';

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: WHITE,
  },
  button: {
    bottom: 0,
    borderRadius: moderateScale(30),
    height: calcHeight(55),
    backgroundColor: ORANGE,
    width: calcWidth(300),
    marginBottom: moderateScale(20),
    alignSelf: 'center',
  },
  scrollView: {
    backgroundColor: WHITE,
  },
  container: {
    color: BLACK,
    fontSize: calcFontSize(14),
    backgroundColor: WHITE,
    //paddingHorizontal: moderateScale(20),
    marginTop: moderateScale(20),
  },
  innerContent: {},
  text: {
    color: '#9F9F9F',
    fontSize: calcFontSize(16),
    marginTop: moderateScale(15),
    paddingBottom: moderateScale(20),
    fontFamily: SUB_TITLE_FONT,
  },
  mainTitle: {
    fontSize: calcFontSize(30),
    marginTop: moderateScale(35),
    fontFamily: MAIN_TITLE_FONT,
  },
  bottomButton: {
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    marginTop: 15,
    marginBottom: 35,
    marginRight: 20,
    backgroundColor: WHITE,
  },
  nextButtonStyle: {
    borderRadius: 50,
    height: 50,
    width: 50,
    backgroundColor: GRAY,
  },
  nextButtonStyleActive: {
    borderRadius: 50,
    height: 50,
    width: 50,
    backgroundColor: ORANGE,
    borderColor: WHITE,
  },
  selectedButtonColor: {
    backgroundColor: ORANGE,
    borderColor: WHITE,
  },
  program: {
    backgroundColor: BLACK,
    height: 350,
    color: WHITE,
    padding: 10,
    borderRadius: 10,
    marginRight: 20,
    minWidth: 300,
    flex: 1,
    resizeMode: 'cover',
    // justifyContent: 'center',
  },
  programImage: {
    borderRadius: 10,
  },
  programContainer: {
    paddingLeft: 0,
    marginLeft: 0,
    // backgroundColor: BLACK,
  },
  programName: {
    color: WHITE,
    fontSize: 30,
    fontFamily: MAIN_TITLE_FONT,
  },
  programDescription: {
    fontSize: 18,
    color: WHITE,
    fontFamily: SUB_TITLE_BOLD_FONT,
  },
  bigText: {
    fontSize: 22,
    lineHeight: 32,
    marginTop: 30,
    fontFamily: MAIN_TITLE_FONT,
  },
  imagePlaceholder: {
    height: 200,
    backgroundColor: '#C4C4C4',
  },
  recommendedProgram: {
    backgroundColor: ORANGE,
    color: WHITE,
    top: -30,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 16,
    marginLeft: 20,
    fontFamily: SUB_TITLE_BOLD_FONT,
  },
  programDetailInfo: {
    paddingLeft: 15,
    paddingRight: 15,
    fontSize: 16,
  },
  programLength: {
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_FONT,
  },
  detailProgramName: {
    fontSize: calcFontSize(30),
    fontFamily: MAIN_TITLE_FONT,
  },
  programCreator: {
    fontSize: calcFontSize(16),
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    marginBottom: moderateScale(10),
  },
  programDetailSubtitle: {
    fontSize: calcFontSize(18),
    marginTop: moderateScale(25),
    fontFamily: MAIN_TITLE_FONT,
  },
  circleCheck: {
    borderRadius: 50,
    width: 20,
    height: 20,
    backgroundColor: ORANGE,
    marginRight: 15,
  },
  checkContainer: {
    marginTop: moderateScale(20),
    flexDirection: 'row',
    paddingRight: moderateScale(30),
  },
  checkText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: BLACK,
  },
  separator: {
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
    borderTopWidth: 0,
    marginTop: moderateScale(10),
    marginBottom: moderateScale(20),
  },
  label: {
    backgroundColor: LIGHT_ORANGE,
    color: '#ffffff',
    paddingVertical: moderateScale(5),
    paddingHorizontal: moderateScale(15),
    fontFamily: SUB_TITLE_BOLD_FONT,
  },
  labelWrapper: {
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 5,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
    paddingBottom: 20,
    marginBottom: 20,
  },
  headerLine: {
    height: moderateScale(1),
    backgroundColor: GRAY_BORDER,
    width: '100%',
    marginTop: -20,
    marginBottom: moderateScale(20),
  },
  goBackButton: {
    backgroundColor: WHITE,
  },
  headerText: {
    fontSize: 18,
    alignSelf: 'center',
    fontFamily: MAIN_TITLE_FONT,
  },
  programBuyContainer: {
    paddingTop: 15,
    paddingBottom: 25,
    backgroundColor: WHITE,
  },
  buyProgram: {
    borderRadius: 25,
    height: 50,
    borderColor: ORANGE,
    borderWidth: 1,
    width: 350,
    marginTop: 40,
    marginBottom: 20,
    backgroundColor: WHITE,
  },
  buyProgramText: {
    color: ORANGE,
    fontSize: 20,
  },
  price: {
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 25,
  },
  priceSubtitle: {
    color: '#9F9F9F',
    fontSize: calcFontSize(14),
    fontFamily: MAIN_TITLE_FONT,
    marginTop: moderateScale(8),
  },
  buttonContainer: {
    alignItems: 'center',
    color: WHITE,
  },
  buttonStyle: {
    borderRadius: 25,
    height: 50,
    backgroundColor: ORANGE,
    width: 350,
    marginTop: 30,
    marginBottom: 20,
  },
  buttonTitleStyles: {
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  recommendedBadgeContainer: {
    flexDirection: 'row',
  },
  mainScrollView: {
    flex: 1,
    backgroundColor: WHITE,
  },
  arrowBack: {
    position: 'absolute',
    top: moderateScale(15),
    left: moderateScale(15),
    zIndex: 999,
  },
  line: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginBottom: 20,
  },
  extraMarginLine: {
    marginTop: moderateScale(15),
  },
  generalPadding: {
    paddingHorizontal: moderateScale(20),
  },
  mockCircle: {
    width: moderateScale(106),
    height: moderateScale(106),
    borderRadius: moderateScale(106) / 2,
    backgroundColor: BLACK,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  personalizedProgramTitleView: {
    marginTop: moderateScale(16),
    marginBottom: moderateScale(8),
  },
  personalizedProgramTitle: {
    color: WHITE,
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(16),
    textAlign: 'center',
  },
  personalizedProgramDescriptionView: {
    marginBottom: moderateScale(16),
    width: '90%',
  },
  personalizedProgramDescriptionText: {
    color: WHITE,
    fontFamily: SUB_TITLE_FONT_SEMI_BOLD,
    fontSize: calcFontSize(14),
    textAlign: 'center',
  },
  containerCopy: {
    flex: 1,
  },
  child: {
    width: deviceWidth,
    justifyContent: 'center',
    alignItems: 'center',
  },
  paginationDot: {
    width: moderateScale(10),
    height: moderateScale(10),
    borderRadius: moderateScale(10) / 2,
  },
  paginationStyle: {
    bottom: moderateScale(10),
  },
  personalExperienceSectionView: {
    marginTop: moderateScale(30),
  },
  frequentlySectionView: {
    marginBottom: moderateScale(30),
    marginTop: moderateScale(40),
  },
  buttonBuy: {
    width: '100%',
    alignSelf: 'center',
  },
  headerView: {
    marginVertical: -moderateScale(15),
  },
  imageCircle: {
    width: moderateScale(106),
    height: moderateScale(106),
    borderRadius: moderateScale(106) / 2,
  },
  genericPadding: {
    paddingHorizontal: moderateScale(20),
  },
  negativeMargin: {
    marginBottom: -moderateScale(10),
  },
  automaticallyRenewTextView: {
    alignSelf: 'center',
    marginBottom: moderateScale(30),
  },
  automaticallyRenewText: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    textAlign: 'left',
  },
  descriptionView: {
    marginTop: moderateScale(10),
    marginBottom: -moderateScale(10),
  },
  whatFindText: {
    color: BLACK,
    fontSize: calcFontSize(15),
    fontFamily: SUB_TITLE_FONT,
    flex: 1,
  },
  equipmentRowView: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
    marginTop: moderateScale(20),
    marginBottom: moderateScale(120),
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: moderateScale(8),
  },
  durationText: {
    color: BLACK,
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_BOLD_FONT,
  },
  circlesText: {
    fontSize: calcFontSize(14),
    marginRight: calcWidth(12),
  },
  proIcon: {
    position: 'absolute',
    top: moderateScale(10),
    right: moderateScale(10),
    width: moderateScale(35),
    height: moderateScale(35),
    zIndex: 999,
  },
  focusText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: '#616362',
    paddingRight: moderateScale(25),
    paddingVertical: moderateScale(15),
  },
  focusRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: calcWidth(1),
    borderBottomColor: GRAY_BORDER,
  },
  focusContainer: {
    marginVertical: moderateScale(20),
    marginHorizontal: moderateScale(5),
  },
});

export default styles;
