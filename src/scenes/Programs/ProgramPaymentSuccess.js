import React, {useContext} from 'react';
import {Icon, Button} from 'react-native-elements';
import {View, Text, ScrollView} from 'react-native';
import * as RNIap from 'react-native-iap';
// import Mixpanel from 'react-native-mixpanel';
import styles from './styles';
import {MButton} from '../../components/buttons';
import {LocalizationContext} from '../../localization/translations';

const MIXPANEL_TOKEN = 'bf47184b8fc05bef8c88adb01de19387';

const ProgramDetail = ({navigation, route}) => {
  // const nextStep = () => {
  //   navigation.navigate('SignupWithEmail', {
  //     onboardingData: responses,
  //   });
  // };

  // const nextButtonStyle = true
  //   ? styles.nextButtonStyleActive
  //   : styles.nextButtonStyle;

  // const renderAvailablePrograms = () => {
  //   console.log(data?.availablePrograms, 'program');
  //   if (data?.availablePrograms) {
  //     return data.availablePrograms.map(program => {
  //       return (
  //         <View key={program.id} style={styles.program}>
  //           <Text style={styles.programDescription}>10 Semanas</Text>
  //           <Text style={styles.programName}>
  //             {program.name || 'From Fat to Fit'}
  //           </Text>
  //         </View>
  //       );
  //     });
  //   }
  //   return null;
  // };
  const {translations} = useContext(LocalizationContext);

  const handleGoBack = () => {
    navigation.goBack();
  };

  const buyProgram = () => {
    const requestPurchase = async sku => {
      try {
        const products = await RNIap.getProducts([sku]);
        console.log({products});
        const res = await RNIap.requestPurchase(sku, false);
        console.log({res});
      } catch (err) {
        console.warn(err.code, err.message);
      }
    };
    requestPurchase('android.test.purchased');
  };

  return (
    <ScrollView style={styles.programBuyContainer}>
      <View style={styles.header}>
        <Button
          onPress={handleGoBack}
          buttonStyle={styles.goBackButton}
          icon={<Icon name="arrow-back" />}
        />
        <View>
          <Text style={styles.headerText}>
            {translations.programBuyScreen.chooseSubscription}
          </Text>
        </View>
      </View>
      <View style>
        <Text style={styles.detailProgramName}>
          Pro 3 {translations.programBuyScreen.months}
        </Text>
        <View style={styles.checkContainer}>
          <View style={styles.circleCheck}>
            <Icon name="check" color="#ffffff" size={20} />
          </View>
          <Text style={styles.checkText}>
            {translations.programBuyScreen.personalizedSessions}
          </Text>
        </View>
        <View style={styles.checkContainer}>
          <View style={styles.circleCheck}>
            <Icon name="check" color="#ffffff" size={20} />
          </View>
          <Text style={styles.checkText}>
            {translations.programBuyScreen.plansRecipes}
          </Text>
        </View>
        <Text style={styles.price}>
          2,10 € / {translations.programBuyScreen.week}
        </Text>
        <Text style={styles.priceSubtitle}>
          {translations.programBuyScreen.price} 24.99€ / 3{' '}
          {translations.programBuyScreen.months}.
          {translations.programBuyScreen.planRenew} 3
          {translations.programBuyScreen.months}.
        </Text>
        <MButton
          onPress={() => buyProgram()}
          title={translations.programBuyScreen.choosePlan}
        />
      </View>
    </ScrollView>
  );
};

export default ProgramDetail;
