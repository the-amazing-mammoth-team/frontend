import React, {useCallback, useContext, useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  ImageBackground,
} from 'react-native';
import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {
  BLACK,
  BUTTON_TITLE,
  GRAY_BORDER,
  LIGHT_GRAY,
  WHITE,
  MEDIUM_GRAY,
  LIGHT_ORANGE,
  ORANGE,
} from '../../styles/colors';
import Header from '../../components/header';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {LocalizationContext} from '../../localization/translations';
import Star from '../../assets/icons/starRate.svg';
import AvailableProgramCard from '../../components/ExerciseScreen/AvailableProgramCard';
import InventoryBackground from '../../assets/images/inventoryBackground.png';

const AVAILABLE_PROGRAMS = gql`
  query availablePrograms($locale: String!) {
    availablePrograms(locale: $locale) {
      id
      description
      name
      pro
      updatedAt
      maxAttribute {
        key
        value
        text
      }
      imageUrl
      image2Url
      programCharacteristics {
        id
        value
      }
      sessions {
        id
        name
        description
      }
      codeName
    }
  }
`;

export const mockComments = [
  {
    id: '25gfaf',
    author: 'Miquel Malet',
    text:
      'I love this app, I recommend it to anyone who wants to get strong by gaining health. Planning training with the calendar is very useful, in general it is very well done, something that motivates you to train.',
    starsAmount: 5,
  },
  {
    id: 'gesgsw243',
    author: 'Felipe Rodriguez',
    text:
      'Lorem ipsum pain sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labor et pain magna aliqua.',
    starsAmount: 4,
  },
  {
    id: 'esgst3',
    author: 'Felipe Roodriguez',
    text:
      'Lorem ipsum pain sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labor et pain magna aliqua.',
    starsAmount: 5,
  },
];

export const handleStars = count => {
  return (
    <>
      <View style={styles.starDistance}>
        <Star
          width={moderateScale(18)}
          height={moderateScale(18)}
          fill={count > 0 ? LIGHT_ORANGE : MEDIUM_GRAY}
        />
      </View>
      <View style={styles.starDistance}>
        <Star
          width={moderateScale(18)}
          height={moderateScale(18)}
          fill={count > 1 ? LIGHT_ORANGE : MEDIUM_GRAY}
        />
      </View>
      <View style={styles.starDistance}>
        <Star
          width={moderateScale(18)}
          height={moderateScale(18)}
          fill={count > 2 ? LIGHT_ORANGE : MEDIUM_GRAY}
        />
      </View>
      <View style={styles.starDistance}>
        <Star
          width={moderateScale(18)}
          height={moderateScale(18)}
          fill={count > 3 ? LIGHT_ORANGE : MEDIUM_GRAY}
        />
      </View>
      <View style={styles.starDistance}>
        <Star
          width={moderateScale(18)}
          height={moderateScale(18)}
          fill={count > 4 ? LIGHT_ORANGE : MEDIUM_GRAY}
        />
      </View>
    </>
  );
};

const ProgramsScreen = ({navigation}) => {
  const [isLoaderVisible, setIsLoaderVisible] = useState(true);

  const handleGoBack = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const {translations} = useContext(LocalizationContext);

  const {data, error, loading} = useQuery(AVAILABLE_PROGRAMS, {
    variables: {locale: 'es'},
  });

  useEffect(() => {
    const timer = setTimeout(() => setIsLoaderVisible(false), 0);
    return () => {
      clearTimeout(timer);
    };
  }, []);

  const allPrograms = data?.availablePrograms.map(program => {
    return {
      availableProgramId: program.id,
      duration: program.sessions ? program.sessions.length : '10',
      background: program.imageUrl,
      backgroundWide: program.image2Url,
      // intensityLevel: program.maxAttribute.value,
      title: program.name,
      musculatureLevel: 1,
      description: program.description,
      programCharacteristics: program.programCharacteristics,
    };
  });

  const handleChooseAnotherProgram = useCallback(() => {
    navigation.navigate('ChooseAnotherProgram', {
      allPrograms,
    });
  }, [allPrograms, navigation]);

  const onProgramPress = useCallback(
    item =>
      navigation.navigate('ProgramDescription', {
        item,
      }),
    [navigation],
  );

  console.log(4444, data);

  return (
    <SafeAreaView style={styles.root}>
      {data && !isLoaderVisible ? (
        <ScrollView>
          <Header title={'Programs'} onPressBackButton={handleGoBack} />
          <View style={styles.headerLine} />
          <View style={styles.titleView}>
            <Text>HOLA ESTOY EN PROGRAMS SCREEN!</Text>
            <Text style={styles.title}>
              {translations.programsScreen.idealPlan}
            </Text>
            <Text style={styles.description}>
              {translations.programsScreen.expertResultTitle}
            </Text>
          </View>

          <View style={styles.programsCarousel}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              {allPrograms.slice(0, 2).map((item, key) => {
                return (
                  <AvailableProgramCard
                    item={item}
                    id={item.availableProgramId}
                    key={key}
                    onPress={() => onProgramPress(item)}
                  />
                );
              })}
              <TouchableOpacity
                onPress={handleChooseAnotherProgram}
                style={styles.blackCard}>
                <ImageBackground
                  source={InventoryBackground}
                  style={styles.chooseAnotherImage}
                  imageStyle={styles.chooseAnotherImage}
                />
                <View style={styles.chooseAnotherProgramView}>
                  <Text style={styles.cardTitle}>Choose another program</Text>
                </View>
              </TouchableOpacity>
            </ScrollView>
          </View>

          <View style={styles.container}>
            <Text style={[styles.title, styles.sectionTitle]}>
              {translations.programsScreen.joinPeople}
            </Text>
            {mockComments.map(comment => (
              <View key={comment.id} style={styles.commentRoot}>
                <View style={styles.commentAuthorTextView}>
                  <Text style={styles.commentAuthorText}>{comment.author}</Text>
                </View>
                <View style={styles.commentTextView}>
                  <Text style={[styles.description, styles.commentTextColor]}>
                    {comment.text}
                  </Text>
                </View>
                <View style={styles.starsRow}>
                  {handleStars(comment.starsAmount)}
                </View>
              </View>
            ))}
          </View>
        </ScrollView>
      ) : (
        <ActivityIndicator
          size={'large'}
          color={ORANGE}
          style={{marginTop: moderateScale(200)}}
        />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    backgroundColor: GRAY_BORDER,
    width: '100%',
    marginTop: -20,
    marginBottom: moderateScale(20),
  },
  titleView: {
    paddingHorizontal: calcWidth(20),
    marginBottom: calcHeight(22),
  },
  title: {
    color: BLACK,
    fontSize: calcFontSize(25),
    fontFamily: MAIN_TITLE_FONT,
  },
  description: {
    color: LIGHT_GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  container: {
    paddingHorizontal: calcWidth(20),
  },
  sectionTitle: {
    fontSize: calcFontSize(20),
    marginBottom: moderateScale(30),
  },
  commentTextColor: {
    color: BUTTON_TITLE,
  },
  commentAuthorText: {
    color: BLACK,
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_FONT,
  },
  commentRoot: {
    marginBottom: moderateScale(30),
  },
  commentAuthorTextView: {
    marginBottom: moderateScale(10),
  },
  commentTextView: {
    marginBottom: moderateScale(5),
  },
  starDistance: {
    marginRight: calcWidth(5),
  },
  starsRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  personalExperienceSectionView: {
    marginTop: moderateScale(30),
  },
  frequentlySectionView: {
    marginBottom: moderateScale(30),
    marginTop: moderateScale(40),
  },
  programsCarousel: {
    marginBottom: calcHeight(20),
    paddingLeft: calcWidth(20),
  },
  blackCard: {
    width: calcWidth(250),
    height: calcHeight(320),
    marginRight: calcWidth(10),
    borderRadius: moderateScale(30) / 2,
    backgroundColor: BLACK,
  },
  chooseAnotherImage: {
    width: calcWidth(250),
    height: calcHeight(320),
    borderRadius: moderateScale(30) / 2,
  },
  chooseAnotherProgramView: {
    position: 'absolute',
    top: moderateScale(34),
    left: moderateScale(10),
  },
  cardTitle: {
    color: WHITE,
    fontSize: calcFontSize(30),
    fontFamily: MAIN_TITLE_FONT,
  },
});

export default ProgramsScreen;
