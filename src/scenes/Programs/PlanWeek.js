import React, {useCallback, useContext, useState, useMemo} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Icon} from 'react-native-elements';
import CustomSlider from '../../components/CustomSlider';
import 'react-native-gesture-handler';
import gql from 'graphql-tag';
import {useMutation, useQuery} from '@apollo/react-hooks';
import {useFocusEffect} from '@react-navigation/native';
import {
  BLACK,
  GRAY_BORDER,
  LIGHT_GRAY,
  GRAY,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import Header from '../../components/header';
import {NextButton} from '../../components/buttons';
import Equipment from '../../components/ProgramsScreens/Equipment';
import {LocalizationContext} from '../../localization/translations';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
  deviceWidth,
} from '../../utils/dimensions';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import ErrorModal from '../../components/Modal/ErrorModal';
import {AuthContext} from '../../../App';
import {trackEvents, trackSuperProperties} from '../../services/analytics';

const GET_USER = gql`
  query user($id: ID!) {
    user(id: $id) {
      id
      trainingDaysSetting
      currentProgram {
        id
        implements {
          id
          name
          imageUrl
        }
      }
    }
  }
`;

const GET_IMPLEMENTS = gql`
  query implements($locale: String!) {
    implements(locale: $locale) {
      id
      name
      imageUrl
    }
  }
`;

const UPDATE_USER = gql`
  mutation updateUserData($input: UpdateUserDataInput!) {
    updateUserData(input: $input) {
      id
      trainingDaysSetting
    }
  }
`;

const PlanWeek = ({navigation, route}) => {
  const {program} = route.params;
  const {userData: userInfo} = useContext(AuthContext);
  const {translations, appLanguage} = useContext(LocalizationContext);
  const {data: userData, loading: userLoading, error: userError} = useQuery(
    GET_USER,
    {
      variables: {id: userInfo?.id},
    },
  );

  // const {
  //   data: implementsData,
  //   loading: implementsLoading,
  //   error: implementsError,
  // } = useQuery(GET_IMPLEMENTS, {
  //   variables: {locale: appLanguage},
  // });

  const [sliderValue, setSliderValue] = useState(
    userDataBackend?.trainingDaysSetting,
  );
  const [implementIds, setImplementIds] = useState([]);
  const [modalErrorState, setModalErrorState] = useState({
    isVisible: false,
    description: '',
  });

  const [update] = useMutation(UPDATE_USER);

  const userDataBackend = userData?.user;

  useFocusEffect(
    useCallback(() => {
      trackEvents({eventName: 'Load Plan Your Workouts'});
    }, []),
  );

  // const implementsList = useMemo(() => {
  //   return implementsData?.implements?.map(item => {
  //     const res = implementIds.includes(item.id);
  //     return {...item, isActive: res};
  //   });
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [implementIds, implementsData?.implements]);

  const handleGoBack = useCallback(() => navigation.goBack(), [navigation]);
  const handleSetImplement = useCallback(
    item => {
      const index = implementIds?.findIndex(el => el === item.id);
      if (index >= 0) {
        setImplementIds(implementIds.filter(el => item.id !== el));
      } else {
        setImplementIds([...implementIds, item.id]);
      }
    },
    [implementIds],
  );
  const handleNextButton = useCallback(async () => {
    try {
      const res = await update({
        variables: {
          input: {
            userData: {
              trainingDaysSetting: sliderValue,
            },
            //implementIds: implementIds,
          },
        },
        context: {
          headers: {
            authorization: userInfo.token,
          },
        },
        refetchQueries: ['user'],
        awaitRefetchQueries: true,
      });
      if (res?.data?.updateUserData) {
        console.log('RESULT', res.data.updateUserData);
        trackSuperProperties({
          properties: {
            training_days: res.data.updateUserData.trainingDaysSetting,
          },
        });
        navigation.navigate('PaymentSuccess', {
          program: program,
          weeklyGoal: sliderValue,
        });
      }
    } catch (error) {
      console.log(777, error);
      setModalErrorState({description: error.message, isVisible: true});
    }
  }, [navigation, program, sliderValue, update, userInfo.token]);

  const handleSliderValue = useCallback(e => setSliderValue(e), []);
  const handleDismissModal = useCallback(
    () => setModalErrorState({...modalErrorState, isVisible: false}),
    [modalErrorState],
  );

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <ErrorModal
          isModalVisible={modalErrorState.isVisible}
          onDismiss={handleDismissModal}
          title={'Error'}
          description={modalErrorState.description}
        />
        <Header
          onPressBackButton={handleGoBack}
          title={translations.planWeekScreen.header}
        />
        <View style={[styles.line, styles.negativeMarginTop]} />
        <View style={styles.container}>
          <View style={styles.mainQuestionView}>
            <Text style={styles.mainQuestionTextTitle}>
              {translations.planWeekScreen.howManyTimesQuestion}
            </Text>
          </View>
          <View style={styles.descriptionMainQuestionView}>
            <Text style={styles.descriptionMainQuestionText}>
              {translations.planWeekScreen.howManyTimesQuestionDescription}
            </Text>
            <View style={styles.sliderStyle}>
              <CustomSlider
                maxValue={7}
                value={sliderValue}
                onChange={handleSliderValue}
              />
            </View>
          </View>
          {/* <View style={styles.line} />
          <View style={styles.questionView}>
            <Text style={styles.questionTextTitle}>
              {translations.planWeekScreen.whatEquipmentDoYouHave}
            </Text>
          </View>
          <FlatList
            data={implementsList}
            renderItem={({item}) => (
              <Equipment item={item} onPress={handleSetImplement} />
            )}
            horizontal
            keyExtractor={el => el.id}
          /> */}
        </View>
      </ScrollView>
      <NextButton onPress={handleNextButton} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  line: {
    backgroundColor: GRAY_BORDER,
    height: moderateScale(1),
  },
  mainQuestionView: {
    marginTop: moderateScale(25),
    marginBottom: moderateScale(12),
  },
  mainQuestionTextTitle: {
    color: BLACK,
    fontSize: calcFontSize(20),
    fontFamily: MAIN_TITLE_FONT,
  },
  questionView: {
    marginTop: moderateScale(35),
    marginBottom: moderateScale(20),
  },
  questionTextTitle: {
    color: BLACK,
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  container: {
    paddingHorizontal: moderateScale(20),
  },
  descriptionMainQuestionView: {
    marginBottom: moderateScale(42),
  },
  descriptionMainQuestionText: {
    color: GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  negativeMarginTop: {
    marginTop: -20,
  },
  hitslopTwenty: {
    top: calcHeight(20),
    bottom: calcHeight(20),
    right: calcWidth(20),
    left: calcWidth(20),
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: moderateScale(50),
  },
  userIconView: {
    alignSelf: 'flex-end',
    marginVertical: moderateScale(20),
  },
  sliderStyle: {
    marginTop: moderateScale(30),
    marginBottom: moderateScale(-30),
  },
});

export default PlanWeek;
