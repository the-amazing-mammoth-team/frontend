import React, {
  useState,
  useRef,
  useContext,
  useCallback,
  useEffect,
} from 'react';
import {Icon} from 'react-native-elements';
import {
  View,
  Text,
  ScrollView,
  ImageBackground,
  Image,
  SafeAreaView,
  Platform,
} from 'react-native';
import {useMutation, useLazyQuery} from '@apollo/react-hooks';
import SwiperFlatList from 'react-native-swiper-flatlist';
import Animated from 'react-native-reanimated';
import RNIap, {purchaseErrorListener, IAPErrorCode} from 'react-native-iap';
import * as Sentry from '@sentry/react-native';
import {useFocusEffect} from '@react-navigation/native';
// import Mixpanel from 'react-native-mixpanel';
import styles from './styles';
import {MButton} from '../../components/buttons';
import {LocalizationContext} from '../../localization/translations';
import ErrorModal from '../../components/Modal/ErrorModal';
import Header from '../../components/header';
import {GRAY, LIGHT_GRAY, ORANGE} from '../../styles/colors';
import PersonalExperienceSection from '../../components/ProgramsScreens/PersonalExperienceSection';
import FrequentQuestionsSection from '../../components/ProgramsScreens/FrequentQuestionsSection';
import ProgramOfferCard from '../../components/ProgramsScreens/ProgramOfferCard';
import gql from 'graphql-tag';
import deviceStorage from '../../services/deviceStorage';
import {deviceHeight, moderateScale} from '../../utils/dimensions';
import {paymentError} from '../../utils/errorMessages';
import {onboardingUpSalesCodes} from '../../utils/offerCodes';
import {trackEvents} from '../../services/analytics';
import {AuthContext} from '../../../App';
import {Loading} from '../../components/Loading';

const MIXPANEL_TOKEN = 'bf47184b8fc05bef8c88adb01de19387';
let myPurchaseErrorListener = () => {};

const GET_PRODUCTS = gql`
  query products($offerCodes: [String!]!, $device: String!) {
    products(offerCodes: $offerCodes, device: $device) {
      id
      storeReference
      price
      name
      hasTrial
      trialDays
    }
  }
`;

const SUBSCRIBE = gql`
  mutation subscribe($input: SubscribeInput!) {
    subscribe(input: $input) {
      id
      status
      startDate
      transactionBody
      user {
        email
        names
      }
    }
  }
`;

// offerCodes for test  ['1y_7999t', '3m_4999t', '1y_7999_trial_test'] the order will be full_price [0], 3months [1], trial [2]
// offerCodes for upsale/test uses the trial also so => products[1]
// offerCodes for production [']

const CheckItem = ({text, checked}) => {
  return (
    <View style={styles.checkContainer}>
      <View
        style={[styles.circleCheck, !checked && {backgroundColor: LIGHT_GRAY}]}>
        <Icon name="check" color="#ffffff" size={20} />
      </View>
      <Text style={styles.checkText}>{text}</Text>
    </View>
  );
};

function getOs() {
  return Platform.OS === 'ios' ? 'apple' : 'google_play';
}

const ProgramDetail = ({navigation, route}) => {
  const {program, prevScreen, offerCodes, user} = route.params;
  const [
    getProducts,
    {data: products, error: err, loading = false},
  ] = useLazyQuery(GET_PRODUCTS, {
    variables: {
      offerCodes,
      device: getOs(),
    },
    fetchPolicy: 'network-only',
  });
  console.log('getprods');
  console.log(program.id);

  console.log('prods down');
  console.log(products, err);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalTextError, setModalTextError] = useState({code: '', message: ''});

  const {translations} = useContext(LocalizationContext);
  const {userData: userInfo} = useContext(AuthContext);

  const [isButtonPress, setIsButtonPress] = useState(false);

  const [subscribe] = useMutation(SUBSCRIBE);

  const handleGoBack = () => {
    navigation.goBack();
  };

  const handleDismissModal = useCallback(() => {
    setIsModalVisible(oldData => !oldData);
    setIsButtonPress(false);
  }, []);

  const [index, setIndex] = useState(0);
  const swiperRef = useRef();
  const animation = useRef(new Animated.Value(0)).current;

  const handlePurchaseErrors = useCallback(
    err => {
      if (err.code !== IAPErrorCode.E_USER_CANCELLED) {
        Sentry.captureMessage(err);
        setModalTextError({message: paymentError(err.code, translations)});
        setIsModalVisible(true);
      } else {
        setIsButtonPress(false);
      }
    },
    [translations],
  );

  useFocusEffect(
    useCallback(() => {
      if (products) {
        trackEvents({
          eventName: 'Load Sales',
          properties: {
            type: 'onboarding',
            offer: offerCodes,
            products: products.products?.map(el => el.storeReference),
            prices: products.products?.map(el => el.price),
            payment_platform: getOs(),
            placement: prevScreen,
          },
        });
      }
    }, [offerCodes, prevScreen, products]),
  );

  useEffect(() => {
    if (!loading && !products) {
      getProducts();
    }
  }, [getProducts, loading, navigation, offerCodes, products]);

  useEffect(() => {
    myPurchaseErrorListener = purchaseErrorListener(err =>
      handlePurchaseErrors(err),
    );
    return () => {
      myPurchaseErrorListener.remove();
    };
  }, [handlePurchaseErrors, navigation]);

  const onSubscribe = useCallback(
    async (productId, receipt, userInfo) => {
      try {
        console.log('user info at subscribe', userInfo);
        const res = await subscribe({
          variables: {
            input: {
              programId: program.id,
              productId: productId,
              transactionBody: receipt.toString(),
            },
          },
          context: {
            headers: {
              authorization: userInfo?.token,
            },
          },
          //refetchQueries: ['user'],
        });
        if (res.data) {
          console.log('Response:', res.data.subscribe);
          await deviceStorage.saveItem(
            '@programs/currentProgram',
            JSON.stringify(program),
          );
          return true;
        }
      } catch (err) {
        Sentry.captureMessage(err);
        setModalTextError({code: err.code, message: err.message});
        setIsModalVisible(true);
        throw new Error(err.message);
      }
    },
    [program, subscribe],
  );

  const buyProgram2 = useCallback(
    async product => {
      setIsButtonPress(true);
      const requestPurchase = async sku => {
        try {
          await RNIap.initConnection();
          const products = await RNIap.getSubscriptions([sku]);
          // await deviceStorage.saveItem(
          //   '@purchase/product',
          //   JSON.stringify(products[0]),
          // );
          const res = await RNIap.requestPurchase(sku, false);
          if (res) {
            await onSubscribe(product.id, res.transactionReceipt, userInfo);
            await deviceStorage.saveItem('@purchase/isrSuccessRequest', 'true');
            setIsButtonPress(false);
            user?.currentProgram
              ? //first user program, asking to choose training days settings
                navigation.navigate('PlanWeek', {program})
              : navigation.navigate('PaymentSuccess', {
                  program,
                  weeklyGoal: user?.trainingDaysSetting,
                });
          }
          setIsButtonPress(false);
        } catch (err) {}
      };
      await requestPurchase(product.storeReference);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [navigation, route.params.program, userInfo],
  );

  console.log(999, program);

  const goToLite = useCallback(
    () =>
      navigation.navigate('Sales', {
        imageHeader: program.backgroundWide,
        program: program,
        products: products,
        paymentPlatform: getOs(),
        offerCodes: onboardingUpSalesCodes,
        user,
      }),
    [navigation, program, products, user],
  );

  return (
    <SafeAreaView style={styles.programBuyContainer}>
      <ScrollView>
        <ErrorModal
          isModalVisible={isModalVisible}
          onDismiss={handleDismissModal}
          title={'Error'}
          description={modalTextError.message}
        />
        <Header
          title={translations.programBuyScreen.subscription}
          onPressBackButton={handleGoBack}
        />
        <View style={styles.headerLine} />
        {/* <ImageBackground
          source={{uri: program.backgroundWide}}
          style={[
            styles.containerCopy,
            {
              width: '100%',
              height: deviceHeight * 0.35,
            },
          ]}>
          <SwiperFlatList
            paginationActiveColor={ORANGE}
            paginationDefaultColor={GRAY}
            paginationStyleItem={styles.paginationDot}
            paginationStyle={styles.paginationStyle}
            ref={swiperRef}
            onViewableItemsChanged={() => {
              setIndex(swiperRef.current?.getCurrentIndex());
            }}
            index={index}
            showPagination
            data={['Image1', 'Image2', 'Image3']}
            renderItem={({item}) => (
              <View style={[styles.child, {backgroundColor: item}]}>
                <Text style={styles.text}>{item}</Text>
              </View>
            )} />
        </ImageBackground> */}
        <View style={styles.container}>
          <View style={styles.genericPadding}>
            <View style={styles.programDetailInfo}>
              <Text style={styles.detailProgramName}>
                {translations.programBuyScreen.ProUpsale.title}
              </Text>
              {translations.programBuyScreen.ProUpsale.characteristics.map(
                char => (
                  <CheckItem text={char} checked={true} />
                ),
              )}
            </View>
            <View style={{marginTop: moderateScale(40)}} />
            {products ? (
              <>
                <ProgramOfferCard
                  id={products?.products[0].id}
                  title={products?.products[0].name}
                  onPress={() => buyProgram2(products?.products[0])}
                  price={products?.products[0].price}
                  pro={true}
                  isTrial={products?.products[0].hasTrial}
                  trialDays={
                    products?.products[0].trialDays +
                    ' ' +
                    translations.programBuyScreen.trialDays
                  }
                  currency={'eur'}
                  local="es"
                  translations={translations.programBuyScreen}
                  coloredBorder={true}
                  disabled={isButtonPress}
                  isAnnual={true}
                />
                <ProgramOfferCard
                  id={products?.products[1].id}
                  title={products?.products[1].name}
                  onPress={() => buyProgram2(products?.products[1])}
                  price={products?.products[1].price}
                  pro={true}
                  isTrial={false}
                  currency={'eur'}
                  local="es"
                  translations={translations.programBuyScreen}
                  disabled={isButtonPress}
                  isAnnual={false}
                />
              </>
            ) : (
              <View style={{marginVertical: moderateScale(40)}}>
                <Loading noMargin />
              </View>
            )}
            <View style={styles.automaticallyRenewTextView}>
              <Text style={styles.automaticallyRenewText}>
                {translations.programBuyScreen.upsaleSubscriptionMessage}
              </Text>
            </View>
            <View style={styles.line} />
            {program.pro === 'false' && (
              <>
                <View style={styles.programDetailInfo}>
                  <Text style={styles.detailProgramName}>
                    {translations.programBuyScreen.LiteUpsale.title}
                  </Text>
                  {translations.programBuyScreen.LiteUpsale.characteristics.map(
                    (char, index) => (
                      <CheckItem text={char} checked={!index} />
                    ),
                  )}
                </View>
                <View style={{paddingVertical: moderateScale(20)}} />
                {products ? (
                  <ProgramOfferCard
                    id={333}
                    title={translations.programBuyScreen.litePlan}
                    onPress={goToLite}
                    pro={false}
                    currency={'eur'}
                    local="es"
                    translations={translations.programBuyScreen}
                  />
                ) : (
                  <View style={{marginBottom: moderateScale(40)}}>
                    <Loading noMargin />
                  </View>
                )}
              </>
            )}
          </View>
          <View style={styles.personalExperienceSectionView}>
            <PersonalExperienceSection translations={translations} />
          </View>
          <View style={[styles.frequentlySectionView, styles.genericPadding]}>
            <FrequentQuestionsSection
              title={translations.programBuyScreen.faq}
              translations={translations}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default ProgramDetail;
