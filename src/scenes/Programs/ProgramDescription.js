import React, {useCallback, useContext} from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Icon, Button} from 'react-native-elements';
import {
  BLACK,
  GRAY_BORDER,
  LIGHT_GRAY,
  GRAY,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceHeight,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_BOLD_FONT,
  SUB_TITLE_FONT,
} from '../../styles/fonts';
import CheckedIcon from '../../assets/icons/checkIcon.svg';
import MagentoEquipment from '../../assets/images/pullUpBar.png';
import KettleBellsEquipment from '../../assets/images/kettleBells.png';
import {levelCircles} from '../../components/ProgramsScreens/CurrentProgramCard';
import {LocalizationContext} from '../../localization/translations';
import DietWomanResult from '../../assets/images/dietWomanResult.png';
import dish1 from '../../assets/images/dish1.png';

export const renderEquipment = (name, image) => {
  return (
    <View style={styles.equipmentView}>
      {name && image ? (
        <>
          <Image source={image} style={styles.equipmentImage} />
          <Text style={styles.equipmentName}>{name}</Text>
        </>
      ) : (
        <Text style={[styles.equipmentName, styles.noEquipment]}>
          No Equipment
        </Text>
      )}
    </View>
  );
};

const CheckItem = ({text}) => {
  return (
    <View style={styles.checkContainer}>
      <View style={styles.circleCheck}>
        <Icon name="check" color="#ffffff" size={20} />
      </View>
      <Text style={styles.checkText}>{text}</Text>
    </View>
  );
};
function isObjective(programCharacteristic) {
  return programCharacteristic?.objective;
}

function isCharacteristic(programCharacteristic) {
  return !programCharacteristic?.objective;
}

const ProgramDescription = ({route, navigation}) => {
  const {
    programCharacteristics,
    duration,
    intensityLevel,
    musculatureLevel,
    title,
    availableProgramId,
    background,
    backgroundWide,
    maxAttribute,
    maxAttribute2,
    technique,
    strength,
    intensity,
    endurance,
    flexibility,
    description,
    creator,
  } = route.params.item;

  console.log({maxAttribute2});
  console.log({creator});
  const goals = programCharacteristics?.filter(isObjective);
  const characteristics = programCharacteristics?.filter(
    isCharacteristic,
  );

  console.log({
    technique,
    strength,
    intensity,
    endurance,
    flexibility,
  });

  const {
    isCirclesLevel = true,
    isEquipment = true,
    isDays = false,
    isNutrition = false,
    //isPro = false,
  } = route.params;

  const isPro = true;

  const {translations} = useContext(LocalizationContext);

  const renderCheckIconsTextFields = useCallback(
    (title, data) => (
      <View style={styles.whatFindView}>
        <View style={styles.whatFindTitleView}>
          <Text style={[styles.programNameText, styles.whatFindTitle]}>
            {title}
          </Text>
        </View>
        {data?.map(item => (
          <View style={styles.whatFindRowView} key={item.id}>
            <View style={styles.purpleCircle}>
              <CheckedIcon width={calcWidth(12)} height={calcHeight(8)} />
            </View>
            <Text style={styles.whatFindText}>{item.text}</Text>
          </View>
        ))}
      </View>
    ),
    [],
  );

  const BackButton = () => {
    return (
      <View style={styles.arrowBack}>
        <Button
          buttonStyle={{backgroundColor: 'transparent'}}
          onPress={() => navigation.goBack()}
          icon={<Icon name="arrow-back" />}
        />
      </View>
    );
  };

  const handleOnChooseProgram = useCallback(() => {
    isPro
      ? navigation.navigate('PlanWeek', {
          program: route.params.item,
        })
      : navigation.navigate('ProgramBuy', {
          program: route.params.item,
        });
  }, [isPro, navigation, route.params.item]);

  const handleOnChooseNutritionProgram = useCallback(
    () =>
      navigation.navigate('Sales', {
        imageHeader: DietWomanResult,
      }),

    // uncomment for testing dish description screen
    //
    // navigation.navigate('DishDescription', {
    //   dish: mockDish,
    // }),
    [navigation],
  );

  // const characteristics = programCharacteristics?.map(char => {
  //   return {
  //     text: char.value,
  //     id: char.id,
  //   };
  // });

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <BackButton />
        <View style={styles.manSqueezingsView}>
          <Image
            source={
              typeof backgroundWide === 'number'
                ? backgroundWide
                : {uri: backgroundWide}
            }
            style={styles.manSqueezingsImage}
          />
          <View style={styles.purpleTip}>
            <Text style={styles.purpleTipText}>{translations.programDetailScreen.recomendForYou}</Text>
          </View>
        </View>
        <View style={styles.container}>
          <View style={styles.durationView}>
            <Text style={styles.durationText}>
              {duration}{' '}
              {isDays
                ? translations.nutritionScreen.days
                : translations.programsScreen.sessions}
            </Text>
          </View>
          <View style={styles.programNameView}>
            <Text style={styles.programNameText}>{title}</Text>
          </View>
          <View style={styles.programNameView}>
            <Text style={styles.creatorText}>{creator}</Text>
          </View>
          {isCirclesLevel && (
            <View>
              <View style={styles.rowView}>
                <Text style={[styles.durationText, styles.circlesText]}>
                  Intensity
                </Text>
                {levelCircles(intensityLevel)}
              </View>
              <View style={styles.rowView}>
                <Text style={[styles.durationText, styles.circlesText]}>
                  Musculature
                </Text>
                {levelCircles(musculatureLevel)}
              </View>
            </View>
          )}
          {description && (
            <View style={styles.descriptionView}>
              <Text style={styles.whatFindText}>{description}</Text>
            </View>
          )}
          {characteristics?.length > 0 && (
            <Text style={styles.programDetailSubtitle}>
              {translations.programDetailScreen.findQuestion}
            </Text>
          )}
          {characteristics?.map(char => {
            return <CheckItem key={char.id} text={char.value} />;
          })}
          {goals?.length > 0 && (
            <Text style={styles.programDetailSubtitle}>
              {translations.programDetailScreen.programGoals}
            </Text>
          )}
          {goals?.map(char => {
            return <CheckItem key={char.id} text={char.value} />;
          })}
          {isEquipment && (
            <View style={styles.extraMarginBottom}>
              <View style={styles.whatFindTitleView}>
                <Text style={[styles.programNameText, styles.whatFindTitle]}>
                  Equipment
                </Text>
              </View>
              <View style={styles.equipmentRowView}>
                {renderEquipment('Pull-up bar', MagentoEquipment)}
                {renderEquipment('Kettlebells', KettleBellsEquipment)}
              </View>
            </View>
          )}
          {!isEquipment && (
            <View
              style={[styles.negativeMarginTop, styles.extraMarginBottom]}
            />
          )}
        </View>
      </ScrollView>
      <TouchableOpacity
        style={styles.chooseProgramButton}
        onPress={
          isNutrition ? handleOnChooseNutritionProgram : handleOnChooseProgram
        }>
        <Text style={styles.chooseProgramButtonText}>
          {translations.programDetailScreen.chooseProgram}
        </Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  imageStyle: {},
  imageView: {
    alignSelf: 'center',
    width: deviceWidth,
    height: deviceHeight * 0.3,
  },
  container: {
    paddingHorizontal: calcWidth(20),
  },
  durationView: {
    marginBottom: moderateScale(5),
    marginTop: calcHeight(30),
  },
  durationText: {
    color: BLACK,
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_BOLD_FONT,
  },
  programNameView: {
    marginBottom: moderateScale(8),
  },
  programNameText: {
    color: BLACK,
    fontSize: calcFontSize(30),
    fontFamily: MAIN_TITLE_FONT,
  },
  creatorText: {
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(16),
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: moderateScale(8),
  },
  circlesText: {
    fontSize: calcFontSize(14),
    marginRight: calcWidth(12),
  },
  whatFindView: {
    marginTop: moderateScale(40),
    marginBottom: moderateScale(40),
  },
  whatFindTitleView: {
    marginBottom: moderateScale(15),
  },
  extraMarginBottomSection: {
    marginBottom: moderateScale(30),
  },
  whatFindTitle: {
    fontSize: calcFontSize(18),
  },
  whatFindText: {
    color: BLACK,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    flex: 1,
  },
  purpleCircle: {
    width: moderateScale(17),
    height: moderateScale(17),
    borderRadius: moderateScale(17) / 2,
    backgroundColor: ORANGE,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: moderateScale(11),
  },
  whatFindRowView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: calcHeight(10),
  },
  equipmentView: {
    borderRadius: moderateScale(25),
    borderWidth: moderateScale(1),
    borderColor: GRAY_BORDER,
    alignItems: 'center',
    justifyContent: 'center',
    height: moderateScale(148),
    width: moderateScale(140),
    marginRight: calcWidth(15),
    paddingHorizontal: calcWidth(10),
    paddingVertical: calcHeight(13),
  },
  equipmentImage: {
    width: calcWidth(100),
    height: calcHeight(100),
  },
  equipmentName: {
    color: BLACK,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
  },
  noEquipment: {
    color: LIGHT_GRAY,
  },
  equipmentRowView: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  leftBar: {
    width: moderateScale(1),
    backgroundColor: GRAY_BORDER,
    height: '90%',
    position: 'absolute',
    left: moderateScale(8.5),
    top: moderateScale(7),
    zIndex: -1,
  },
  lightOrangeCircle: {
    width: moderateScale(17),
    height: moderateScale(17),
    borderRadius: moderateScale(17) / 2,
    backgroundColor: LIGHT_ORANGE,
    marginRight: moderateScale(17),
  },
  trajectoryTitle: {
    color: BLACK,
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_FONT,
    marginBottom: calcHeight(5),
  },
  trajectoryDescription: {
    color: LIGHT_GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  separator: {
    height: moderateScale(1),
    width: deviceWidth,
    alignSelf: 'center',
    backgroundColor: GRAY_BORDER,
    marginVertical: calcHeight(25),
  },
  chooseProgramButton: {
    backgroundColor: ORANGE,
    alignSelf: 'center',
    width: deviceWidth * 0.8,
    borderRadius: moderateScale(25),
    paddingVertical: moderateScale(13),
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: moderateScale(25),
  },
  chooseProgramButtonText: {
    color: WHITE,
    fontSize: calcFontSize(18),
    fontFamily: MAIN_TITLE_FONT,
  },
  trajectoryRow: {
    flexDirection: 'row',
    marginBottom: moderateScale(20),
    alignItems: 'baseline',
  },
  flexOne: {
    flex: 1,
  },
  manSqueezingsView: {
    width: '100%',
  },
  manSqueezingsImage: {
    width: '100%',
    height: deviceHeight * 0.35,
    //resizeMode: 'stretch',
    backgroundColor: BLACK,
  },
  purpleTip: {
    backgroundColor: ORANGE,
    paddingHorizontal: moderateScale(5),
    paddingVertical: moderateScale(5),
    position: 'absolute',
    bottom: -moderateScale(15),
    left: calcWidth(20),
  },
  purpleTipText: {
    color: WHITE,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_BOLD_FONT,
  },
  negativeMarginTop: {
    marginTop: -moderateScale(50),
  },
  extraMarginBottom: {
    marginBottom: moderateScale(100),
  },
  descriptionView: {
    marginTop: moderateScale(10),
    marginBottom: -moderateScale(10),
  },
  arrowBack: {
    alignSelf: 'flex-start',
    width: 50,
    height: 50,
    position: 'absolute',
    left: 0,
    top: 10,
    zIndex: 999,
  },
});

export default ProgramDescription;
