import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {BLACK, WHITE} from '../../styles/colors';
import {calcHeight, moderateScale} from '../../utils/dimensions';
import {Loading} from '../../components/Loading';

export default () => {
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Image
          style={styles.logo}
          source={require('../../assets/icons/mh-logo.png')}
        />
        <Loading />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: BLACK,
  },
  logo: {
    height: moderateScale(60),
    width: moderateScale(150),
  },
  top: {
    justifyContent: 'flex-start',
    marginTop: calcHeight(165),
    // marginBottom: calcHeight(80),
  },
});
