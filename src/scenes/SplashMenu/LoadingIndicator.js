import React from 'react';
import {StyleSheet} from 'react-native';
import interpolate from 'color-interpolate';
import Svg, {
  Circle,
  Defs,
  G,
  LinearGradient,
  Path,
  Stop,
} from 'react-native-svg';
import Animated, {Value, Easing} from 'react-native-reanimated';
import {BLACK, LIGHT_ORANGE, ORANGE} from '../../styles/colors';
import {calcHeight, moderateScale} from '../../utils/dimensions';

const {PI, cos, sin} = Math;
const {multiply, sub} = Animated;
const AnimatedCircle = Animated.createAnimatedComponent(Circle);
const AnimatedPath = Animated.createAnimatedComponent(Path);
const colors = [ORANGE, LIGHT_ORANGE];
const palette = interpolate(colors);
const size = moderateScale(80);
const strokeWidth = moderateScale(6);
const r = size / 2 - strokeWidth / 2;
const cx = size / 2;
const cy = size / 2;
const A = 2 * PI;
const sampling = 2;
const step = A / sampling;
const x = a => cx - r * cos(a);
const y = a => -r * sin(a) + cy;
// A rx ry x-axis-rotation large-arc-flag sweep-flag x y
const arc = a => `A ${r} ${r} 0 0 1 ${x(a)} ${y(a)}`;
const arcs = new Array(sampling).fill(0).map((_0, i) => {
  const a = i * step;
  return `M ${x(a)} ${y(a)} ${arc(a + step)}`;
});

export default () => {
  const progress = new Value(0.5);
  const transition = new Value(0.5);
  const circumference = r * 2 * PI;
  const α = multiply(sub(4, multiply(progress, transition)), PI * 2);
  const strokeDashoffset = sub(circumference, multiply(α, -r));
  const spin = React.useRef(new Animated.Value(0)).current;
  React.useEffect(() => {
    const startSpin = () => {
      Animated.timing(spin, {
        toValue: 1,
        duration: 3000,
        easing: Easing.linear,
        useNativeDriver: true,
      }).start();
    };

    startSpin();
  }, [spin]);

  return (
    <Animated.View style={{transform: [{rotate: spin}]}}>
      <Svg style={styles.svg} width={size + 5} height={size + 5}>
        <Defs>
          {arcs.map((_d, key) => {
            const isReversed = key / sampling >= 0.5;
            return (
              <LinearGradient key={key} id={`gradient-${key}`}>
                <Stop
                  stopColor={palette(key / sampling)}
                  offset={`${isReversed ? 100 : 0}%`}
                />
                <Stop
                  stopColor={palette((key + 1) / sampling)}
                  offset={`${isReversed ? 0 : 100}%`}
                />
              </LinearGradient>
            );
          })}
        </Defs>
        <G
          transform={`translate(${cx}, ${cy}) rotate(180) translate(${-cx}, ${-cy})`}>
          {arcs.map((d, key) => (
            <AnimatedPath
              key={key}
              d={d}
              fill="transparent"
              stroke={`url(#gradient-${key})`}
              {...{strokeWidth}}
            />
          ))}
        </G>
        <AnimatedCircle
          fill="transparent"
          strokeDashoffset={strokeDashoffset}
          strokeDasharray={`${circumference}, ${circumference}`}
          {...{strokeWidth, r, cx, cy}}
        />
      </Svg>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  svg: {
    transform: [{rotateZ: '90deg'}],
  },
  logo: {
    height: moderateScale(70),
    width: moderateScale(150),
  },
  top: {
    justifyContent: 'flex-start',
    marginTop: calcHeight(165),
    marginBottom: calcHeight(50),
  },
});
