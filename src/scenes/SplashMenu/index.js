import React, {useContext, useLayoutEffect} from 'react';
import {SafeAreaView, StyleSheet, View, Text, Image} from 'react-native';
import {Button} from 'react-native-elements';
import Mixpanel from 'react-native-mixpanel';
import {MButton} from '../../components/buttons';
import {MAIN_TITLE_FONT} from '../../styles/fonts';
import {BLACK, WHITE, LIGHT_ORANGE} from '../../styles/colors';
import {LocalizationContext} from '../../localization/translations';
import {calcHeight, calcWidth, deviceWidth} from '../../utils/dimensions';
import {validateEmail} from '../../utils/regexes';
import {Platforms, trackEvents} from '../../services/analytics';

const Login = ({navigation}) => {
  const {translations} = useContext(LocalizationContext);

  const handleLogin = () => {
    navigation.navigate('Login');
  };
  const handleRegister = () => {
    navigation.navigate('Onboarding');
  };

  useLayoutEffect(() => {
    Mixpanel.getDistinctId(id => {
      const res = validateEmail.test(id);
      !res &&
        trackEvents({
          eventName: 'Load Start',
          platforms: {1: Platforms.mixPanel},
        });
    });
  }, []);

  return (
    <>
      <SafeAreaView style={styles.container}>
        <View style={styles.top}>
          <Image
            style={styles.logo}
            source={require('../../assets/icons/mh-logo.png')}
          />
        </View>
        <View style={styles.bottom}>
          <MButton
            main
            onPress={handleRegister}
            title={translations.splashScreen.startButton}
            buttonExtraStyles={{maxWidth: deviceWidth * 0.85}}
          />
          <Text style={styles.textColor}>
            {translations.splashScreen.isAccountExists}
          </Text>
          <Button
            onPress={handleLogin}
            title={translations.splashScreen.login}
            buttonStyle={styles.signup}
            titleStyle={styles.loginButton}
            hitSlop={{
              top: calcHeight(20),
              bottom: calcHeight(20),
              left: calcWidth(20),
              right: calcWidth(20),
            }}
          />
        </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  logo: {
    height: 70,
    width: 150,
  },
  loginButton: {
    color: LIGHT_ORANGE,
    fontSize: 18,
    fontFamily: MAIN_TITLE_FONT,
  },
  startButton: {
    fontSize: 18,
  },
  top: {
    justifyContent: 'flex-start',
    marginTop: 150,
  },
  bottom: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    flex: 1,
    marginBottom: 40,
  },
  container: {
    backgroundColor: BLACK,
    color: WHITE,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textColor: {
    color: WHITE,
    fontSize: 18,
    marginTop: 30,
    fontFamily: MAIN_TITLE_FONT,
  },
  signup: {
    backgroundColor: '#2B2B2B',
  },
});

export default Login;
