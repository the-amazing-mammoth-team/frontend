import React, {useCallback, useContext} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  SectionList,
} from 'react-native';
import Header from '../../components/header';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';
import {LocalizationContext} from '../../localization/translations';
import {AuthContext} from '../../../App';
import {
  BLACK,
  GRAY,
  GRAY_BORDER,
  LIGHT_ORANGE,
  WHITE,
} from '../../styles/colors';
import ChallengesIcon from '../../assets/icons/challenges';
import LevelsIcon from '../../assets/icons/levels';
import ProgramsIcon from '../../assets/icons/programs';
import {calcFontSize, moderateScale} from '../../utils/dimensions';
import {trackEvents} from '../../services/analytics';
import {SUB_TITLE_FONT} from '../../styles/fonts';

const Achievements = ({navigation, route}) => {
  const {translations} = useContext(LocalizationContext);
  const {userData: userInfo} = useContext(AuthContext);
  const isFocused = useIsFocused();
  const {achievments = [], currentSession, currentProgram} =
    route?.params || {};

  useFocusEffect(
    useCallback(() => {
      isFocused && trackEvents({eventName: 'Load Achivements'});
    }, [isFocused]),
  );

  const handleGoBack = useCallback(() => navigation.goBack(), [navigation]);

  const goToWorkout = useCallback(
    (program, session) => {
      navigation.navigate('Workout', {
        programId: program?.id,
        programCodeName: program?.codeName,
        sessionId: session?.id,
        sessionDescription: session?.description,
        sessionName: session?.name,
        userId: userInfo?.id,
      });
    },
    [navigation, userInfo],
  );

  const goToChallenges = useCallback(() => {
    navigation.navigate('ListChallengeSessionsProgram', {
      userId: userInfo?.id,
      goToWorkout: goToWorkout,
    });
  }, [goToWorkout, navigation, userInfo]);

  const renderSectionTitle = title => (
    <Text style={styles.sectionText}>{title}</Text>
  );

  const renderSection = ({item, index, section}) => {
    if (item.data?.length) {
      const items =
        item.data?.map(item => (
          <View key={item.id} style={styles.itemViewRect}>
            <Image source={{uri: item.iconUrl}} style={styles.icon} />
            <Text style={styles.genericBlackText}>{item.name}</Text>
          </View>
        )) || [];
      return <View style={styles.rowView}>{items}</View>;
    } else {
      return (
        <View style={styles.defaultItemContainer}>
          <View style={styles.defaultIconContainer}>{section.defaultIcon}</View>
          <Text style={styles.defaultText}>{section.defaultDesc}</Text>
          <Text style={styles.defaultLink} onPress={section.defaultLinkFunc}>
            {section.defaultLink}
          </Text>
        </View>
      );
    }
  };

  const defaultValuesForList = [
    {
      title: translations.progressScreen.levelsCaps,
      data: [{data: []}],
      defaultIcon: (
        <LevelsIcon
          width={styles.defaultIcon.width}
          height={styles.defaultIcon.height}
        />
      ),
      defaultDesc: translations.progressScreen.defLevelDesc,
      defaultLink: translations.progressScreen.defLevelLink,
      defaultLinkFunc: () => goToWorkout(currentProgram, currentSession),
    },
    {
      title: translations.progressScreen.programsCaps,
      data: [{data: []}],
      defaultIcon: (
        <ProgramsIcon
          width={styles.defaultIcon.width}
          height={styles.defaultIcon.height}
        />
      ),
      defaultDesc: translations.progressScreen.defProgramDesc,
      defaultLink: translations.progressScreen.defProgramLink,
      defaultLinkFunc: () => goToWorkout(currentProgram, currentSession),
    },
    {
      title: translations.progressScreen.challenges,
      data: [{data: []}],
      defaultIcon: (
        <ChallengesIcon
          width={styles.defaultIcon.width}
          height={styles.defaultIcon.height}
        />
      ),
      defaultDesc: translations.progressScreen.defChallengeDesc,
      defaultLink: translations.progressScreen.defChallengeLink,
      defaultLinkFunc: goToChallenges,
    },
    // {
    //   title: translations.progressScreen.extraordinaryTrophies,
    //   data: [{data: []}],
    // },
  ];

  const sortedAchievements = achievments?.reduce((acc, item, index, arr) => {
    switch (item.achievmentType) {
      case 'level': {
        acc[0].data[0].data.push(item);
        break;
      }
      case 'program': {
        acc[1].data[0].data.push(item);
        break;
      }
      case 'challenge': {
        acc[2].data[0].data.push(item);
        break;
      }
      // case 'trophie': {
      //   acc[3].data[0].data.push(item);
      //   break;
      // }
    }
    return acc;
  }, defaultValuesForList);

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Header
          onPressBackButton={handleGoBack}
          title={translations.progressScreen.achievements}
        />
        <View style={styles.headerLine} />
        <SectionList
          sections={sortedAchievements}
          keyExtractor={(item, index) => item + index}
          renderItem={data => renderSection(data)}
          renderSectionHeader={({section: {title}}) =>
            renderSectionTitle(title)
          }
          contentContainerStyle={styles.container}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginTop: -20,
    marginBottom: moderateScale(32),
  },
  container: {
    paddingHorizontal: moderateScale(20),
  },
  sectionText: {
    color: GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    marginBottom: moderateScale(20),
  },
  genericBlackText: {
    color: BLACK,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
  },
  icon: {
    marginBottom: moderateScale(12),
    width: moderateScale(57),
    height: moderateScale(57),
  },
  rowView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
  },
  itemViewRect: {
    alignItems: 'center',
    marginBottom: moderateScale(30),
    marginHorizontal: moderateScale(10),
  },
  defaultItemContainer: {
    alignItems: 'center',
  },
  defaultIconContainer: {
    marginVertical: moderateScale(20),
  },
  defaultIcon: {
    width: moderateScale(100),
    height: moderateScale(100),
  },
  defaultText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
    textAlign: 'center',
    marginVertical: moderateScale(5),
  },
  defaultLink: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    color: LIGHT_ORANGE,
    textAlign: 'center',
    textDecorationLine: 'underline',
    marginBottom: moderateScale(75),
    marginVertical: moderateScale(5),
  },
});

export default Achievements;
