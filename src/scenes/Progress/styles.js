import {StyleSheet} from 'react-native';
import {calcFontSize, deviceWidth, moderateScale} from '../../utils/dimensions';
import {BLACK, GRAY, GRAY_BORDER, LIGHT_GRAY, WHITE} from '../../styles/colors';
import {
  MAIN_TITLE_FONT,
  MAIN_TITLE_FONT_REGULAR,
  SUB_TITLE_FONT,
} from '../../styles/fonts';

export const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginTop: -20,
    marginBottom: moderateScale(27),
  },
  container: {
    paddingHorizontal: moderateScale(20),
  },
  circularContent: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageIconCircle: {
    width: moderateScale(100),
    height: moderateScale(100),
  },
  imageIconDefault: {
    width: moderateScale(75),
    height: moderateScale(75),
    borderWidth: moderateScale(3),
    borderColor: GRAY_BORDER,
    transform: [{rotate: '45deg'}],
  },
  grayTextGeneric: {
    fontSize: calcFontSize(14),
    color: GRAY,
    fontFamily: SUB_TITLE_FONT,
    textAlign: 'center',
  },
  blackTextGeneric: {
    fontSize: calcFontSize(14),
    color: BLACK,
    fontFamily: SUB_TITLE_FONT,
  },
  blackTextSmall: {
    fontSize: calcFontSize(12),
  },
  valuesRowRoot: {
    marginTop: moderateScale(50),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  separateLine: {
    width: moderateScale(1),
    height: '100%',
    backgroundColor: GRAY_BORDER,
  },
  valuesBlackText: {
    color: BLACK,
    fontSize: calcFontSize(20),
    fontFamily: MAIN_TITLE_FONT_REGULAR,
  },
  valueBox: {
    alignItems: 'flex-start',
  },
  genericSeparateLineContainer: {
    width: '100%',
    alignSelf: 'center',
    height: moderateScale(1),
    backgroundColor: GRAY_BORDER,
    marginTop: moderateScale(35),
    marginBottom: moderateScale(25),
  },
  boldBlack: {
    fontFamily: MAIN_TITLE_FONT,
  },
  centerView: {
    alignSelf: 'center',
  },
  weeklyStreaksViewRoot: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: moderateScale(15),
  },
  valueBoxStreaks: {
    alignItems: 'center',
  },
  valueBoxBoldText: {
    color: BLACK,
    fontSize: calcFontSize(40),
    fontFamily: MAIN_TITLE_FONT,
  },
  resultsCardView: {
    marginBottom: moderateScale(45),
  },
  sectionTitle: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
    color: BLACK,
  },
  sectionDescription: {
    color: LIGHT_GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
    marginTop: moderateScale(8),
  },
  pieChartView: {
    marginTop: moderateScale(70),
    marginBottom: moderateScale(70),
  },
  resultsValuesRowRoot: {
    marginTop: moderateScale(20),
  },
  footerLine: {
    width: '100%',
    alignSelf: 'center',
    height: moderateScale(1),
    backgroundColor: GRAY_BORDER,
    marginBottom: moderateScale(10),
  },
  footerValueView: {
    marginTop: moderateScale(2),
    marginBottom: moderateScale(18),
  },
  radarChart: {
    marginTop: moderateScale(75),
    marginBottom: moderateScale(50),
    alignSelf: 'center',
  },
  initialValueTextCircle: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: moderateScale(10),
    bottom: 0,
    zIndex: 999,
  },
  finishValueTextCircle: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: -moderateScale(25),
    zIndex: 999,
  },
  bottomRowsRootView: {
    marginTop: moderateScale(50),
    marginBottom: moderateScale(20),
  },
  containerLabels: {
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
    position: 'absolute',
  },
  label: {
    position: 'absolute',
    textAlign: 'center',
    fontSize: calcFontSize(12),
    fontFamily: SUB_TITLE_FONT,
    color: BLACK,
    zIndex: 999,
  },
});
