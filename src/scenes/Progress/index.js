import React, {
  useCallback,
  useContext,
  useEffect,
  useState,
  useRef,
  useLayoutEffect,
} from 'react';
import {ScrollView, View, Text, Image, processColor} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Circle, Text as SvgText} from 'react-native-svg';
import gql from 'graphql-tag';
import {useLazyQuery} from '@apollo/react-hooks';
import {styles} from './styles';
import Header from '../../components/header';
import {LocalizationContext} from '../../localization/translations';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {moderateScale} from '../../utils/dimensions';
import {GRAY_BORDER, LIGHT_ORANGE} from '../../styles/colors';
import {useFocusEffect} from '@react-navigation/native';
import EagleIcon from '../../assets/images/profileIcon1.png';
import DefaultLevelIcon from '../../assets/icons/defaultLevel.svg';
import AchievementsCard from '../../components/ProgressScreen/AchievementsCard';
import ResultsCard from '../../components/ProgressScreen/ResultsCard';
import CardPieChart from '../../components/ProgressScreen/CardPieChart';
import RadarChartComponent from '../../components/ProgressScreen/RadarChart';
import LineChartComponent from '../../components/ProgressScreen/LineChart';
import {Loading} from '../../components/Loading';
import {AuthContext} from '../../../App';
import {Platforms, trackEvents} from '../../services/analytics';

const GET_USER = gql`
  query user($id: ID!, $locale: String) {
    user(id: $id, locale: $locale) {
      id
      level {
        id
        iconUrl
        name
        points
      }
      nextLevel {
        id
        points
      }
      points
      achievments {
        id
        iconUrl
        name
        achievmentType
      }
      currentWeeklyStreak
      bestWeeklyStreak
      currentProgram {
        id
        name
        user {
          id
          names
        }
        programCharacteristics {
          id
          value
          objective
        }
        sessions {
          id
          order
        }
        codeName
      }
      currentSession {
        id
        imageUrl
        name
        description
        order
        timeDuration
        calories
        reps
        bodyPartsFocused
      }
    }
  }
`;

const calcTintCircularProgress = (
  currentLevel = 0,
  currentPoints = 0,
  nextLevel = 0,
) => {
  return ((currentPoints - currentLevel) / (nextLevel - currentLevel)) * 100;
};

const Progress = ({navigation}) => {
  const {translations, appLanguage} = useContext(LocalizationContext);
  const {userData: userInfo} = useContext(AuthContext);

  const [getSummary, {data, error, loading}] = useLazyQuery(GET_USER, {
    variables: {id: userInfo?.id, locale: appLanguage},
    fetchPolicy: 'network-only',
    onCompleted: () => {
      animatedCircularProgressRef.current.reAnimate(
        0,
        calcTintCircularProgress(
          data?.user?.level?.points,
          data?.user?.points,
          data?.user?.nextLevel?.points,
        ),
        1500,
      );
    },
  });

  useFocusEffect(useCallback(() => getSummary(), [getSummary]));

  useLayoutEffect(() => {
    trackEvents({eventName: 'Load Progress'});
  }, []);

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      animatedCircularProgressRef.current.reAnimate(0, 0, 0);
    });
    return unsubscribe;
  }, [navigation]);

  console.log(error);

  const [isLoaderVisible, setIsLoaderVisible] = useState(true);

  const animatedCircularProgressRef = useRef();

  const user = data?.user;
  const level = user?.level;
  const nextLevel = user?.nextLevel;
  const currentSession = user?.currentSession;
  const currentProgram = user?.currentProgram;

  const handleGoBack = useCallback(() => navigation.goBack(), [navigation]);
  const renderBodyCircle = useCallback(
    () => (
      <View>
        <View style={{alignItems: 'center'}}>
          {level?.iconUrl ? (
            <Image
              source={{uri: level?.iconUrl}}
              style={styles.imageIconCircle}
            />
          ) : (
            <View style={styles.imageIconDefault} />
          )}
          {/* <Text style={styles.grayTextGeneric}>{level?.name}</Text> */}
        </View>
      </View>
    ),
    [level],
  );

  useEffect(() => {
    setIsLoaderVisible(false);
  }, []);

  console.log(user?.achievments);

  const onPressAchievementsCard = useCallback(
    () =>
      navigation.navigate('Achievements', {
        achievments: user?.achievments,
        currentSession,
        currentProgram,
      }),
    [currentProgram, currentSession, navigation, user],
  );

  const mockResultsFooter = [
    {
      id: 'egseg',
      title: translations.progressScreen.pushUps,
      value: 35,
    },
    {
      id: 'segeses',
      title: translations.progressScreen.squats,
      value: 50,
    },
    {
      id: 'dwaeg',
      title: translations.progressScreen.plankBalance,
      value: 19,
    },
    {
      id: 'faesf',
      title: translations.progressScreen.burpee,
      value: 22,
    },
    {
      id: 'tjyk',
      title: translations.progressScreen.dominated,
      value: 22,
    },
    {
      id: 'zsfse',
      title: translations.progressScreen.effortTolerance,
      value: 597,
    },
  ];

  const pointsForNextLevel = nextLevel?.points - user?.points;
  const achievments = user?.achievments?.slice(0, 3);
  console.log(user);

  // console.log('achievements', achievements);
  // console.log(user);

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Header
          onPressBackButton={handleGoBack}
          title={translations.progressScreen.progression}
        />
        <View style={styles.headerLine} />
        {!isLoaderVisible ? (
          <View style={styles.container}>
            <View>
              <View style={{alignSelf: 'center'}}>
                {/* <View style={styles.initialValueTextCircle}>
                  <Text style={styles.grayTextGeneric}>
                    {!isNaN(pointsForNextLevel) && pointsForNextLevel + 'p'}
                  </Text>
                </View>
                <View style={styles.finishValueTextCircle}>
                  <Text style={[styles.grayTextGeneric, {color: LIGHT_ORANGE}]}>
                    {user?.points >= 0 && user?.points + 'p'}
                  </Text>
                </View> */}
                <AnimatedCircularProgress
                  padding={1}
                  size={moderateScale(156)}
                  width={moderateScale(3)}
                  fill={0}
                  tintColor={LIGHT_ORANGE}
                  backgroundColor={GRAY_BORDER}
                  rotation={0}
                  duration={0}
                  style={styles.circularContent}
                  ref={animatedCircularProgressRef}>
                  {() => renderBodyCircle()}
                </AnimatedCircularProgress>
              </View>
            </View>
            <View style={styles.valuesRowRoot}>
              <View style={styles.valueBox}>
                <Text style={styles.grayTextGeneric}>
                  {translations.progressScreen.level}
                </Text>
                <Text style={styles.valuesBlackText}>{level?.name ?? 0}</Text>
              </View>
              <View style={styles.separateLine} />
              <View style={styles.valueBox}>
                <Text style={styles.grayTextGeneric}>
                  {translations.progressScreen.points}
                </Text>
                <Text style={styles.valuesBlackText}>{user?.points ?? 0}</Text>
              </View>
              <View style={styles.separateLine} />
              <View style={styles.valueBox}>
                <Text style={styles.grayTextGeneric}>
                  {translations.progressScreen.nextLevel}
                </Text>
                <Text style={styles.valuesBlackText}>
                  {nextLevel?.points ?? 0}
                </Text>
              </View>
            </View>

            <View style={styles.genericSeparateLineContainer} />

            <View style={styles.centerView}>
              <Text style={[styles.valuesBlackText, styles.boldBlack]}>
                {translations.progressScreen.weaklyStreaks}
              </Text>
            </View>

            <View style={styles.weeklyStreaksViewRoot}>
              <View style={styles.valueBoxStreaks}>
                <Text style={styles.valueBoxBoldText}>
                  {user?.currentWeeklyStreak ?? 0}
                </Text>
                <Text style={styles.blackTextGeneric}>
                  {translations.progressScreen.weeksInRow}
                </Text>
              </View>
              <View style={styles.separateLine} />
              <View style={styles.valueBoxStreaks}>
                <Text style={styles.valueBoxBoldText}>
                  {user?.bestWeeklyStreak ?? 0}
                </Text>
                <Text style={styles.blackTextGeneric}>
                  {translations.progressScreen.yourBestStreak}
                </Text>
              </View>
            </View>

            <AchievementsCard
              onPress={onPressAchievementsCard}
              data={achievments}
            />

            {/* <View style={styles.resultsCardView}>
              <ResultsCard
                totalSessionsCount={123}
                kcalCountSession={600}
                repsCountSession={203}
                timeTotalCount={"'10h 05'"}
              />
            </View>

            <Text style={styles.sectionTitle}>
              {translations.progressScreen.monthlyActivity}
            </Text>
            <Text style={styles.sectionDescription}>
              You have done 7 sessions so far this month, compared to 4 last
              month at this time
            </Text>

            <LineChartComponent />

            <View style={styles.pieChartView}>
              <CardPieChart
                title={translations.progressScreen.physicalAbilities}
                description={
                  translations.progressScreen.physicalAbilitiesDescription
                }
              />
            </View>

            <Text style={styles.sectionTitle}>
              {translations.progressScreen.youHaveTrained}
            </Text>
            <Text style={styles.sectionDescription}>
              {translations.progressScreen.physicalAbilitiesDescription}
            </Text>

            <View>
              <RadarChartComponent data={dataForRadarChart} />
            </View>

            <View style={styles.bottomRowsRootView}>
              <Text style={styles.sectionTitle}>
                {translations.progressScreen.latestTestResults}
              </Text>
              <View style={styles.resultsValuesRowRoot}>
                {mockResultsFooter.map((item, key) => (
                  <View key={item.id}>
                    <Text style={styles.grayTextGeneric}>{item.title}</Text>
                    <View style={styles.footerValueView}>
                      <Text
                        style={[
                          styles.blackTextGeneric,
                          styles.blackTextSmall,
                        ]}>
                        {item.value} rep/min
                      </Text>
                    </View>
                    {key !== mockResultsFooter.length - 1 && (
                      <View style={styles.footerLine} />
                    )}
                  </View>
                ))}
              </View>
            </View> */}
          </View>
        ) : (
          <Loading />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default Progress;
