const bodyfatImages = {
  male: [
    {
      bodyFatPercentage: 10,
      source: require('../../assets/images/bodyfat-picker/men_1-b4e1c88fc4c1a82e8ccfebb11a8b9d32_10.jpg'),
    },
    {
      bodyFatPercentage: 15,
      source: require('../../assets/images/bodyfat-picker/men_2-dfb0ad44bb4894cff38f7f5589405a09.jpg'),
    },
    {
      bodyFatPercentage: 20,
      source: require('../../assets/images/bodyfat-picker/men_3-69602966b4159c1cb6683273a7ca2dca.jpg'),
    },
    {
      bodyFatPercentage: 25,
      source: require('../../assets/images/bodyfat-picker/men_4-80e2fb318f88d2996ee2ae9269d178a9.jpg'),
    },
    {
      bodyFatPercentage: 30,
      source: require('../../assets/images/bodyfat-picker/men_5-4ab4324c674970bc37b90cea8937a758.jpg'),
    },
    {
      bodyFatPercentage: 35,
      source: require('../../assets/images/bodyfat-picker/men_6-2d17cab506810fb0e2b7878aafa4e969.jpg'),
    },
    {
      bodyFatPercentage: 40,
      source: require('../../assets/images/bodyfat-picker/men_7-a93fe7cb6bc167ad7bdbaeba8b759391.jpg'),
    },
    {
      bodyFatPercentage: 45,
      source: require('../../assets/images/bodyfat-picker/men_8-391c79f7b6987f8495eb7b413ccc8af4.jpg'),
    },
    {
      bodyFatPercentage: 50,
      source: require('../../assets/images/bodyfat-picker/men_9-ced322ab38b3ee28dbe60cff81733f47_50.jpg'),
    },
  ],
  female: [
    {
      bodyFatPercentage: 10,
      source: require('../../assets/images/bodyfat-picker/woman_1-9abf5fb76f6f891ae8cc2c01b8ba976b_10.jpg'),
    },
    {
      bodyFatPercentage: 15,
      source: require('../../assets/images/bodyfat-picker/woman_2-e4e5f25618cf7cd594cd80a0029398d6.jpg'),
    },
    {
      bodyFatPercentage: 20,
      source: require('../../assets/images/bodyfat-picker/woman_3-c9cf251fd5f3d67ce14c7b247e6e0c65.jpg'),
    },
    {
      bodyFatPercentage: 25,
      source: require('../../assets/images/bodyfat-picker/woman_4-6a1c39620810d328b9ed19a656c2b9e7.jpg'),
    },
    {
      bodyFatPercentage: 30,
      source: require('../../assets/images/bodyfat-picker/woman_5-f3ad7e6f77ae59eb44d725dc20314337.jpg'),
    },
    {
      bodyFatPercentage: 35,
      source: require('../../assets/images/bodyfat-picker/woman_6-de51d5a5c7af31f2ca9250ab737dc7ce.jpg'),
    },
    {
      bodyFatPercentage: 40,
      source: require('../../assets/images/bodyfat-picker/woman_7-169bd2f4d57f1031a3a7338cd4a7d6bb.jpg'),
    },
    {
      bodyFatPercentage: 45,
      source: require('../../assets/images/bodyfat-picker/woman_8-661cc944dd11ccedbe528590fc2b0d68_45.jpg'),
    },
  ],
};

export default bodyfatImages;
