import React, {useCallback, useState, useRef, useContext} from 'react';
import {View, Text, TouchableOpacity, Platform} from 'react-native';
import {Icon} from 'react-native-elements';
import moment from 'moment';
import PropTypes from 'prop-types';
import SegmentedPicker from 'react-native-segmented-picker';
import DatePicker from 'react-native-datepicker';
import {LocalizationContext} from '../../localization/translations';
import styles from './styles';
import {WHITE, GRAY_BORDER, BLACK, ORANGE} from '../../styles/colors';
import {
  weightSystems,
  weightValuesMetric,
  weightValuesImperial,
  heightValuesMetric,
  heightValuesImperial,
  heightSystems,
} from './weightHeightData';

const QuestionForm = ({translations, responses, setResponses, position}) => {
  const [date, setDate] = useState(new Date(2000, 0, 1));
  const [wheelsPickersState, setWheelsPickersState] = useState({
    selectedWeightSystem: 'kg',
    selectedHeightSystem: 'cm',
    initialSelectionsWeight: {
      weight_value: '60',
      weight_system: 'kg',
    },
    initialSelectionsHeight: {
      height_value: '160',
      height_system: 'cm',
    },
  });

  const datePickerRef = useRef(null);

  const {appLanguage} = useContext(LocalizationContext);

  const onChange = (event, selectedDate) => {
    console.log('selected date:', selectedDate);
    setDate(selectedDate);
    setResponses({
      ...responses,
      dateOfBirth: moment(selectedDate).format('DD/MM/YYYY'),
    });
  };

  console.log('date', date);

  const questionFilled = !!responses.weight && !!responses.height;
  const questionActive =
    Math.max(...responses.answeredPositions) === position - 1 || position === 1;

  const getCheckStyles = () => {
    if (questionFilled) {
      return styles.circleCheck;
    }
    // if (questionActive) {
    //   return styles.activeCheck;
    // }
    return styles.circleCheckUnfilled;
  };

  const checkStyles = getCheckStyles();

  const weightSegmentedPicker = React.createRef();
  const heightSegmentedPicker = React.createRef();

  const onConfirmWeight = selections => {
    setResponses({
      ...responses,
      answeredPositions: [
        ...responses.answeredPositions,
        responses.height ? position : position - 1,
      ],
      weight: `${selections.weight_value} ${selections.weight_system}`,
      dateOfBirth: moment(date).format('DD/MM/YYYY'),
    });
    setWheelsPickersState({
      ...wheelsPickersState,
      initialSelectionsWeight: {
        ...wheelsPickersState.initialSelectionsWeight,
        weight_system: selections.weight_system,
        weight_value: selections.weight_value,
      },
    });
  };

  const onConfirmHeight = selections => {
    setResponses({
      ...responses,
      answeredPositions: [
        ...responses.answeredPositions,
        responses.weight ? position : position - 1,
      ],
      height: `${selections.height_value} ${selections.height_system}`,
      dateOfBirth: moment(date).format('DD/MM/YYYY'),
    });
    setWheelsPickersState({
      ...wheelsPickersState,
      initialSelectionsHeight: {
        ...wheelsPickersState.initialSelectionsHeight,
        height_system: selections.height_system,
        height_value: selections.height_value,
      },
    });
  };

  const handleValueChangeWheel = useCallback(
    (column, value) => {
      switch (column) {
        case 'weight_system':
          {
            setWheelsPickersState({
              ...wheelsPickersState,
              selectedWeightSystem: value,
            });
          }
          break;
        case 'height_system':
          {
            setWheelsPickersState({
              ...wheelsPickersState,
              selectedHeightSystem: value,
            });
          }
          break;
        default:
          return;
      }
    },
    [wheelsPickersState],
  );

  return (
    <View style={styles.questionContainerForm}>
      <View>
        <View style={checkStyles}>
          {questionFilled && <Icon name="check" color="#ffffff" />}
        </View>
      </View>
      <View style={styles.questionContent}>
        <Text style={styles.questionText}>
          {translations.onBoarding.question4.title}
        </Text>
        <View>
          <View style={styles.formContainer}>
            <View style={styles.formTextContainer}>
              <Text style={styles.formText}>
                {translations.onBoarding.question4.option1}
              </Text>
              <DatePicker
                date={new Date(date)}
                onDateChange={onChange}
                androidMode="spinner"
                format={'DD MMM. YYYY'}
                ref={datePickerRef}
                locale={appLanguage}
                confirmBtnText="OK"
                cancelBtnText="Cancel"
                maxDate={new Date()}
                showIcon={false}
                customStyles={{
                  dateInput: styles.dateWrapper,
                  dateText: [styles.formTextInput, styles.dateText],
                  datePicker: {justifyContent: 'center'},
                }}
              />
            </View>

            <SegmentedPicker
              defaultSelections={wheelsPickersState.initialSelectionsWeight}
              ref={weightSegmentedPicker}
              onValueChange={({column, value}) =>
                handleValueChangeWheel(column, value)
              }
              confirmTextColor={ORANGE}
              confirmText={translations.onBoarding.done + ' '}
              toolbarBackgroundColor={WHITE}
              toolbarBorderColor={GRAY_BORDER}
              onConfirm={onConfirmWeight}
              options={[
                wheelsPickersState.selectedWeightSystem === 'kg'
                  ? weightValuesMetric
                  : weightValuesImperial,
                weightSystems,
              ]}
              size={0.5}
            />
            <View style={styles.formTextContainer}>
              <Text style={styles.formText}>
                {translations.onBoarding.question4.option2}
              </Text>
              <View style={styles.fieldWrapper}>
                <TouchableOpacity
                  onPress={() => weightSegmentedPicker.current.show()}>
                  <Text style={styles.formTextInput}>
                    {responses.weight
                      ? responses.weight
                      : translations.onBoarding.selectWeight}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <SegmentedPicker
              defaultSelections={wheelsPickersState.initialSelectionsHeight}
              ref={heightSegmentedPicker}
              onValueChange={({column, value}) =>
                handleValueChangeWheel(column, value)
              }
              confirmText={translations.onBoarding.done + ' '}
              confirmTextColor={ORANGE}
              toolbarBackgroundColor={WHITE}
              toolbarBorderColor={GRAY_BORDER}
              pickerItemTextColor={BLACK}
              selectionBorderColor={GRAY_BORDER}
              onConfirm={onConfirmHeight}
              options={[
                wheelsPickersState.selectedHeightSystem === 'cm'
                  ? heightValuesMetric
                  : heightValuesImperial,
                heightSystems,
              ]}
              size={0.5}
            />
            <View style={styles.formTextContainer}>
              <Text style={styles.formText}>
                {translations.onBoarding.question4.option3}
              </Text>
              <View style={styles.fieldWrapper}>
                <TouchableOpacity
                  onPress={() => heightSegmentedPicker.current.show()}>
                  <Text style={styles.formTextInput}>
                    {responses.height
                      ? responses.height
                      : translations.onBoarding.selectHeight}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

QuestionForm.propTypes = {
  responses: PropTypes.object,
  setResponses: PropTypes.func,
  translations: PropTypes.object,
  position: PropTypes.number,
};

export default QuestionForm;
