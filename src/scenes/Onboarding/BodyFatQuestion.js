import React, {useCallback} from 'react';
import {View, Text} from 'react-native';
import {Icon} from 'react-native-elements';
import PropTypes from 'prop-types';
import styles from './styles';
import BodyfatPicker from './BodyfatPicker';

const BodyFatQuestion = ({
  gender,
  setBodyFat,
  isFilled,
  position,
  responses,
  translations,
}) => {
  const questionActive =
    Math.max(...responses.answeredPositions) === position - 1 || position === 1;

  const getCheckStyles = () => {
    if (isFilled) {
      return styles.circleCheck;
    }
    // if (questionActive) {
    //   return styles.activeCheck;
    // }
    return styles.circleCheckUnfilled;
  };

  const checkStyles = getCheckStyles();
  return (
    <View style={styles.questionContainer}>
      <View>
        <View style={checkStyles}>
          {isFilled && <Icon name="check" color="#ffffff" />}
        </View>
      </View>
      <View style={styles.questionContent}>
        <Text style={styles.questionText}>
          {translations.onBoarding.question5}
        </Text>
        <BodyfatPicker
          gender={gender}
          setBodyFat={setBodyFat}
          currentValue={responses.bodyFat}
          showValue={true}
        />
      </View>
    </View>
  );
};

BodyFatQuestion.propTypes = {
  gender: PropTypes.string,
  setBodyFat: PropTypes.func,
  isFilled: PropTypes.bool,
  translations: PropTypes.object,
  position: PropTypes.number,
};

export default BodyFatQuestion;
