import React, {useEffect} from 'react';
import {View, Image, StyleSheet, Platform} from 'react-native';
import Animated from 'react-native-reanimated';
import {SUB_TITLE_FONT} from '../../styles/fonts';
import {LIGHT_GRAY, ORANGE} from '../../styles/colors';
import bodyfatImages from './bodyfatImages';
import {moderateScale} from '../../utils/dimensions';

const IMAGE_WIDTH = 140;
let dragStarted = false;
let isScrollTo = false;
let momentumStarted = false;
let timer = null;
let currentValueX = 0;

const BodyFatPicker = ({setBodyFat, gender, showValue, currentValue}) => {
  const targetGender = gender || 'female';

  const onScrollEnd = ({nativeEvent}) => {
    const scrollX = nativeEvent.contentOffset.x;
    const activeIndex = Math.round(scrollX / IMAGE_WIDTH);
    const selectedPosition = activeIndex * IMAGE_WIDTH;
    if (selectedPosition !== scrollX) {
      if (Platform.OS === 'ios') {
        isScrollTo = true;
      }
      scrollFix(selectedPosition);
    }
    const bodyfatPercentage =
      bodyfatImages[targetGender][activeIndex]?.bodyFatPercentage;
    setBodyFat(bodyfatPercentage);
  };

  const scrollFix = x => {
    scrollViewRef.current._component.scrollTo({
      x,
      y: 0,
      animated: true,
    });
  };

  const scrollX = React.useRef(new Animated.Value(0)).current;

  const scrollViewRef = React.useRef();

  useEffect(() => {
    const index = bodyfatImages[targetGender].findIndex(
      el => el.bodyFatPercentage === currentValue,
    );
    index >= 0 && (currentValueX = IMAGE_WIDTH * index);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.scrollViewContainer}>
      <Animated.ScrollView
        onScroll={Animated.event(
          [{nativeEvent: {contentOffset: {x: scrollX}}}],
          {
            useNativeDriver: true,
          },
        )}
        onScrollBeginDrag={() => {
          dragStarted = true;
          if (Platform.OS === 'ios') {
            isScrollTo = false;
          }
          timer && clearTimeout(timer);
        }}
        onScrollEndDrag={({nativeEvent}) => {
          dragStarted = false;
          timer && clearTimeout(timer);
          let callback = e => {
            if (!momentumStarted && !dragStarted) {
              onScrollEnd(e);
            }
          };
          timer = setTimeout(callback, 10, {nativeEvent});
        }}
        onMomentumScrollBegin={() => {
          momentumStarted = true;
          timer && clearTimeout(timer);
        }}
        onMomentumScrollEnd={nativeEvent => {
          momentumStarted = false;
          if (!isScrollTo && !momentumStarted && !dragStarted) {
            onScrollEnd(nativeEvent);
          }
        }}
        horizontal={true}
        ref={scrollViewRef}
        onContentSizeChange={() => {
          scrollViewRef.current._component.scrollTo({
            x: currentValueX ?? 280,
            y: 0,
            animated: false,
          });
        }}
        showsHorizontalScrollIndicator={false}
        scrollEventThrottle={16}
        contentContainerStyle={styles.container}>
        {bodyfatImages[targetGender].map((image, idx) => {
          const inputRange = [
            IMAGE_WIDTH * (idx - 1),
            IMAGE_WIDTH * idx,
            IMAGE_WIDTH * (idx + 1),
          ];

          // const isLastSlide = idx === bodyfatImages[gender].length - 1;

          console.log(image.bodyFatPercentage, showValue, currentValue);
          return (
            <Animated.View
              key={image.bodyFatPercentage}
              style={[
                {
                  opacity: scrollX.interpolate({
                    inputRange,
                    outputRange: [0.3, 1, 0.3],
                  }),
                },
                {
                  transform: [
                    {
                      scale: scrollX.interpolate({
                        inputRange,
                        outputRange: [0.6, 1, 0.6],
                      }),
                    },
                  ],
                },
                image.bodyFatPercentage === currentValue &&
                  styles.borderActiveItem,
              ]}>
              <Image
                style={[styles.image, currentValue === showValue]}
                source={image.source}
                resizeMode="contain"
              />
              {showValue && (
                <Animated.Text
                  style={[
                    styles.bfTextActive,
                    {
                      fontSize: scrollX.interpolate({
                        inputRange,
                        outputRange: [15, 25, 15],
                      }),
                    },
                  ]}>{`${image.bodyFatPercentage}%`}</Animated.Text>
              )}
            </Animated.View>
          );
        })}
      </Animated.ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    paddingLeft: 80,
    paddingRight: 80,
    marginTop: 20,
    //marginBottom: 50,
  },
  image: {
    width: IMAGE_WIDTH,
    height: 250,
  },
  bfTextActive: {
    fontSize: 20,
    textAlign: 'center',
    color: LIGHT_GRAY,
    fontWeight: 'bold',
    fontFamily: SUB_TITLE_FONT,
  },
  scrollViewContainer: {
    height: 330,
    paddingRight: 50,
  },
  borderActiveItem: {
    borderColor: ORANGE,
    borderWidth: moderateScale(1),
    borderRadius: moderateScale(25),
    overflow: 'hidden',
  },
});

export default BodyFatPicker;
