const weightItemsMetric = [];
for (let i = 40; i < 151; i++) {
  weightItemsMetric.push({label: i.toString(), value: i.toString()});
}

const weightItemsImperial = [];
for (let j = 85; j < 351; j++) {
  weightItemsImperial.push({label: j.toString(), value: j.toString()});
}

const heightItemsMetric = [];
for (let k = 130; k < 251; k++) {
  heightItemsMetric.push({label: k.toString(), value: k.toString()});
}

const heightItemsImperial = [];
for (let n = 4; n < 9; n++) {
  for (let x = 0; x < 12; x++) {
    heightItemsImperial.push({
      label: `${n}'${x}"`,
      value: `${n}'${x}"`,
    });
  }
}

export const weightSystems = {
  key: 'weight_system',
  items: [{label: 'Kg', value: 'kg'}, {label: 'lbs', value: 'lbs'}],
};

export const heightSystems = {
  key: 'height_system',
  items: [{label: 'Cm', value: 'cm'}, {label: 'ft', value: 'ft'}],
};

export const weightValuesMetric = {
  key: 'weight_value',
  items: weightItemsMetric,
};

export const weightValuesImperial = {
  key: 'weight_value',
  items: weightItemsImperial,
};

export const heightValuesMetric = {
  key: 'height_value',
  items: heightItemsMetric,
};

export const heightValuesImperial = {
  key: 'height_value',
  items: heightItemsImperial,
};
