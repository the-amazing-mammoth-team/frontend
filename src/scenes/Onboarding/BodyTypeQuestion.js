import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Image, Platform} from 'react-native';
import {Icon} from 'react-native-elements';
import Animated from 'react-native-reanimated';
import PropTypes from 'prop-types';
import styles from './styles';
import Strong from '../../assets/icons/Grande.svg';
import Medium from '../../assets/icons/Mediana.svg';
import Small from '../../assets/icons/Pequeña.svg';
import {LIGHT_GRAY, GRAY, ORANGE, WHITE} from '../../styles/colors';
import {SUB_TITLE_FONT} from '../../styles/fonts';
import {calcHeight, moderateScale} from '../../utils/dimensions';

const IMAGE_WIDTH = 170;
let dragStarted = false;
let isScrollTo = false;
let momentumStarted = false;
let timer = null;
let currentValueX = 0;

const BodyTypeQuestion = ({
  question,
  setResponses,
  responses,
  isFilled,
  position,
  translations,
  showLabel,
  currentValue,
}) => {
  const setResponse = responseValue => {
    setResponses({
      ...responses,
      answeredPositions: [...responses.answeredPositions, position],
      [question.value]: responseValue,
    });
  };

  const [label, setLabel] = useState(translations.onBoarding.question6.option1);

  const onScrollEnd = ({nativeEvent}) => {
    const scrollX = nativeEvent.contentOffset.x;
    const activeIndex = Math.round(scrollX / IMAGE_WIDTH);
    const selectedPosition = activeIndex * IMAGE_WIDTH;
    if (selectedPosition !== scrollX) {
      if (Platform.OS === 'ios') {
        isScrollTo = true;
      }
      scrollFix(selectedPosition);
    }
    const item = wristImages[activeIndex];
    setResponse(item?.value);
    setLabel(item?.label);
  };

  const scrollFix = x => {
    scrollViewRef.current._component.scrollTo({
      x,
      y: 0,
      animated: true,
    });
  };

  const scrollX = React.useRef(new Animated.Value(0)).current;

  const scrollViewRef = React.useRef();

  const {bodyType} = responses;

  const wristImages = [
    {
      id: 'id-first',
      icon: (
        <Small
          height={moderateScale(IMAGE_WIDTH)}
          width={moderateScale(IMAGE_WIDTH)}
          fill={LIGHT_GRAY}
        />
      ),
      value: 'lean',
      label: translations.onBoarding.question6.option1,
    },
    {
      id: 'id-second',
      icon: (
        <Medium
          height={moderateScale(IMAGE_WIDTH)}
          width={moderateScale(IMAGE_WIDTH)}
          fill={LIGHT_GRAY}
        />
      ),
      value: 'medium',
      label: translations.onBoarding.question6.option2,
    },
    {
      id: 'id-third',
      icon: (
        <Strong
          height={moderateScale(IMAGE_WIDTH)}
          width={moderateScale(IMAGE_WIDTH)}
          fill={LIGHT_GRAY}
        />
      ),
      value: 'strong',
      label: translations.onBoarding.question6.option3,
    },
  ];

  const labels = {
    lean: translations.onBoarding.question6.labelSmall,
    medium: translations.onBoarding.question6.labelMedian,
    strong: translations.onBoarding.question6.labelLarge,
  };

  const questionActive =
    Math.max(...responses.answeredPositions) === position - 1 || position === 1;

  const getCheckStyles = () => {
    if (isFilled) {
      return styles.circleCheck;
    }
    // if (questionActive) {
    //   return styles.activeCheck;
    // }
    return styles.circleCheckUnfilled;
  };

  const checkStyles = getCheckStyles();

  useEffect(() => {
    const index = wristImages.findIndex(el => el.value === currentValue);
    index >= 0 && (currentValueX = IMAGE_WIDTH * index);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.questionContainer}>
      {showLabel && (
        <View>
          <View style={checkStyles}>
            {isFilled && <Icon name="check" color="#ffffff" />}
          </View>
        </View>
      )}
      <View style={styles.questionContent}>
        {showLabel && (
          <>
            <Text style={styles.questionText}>{question.label}</Text>
            <Text style={localStyles.legend}>
              {label && `${label}, ${labels[bodyType]}`}
            </Text>
          </>
        )}
        <Animated.ScrollView
          onScroll={Animated.event(
            [{nativeEvent: {contentOffset: {x: scrollX}}}],
            {
              useNativeDriver: true,
            },
          )}
          onScrollBeginDrag={() => {
            dragStarted = true;
            if (Platform.OS === 'ios') {
              isScrollTo = false;
            }
            timer && clearTimeout(timer);
          }}
          onScrollEndDrag={({nativeEvent}) => {
            dragStarted = false;
            timer && clearTimeout(timer);
            let callback = e => {
              if (!momentumStarted && !dragStarted) {
                onScrollEnd(e);
              }
            };
            timer = setTimeout(callback, 10, {nativeEvent});
          }}
          onMomentumScrollBegin={() => {
            momentumStarted = true;
            timer && clearTimeout(timer);
          }}
          onMomentumScrollEnd={nativeEvent => {
            momentumStarted = false;
            if (!isScrollTo && !momentumStarted && !dragStarted) {
              onScrollEnd(nativeEvent);
            }
          }}
          horizontal={true}
          ref={scrollViewRef}
          onContentSizeChange={() => {
            scrollViewRef.current._component.scrollTo({
              x: currentValueX ?? 280,
              y: 0,
              animated: false,
            });
          }}
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          contentContainerStyle={localStyles.scrollview}>
          {wristImages.map((image, idx) => {
            const inputRange = [
              IMAGE_WIDTH * (idx - 1),
              IMAGE_WIDTH * idx,
              IMAGE_WIDTH * (idx + 1),
            ];
            return (
              <Animated.View
                key={image.id}
                style={[
                  {
                    opacity: scrollX.interpolate({
                      inputRange,
                      outputRange: [0.8, 1, 0.8],
                    }),
                  },
                  {
                    transform: [
                      {
                        scale: scrollX.interpolate({
                          inputRange,
                          outputRange: [0.75, 1, 0.75],
                        }),
                      },
                    ],
                  },
                  localStyles.iconContainer,
                  image.value === currentValue && {
                    borderColor:
                      responses[question.value] === image.value
                        ? ORANGE
                        : WHITE,
                  },
                ]}>
                {image.icon}
              </Animated.View>
            );
          })}
        </Animated.ScrollView>
      </View>
    </View>
  );
};

const localStyles = StyleSheet.create({
  svgContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
  },
  legend: {
    color: GRAY,
    fontSize: 16,
    marginTop: 10,
    fontFamily: SUB_TITLE_FONT,
    height: calcHeight(60),
  },
  iconContainer: {
    borderColor: WHITE,
    marginTop: moderateScale(10),
    marginRight: moderateScale(20),
    borderRadius: moderateScale(25),
    borderWidth: moderateScale(1),
    paddingVertical: moderateScale(11),
    paddingHorizontal: moderateScale(11),
  },
  scrollview: {
    marginBottom: moderateScale(100),
  },
});

BodyTypeQuestion.propTypes = {
  responses: PropTypes.object,
  setResponses: PropTypes.func,
  translations: PropTypes.object,
  responseTitles: PropTypes.array,
  isFilled: PropTypes.bool,
  position: PropTypes.number,
};

export default BodyTypeQuestion;
