import React, {useState, useLayoutEffect, useContext} from 'react';
import {SafeAreaView, View, Text, ScrollView} from 'react-native';
import Mixpanel from 'react-native-mixpanel';
import styles from './styles';
import BodyFatQuestion from './BodyFatQuestion';
import {NextButton} from '../../components/buttons';
import BodyTypeQuestion from './BodyTypeQuestion';
import Question from './Question';
import QuestionForm from './QuestionForm';
import {LocalizationContext} from '../../localization/translations';
import {
  Platforms,
  trackEvents,
  trackSuperProperties,
} from '../../services/analytics';

const activeLevels = [
  {analyticName: 'sedentary', name: 'sedentary'},
  {analyticName: 'active', name: 'medium_active'},
  {analyticName: 'very active', name: 'very_active'},
];

// {
//   label: translations.onBoarding.question1.option1,
//   value: 'loss_weight',
// },
// {
//   label: translations.onBoarding.question1.option2,
//   value: 'gain_muscle',
// },
// {
//   label: translations.onBoarding.question1.option3,
//   value: 'antiaging',

const goals = [
  {analyticName: 'lose Fat', name: 'loss_weight'},
  {analyticName: 'gain muscle', name: 'gain_muscle'},
  {analyticName: 'live longer and Healthier', name: 'antiaging'},
];

const bodyTypes = [
  {analyticName: 'wrist small ', name: 'lean'},
  {analyticName: 'wrist middle', name: 'medium'},
  {analyticName: 'wrist wide', name: 'strong'},
];

const Onboarding = ({navigation}) => {
  const [responses, setResponses] = useState({
    height: '', // 100
    weight: '', //100,
    goal: '', // 'gain_muscle',
    gender: '',
    bodyType: 'lean',
    activityLevel: '',
    dateOfBirth: '',
    bodyFat: 20.0,
    answeredPositions: [],
  });

  const {translations, appLanguage} = useContext(LocalizationContext);

  useLayoutEffect(() => {
    trackEvents({
      eventName: 'Load Onboarding',
      platforms: {1: Platforms.mixPanel},
    });
  }, []);

  const nextStep = () => {
    const weightSplit = responses.weight.split(' ');
    let weightNumber = Number(weightSplit[0]);
    const weightUnit = weightSplit[1];
    const heightSplit = responses.height.split(' ');
    let heightNumber = heightSplit[0];
    const heightUnit = heightSplit[1];

    if (weightUnit !== 'kg') {
      weightNumber = weightNumber / 2.205;
    }

    if (heightUnit !== 'cm') {
      const feet = Number(heightNumber.split("'")[0]);
      const inches = Number(heightNumber.split("'")[1].replace('"', ''));
      heightNumber = feet * 30.48 + inches * 2.54;
    }

    heightNumber = Number(heightNumber);

    const measuringSystem =
      weightUnit === 'kg' && heightUnit === 'cm' ? 'metric' : 'imperial';

    const {
      goal,
      activityLevel,
      gender,
      dateOfBirth,
      bodyFat,
      bodyType,
    } = responses;

    trackSuperProperties({
      platforms: {1: Platforms.mixPanel},
      properties: {
        goal: goals.find(el => el.name === goal)?.analyticName,
        activity_level: activeLevels.find(el => el.name === activityLevel)
          ?.analyticName,
        gender,
        birthday: dateOfBirth,
        height_cm: heightNumber,
        weight_gr: weightNumber,
        units: measuringSystem,
        body_fat: bodyFat,
        constitution: bodyTypes.find(el => el.name === bodyType)?.analyticName,
        locale: appLanguage,
      },
    });

    navigation.navigate('SignupServices', {
      onboardingData: {
        ...responses,
        weight: weightNumber,
        height: heightNumber,
        measuringSystem: measuringSystem,
        language: 'es',
      },
    });
  };

  const setBodyFat = bodyFat => {
    setResponses({
      ...responses,
      answeredPositions: [...responses.answeredPositions, 5],
      bodyFat: bodyFat,
    });
  };

  const allResponded = Object.keys(responses).every(key => !!responses[key]);

  return (
    <SafeAreaView>
      <ScrollView style={styles.scrollView}>
        <View style={styles.container}>
          <View style={styles.innerContent}>
            <Text style={styles.mainTitle}>
              {translations.onBoarding.hello}
            </Text>
            <Text style={styles.text}>{translations.onBoarding.intro}</Text>
          </View>
          <View style={[styles.innerContent, styles.borderTopGray]}>
            <View style={styles.allQuestions}>
              <View style={styles.leftBar} />
              <Question
                position={1}
                responses={responses}
                setResponses={setResponses}
                question={{
                  label: translations.onBoarding.question1.title,
                  value: 'goal',
                }}
                responseTitles={[
                  {
                    label: translations.onBoarding.question1.option1,
                    value: goals[0].name,
                  },
                  {
                    label: translations.onBoarding.question1.option2,
                    value: goals[1].name,
                  },
                  {
                    label: translations.onBoarding.question1.option3,
                    value: goals[2].name,
                  },
                ]}
                showLabel={true}
              />
              <Question
                position={2}
                responses={responses}
                setResponses={setResponses}
                question={{
                  label: translations.onBoarding.question2.title,
                  value: 'activityLevel',
                }}
                responseTitles={[
                  {
                    label: translations.onBoarding.question2.option1,
                    value: activeLevels[0].name,
                  },
                  {
                    label: translations.onBoarding.question2.option2,
                    value: activeLevels[1].name,
                  },
                  {
                    label: translations.onBoarding.question2.option3,
                    value: activeLevels[2].name,
                  },
                ]}
                showLabel={true}
              />
              <Question
                position={3}
                responses={responses}
                setResponses={setResponses}
                question={{
                  label: translations.onBoarding.question3.title,
                  value: 'gender',
                }}
                responseTitles={[
                  {
                    label: translations.onBoarding.question3.option1,
                    value: 'female',
                  },
                  {
                    label: translations.onBoarding.question3.option2,
                    value: 'male',
                  },
                ]}
                showLabel={true}
              />
              <QuestionForm
                position={4}
                responses={responses}
                setResponses={setResponses}
                translations={translations}
              />
              <BodyFatQuestion
                responses={responses}
                position={5}
                isFilled={!!responses.bodyFat}
                gender={responses.gender}
                setBodyFat={setBodyFat}
                translations={translations}
              />
              {/* for testing changes */}
              {/* <Text style={{alignSelf: 'center'}}>{responses.bodyFat}</Text> */}
              <BodyTypeQuestion
                position={6}
                isFilled={!!responses.bodyType}
                responses={responses}
                setResponses={setResponses}
                question={{
                  label: translations.onBoarding.question6.title,
                  value: 'bodyType',
                }}
                translations={translations}
                showLabel={true}
                currentValue={responses.bodyType}
              />
            </View>
          </View>
        </View>
      </ScrollView>
      <NextButton onPress={nextStep} disabled={!allResponded} />
    </SafeAreaView>
  );
};

export default Onboarding;
