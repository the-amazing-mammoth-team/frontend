import {StyleSheet} from 'react-native';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {
  GRAY,
  BUTTON_TITLE,
  WHITE,
  BLACK,
  GRAY_BORDER,
  ORANGE,
  LIGHT_ORANGE,
  LIGHT_GRAY,
} from '../../styles/colors';
import {
  calcFontSize,
  moderateScale,
  deviceHeight,
  deviceWidth,
} from '../../utils/dimensions';

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: WHITE,
  },
  container: {
    color: BLACK,
    fontSize: calcFontSize(14),
    backgroundColor: WHITE,
    flex: 1,
    borderBottomWidth: 1,
    borderColor: GRAY_BORDER,
    borderWidth: 2,
  },
  innerContent: {
    paddingLeft: 20,
    //paddingRight: 25,
  },
  borderTopGray: {
    borderTopWidth: 1,
    borderColor: GRAY_BORDER,
  },
  questionContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  questionContainerForm: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingRight: 25,
  },
  questionContent: {
    marginLeft: 20,
    flex: 1,
  },
  buttonsRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 10,
    marginBottom: 30,
  },
  text: {
    color: GRAY,
    fontSize: calcFontSize(14),
    marginTop: 10,
    paddingBottom: 20,
    fontFamily: SUB_TITLE_FONT,
  },
  mainTitle: {
    fontSize: calcFontSize(30),
    marginTop: 15,
    fontFamily: MAIN_TITLE_FONT,
  },
  circleCheck: {
    borderRadius: 50,
    width: 25,
    height: 25,
    backgroundColor: ORANGE,
  },
  activeCheck: {
    borderRadius: 50,
    width: 25,
    height: 25,
    backgroundColor: LIGHT_ORANGE,
  },
  circleCheckUnfilled: {
    width: 25,
    height: 25,
    borderRadius: 50,
    backgroundColor: LIGHT_GRAY,
  },
  buttonContainer: {
    marginRight: 5,
  },
  buttonTitleStyle: {
    color: BUTTON_TITLE,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  selectedButtonTitleStyle: {
    color: WHITE,
  },
  buttonStyle: {
    backgroundColor: WHITE,
    borderWidth: 1,
    borderColor: GRAY_BORDER,
    borderRadius: 50,
    paddingLeft: 15,
    paddingRight: 15,
    marginTop: 10,
  },
  questionText: {
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  allQuestions: {
    paddingTop: 15,
  },
  formContainer: {
    fontSize: calcFontSize(18),
    flex: 1,
    marginTop: 10,
    marginBottom: 30,
  },
  formText: {
    fontSize: calcFontSize(14),
    color: BUTTON_TITLE,
    fontFamily: SUB_TITLE_FONT,
  },
  fieldWrapper: {},
  formTextContainer: {
    borderBottomWidth: 1,
    borderColor: GRAY_BORDER,
    paddingBottom: 15,
    paddingTop: 15,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  formTextInput: {
    fontSize: calcFontSize(16),
    paddingBottom: 0,
    paddingTop: 0,
    marginTop: 0,
    lineHeight: 25,
    fontFamily: SUB_TITLE_FONT,
    color: BLACK,
  },
  inputComponent: {
    flex: 1,
    margin: 'auto',
  },
  selectedButtonColor: {
    backgroundColor: ORANGE,
    borderColor: WHITE,
  },
  dateWrapper: {
    flex: 1,
    borderWidth: 0,
    alignItems: 'flex-end',
  },
  dateText: {
    textAlign: 'right',
  },
  leftBar: {
    width: 1,
    backgroundColor: GRAY_BORDER,
    height: '100%',
    position: 'absolute',
    left: 12,
  },
});

export default styles;
