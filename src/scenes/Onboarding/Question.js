import React from 'react';
import {View, Text} from 'react-native';
import {Icon, Button} from 'react-native-elements';
import PropTypes from 'prop-types';
import styles from './styles';
import {
  GRAY,
  BUTTON_TITLE,
  WHITE,
  BLACK,
  GRAY_BORDER,
  ORANGE,
  LIGHT_ORANGE,
  LIGHT_GRAY,
} from '../../styles/colors';

const Question = ({
  question,
  responseTitles,
  setResponses,
  responses,
  position,
  showLabel,
}) => {
  const setResponse = responseValue => {
    setResponses({
      ...responses,
      answeredPositions: [...responses.answeredPositions, position],
      [question.value]: responseValue,
    });
  };

  const questionFilled = !!responses[question.value];
  const questionActive =
    Math.max(...responses.answeredPositions) === position - 1 || position === 1;

  const getCheckStyles = () => {
    if (questionFilled) {
      return styles.circleCheck;
    }
    // if (questionActive) {
    //   return styles.activeCheck;
    // }
    return styles.circleCheckUnfilled;
  };

  const checkStyles = getCheckStyles();

  return (
    <View style={styles.questionContainer}>
      {showLabel && (
        <View>
          <View style={checkStyles}>
            {questionFilled && <Icon name="check" color={LIGHT_GRAY} />}
          </View>
        </View>
      )}
      <View style={styles.questionContent}>
        {showLabel && <Text style={styles.questionText}>{question.label}</Text>}
        <View style={styles.buttonsRow}>
          {responseTitles.map(({label, value}) => {
            const buttonStyles =
              responses[question.value] === value
                ? [styles.buttonStyle, styles.selectedButtonColor]
                : styles.buttonStyle;
            const titleStyles =
              responses[question.value] === value
                ? [styles.buttonTitleStyle, styles.selectedButtonTitleStyle]
                : styles.buttonTitleStyle;
            return (
              <View key={label} style={styles.buttonContainer}>
                <Button
                  onPress={() => setResponse(value)}
                  titleStyle={titleStyles}
                  buttonStyle={buttonStyles}
                  title={label}
                />
              </View>
            );
          })}
        </View>
      </View>
    </View>
  );
};

Question.propTypes = {
  responses: PropTypes.object,
  setResponses: PropTypes.func,
  responseTitles: PropTypes.array,
  question: PropTypes.object,
  position: PropTypes.number,
};

export default Question;
