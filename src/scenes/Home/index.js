import React, {
  useCallback,
  useContext,
  useEffect,
  useReducer,
  useMemo,
  useState,
} from 'react';
import {ScrollView, View, Text, SafeAreaView, FlatList} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import {useQuery, useMutation} from '@apollo/react-hooks';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';
import gql from 'graphql-tag';
import moment from 'moment';
import {GRAY_BORDER, BLACK, ORANGE} from '../../styles/colors';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {AuthContext} from '../../../App';
import styles from './styles';
import deviceStorage from '../../services/deviceStorage';
import {LocalizationContext} from '../../localization/translations';
import {trackEvents} from '../../services/analytics';
import NewsGradientCard from '../../components/HomeScreen/NewsGradientCard';
import GenericWhitePill from '../../components/HomeScreen/GenericWhitePill';
import UserIcon from '../../assets/icons/user.svg';
import {moderateScale} from '../../utils/dimensions';
import {getVideos, convertSessionBlock} from '../../services/downloadingVideos';
import SuccessNewsBackgroud from '../../assets/images/active-adult-beautiful.png';
import CompletedProgramBackground from '../../assets/images/completedProgram.png';
import RecoveryImage from '../../assets/images/homeImage2.png';
import CurrentSessionImage from '../../assets/images/homeImage1.png';
import {Loading} from '../../components/Loading';
import GenericSessionCard from '../../components/HomeScreen/GenericSessionCard';
import GenericSessionFooter from '../../components/HomeScreen/GenericSessionFooter';

const GET_USER = gql`
  query user($id: ID!, $locale: String) {
    user(id: $id, locale: $locale) {
      id
      names
      email
      currentProgram {
        id
        name
        user {
          id
          names
        }
        programCharacteristics {
          id
          value
          objective
        }
        sessions {
          id
          order
        }
        codeName
      }
      currentSession {
        id
        imageUrl
        name
        description
        order
        timeDuration
        calories
        reps
        bodyPartsFocused
        sessionBlocks {
          sessionSets {
            exercises {
              id
            }
            exerciseSets {
              id
              exercise {
                id
                video
              }
            }
          }
        }
      }
      userProgram {
        id
        programSessionsCount
        executedSessionsCount
        progress
        completed
        executedSessions {
          id
        }
      }
      updatedAt
      weeklyExecutions {
        id
        sessionId
        createdAt
        session {
          id
          order
        }
        userProgram {
          id
          program {
            id
            codeName
          }
        }
      }
      trainingDaysSetting
      activeRests {
        id
        sessions {
          id
          order
          name
          description
          timeDuration
          calories
          reps
        }
      }
      challenges {
        id
        sessions {
          id
          order
          name
          description
          timeDuration
          calories
          reps
        }
      }
    }
  }
`;

const SAVE_SESSION_EXECUTION = gql`
  mutation saveSessionExecution($input: SaveSessionExecutionInput!) {
    saveSessionExecution(input: $input) {
      id
    }
  }
`;

const Home = ({navigation, route}) => {
  const {translations, appLanguage: locale} = useContext(LocalizationContext);
  const doRefetch = route.params?.doRefetch || false;
  console.log(locale);
  console.log('re', doRefetch);
  const {signOut, userData: userInfo} = React.useContext(AuthContext);

  const {data, loading = true, error, refetch} = useQuery(GET_USER, {
    variables: {id: userInfo?.id, locale: locale},
    fetchPolicy: 'network-only',
  });

  const isFocused = useIsFocused();

  const customRefetch = useCallback(() => {
    setTimeout(() => refetch(), 0);
  }, [refetch]);

  const [saveSessionExecution] = useMutation(SAVE_SESSION_EXECUTION);

  useEffect(() => {
    if (!doRefetch) {
      customRefetch();
    }
  }, [customRefetch, doRefetch, locale]);

  useFocusEffect(
    useCallback(() => {
      if (doRefetch) {
        console.log('refetching...');
        customRefetch();
        navigation.setParams({doRefetch: false});
      }
    }, [customRefetch, navigation, doRefetch]),
  );

  useFocusEffect(
    useCallback(() => {
      trackEvents({eventName: 'Load Today'});
    }, []),
  );

  useEffect(() => {
    if (data?.user?.currentSession) {
      const wo = data?.user?.currentSession?.sessionBlocks.map(block => {
        let allSets = {};
        const allSessionSets = block.sessionSets;
        allSets = allSessionSets.reduce((acc, cur, index) => {
          acc[index + 1] = {
            allExercises: cur.exerciseSets,
          };
          return acc;
        }, {});
        return {
          allSets,
        };
      });
      getVideos(wo);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data?.user?.currentSession?.id]);

  useEffect(() => {
    // will be send saved session executions when internet will be restored
    // and on first render
    const unsubscribe = NetInfo.addEventListener(async state => {
      if (state.isConnected) {
        try {
          refetch();
        } catch (err) {
          console.log('error refetch');
        }
        try {
          const sessionExecution = JSON.parse(
            await deviceStorage.getItem('completedSessions'),
          );
          if (sessionExecution) {
            try {
              const res = await saveSessionExecution({
                variables: {
                  input: {
                    sessionExecution,
                  },
                },
                context: {
                  headers: {
                    authorization: userInfo.token,
                  },
                },
                refetchQueries: ['user'],
              });
              console.log('session executions successfully sent', res);
              if (res?.data?.saveSessionExecution) {
                await deviceStorage.removeItem('completedSessions');
              }
            } catch (err) {
              console.log(err);
            }
          }
        } catch (error) {
          console.log(error);
        }
      }
    });

    return () => unsubscribe();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (isFocused) {
      const checkCurrentProgram = async () => {
        await refetch();
        let tempProgram = await deviceStorage.getItem(
          '@programs/currentProgram',
        );
        if (!loading && !error) {
          if (currentProgram) {
            if (!tempProgram) {
              const program = {
                availableProgramId: currentProgram.id,
                duration: currentProgram.sessions
                  ? currentProgram.sessions.length
                  : '10',
                background: currentProgram.imageUrl,
                backgroundWide: currentProgram.image2Url,
                title: currentProgram.name,
                musculatureLevel: 1,
                description: currentProgram.description,
                programCharacteristics: currentProgram.programCharacteristics,
              };
              await deviceStorage.saveItem(
                '@programs/currentProgram',
                JSON.stringify(program),
              );
            }
          } else {
            navigation.navigate('SuggestedPrograms');
          }
        }
      };
      checkCurrentProgram();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentProgram, navigation, loading, isFocused]);

  const logout = () => {
    // AsyncStorage.clear();
    signOut();
  };

  // AsyncStorage.clear();

  console.log(error);

  console.log(data?.user);

  const user = data?.user;
  const userProgram = user?.userProgram;
  // userProgram && (userProgram.completed = true); //test choose new program flow
  const currentSession = user?.currentSession;
  const currentProgram = user?.currentProgram;
  const weeklyExecutions = user?.weeklyExecutions;
  const activeRests = user?.activeRests?.sessions;
  const trainingDaysSetting = user?.trainingDaysSetting;
  const executedSessionsCount = userProgram?.executedSessionsCount;
  const programSessionsCount = userProgram?.programSessionsCount;
  const executedSessions = userProgram?.executedSessions;
  const programProgress = userProgram?.progress;

  const listWeekGoals = (() => {
    let list = weeklyExecutions?.map(item => {
      let newItem = {};
      const res = item?.userProgram?.program?.codeName;
      if (res === 'active rests') {
        newItem = {
          name: translations.homeScreen.activeRest,
          backgroundColor: BLACK,
          createdAt: item.createdAt,
          id: item.sessionId,
          execution_id: item.id,
        };
      } else if (res === 'challenges') {
        newItem = {
          name: translations.homeScreen.challenges,
          backgroundColor: BLACK,
          createdAt: item.createdAt,
          id: item.sessionId,
          execution_id: item.id,
        };
      } else if (res === 'legacy') {
        newItem = {
          name: 'LE',
          backgroundColor: BLACK,
          createdAt: item.createdAt,
          id: item.sessionId,
          execution_id: item.id,
        };
      } else {
        newItem = {
          name: `S${item?.session?.order}`,
          backgroundColor: ORANGE,
          createdAt: item.createdAt,
          id: item.sessionId,
          execution_id: item.id,
        };
      }
      return newItem;
    });
    if (weeklyExecutions?.length < trainingDaysSetting) {
      list.length = trainingDaysSetting;
      return list.fill({}, weeklyExecutions.length, trainingDaysSetting);
    }
    return list;
  })();

  const dailySessionExecutions =
    weeklyExecutions?.filter(el =>
      moment(new Date(el.createdAt)).isSame(new Date(), 'day'),
    ) || [];

  const isTrainedToday = dailySessionExecutions?.length ? true : false;

  const todaysTraining = (() => {
    if (!isTrainedToday) {
      return {...currentSession, type: 'currentSession'};
    } else if (isTrainedToday && dailySessionExecutions.length > 1) {
      return {type: 'goodWork'};
    } else if (isTrainedToday) {
      const randomElement =
        activeRests &&
        activeRests[Math.floor(Math.random() * activeRests.length)];
      return {...randomElement, type: 'activeSession'} || {type: ''};
    } else {
      return {type: ''};
    }
  })();

  const suggestedPrograms = useCallback(
    () => navigation.navigate('SuggestedPrograms'),
    [navigation],
  );

  const handleOnPressUserIcon = useCallback(
    () => navigation.navigate('Profile', {program: currentProgram}),
    [currentProgram, navigation],
  );

  const goToWorkout = useCallback(
    (program, session, prevScreen = 'Home') => {
      navigation.navigate('Workout', {
        programId: program?.id,
        programCodeName: program?.codeName,
        sessionId: session?.id,
        sessionDescription: session?.description,
        sessionName: session?.name,
        userId: data?.user?.id,
        prevScreen,
      });
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [navigation, data],
  );

  const renderSessionCard = () => {
    if (!userProgram?.completed || programProgress !== 100) {
      switch (todaysTraining.type) {
        case 'currentSession': {
          return (
            <GenericSessionCard
              key={todaysTraining.id}
              isTouch={true}
              onPress={() => goToWorkout(currentProgram, currentSession)}
              title={todaysTraining.name}
              bodyPartsFocused={todaysTraining.bodyPartsFocused}
              calories={todaysTraining.calories}
              timeDuration={todaysTraining.timeDuration}
              reps={todaysTraining.reps}
              footerShow={true}
              extraStyles={{marginHorizontal: 0}}
            />
          );
        }
        case 'activeSession': {
          return (
            <GenericSessionCard
              key={todaysTraining.id}
              isTouch={true}
              onPress={() => goToWorkout(currentProgram, todaysTraining)}
              title={translations.homeScreen.restSessionName}
              description={translations.homeScreen.restSessionDesc}
              backgroundImage={RecoveryImage}
              calories={todaysTraining.calories}
              timeDuration={todaysTraining.timeDuration}
              reps={todaysTraining.reps}
              footerShow={true}
              extraStyles={{marginHorizontal: 0}}
            />
          );
        }
        case 'goodWork': {
          return (
            <GenericSessionCard
              key={todaysTraining.id}
              isTouch={false}
              title={translations.homeScreen.pillGoodWorkName}
              description={translations.homeScreen.pillGoodWorkDesc}
              backgroundImage={SuccessNewsBackgroud}
              extraStyles={{marginHorizontal: 0}}
            />
          );
        }
      }
    } else {
      return (
        <GenericSessionCard
          key={todaysTraining.id}
          isTouch={true}
          onPress={() => {
            navigation.navigate('SuggestedPrograms');
          }}
          title={translations.homeScreen.pillComletedProgram}
          description={translations.homeScreen.pillComletedProgramDesc}
          backgroundImage={CompletedProgramBackground}
          extraStyles={{marginHorizontal: 0}}
        />
      );
    }
  };

  if (!loading && user && user.id) {
    return (
      <SafeAreaView style={styles.safeArea}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.header}>
            <Text style={styles.today}>{translations.homeScreen.today}</Text>
            <View style={styles.userIconView}>
              <TouchableOpacity
                onPress={handleOnPressUserIcon}
                hitSlop={styles.hitSlopFifteen}>
                <UserIcon
                  width={moderateScale(26)}
                  height={moderateScale(26)}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* <View>
            <TouchableOpacity
              onPress={suggestedPrograms}
              hitSlop={styles.hitSlopFifteen}>
              <UserIcon width={moderateScale(26)} height={moderateScale(26)} />
            </TouchableOpacity>
          </View> */}
          <View style={styles.container}>
            <Text style={styles.text}>
              {translations.homeScreen.yourTraining}
            </Text>
          </View>
          <View style={styles.genericHorizontalPadding}>
            <View style={[{paddingBottom: moderateScale(10)}]}>
              {renderSessionCard()}
            </View>
            <GenericWhitePill
              isTouch
              title={translations.homeScreen.weekGoals}
              description={`${
                weeklyExecutions?.length
              } / ${trainingDaysSetting} ${
                translations.homeScreen.sessionsCompleted
              }`}
              onPress={() =>
                navigation.navigate('WeeklyGoals', {
                  weeklyGoals: listWeekGoals,
                  executedSessions,
                  userId: userInfo.id,
                  goToWorkout,
                })
              }
              extraStyles={{marginHorizontal: 0}}>
              <View style={styles.genericComponentRepsView}>
                {listWeekGoals?.map((item, index) => (
                  <View
                    key={index}
                    style={[
                      styles.circle,
                      {backgroundColor: item.backgroundColor || GRAY_BORDER},
                    ]}>
                    <Text style={styles.circleText}>{item.name}</Text>
                  </View>
                ))}
              </View>
            </GenericWhitePill>
            <GenericWhitePill
              isTouch
              title={translations.homeScreen.yourProgram}
              description={`${currentProgram?.name} | ${currentProgram?.user
                ?.names || 'Mammoth Hunters'}`}
              onPress={() => {
                navigation.navigate('ListSessionsProgram', {
                  userId: userInfo.id,
                  goToWorkout,
                });
              }}
              extraStyles={{marginHorizontal: 0}}>
              <View style={styles.scaleAndTextView}>
                <View style={[styles.rootGrayScale]}>
                  <View
                    style={[
                      styles.filledScale,
                      {
                        flex: programProgress / 100 || 0,
                      },
                    ]}
                  />
                </View>
                <Text style={styles.pillTextGray}>
                  {executedSessionsCount}/{programSessionsCount}
                </Text>
              </View>
            </GenericWhitePill>
          </View>
          <View style={styles.genericHorizontalPadding}>
            <View
              style={[
                styles.scaleAndTextView,
                {marginBottom: moderateScale(3)},
              ]}>
              <Text style={styles.sectionNameText}>
                {translations.homeScreen.activeRestsTitle}
              </Text>
              <TouchableOpacity
                hitSlop={styles.hitSlopFifteen}
                style={styles.seeAllTextView}
                onPress={() =>
                  navigation.navigate('ListActiveSessionsProgram', {
                    userId: user?.id,
                    goToWorkout,
                  })
                }>
                <Text style={styles.seeAllText}>
                  {translations.homeScreen.activeRestsSeeAll}
                </Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.pillTextGray}>
              {translations.homeScreen.activeRestsDesc}
            </Text>
            <View style={styles.extraSectionMargin} />
          </View>
          <FlatList
            data={activeRests}
            renderItem={({item}) => (
              <GenericWhitePill
                key={item.id}
                title={item.name}
                extraStyles={styles.activeRestItem}
                onPress={() => goToWorkout(currentProgram, item)}>
                <GenericSessionFooter
                  calories={item.calories}
                  reps={item.reps}
                  timeDuration={item.timeDuration}
                />
              </GenericWhitePill>
            )}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={item => item.toString()}
            style={{marginHorizontal: moderateScale(10)}}
          />
          <View style={{marginBottom: moderateScale(30)}} />
        </ScrollView>
      </SafeAreaView>
    );
  }

  return <Loading />;
};

export default Home;
