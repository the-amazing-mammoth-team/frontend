import React, {useCallback, useContext} from 'react';
import 'moment/locale/es';
import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import Header from '../../components/header';
import {LocalizationContext} from '../../localization/translations';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';
import {SafeAreaView, StyleSheet, Text, View, FlatList} from 'react-native';
import {BLACK, GRAY, GRAY_BORDER, ORANGE, WHITE} from '../../styles/colors';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  moderateScale,
} from '../../utils/dimensions';
import {parseIsoDatetime} from '../../utils/dateTime';
import {trackEvents} from '../../services/analytics';
import {SUB_TITLE_FONT} from '../../styles/fonts';
import {Loading} from '../../components/Loading';
import GenericSessionCard from '../../components/HomeScreen/GenericSessionCard';
import DayComponent from '../../components/Calendar/DayComponent';

const GET_USER = gql`
  query user($id: ID!) {
    user(id: $id) {
      id
      currentProgram {
        id
        sessions {
          id
          name
          order
          description
          calories
          timeDuration
          reps
        }
      }
    }
  }
`;

const WeeklyGoals = ({navigation, route}) => {
  const handleGoBack = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const {translations, appLanguage} = useContext(LocalizationContext);
  const {userId, goToWorkout, weeklyGoals, executedSessions} = route?.params;
  const isFocused = useIsFocused();
  const {data, error, loading} = useQuery(GET_USER, {
    variables: {id: userId},
  });

  useFocusEffect(
    useCallback(() => {
      isFocused && trackEvents({eventName: 'Load Weekly Goal'});
    }, [isFocused]),
  );

  const currentProgram = data?.user?.currentProgram;
  const programSessions = currentProgram?.sessions;

  const daysOfWeek = (() => {
    const week = [];
    const current = new Date();
    current.setDate(current.getDate() - current.getDay() + 1);
    for (var i = 0; i < 7; i++) {
      week.push(new Date(current));
      current.setDate(current.getDate() + 1);
    }
    return week;
  })();

  const availableProgramSessions = (() => {
    const list =
      executedSessions &&
      programSessions?.reduce((sortedArray, item, index) => {
        const isDone = executedSessions.some(el => el.id === item.id);
        return isDone ? sortedArray : [...sortedArray, item];
      }, []);
    list?.sort((a, b) => {
      return a.order - b.order;
    });
    return list;
  })();

  return (
    <SafeAreaView style={styles.safeArea}>
      <Header
        title={translations.weeklyGoalsScreen.header}
        onPressBackButton={handleGoBack}
      />
      <View style={styles.line} />
      <View style={styles.container}>
        <View style={styles.infoView}>
          <View style={styles.orangeCircle} />
          <Text style={styles.infoText}>
            {translations.weeklyGoalsScreen.workouts}
          </Text>
          <View style={styles.blackCircle} />
          <Text style={styles.infoText}>
            {translations.weeklyGoalsScreen.other}
          </Text>
        </View>
        <View style={styles.calendarContainer}>
          {daysOfWeek.map((date, index) => {
            return (
              <DayComponent
                date={date}
                key={index}
                index={index}
                goals={weeklyGoals}
                navigation={navigation}
              />
            );
          })}
        </View>
        <Text style={[styles.dayText, {alignSelf: 'flex-start'}]}>
          {translations.weeklyGoalsScreen.sessionsNote}
        </Text>
      </View>
      <View style={styles.sessionsListContainer}>
        {loading ? (
          <Loading />
        ) : (
          <FlatList
            data={availableProgramSessions}
            renderItem={({item}) => (
              <GenericSessionCard
                title={item.name}
                isTouch={!item.isDone}
                onPress={() => goToWorkout(currentProgram, item, 'WeeklyGoals')}
                calories={item.calories}
                timeDuration={item.timeDuration}
                reps={item.reps}
                footerShow={true}
              />
            )}
            showsVerticalScrollIndicator={false}
            keyExtractor={item => item.id}
          />
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: WHITE,
  },
  infoView: {
    alignSelf: 'flex-end',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: calcHeight(10),
  },
  container: {
    paddingHorizontal: calcWidth(20),
  },
  calendarContainer: {
    marginHorizontal: calcWidth(10),
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: moderateScale(50),
  },
  infoText: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: GRAY,
  },
  orangeCircle: {
    width: moderateScale(8),
    height: moderateScale(8),
    borderRadius: moderateScale(8) / 2,
    backgroundColor: ORANGE,
    marginRight: calcWidth(7),
  },
  blackCircle: {
    width: moderateScale(8),
    height: moderateScale(8),
    borderRadius: moderateScale(8) / 2,
    backgroundColor: BLACK,
    marginRight: calcWidth(7),
    marginLeft: calcWidth(20),
  },
  dayText: {
    textAlign: 'center',
    fontFamily: SUB_TITLE_FONT,
    color: GRAY,
    fontSize: calcFontSize(14),
  },
  line: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginBottom: moderateScale(15),
    marginTop: moderateScale(-15),
  },
  sessionsListContainer: {
    flex: 1,
    marginTop: moderateScale(20),
  },
});

export default WeeklyGoals;
