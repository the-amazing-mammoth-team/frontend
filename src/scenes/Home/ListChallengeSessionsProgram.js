import React, {useState, useEffect, useContext, useCallback} from 'react';
import {SafeAreaView, StyleSheet, View, FlatList} from 'react-native';
import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';
import Header from '../../components/header';
import GenericWhitePill from '../../components/HomeScreen/GenericWhitePill';
import GenericImagePill from '../../components/HomeScreen/GenericImagePill';
import {Loading} from '../../components/Loading';
import {WHITE, GRAY_BORDER} from '../../styles/colors';
import {
  calcWidth,
  moderateScale,
  deviceWidth,
  calcFontSize,
} from '../../utils/dimensions';
import {LocalizationContext} from '../../localization/translations';
import {trackEvents} from '../../services/analytics';
import challengesBackground from '../../assets/images/challengesBackground.png';

const GET_USER = gql`
  query user($id: ID!) {
    user(id: $id) {
      id
      challenges {
        id
        sessions {
          id
          name
          description
          imageUrl
        }
      }
      currentProgram {
        id
        codeName
      }
    }
  }
`;

const ListActiveSessionsProgram = ({navigation, route}) => {
  const {translations} = useContext(LocalizationContext);
  const {userId, goToWorkout} = route.params;
  const {data, error, loading} = useQuery(GET_USER, {
    variables: {id: userId},
  });
  const isFocused = useIsFocused();
  const challenges = data?.user?.challenges?.sessions;
  const currentProgram = data?.user?.currentProgram;

  useFocusEffect(
    useCallback(() => {
      isFocused && trackEvents({eventName: 'Load Challenges'});
    }, [isFocused]),
  );

  return (
    <SafeAreaView style={styles.safeArea}>
      {!loading ? (
        <>
          <Header
            title={translations.listChallengeSessionsProgramScreen.headerText}
            onPressBackButton={() => {
              navigation.goBack();
            }}
          />
          <View style={styles.line} />
          <FlatList
            data={challenges}
            renderItem={({item}) => {
              const Item = item?.imageUrl ? GenericImagePill : GenericWhitePill;
              return (
                <Item
                  key={item.id}
                  title={item.name}
                  extraStyles={styles.activeRestItem}
                  onPress={() => goToWorkout(currentProgram, item)}
                  extraStylesTitle={{fontSize: calcFontSize(25)}}
                  backgroundImage={challengesBackground}
                />
              );
            }}
            keyExtractor={item => item.id}
          />
        </>
      ) : (
        <Loading />
      )}
    </SafeAreaView>
  );
};

export default ListActiveSessionsProgram;

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: WHITE,
  },
  container: {
    paddingBottom: calcWidth(20),
  },
  line: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginBottom: moderateScale(15),
    marginTop: moderateScale(-15),
  },
  activeRestItem: {
    minHeight: deviceWidth * 0.35,
    marginHorizontal: moderateScale(20),
    marginBottom: moderateScale(7),
    marginTop: moderateScale(7),
  },
});
