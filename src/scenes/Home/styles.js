import {StyleSheet} from 'react-native';
import {
  BLACK,
  GRAY,
  GRAY_BORDER,
  LIGHT_GRAY,
  LIGHT_ORANGE,
  ORANGE,
  WHITE,
} from '../../styles/colors';
import {MAIN_TITLE_FONT, SUB_TITLE_FONT} from '../../styles/fonts';
import {exp} from 'react-native-reanimated';
import {
  calcFontSize,
  calcHeight,
  calcWidth,
  deviceWidth,
  moderateScale,
} from '../../utils/dimensions';

const styles = StyleSheet.create({
  safeArea: {
    backgroundColor: WHITE,
    flex: 1,
  },
  text: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(20),
  },
  header: {
    borderBottomWidth: moderateScale(1),
    borderBottomColor: GRAY_BORDER,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: WHITE,
  },
  userIconView: {
    position: 'absolute',
    right: moderateScale(20),
  },
  today: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(18),
    paddingVertical: moderateScale(17),
    backgroundColor: WHITE,
  },
  container: {
    padding: 20,
    backgroundColor: WHITE,
  },
  linearGradient: {
    flex: 1,
    paddingHorizontal: moderateScale(20),
    paddingVertical: moderateScale(25),
    marginHorizontal: moderateScale(20),
    borderRadius: moderateScale(10),
    marginBottom: moderateScale(28),
  },
  buttonText: {
    fontSize: calcFontSize(18),
    color: '#ffffff',
    backgroundColor: 'transparent',
    fontFamily: MAIN_TITLE_FONT,
  },
  mainPillParagraph: {
    fontSize: calcFontSize(14),
    color: WHITE,
    marginTop: moderateScale(20),
    fontFamily: SUB_TITLE_FONT,
  },
  pill: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.2,
    marginHorizontal: moderateScale(20),
    elevation: 4,
    borderRadius: moderateScale(10),
    marginBottom: moderateScale(28),
    paddingHorizontal: moderateScale(20),
    paddingVertical: moderateScale(15),
    backgroundColor: WHITE,
    justifyContent: 'space-between',
  },
  pillContainer: {
    backgroundColor: WHITE,
    borderRadius: moderateScale(10),
    paddingHorizontal: moderateScale(20),
    paddingVertical: moderateScale(15),
  },
  pillText: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(20),
    color: BLACK,
  },
  pillTextGray: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: GRAY,
  },
  pillLink: {
    color: LIGHT_ORANGE,
    marginTop: moderateScale(15),
    fontSize: calcFontSize(16),
  },
  buttonBottom: {
    alignItems: 'center',
  },
  understandText: {
    color: LIGHT_ORANGE,
    textDecorationLine: 'underline',
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
  },
  hitSlopFifteen: {
    top: moderateScale(15),
    bottom: moderateScale(15),
    right: moderateScale(15),
    left: moderateScale(15),
  },
  understandTextView: {
    marginTop: moderateScale(15),
  },
  scaleAndTextView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rootGrayScale: {
    height: moderateScale(11),
    flex: 1,
    maxWidth: deviceWidth * 0.62,
    backgroundColor: GRAY_BORDER,
    borderRadius: moderateScale(5),
    flexDirection: 'row',
  },
  circle: {
    width: moderateScale(31),
    height: moderateScale(31),
    borderRadius: moderateScale(31),
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: moderateScale(10),
  },
  circleText: {
    color: WHITE,
    fontSize: calcFontSize(12),
    fontFamily: SUB_TITLE_FONT,
  },
  filledScale: {
    height: moderateScale(11),
    maxWidth: deviceWidth * 0.62,
    backgroundColor: ORANGE,
    borderTopLeftRadius: moderateScale(5),
    borderBottomLeftRadius: moderateScale(5),
  },
  line: {
    backgroundColor: GRAY_BORDER,
    height: moderateScale(1),
    marginBottom: moderateScale(20),
  },
  sectionNameText: {
    color: BLACK,
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(20),
  },
  genericHorizontalPadding: {
    paddingHorizontal: calcWidth(20),
  },
  programsCarousel: {
    marginTop: calcHeight(20),
    paddingLeft: calcWidth(20),
  },
  programItem: {
    width: deviceWidth * 0.9,
    borderRadius: 10,
    marginRight: calcWidth(20),
    marginLeft: calcHeight(3),
  },
  programItemGray: {
    width: deviceWidth * 0.9,
    borderRadius: 10,
    backgroundColor: GRAY,
    marginRight: calcWidth(20),
    marginLeft: calcHeight(3),
  },
  programItemWhite: {
    width: deviceWidth * 0.75,
    borderRadius: 10,
    backgroundColor: WHITE,
    marginRight: calcWidth(10),
    elevation: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    marginVertical: calcHeight(10),
    marginLeft: calcHeight(3),
  },
  programCardContent: {
    paddingHorizontal: calcWidth(15),
    paddingVertical: calcHeight(15),
  },
  programCardTitle: {
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(25),
    color: WHITE,
  },
  programCardTitleBlack: {
    color: BLACK,
  },
  programCardDescription: {
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    color: WHITE,
  },
  programCardDescriptionGray: {
    color: GRAY,
  },
  programCardFooter: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginTop: moderateScale(10),
  },
  genericComponentLikeView: {
    alignSelf: 'flex-end',
  },
  genericComponentRepsView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  genericComponentFooterPadding: {
    marginLeft: calcWidth(7),
  },
  genericComponentTextBlack: {
    color: BLACK,
  },
  genericComponentSeparateLine: {
    marginHorizontal: calcWidth(8),
  },
  lineWithMarginV: {
    backgroundColor: GRAY_BORDER,
    height: moderateScale(1),
    marginVertical: moderateScale(38),
  },
  seeAllText: {
    color: LIGHT_ORANGE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    textDecorationLine: 'underline',
  },
  absoluteBackgroundImageView: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    bottom: 0,
    right: 0,
    top: 0,
  },
  extraSectionMargin: {
    marginBottom: moderateScale(20),
  },
  activeRestItem: {
    width: deviceWidth * 0.7,
    minHeight: deviceWidth * 0.35,
    marginHorizontal: moderateScale(10),
    marginBottom: moderateScale(5),
    marginTop: moderateScale(5),
  },
});

export default styles;
