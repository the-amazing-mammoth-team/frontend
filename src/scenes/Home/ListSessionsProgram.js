import React, {useState, useEffect, useContext, useCallback} from 'react';
import {SafeAreaView, StyleSheet, View, FlatList} from 'react-native';
import gql from 'graphql-tag';
import {useFocusEffect, useIsFocused} from '@react-navigation/native';
import {useLazyQuery} from '@apollo/react-hooks';
import Header from '../../components/header';
import GenericSessionCard from '../../components/HomeScreen/GenericSessionCard';
import {Loading} from '../../components/Loading';
import {WHITE, GRAY_BORDER} from '../../styles/colors';
import {calcWidth, moderateScale} from '../../utils/dimensions';
import {trackEvents} from '../../services/analytics';
import {LocalizationContext} from '../../localization/translations';

const GET_USER = gql`
  query user($id: ID!, $locale: String!) {
    user(id: $id, locale: $locale) {
      id
      userProgram {
        id
        executedSessions {
          id
        }
        sessionExecutions {
          id
          sessionId
          discarded
        }
      }
      currentProgram {
        id
        name
        codeName
        sessions {
          id
          name
          description
          order
          calories
          timeDuration
          reps
          bodyPartsFocused
        }
      }
    }
  }
`;

const ListSessionsProgram = ({navigation, route}) => {
  const locale = LocalizationContext._currentValue.appLanguage;
  console.log('localesss', locale);

  const {translations} = useContext(LocalizationContext);
  const [sessions, setSessions] = useState(null);
  const isFocused = useIsFocused();
  const {userId, goToWorkout} = route?.params;
  const [getUserData, {data, error, loading}] = useLazyQuery(GET_USER, {
    variables: {id: userId, locale: locale},
    fetchPolicy: 'network-only',
  });
  const currentProgram = data?.user?.currentProgram;
  const goToSummary = useCallback(
    session_execution_id => {
      navigation.push('Summary', {
        data: {
          difficultyFeedback: 1,
          sessionExecutionId: session_execution_id,
        },
      });
    },
    [navigation],
  );
  console.log(error);

  useFocusEffect(useCallback(() => getUserData(), [getUserData]));
  useFocusEffect(
    useCallback(() => {
      isFocused && trackEvents({eventName: 'Load Your Program'});
    }, [isFocused]),
  );

  useEffect(() => {
    const sessions = currentProgram?.sessions;
    const executedSessions = data?.user?.userProgram?.executedSessions;
    const sessionExecutions = data?.user?.userProgram?.sessionExecutions;
    console.log('ses', sessionExecutions);

    if (sessions && sessionExecutions) {
      const res = sessions.map(item => {
        const sessionExecutionIndex = sessionExecutions.findIndex(
          el => el.sessionId === item.id,
        );
        return {
          ...item,
          isDone: sessionExecutionIndex >= 0 ? true : false,
          discarded:
            sessionExecutionIndex >= 0 &&
            sessionExecutions[sessionExecutionIndex]?.discarded,
          sessionExecutionId: sessionExecutions[sessionExecutionIndex]?.id, // [executedSessions.findIndex(el => el.id === item.id,)],
        };
      });
      res.sort((a, b) => {
        return a.order - b.order;
      });
      setSessions(res);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  return (
    <SafeAreaView style={styles.safeArea}>
      {!loading ? (
        <>
          <Header
            title={translations.listSessionsProgramScreen.headerText}
            onPressBackButton={() => {
              navigation.goBack();
            }}
          />
          <View style={styles.line} />
          <FlatList
            data={sessions}
            renderItem={({item}) => (
              <GenericSessionCard
                title={item.name}
                isTouch={!item.isDone}
                onPress={
                  item.isDone
                    ? () => goToSummary(item.sessionExecutionId)
                    : () => goToWorkout(currentProgram, item, 'Your Program')
                }
                calories={item.calories}
                timeDuration={item.timeDuration}
                reps={item.reps}
                bodyPartsFocused={item.bodyPartsFocused}
                discarded={item.discarded}
                footerShow={true}
              />
            )}
            keyExtractor={item => item.id}
          />
        </>
      ) : (
        <Loading />
      )}
    </SafeAreaView>
  );
};

export default ListSessionsProgram;

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: WHITE,
  },
  container: {
    paddingBottom: calcWidth(20),
  },
  line: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginBottom: moderateScale(15),
    marginTop: moderateScale(-15),
  },
});
