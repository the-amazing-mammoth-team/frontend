import React, {
  useCallback,
  useContext,
  useMemo,
  useState,
  useLayoutEffect,
} from 'react';
import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import {Icon, Input, Button} from 'react-native-elements';
import gql from 'graphql-tag';
import {useMutation} from '@apollo/react-hooks';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {AccessToken, LoginManager} from 'react-native-fbsdk-next';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';
import {appleAuth} from '@invertase/react-native-apple-authentication';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_FONT,
  SUB_TITLE_BOLD_FONT,
} from '../../styles/fonts';
import {NextButton} from '../../components/buttons';
import {BLACK, GRAY, WHITE, ORANGE, LIGHT_ORANGE} from '../../styles/colors';
import {AuthContext} from '../../../App';
import {LocalizationContext} from '../../localization/translations';
import NoFacebookAccountModal from '../SignupServices/NoFacebookAccountModal';
import ErrorModal from '../../components/Modal/ErrorModal';
import {calcFontSize, moderateScale} from '../../utils/dimensions';
import EyeHide from '../../assets/icons/eyeHIde.svg';
import EyeShow from '../../assets/icons/eyeShow.svg';
import deviceStorage from '../../services/deviceStorage';
import {WEB_CLIENT_ID} from '../../private/credentials';
import Encryption from '../../services/encryptions';
import {Platforms, trackEvents, loginAnalytics} from '../../services/analytics';
import * as Sentry from '@sentry/react-native';
import jwt_decode from 'jwt-decode';
import {validateEmail} from '../../utils/regexes';

const LOGIN = gql`
  mutation login($input: LoginInput!) {
    login(input: $input) {
      email
      id
      jwt
      gender
    }
  }
`;

const Login = ({navigation, route}) => {
  // const {onboardingData} = route.params;
  const handleGoBack = () => {
    navigation.goBack();
  };
  const [formData, setFormData] = useState({
    password: '',
    email: '',
    errors: {},
  });

  const auth = React.useContext(AuthContext);
  const {translations} = useContext(LocalizationContext);

  const [isSecureTextEntry, setIsSecureTextEntry] = useState(true);
  const [
    isNoEmailAccountModalVisible,
    setIsNoEmailAccountModalVisible,
  ] = useState(false);
  const [
    isNoFacebookAccountModalVisible,
    setIsNoFacebookAccountModalVisible,
  ] = useState(false);

  const [googleErrorState, setGoogleErrorState] = useState({
    isModalVisible: false,
    errorText: '',
  });

  const handleGoogleErrors = useCallback(error => {
    setGoogleErrorState({isModalVisible: true, errorText: error.message});
  }, []);

  const onChangeText = ({key, value}) => {
    const newFormData = {...formData, [key]: value};
    setFormData(newFormData);
  };
  const [facebookTextErrors, setFacebookTextErrors] = useState();

  const [login] = useMutation(LOGIN);
  const [loginFacebook] = useMutation(LOGIN);
  const [loginGoogle] = useMutation(LOGIN);
  const [loginApple] = useMutation(LOGIN);

  useLayoutEffect(() => {
    trackEvents({eventName: 'Load Login', platforms: {1: Platforms.mixPanel}});
  }, []);

  const handleError = error => {
    switch (true) {
      case error.message.includes('Password is too short'):
        setFormData({
          ...formData,
          errors: {
            password: {
              message: translations.loginScreen.tooShort,
            },
          },
        });
        break;
      case error.message.includes('Email is invalid'):
        setFormData({
          ...formData,
          errors: {
            email: {
              message: translations.loginScreen.invalidEmail,
            },
          },
        });
        break;
      case error.message.includes('GraphQL error: User wrong password'):
        setFormData({
          ...formData,
          errors: {
            password: {
              message: translations.recoverPasswordScreen.userWrongPassword,
            },
          },
        });
        break;
      default:
        setIsNoEmailAccountModalVisible(true);
    }
  };

  const handleFacebookError = useCallback(
    error => {
      if (error.toString().includes('GraphQL error: user not in the db')) {
        setFacebookTextErrors(translations.loginScreen.noEmailAccountError);
      }
      setIsNoFacebookAccountModalVisible(oldData => !oldData);
    },
    [translations.loginScreen.noEmailAccountError],
  );

  const {email, password} = formData;
  const submit = async () => {
    const input = {
      email: email.trim().toLowerCase(),
      password: password.trim(),
    };
    console.log(999, input);
    try {
      if (!validateEmail.test(String(input.email).toLowerCase())) {
        throw new Error('Email is invalid');
      } else if (input.password?.length < 6) {
        throw new Error('Password is too short');
      }
      const res = await login({
        variables: {
          input: {...input},
        },
      });
      if (res.data?.login?.jwt) {
        loginAnalytics(email);
        await deviceStorage.saveItem(
          '@user/credentials',
          Encryption.encrypt(input),
        );
        await auth.signIn({
          token: res.data.login.jwt,
          id: res.data.login.id,
          gender: res.data.login.gender,
          email: res.data.login.email,
        });
      }
    } catch (err) {
      console.log(err?.message);
      handleError(err);
    }
  };

  const {errors} = formData;

  const handleForgotPassword = useCallback(() => {
    navigation.navigate('RecoverPassword');
  }, [navigation]);

  async function handleAppleSignIn() {
    try {
      // performs login request
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });
      // get current authentication state for user
      // /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
      const credentialState = await appleAuth.getCredentialStateForUser(
        appleAuthRequestResponse.user,
      );

      // use credentialState response to ensure the user is authenticated
      if (credentialState === appleAuth.State.AUTHORIZED) {
        // user is authenticated
        let userEmail = '';
        let {identityToken, fullName, email} = appleAuthRequestResponse;
        if (fullName?.givenName) {
          await deviceStorage.saveItem('@user/name', JSON.stringify(fullName));
        }
        if (email) {
          userEmail = email;
        } else {
          const {email} = jwt_decode(identityToken);
          userEmail = email;
        }

        const input = {
          appleIdToken: userEmail,
        };
        console.log('INPUT:', input);
        const res = await loginApple({
          variables: {
            input: {...input},
          },
        });
        console.log('response', res);
        if (res.data?.login?.jwt) {
          loginAnalytics(res.data.login.email);
          await auth.signIn({
            token: res.data.login.jwt,
            id: res.data.login.id,
            gender: res.data.login.gender,
            email: res.data.login.email,
          });
          await deviceStorage.saveItem(
            '@user/credentials',
            Encryption.encrypt(input),
          );
        }
      }
    } catch (err) {
      if (err.code === appleAuth.Error.CANCELED) {
        console.log('canceled');
      } else {
        Sentry.captureMessage(err);
        console.log(err);
      }
    }
  }

  const handleGoogleLogin = useCallback(async () => {
    try {
      GoogleSignin.configure({
        webClientId: WEB_CLIENT_ID,
      });
      console.log('webclientid', WEB_CLIENT_ID);
      const hasplay = await GoogleSignin.hasPlayServices();
      console.log('has play login', hasplay);
      //can remove next 4 lines if it necessary (user will be signIn in the app via google account that the user has already logged into)
      const isSignedIn = await GoogleSignin.isSignedIn();
      if (isSignedIn) {
        await GoogleSignin.signOut();
      }
      const userInfoGoogle = await GoogleSignin.signIn();
      console.log('userinfogooglesingin', userInfoGoogle);

      const {id, email: googleUserEmail, name, photo} = userInfoGoogle.user;
      console.log(333, id, googleUserEmail, name, photo);
      const input = {
        googleUid: id,
      };
      console.log('INPUT:', input);
      const res = await loginGoogle({
        variables: {
          input: {...input},
        },
      });
      console.log('response', res);
      if (res.data?.login?.jwt) {
        loginAnalytics(googleUserEmail);
        await auth.signIn({
          token: res.data.login.jwt,
          id: res.data.login.id,
          gender: res.data.login.gender,
          email: res.data.login.email,
        });
        await deviceStorage.saveItem(
          '@user/credentials',
          Encryption.encrypt(input),
        );
        await deviceStorage.saveItem('@user/photo', photo);
      }
    } catch (error) {
      console.log('eerror login', error.code);
      console.log('eerror login', error.message);

      if (statusCodes.SIGN_IN_CANCELLED !== error.code) {
        handleGoogleErrors(error);
      }
    }
  }, [auth, handleGoogleErrors, loginGoogle]);

  const initUser = useCallback(
    token => {
      fetch(
        'https://graph.facebook.com/v8.0/me?fields=email,name,picture.type(large)&access_token=' +
          token,
      )
        .then(response => response.json())
        .then(async json => {
          const input = {
            facebookUid: json.id,
          };
          try {
            console.log('INPUT:', input);
            const res = await loginFacebook({
              variables: {
                input: {...input},
              },
            });
            console.log('response', res);
            if (res.data?.login?.jwt) {
              loginAnalytics(res.data?.login?.email);
              await auth.signIn({
                token: res.data.login.jwt,
                id: res.data.login.id,
                gender: res.data.login.gender,
                email: res.data.login.email,
              });
              await deviceStorage.saveItem(
                '@user/credentials',
                Encryption.encrypt(input),
              );
              await deviceStorage.saveItem(
                '@user/photo',
                json.picture.data.url,
              );
            }
          } catch (err) {
            console.log(err);
            handleFacebookError(err[0]);
          }
        })
        .catch(error => {
          console.log(error);
          handleFacebookError(error);
        });
    },
    [auth, handleFacebookError, loginFacebook],
  );

  const handleEyeIcon = useCallback(() => {
    return (
      <TouchableOpacity
        onPress={() => setIsSecureTextEntry(prevState => !prevState)}
        hitSlop={styles.hitSlopTen}>
        {isSecureTextEntry ? <EyeHide /> : <EyeShow />}
      </TouchableOpacity>
    );
  }, [isSecureTextEntry]);

  const closeNoEmailAccountModal = useCallback(() => {
    setIsNoEmailAccountModalVisible(oldData => !oldData);
  }, []);

  const closeNoFacebookAccountModal = useCallback(() => {
    setIsNoFacebookAccountModalVisible(oldData => !oldData);
  }, []);

  const handleFacebookLogin = useCallback(() => {
    AccessToken.getCurrentAccessToken().then(data => {
      if (data != null) {
        console.log('logOut WORK');
        LoginManager.logOut();
      }
    });
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      function(result) {
        if (result.isCancelled) {
          console.log('Login was cancelled');
        } else {
          console.log(
            'Login was successful with permissions: ' +
              result.grantedPermissions.toString(),
          );
          AccessToken.getCurrentAccessToken().then(data => {
            const {accessToken} = data;
            console.log(222, data);
            initUser(accessToken);
          });
        }
      },
      error => {
        console.log('Login failed with error: ' + error);
        setFacebookTextErrors(error.toString());
        setIsNoFacebookAccountModalVisible(oldData => !oldData);
      },
    );
  }, [initUser]);

  const renderNoEmailAccountModal = useMemo(() => {
    return (
      <ErrorModal
        isModalVisible={isNoEmailAccountModalVisible}
        onDismiss={closeNoEmailAccountModal}
        title={'Error'}
        description={translations.loginScreen.noEmailAccountError}
      />
    );
  }, [
    closeNoEmailAccountModal,
    isNoEmailAccountModalVisible,
    translations.loginScreen.noEmailAccountError,
  ]);

  const closeGoogleErrorModal = useCallback(() => {
    setGoogleErrorState({...googleErrorState, isModalVisible: false});
  }, [googleErrorState]);

  const renderGoogleErrorModal = useMemo(() => {
    return (
      <ErrorModal
        isModalVisible={googleErrorState.isModalVisible}
        onDismiss={closeGoogleErrorModal}
        title={'Error'}
        description={googleErrorState.errorText}
        textButton={'Create an account'}
        onButtonPress={() => navigation.navigate('Onboarding')}
      />
    );
  }, [
    closeGoogleErrorModal,
    googleErrorState.errorText,
    googleErrorState.isModalVisible,
    navigation,
  ]);

  const renderNoFacebookAccountModal = useMemo(() => {
    return (
      <NoFacebookAccountModal
        isVisible={isNoFacebookAccountModalVisible}
        closeModal={closeNoFacebookAccountModal}
        title={
          !facebookTextErrors
            ? translations.loginScreen.noFacebookAccountError
            : facebookTextErrors
        }
        buttonText={
          !facebookTextErrors
            ? translations.loginScreen.goToOnBoardingScreen
            : translations.loginScreen.gotIt
        }
        navigation={navigation}
        temp={facebookTextErrors}
      />
    );
  }, [
    closeNoFacebookAccountModal,
    facebookTextErrors,
    isNoFacebookAccountModalVisible,
    navigation,
    translations.loginScreen.goToOnBoardingScreen,
    translations.loginScreen.gotIt,
    translations.loginScreen.noFacebookAccountError,
  ]);

  const disableButtonNext = email && password ? false : true;

  return (
    <SafeAreaView style={styles.safeView}>
      <View style={styles.container}>
        {renderNoEmailAccountModal}
        {renderNoFacebookAccountModal}
        {renderGoogleErrorModal}
        <View style={styles.header}>
          <View style={styles.arrowBack}>
            <Button
              onPress={handleGoBack}
              buttonStyle={styles.goBackButton}
              icon={<Icon name="arrow-back" />}
            />
          </View>
          <View>
            <Text style={styles.headerText}>
              {translations.loginScreen.tabHeader}
            </Text>
          </View>
        </View>
        <View style={styles.allInputs}>
          <View style={styles.inputs}>
            <Input
              errorStyle={styles.errors}
              errorMessage={errors.email?.message}
              onChangeText={text =>
                onChangeText({key: 'email', value: text.toLowerCase()})
              }
              value={formData.email}
              label={translations.loginScreen.emailField}
              labelStyle={styles.inputLabelStyle}
              inputContainerStyle={styles.inputLabelContainerstyle}
              keyboardType={'email-address'}
              autoCapitalize={'none'}
            />
          </View>
          <View style={styles.inputs}>
            <Input
              errorStyle={styles.errors}
              errorMessage={errors.password?.message}
              onChangeText={text =>
                onChangeText({key: 'password', value: text})
              }
              value={formData.password}
              secureTextEntry={isSecureTextEntry}
              label={translations.signUpWithEmailScreen.passwordField}
              inputContainerStyle={styles.inputLabelContainerstyle}
              labelStyle={styles.inputLabelStyle}
              rightIcon={handleEyeIcon}
              autoCapitalize={'none'}
            />
          </View>
        </View>
        <TouchableOpacity onPress={handleForgotPassword}>
          <Text style={styles.orangeText}>
            {translations.loginScreen.forgotPassword}
          </Text>
        </TouchableOpacity>
        <View style={styles.loginButtons}>
          {/* <Button
            onPress={handleGoogleLogin}
            title={translations.loginScreen.accessGoogleButton}
            titleStyle={styles.googleButtonText}
            buttonStyle={[styles.button, styles.google]}
            accessibilityLabel="Learn more about this purple button"
          /> */}
          <Button
            onPress={handleFacebookLogin}
            title={translations.loginScreen.accessFacebookButton}
            titleStyle={styles.buttonText}
            buttonStyle={[styles.button, styles.facebook]}
            accessibilityLabel="Learn more about this purple button"
          />
          {appleAuth.isSupported && (
            <Button
              onPress={handleAppleSignIn}
              title={translations.signUpServicesScreen.accessAppleButton}
              titleStyle={styles.buttonText}
              buttonStyle={[styles.button, styles.apple]}
              accessibilityLabel="Learn more about this purple button"
            />
          )}
        </View>
      </View>
      <NextButton onPress={submit} disabled={disableButtonNext} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeView: {
    flex: 1,
  },
  container: {
    flex: 1,
    paddingTop: 15,
    paddingLeft: 25,
    paddingRight: 25,
    paddingBottom: 25,
    backgroundColor: WHITE,
    justifyContent: 'flex-start',
  },
  inputLabelStyle: {
    color: GRAY,
    fontWeight: 'normal',
    fontFamily: SUB_TITLE_FONT,
  },
  inputLabelContainerstyle: {
    borderColor: '#BFBFBF',
  },
  headerText: {
    fontSize: 18,
    fontFamily: MAIN_TITLE_FONT,
    color: BLACK,
  },
  arrowBack: {
    alignSelf: 'flex-start',
    width: 50,
    height: 50,
    position: 'absolute',
    left: 0,
    top: 5,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },
  inputs: {
    height: 20,
    marginBottom: 70,
  },
  allInputs: {
    marginTop: 15,
  },
  orangeText: {
    color: LIGHT_ORANGE,
    fontSize: calcFontSize(14),
    marginLeft: 10,
    marginTop: 10,
    fontFamily: SUB_TITLE_BOLD_FONT,
    textDecorationLine: 'underline',
  },
  buttonStyle: {
    borderRadius: 50,
    height: 50,
    width: 50,
    backgroundColor: '#BFBFBF',
  },
  goBackButton: {
    backgroundColor: WHITE,
  },
  errors: {
    color: ORANGE,
  },
  serverError: {
    marginBottom: 10,
    marginLeft: 10,
  },
  button: {
    borderRadius: 25,
    height: 50,
    backgroundColor: ORANGE,
    marginTop: 20,
  },
  facebook: {
    backgroundColor: '#3B5999',
  },
  apple: {
    backgroundColor: 'black',
  },
  google: {
    borderColor: '#E5E5E5',
    borderWidth: 1,
    borderStyle: 'solid',
    backgroundColor: '#FFFFFF',
  },
  googleButtonText: {
    fontSize: 18,
    color: BLACK,
    fontFamily: MAIN_TITLE_FONT,
  },
  buttonText: {
    fontSize: 18,
    fontFamily: MAIN_TITLE_FONT,
  },
  loginButtons: {
    marginTop: 30,
  },
  hitSlopTen: {
    top: moderateScale(10),
    bottom: moderateScale(10),
    right: moderateScale(10),
    left: moderateScale(10),
  },
});

export default Login;
