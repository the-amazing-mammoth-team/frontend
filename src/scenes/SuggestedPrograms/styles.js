import {StyleSheet} from 'react-native';
import {
  MAIN_TITLE_FONT,
  SUB_TITLE_FONT,
  SUB_TITLE_BOLD_FONT,
} from '../../styles/fonts';
import {
  WHITE,
  BLACK,
  GRAY,
  GRAY_BORDER,
  BUTTON_TITLE,
  ORANGE,
  LIGHT_ORANGE,
  LIGHT_GRAY,
} from '../../styles/colors';
import {
  calcFontSize,
  deviceHeight,
  moderateScale,
  deviceWidth,
  calcHeight,
  calcWidth,
} from '../../utils/dimensions';

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: WHITE,
  },
  scrollView: {
    backgroundColor: WHITE,
  },
  container: {
    color: BLACK,
    fontSize: calcFontSize(14),
    backgroundColor: WHITE,
    //paddingHorizontal: moderateScale(20),
    marginTop: moderateScale(20),
  },
  innerContent: {},
  text: {
    color: GRAY,
    fontSize: calcFontSize(14),
    marginTop: moderateScale(5),
    marginBottom: moderateScale(30),
    fontFamily: SUB_TITLE_FONT,
  },
  mainTitle: {
    fontSize: calcFontSize(25),
    fontFamily: MAIN_TITLE_FONT,
    color: BLACK,
  },
  bottomButton: {
    alignSelf: 'flex-end',
    alignItems: 'flex-end',
    marginTop: 15,
    marginBottom: 35,
    marginRight: 20,
    backgroundColor: WHITE,
  },
  nextButtonStyle: {
    borderRadius: 50,
    height: 50,
    width: 50,
    backgroundColor: GRAY,
  },
  nextButtonStyleActive: {
    borderRadius: 50,
    height: 50,
    width: 50,
    backgroundColor: ORANGE,
    borderColor: WHITE,
  },
  selectedButtonColor: {
    backgroundColor: ORANGE,
    borderColor: WHITE,
  },
  program: {
    flex: 1,
    backgroundColor: BLACK,
    height: 350,
    color: WHITE,
    padding: 10,
    borderRadius: 10,
    marginRight: moderateScale(12),
    marginBottom: moderateScale(25),
    width: 280,
    resizeMode: 'cover',
    justifyContent: 'space-between',
  },
  programImage: {
    borderRadius: 10,
    maxWidth: 280,
  },
  programContainer: {
    paddingLeft: 0,
    marginLeft: 0,
    backgroundColor: BLACK,
  },
  programName: {
    color: WHITE,
    fontSize: calcFontSize(30),
    fontFamily: MAIN_TITLE_FONT,
  },
  programDescription: {
    fontSize: calcFontSize(16),
    color: WHITE,
    fontFamily: SUB_TITLE_FONT,
  },
  bigText: {
    fontSize: 22,
    lineHeight: 32,
    marginVertical: moderateScale(30),
    fontFamily: MAIN_TITLE_FONT,
  },
  imagePlaceholder: {
    height: 200,
    backgroundColor: '#C4C4C4',
  },
  recommendedProgram: {
    flex: 1,
    position: 'absolute',
    overflow: 'visible',
    zIndex: 9999,
    backgroundColor: ORANGE,
    color: WHITE,
    bottom: moderateScale(-15),
    right: moderateScale(30),
    paddingHorizontal: moderateScale(20),
    paddingVertical: moderateScale(5),
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_BOLD_FONT,
  },
  programDetailInfo: {
    paddingLeft: 15,
    paddingRight: 15,
    fontSize: 16,
  },
  programLength: {
    fontSize: 18,
    fontFamily: SUB_TITLE_BOLD_FONT,
  },
  detailProgramName: {
    fontSize: calcFontSize(30),
    fontFamily: MAIN_TITLE_FONT,
  },
  programCreator: {
    fontSize: 18,
    color: '#9F9F9F',
    fontFamily: SUB_TITLE_BOLD_FONT,
  },
  programDetailSubtitle: {
    fontSize: 20,
    marginTop: 25,
    fontFamily: SUB_TITLE_BOLD_FONT,
  },
  circleCheck: {
    borderRadius: 50,
    width: 20,
    height: 20,
    backgroundColor: ORANGE,
    marginRight: 15,
  },
  checkContainer: {
    marginTop: moderateScale(20),
    flexDirection: 'row',
    paddingRight: moderateScale(30),
  },
  checkText: {
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_FONT,
  },
  button: {
    borderRadius: moderateScale(25),
    height: 50,
    backgroundColor: ORANGE,
    width: 350,
    marginTop: 40,
    marginBottom: 20,
  },
  separator: {
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
    borderTopWidth: 0,
    marginTop: moderateScale(10),
    marginBottom: moderateScale(20),
  },
  label: {
    backgroundColor: LIGHT_ORANGE,
    color: '#ffffff',
    paddingVertical: moderateScale(5),
    paddingHorizontal: moderateScale(15),
    fontFamily: SUB_TITLE_BOLD_FONT,
  },
  labelWrapper: {
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 5,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    borderBottomColor: '#E5E5E5',
    borderBottomWidth: 1,
    paddingBottom: 20,
    marginBottom: 20,
  },
  goBackButton: {
    backgroundColor: WHITE,
  },
  headerText: {
    fontSize: 18,
    alignSelf: 'center',
    fontFamily: MAIN_TITLE_FONT,
  },
  programBuyContainer: {
    paddingTop: 15,
    paddingBottom: 25,
    backgroundColor: WHITE,
  },
  buyProgram: {
    borderRadius: 25,
    height: 50,
    borderColor: ORANGE,
    borderWidth: 1,
    width: 350,
    marginTop: 40,
    marginBottom: 20,
    backgroundColor: WHITE,
  },
  buyProgramText: {
    color: ORANGE,
    fontSize: 20,
  },
  price: {
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 25,
  },
  priceSubtitle: {
    color: '#9F9F9F',
    fontSize: calcFontSize(14),
    fontFamily: MAIN_TITLE_FONT,
    marginTop: moderateScale(8),
  },
  buttonContainer: {
    alignItems: 'center',
  },
  buttonTitleStyles: {
    fontSize: 20,
    fontFamily: MAIN_TITLE_FONT,
  },
  mainScrollView: {
    flex: 1,
    backgroundColor: WHITE,
  },
  arrowBack: {
    //marginLeft: moderateScale(20)
  },
  line: {
    height: moderateScale(1),
    width: '100%',
    backgroundColor: GRAY_BORDER,
    marginBottom: 20,
  },
  extraMarginLine: {
    marginTop: moderateScale(15),
  },
  generalPadding: {
    paddingHorizontal: moderateScale(20),
  },
  mockCircle: {
    width: moderateScale(106),
    height: moderateScale(106),
    borderRadius: moderateScale(106) / 2,
    backgroundColor: BLACK,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  personalizedProgramTitleView: {
    marginTop: moderateScale(16),
    marginBottom: moderateScale(8),
  },
  personalizedProgramTitle: {
    color: BLACK,
    fontFamily: MAIN_TITLE_FONT,
    fontSize: calcFontSize(16),
    textAlign: 'center',
  },
  personalizedProgramDescriptionView: {
    marginBottom: moderateScale(16),
    width: '90%',
  },
  personalizedProgramDescriptionText: {
    color: BUTTON_TITLE,
    fontFamily: SUB_TITLE_FONT,
    fontSize: calcFontSize(14),
    textAlign: 'center',
  },
  containerCopy: {
    flex: 1,
  },
  child: {
    width: deviceWidth,
    justifyContent: 'center',
    alignItems: 'center',
  },
  paginationDot: {
    width: moderateScale(10),
    height: moderateScale(10),
    borderRadius: moderateScale(10) / 2,
  },
  paginationStyle: {
    bottom: moderateScale(-20),
  },
  personalExperienceSectionView: {
    marginTop: moderateScale(30),
  },
  frequentlySectionView: {
    marginBottom: moderateScale(30),
    marginTop: moderateScale(40),
  },
  buttonBuy: {
    width: '100%',
    alignSelf: 'center',
  },
  headerView: {
    marginVertical: -moderateScale(15),
  },
  imageCircle: {
    width: moderateScale(106),
    height: moderateScale(106),
    borderRadius: moderateScale(106) / 2,
  },
  genericPadding: {
    paddingHorizontal: moderateScale(20),
  },
  negativeMargin: {
    marginBottom: -moderateScale(10),
  },
  root: {
    flex: 1,
    backgroundColor: WHITE,
  },
  headerLine: {
    height: moderateScale(1),
    backgroundColor: GRAY_BORDER,
    width: '100%',
    marginTop: -20,
    marginBottom: moderateScale(20),
  },
  titleView: {
    paddingHorizontal: calcWidth(20),
    marginBottom: calcHeight(22),
  },
  title: {
    color: BLACK,
    fontSize: calcFontSize(25),
    fontFamily: MAIN_TITLE_FONT,
  },
  description: {
    color: LIGHT_GRAY,
    fontSize: calcFontSize(14),
    fontFamily: SUB_TITLE_FONT,
  },
  container: {
    paddingHorizontal: calcWidth(20),
  },
  sectionTitle: {
    fontSize: calcFontSize(20),
    marginBottom: moderateScale(30),
  },
  commentTextColor: {
    color: BUTTON_TITLE,
  },
  commentAuthorText: {
    color: BLACK,
    fontSize: calcFontSize(16),
    fontFamily: SUB_TITLE_FONT,
  },
  commentRoot: {
    marginBottom: moderateScale(30),
  },
  commentAuthorTextView: {
    marginBottom: moderateScale(10),
  },
  commentTextView: {
    marginBottom: moderateScale(5),
  },
  starDistance: {
    marginRight: calcWidth(5),
  },
  starsRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  personalExperienceSectionView: {
    marginTop: moderateScale(30),
  },
  frequentlySectionView: {
    marginBottom: moderateScale(30),
    marginTop: moderateScale(40),
  },
  programsCarousel: {
    paddingLeft: calcWidth(0),
  },
  blackCard: {
    width: calcWidth(250),
    height: calcHeight(320),
    marginRight: calcWidth(10),
    borderRadius: moderateScale(30) / 2,
    backgroundColor: BLACK,
  },
  chooseAnotherImage: {
    width: calcWidth(250),
    height: calcHeight(320),
    borderRadius: moderateScale(30) / 2,
  },
  chooseAnotherProgramView: {
    position: 'absolute',
    top: moderateScale(34),
    left: moderateScale(10),
  },
  cardTitle: {
    color: WHITE,
    fontSize: calcFontSize(30),
    fontFamily: MAIN_TITLE_FONT,
  },
  proIcon: {
    position: 'absolute',
    top: moderateScale(5),
    right: moderateScale(7),
    width: moderateScale(30),
    height: moderateScale(30),
    zIndex: 999,
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: moderateScale(8),
  },
  characteristicsName: {
    color: WHITE,
    fontFamily: SUB_TITLE_BOLD_FONT,
    fontSize: calcFontSize(14),
    marginRight: calcWidth(12),
  },
});

export default styles;
