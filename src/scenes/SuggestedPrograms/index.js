import React, {useContext, useCallback} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
} from 'react-native';
import {useQuery} from '@apollo/react-hooks';
import gql from 'graphql-tag';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {useFocusEffect, useNavigationState} from '@react-navigation/native';
import styles from './styles';
import {MEDIUM_GRAY, LIGHT_ORANGE} from '../../styles/colors';
import {moderateScale} from '../../utils/dimensions';
import {trackEvents} from '../../services/analytics';
import {LocalizationContext} from '../../localization/translations';
import {getPreviousRouteFromState} from '../../services/navigation';
import Star from '../../assets/icons/starRate.svg';
import InventoryBackground from '../../assets/images/inventoryBackground.png';
import {AuthContext} from '../../../App';
import Header from '../../components/header';
import ProIcon from '../../assets/icons/pro.svg';
import {Loading} from '../../components/Loading';
import {levelCircles} from '../../components/ProgramsScreens/CurrentProgramCard';

const SUGGESTED_PROGRAMS = gql`
  query suggestedPrograms($userId: ID!, $locale: String!) {
    suggestedPrograms(userId: $userId, locale: $locale) {
      id
      description
      name
      codeName
      pro
      updatedAt
      imageUrl
      image2Url
      maxAttribute {
        key
        value
      }
      maxAttribute2 {
        key
        value
      }
      strength
      endurance
      flexibility
      intensity
      technique
      user {
        id
        names
      }
      products {
        id
        storeReference
      }
      sessions {
        id
        name
        description
      }
      programCharacteristics {
        id
        value
        objective
      }
    }
  }
`;

export const handleStars = count => {
  return (
    <>
      <View style={styles.starDistance}>
        <Star
          width={moderateScale(18)}
          height={moderateScale(18)}
          fill={count > 0 ? LIGHT_ORANGE : MEDIUM_GRAY}
        />
      </View>
      <View style={styles.starDistance}>
        <Star
          width={moderateScale(18)}
          height={moderateScale(18)}
          fill={count > 1 ? LIGHT_ORANGE : MEDIUM_GRAY}
        />
      </View>
      <View style={styles.starDistance}>
        <Star
          width={moderateScale(18)}
          height={moderateScale(18)}
          fill={count > 2 ? LIGHT_ORANGE : MEDIUM_GRAY}
        />
      </View>
      <View style={styles.starDistance}>
        <Star
          width={moderateScale(18)}
          height={moderateScale(18)}
          fill={count > 3 ? LIGHT_ORANGE : MEDIUM_GRAY}
        />
      </View>
      <View style={styles.starDistance}>
        <Star
          width={moderateScale(18)}
          height={moderateScale(18)}
          fill={count > 4 ? LIGHT_ORANGE : MEDIUM_GRAY}
        />
      </View>
    </>
  );
};

const getPrevScreen = navState => {
  const res = getPreviousRouteFromState(navState).name;
  console.log(res);
  let prevScreen = '';
  if (res === 'FinishProgram') {
    prevScreen = 'Finish Program';
  } else {
    prevScreen = 'Onboarding';
  }
};

const SuggestedPrograms = ({navigation}) => {
  const {userData: userInfo} = React.useContext(AuthContext);
  const {data, error, loading} = useQuery(SUGGESTED_PROGRAMS, {
    variables: {
      userId: userInfo.id,
      locale: LocalizationContext._currentValue.appLanguage,
    },
  });
  const {translations} = useContext(LocalizationContext);
  const mockComments = [
    {
      id: '25gfaf',
      author: 'Miquel Malet',
      text: translations.suggestedProgramsScreen.miquelMalet,
      starsAmount: 5,
    },
    {
      id: 'gesgsw243',
      author: 'Lola Ibanez',
      text: translations.suggestedProgramsScreen.lolaIbanez,
      starsAmount: 5,
    },
    {
      id: 'esgst3',
      author: 'Hele Biker',
      text: translations.suggestedProgramsScreen.heleBiker,
      starsAmount: 5,
    },
  ];

  const suggested_program = data?.suggestedPrograms[0];
  var sPrograms = [];
  if (suggested_program != null) {
    sPrograms = [data?.suggestedPrograms[0]];
  }

  const navState = useNavigationState(state => state);

  useFocusEffect(
    useCallback(() => {
      if (suggested_program) {
        trackEvents({
          eventName: 'Load Onboading Featured Program',
          properties: {
            training_program: suggested_program?.codeName,
          },
        });
      }
    }, [suggested_program]),
  );

  const handleGoBack = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const handleChooseAnotherProgram = useCallback(() => {
    navigation.navigate('ChooseAnotherProgram', {
      suggested_program,
      prevScreen: getPrevScreen(navState),
    });
  }, [navState, navigation, suggested_program]);

  const renderSuggestedPrograms = () => {
    if (error) {
      console.log('error here', error);
    }
    console.log('suggested data down here');
    console.log(data?.suggestedPrograms);
    if (!loading && data?.suggestedPrograms && sPrograms?.length > 0) {
      return sPrograms.map(program => {
        return (
          <TouchableWithoutFeedback
            key={program.id}
            onPress={() =>
              navigation.navigate('ProgramDetail', {
                program,
                prevScreen: getPrevScreen(navState),
              })
            }>
            <ImageBackground
              source={{uri: program.imageUrl}}
              imageStyle={styles.programImage}
              style={styles.program}>
              {Boolean.parse(program?.pro) && (
                <ProIcon
                  width={styles.proIcon.width}
                  height={styles.proIcon.height}
                  style={styles.proIcon}
                />
              )}
              <View>
                <Text style={styles.programDescription}>
                  {program.sessions.length}{' '}
                  {translations.programsScreen.sessions}
                </Text>
                <Text style={styles.programName}>
                  {program.name || 'From Fat to Fit'}
                </Text>
              </View>
              <View>
                <View style={styles.rowView}>
                  <Text style={styles.characteristicsName}>
                    {program.maxAttribute.key}
                  </Text>
                  {levelCircles(program.maxAttribute.value)}
                </View>
                <View style={styles.rowView}>
                  <Text style={styles.characteristicsName}>
                    {program.maxAttribute2.key}
                  </Text>
                  {levelCircles(program.maxAttribute2.value)}
                </View>
              </View>
              <Text style={styles.recommendedProgram}>
                {translations.programDetailScreen.recomendForYou}
              </Text>
            </ImageBackground>
          </TouchableWithoutFeedback>
        );
      });
    }
    return null;
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      {!loading ? (
        <ScrollView style={styles.scrollView}>
          <Header
            title={translations.suggestedProgramsScreen.header}
            onPressBackButton={handleGoBack}
          />
          <View style={styles.headerLine} />
          <View style={styles.container}>
            <View style={styles.innerContent}>
              <Text style={styles.mainTitle}>
                {translations.suggestedProgramsScreen.ourSuggestion}
              </Text>
              <Text style={styles.text}>
                {translations.suggestedProgramsScreen.recommendedProgramsDesc}
              </Text>
            </View>
            {/* <View style={styles.programsCarousel}> */}
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={styles.programsCarousel}>
              {renderSuggestedPrograms()}
              <TouchableOpacity onPress={handleChooseAnotherProgram}>
                <ImageBackground
                  imageStyle={styles.programImage}
                  style={styles.program}
                  source={InventoryBackground}>
                  <View style={styles.chooseAnotherProgramView}>
                    <Text style={styles.cardTitle}>
                      {translations.suggestedProgramsScreen.chooseAnother}
                    </Text>
                  </View>
                </ImageBackground>
              </TouchableOpacity>
            </ScrollView>
            {/* </View> */}
            {/* COMMENTS SECTION BELOW */}
            <View>
              <Text style={styles.bigText}>
                {translations.programsScreen.joinPeople}
              </Text>
              {mockComments.map(comment => (
                <View key={comment.id} style={styles.commentRoot}>
                  <View style={styles.commentAuthorTextView}>
                    <Text style={styles.commentAuthorText}>
                      {comment.author}
                    </Text>
                  </View>
                  <View style={styles.commentTextView}>
                    <Text style={[styles.description, styles.commentTextColor]}>
                      {comment.text}
                    </Text>
                  </View>
                  <View style={styles.starsRow}>
                    {handleStars(comment.starsAmount)}
                  </View>
                </View>
              ))}
            </View>
          </View>
        </ScrollView>
      ) : (
        <Loading />
      )}
    </SafeAreaView>
  );
};

export default SuggestedPrograms;
