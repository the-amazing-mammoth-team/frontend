package com.mhunters.app;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import com.facebook.react.PackageList;
import com.facebook.react.ReactApplication;
import com.moe.pushlibrary.MoEHelper;
import com.moengage.core.MoEngage;
import com.moengage.core.model.AppStatus;
import com.moengage.react.MoEInitializer;
import com.moengage.react.MoEReactPackage;
import com.zmxv.RNSound.RNSoundPackage;
import com.corbt.keepawake.KCKeepAwakePackage;
import com.reactnativecommunity.netinfo.NetInfoPackage;
import com.facebook.react.ReactInstanceManager;
import com.surajit.rnrg.RNRadialGradientPackage;
import com.dooboolab.RNIap.RNIapPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost =
      new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
          return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {
          @SuppressWarnings("UnnecessaryLocalVariable")
          List<ReactPackage> packages = new PackageList(this).getPackages();
          // Packages that cannot be autolinked yet can be added manually here, for example:
          // packages.add(new MyReactNativePackage());
          return packages;
        }

        @Override
        protected String getJSMainModuleName() {
          return "index";
        }
      };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    MoEngage.Builder moEngage =
      new MoEngage.Builder(this, "MEUPGM97VSQAIDOQ6J8O43P5");
    MoEInitializer.INSTANCE.initialize(getApplicationContext(), moEngage);
    detectInstallOrUpdate(this);
        SoLoader.init(this, /* native exopackage */ false);
    initializeFlipper(this, getReactNativeHost().getReactInstanceManager()); // Remove this line if you don't want Flipper enabled
  }

  /**
   * Loads Flipper in React Native templates.
   *
   * @param context
   * @param reactInstanceManager
   */
  private static void initializeFlipper(
      Context context, ReactInstanceManager reactInstanceManager) {
    if (BuildConfig.DEBUG) {
      try {
        /*
         We use reflection here to pick up the class that initializes Flipper,
        since Flipper library is not available in release mode
        */
        Class<?> aClass = Class.forName("com.facebook.flipper.ReactNativeFlipper");
        aClass
          .getMethod("initializeFlipper", Context.class, ReactInstanceManager.class)
          .invoke(null, context, reactInstanceManager);
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        e.printStackTrace();
      }
    }
  }

  private static void detectInstallOrUpdate(Context context) {
    //--- Detection of program status, installed or updated ----------
    try {
      long firstInstallTime = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).firstInstallTime;
      long lastUpdateTime = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).lastUpdateTime;
      if(firstInstallTime == lastUpdateTime){
        // First start after installing the app
        MoEHelper.getInstance(context).setAppStatus(AppStatus.INSTALL);
      }
      else if(firstInstallTime != lastUpdateTime){
        // App was updated since last run
        MoEHelper.getInstance(context).setAppStatus(AppStatus.UPDATE);
      }
    } catch (PackageManager.NameNotFoundException e) {
      e.printStackTrace();
    }
    //-----------------------------------------------------------------
  } 
}
