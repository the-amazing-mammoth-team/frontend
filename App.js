import 'react-native-gesture-handler';
import * as React from 'react';
import * as Sentry from '@sentry/react-native';
import KeyboardManager from 'react-native-keyboard-manager';
import {useCallback, useContext, useEffect, useRef, useState} from 'react';
import {Platform} from 'react-native';
import gql from 'graphql-tag';
import {useMutation} from '@apollo/react-hooks';
import {enableScreens} from 'react-native-screens';
import jwt_decode from 'jwt-decode';
import {LocalizationContext} from './src/localization/translations';
import Navigator from './src/navigation';
import Loader from './src/scenes/SplashMenu/Loader';
import deviceStorage from './src/services/deviceStorage';
import Encryptions from './src/services/encryptions';
import * as RNIap from 'react-native-iap';
import ErrorModal from './src/components/Modal/ErrorModal';
import {falsy} from './src/utils/regexes';
import {initAnalytics, loginAnalytics} from './src/services/analytics';

Boolean.parse = function(val) {
  return !falsy.test(val) && !!val;
};

enableScreens();

Sentry.init({
  dsn:
    'https://18afdf0be4b4483eb2e072de4e2651d9@o335134.ingest.sentry.io/5619853',
  //integrations: [new Integrations.BrowserTracing()],

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  //tracesSampleRate: 1.0,
  beforeSend(event, hint) {
    // Modify the event here
    console.log('error sent: ', event);
    console.log('additional parameters that are not sent: ', hint);
    return event;
  },
});

const LOGIN = gql`
  mutation login($input: LoginInput!) {
    login(input: $input) {
      email
      id
      jwt
      gender
    }
  }
`;

export const AuthContext = React.createContext();

export default function App() {
  const [isAnalyticsInitialized, setIsAnalyticsInitialized] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const {initializeAppLanguage, translations} = useContext(LocalizationContext);
  const [login] = useMutation(LOGIN);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [modalTextError, setModalTextError] = useState({code: '', message: ''});
  const [localStorageInfo, setLocalStorageInfo] = useState(null);

  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userData: action.userData,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userData: action.data,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userData: null,
          };
        case 'UPDATE_USER_DATA':
          return {
            ...prevState,
            userData: {...prevState.userData, ...action.data},
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userData: null,
    },
  );

  const handleDismissModal = useCallback(
    () => setIsModalVisible(oldData => !oldData),
    [],
  );

  useEffect(() => {
    const initializeAnalytics = async () => {
      await initAnalytics();
      setIsAnalyticsInitialized(true);
    };
    initializeAnalytics();
  }, []);

  useEffect(() => {
    const getInfoLS = async () => {
      // const product = await deviceStorage.getItem('@purchase/product');
      // const parsedProduct = JSON.parse(product);
      const isSuccessRequest = await deviceStorage.getItem(
        '@purchase/isrSuccessRequest',
      );
      //console.log(333, parsedProduct);
      setLocalStorageInfo({
        //product: parsedProduct,
        isSuccessRequest: isSuccessRequest,
      });
    };
    getInfoLS();
  }, []);

  React.useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let userToken;
      let userId;
      let gender;
      let email;
      try {
        userToken = await deviceStorage.getItem('jwt');
        userId = await deviceStorage.getItem('userId');
        gender = await deviceStorage.getItem('gender');
        email = await deviceStorage.getItem('email');
        const userData = {id: userId, token: userToken, gender, email};
        if (userToken) {
          const now = Math.round(+new Date() / 1000);
          const decoded = jwt_decode(userToken);
          if (decoded.exp > now) {
            email && loginAnalytics(email);
            dispatch({type: 'RESTORE_TOKEN', userData});
          } else {
            try {
              const credentials = await deviceStorage.getItem(
                '@user/credentials',
              );
              if (credentials) {
                //refresh token
                const input = Encryptions.decrypt(credentials);
                const res = await login({
                  variables: {
                    input: {...input},
                  },
                });
                if (res.data?.login?.jwt) {
                  loginAnalytics(res.data.login.email);
                  await authContext.signIn({
                    token: res.data.login.jwt,
                    id: res.data.login.id,
                    gender: res.data.login.gender,
                  });
                }
              }
            } catch (err) {
              console.log(err);
            }
          }
        }
        console.log({userToken});
      } catch (e) {
        console.log('message usetoken', e);
        // Restoring token failed
      }
    };

    if (isAnalyticsInitialized) {
      bootstrapAsync();
      initializeAppLanguage();
    }

    if (Platform.OS === 'ios') {
      KeyboardManager.setToolbarPreviousNextButtonEnable(
        translations.onBoarding.done,
      );
    }
  }, [
    authContext,
    initializeAppLanguage,
    login,
    translations,
    isAnalyticsInitialized,
  ]);

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsLoading(false);
    }, 1000);
    return () => {
      clearTimeout(timer);
    };
  }, []);

  useEffect(() => {
    let purchaseUpdateSubscription = null;
    let purchaseErrorSubscription = null;

    if (localStorageInfo?.isSuccessRequest) {
      RNIap.initConnection()
        .then(() => {
          // we make sure that "ghost" pending payment are removed
          // (ghost = failed pending payment that are still marked as pending in Google's native Vending module cache)
          RNIap.flushFailedPurchasesCachedAsPendingAndroid()
            .catch(error => {
              Sentry.captureMessage(error);
              // exception can happen here if:
              // - there are pending purchases that are still pending (we can't consume a pending purchase)
              // in any case, you might not want to do anything special with the error
            })
            .then(async () => {
              purchaseUpdateSubscription = RNIap.purchaseUpdatedListener(
                async purchase => {
                  console.log('purchaseUpdatedListener', purchase);
                  const receipt = purchase.transactionReceipt;
                  if (receipt) {
                    await RNIap.finishTransaction(purchase, true);
                    //await deviceStorage.removeItem('@purchase/product');
                    await deviceStorage.removeItem(
                      '@purchase/isrSuccessRequest',
                    );
                  } else {
                    // Retry / conclude the purchase is fraudulent, etc...
                  }
                },
              );
              purchaseErrorSubscription = RNIap.purchaseErrorListener(error => {
                setModalTextError({code: error.code, message: error.message});
                setIsModalVisible(true);
              });
            })
            .catch(error => {
              Sentry.captureMessage(error);
            });
        })
        .catch(error => {
          Sentry.captureMessage(error);
        });
    }
    return () => {
      if (purchaseUpdateSubscription) {
        purchaseUpdateSubscription.remove();
        purchaseUpdateSubscription = null;
      }
      if (purchaseErrorSubscription) {
        purchaseErrorSubscription.remove();
        purchaseErrorSubscription = null;
      }
    };
  });

  console.log(8888, localStorageInfo);

  const authContext = React.useMemo(
    () => ({
      signIn: async data => {
        const {token, id, gender, email} = data;
        await deviceStorage.saveItem('jwt', token);
        await deviceStorage.saveItem('userId', id);
        await deviceStorage.saveItem('gender', gender);
        await deviceStorage.saveItem('email', email);
        dispatch({type: 'SIGN_IN', data});
      },
      signOut: async () => {
        await deviceStorage.clearAppData();
        dispatch({type: 'SIGN_OUT'});
      },
      updateUserData: async data => {
        Object.entries(data)?.forEach(async ([key, value]) => {
          console.log(key, value);
          switch (key) {
            case 'token': {
              await deviceStorage.saveItem('jwt', value);
              dispatch({type: 'UPDATE_USER_DATA', data: {token: value}});
              break;
            }
            case 'id': {
              await deviceStorage.saveItem('userId', value);
              dispatch({type: 'UPDATE_USER_DATA', data: {id: value}});
              break;
            }
            case 'gender': {
              await deviceStorage.saveItem('gender', value);
              dispatch({type: 'UPDATE_USER_DATA', data: {gender: value}});
              break;
            }
            default:
              break;
          }
        });
      },
      userData: state.userData,
    }),
    [state.userData],
  );

  console.log('state', {state});

  return (
    <AuthContext.Provider value={authContext}>
      <ErrorModal
        isModalVisible={isModalVisible}
        onDismiss={handleDismissModal}
        title={'Error'}
        description={modalTextError.message}
      />
      {isLoading && !isAnalyticsInitialized ? <Loader /> : <Navigator />}
    </AuthContext.Provider>
  );
}
