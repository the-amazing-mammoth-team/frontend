import {HttpLink} from 'apollo-link-http';
import {ApolloClient} from 'apollo-client';
import {InMemoryCache} from 'apollo-cache-inmemory';
import deviceStorage from './src/services/deviceStorage';

// export const URI = 'http://52.214.57.117:9876/graphql';
export const URL = 'https://backend.mhunters.com/';
// const URI = 'http://192.168.18.14:3000/graphql';
// export const URL = 'http://192.168.18.14:3000';
export const URI = 'https://backend.mhunters.com/graphql';

const makeApolloClient = token => {
  // create an apollo link instance, a network interface for apollo client
  const link = new HttpLink({
    uri: 'https://backend.mhunters.com/graphql',
    headers: {
      Authorization: `Bearer ${deviceStorage.getItem('jwt')}`,
    },
  });
  // create an inmemory cache instance for caching graphql data
  const cache = new InMemoryCache();
  // instantiate apollo client with apollo link instance and cache instance
  const client = new ApolloClient({
    link,
    cache,
  });
  return client;
};

export default makeApolloClient;
